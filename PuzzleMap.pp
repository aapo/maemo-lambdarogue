{
     Copyright (C) 2006-2008 by Mario Donick    
     mario.donick@gmail.com
                                                                            
     This program is free software; you can redistribute it and/or modify   
     it under the terms of the GNU General Public License as published by   
     the Free Software Foundation; either version 2 of the License, or      
     (at your option) any later version.                                    
                                                                            
     This program is distributed in the hope that it will be useful,        
     but WITHOUT ANY WARRANTY; without even the implied warranty of         
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
     GNU General Public License for more details.                           
                                                                            
     You should have received a copy of the GNU General Public License      
     along with this program; if not, write to the                          
     Free Software Foundation, Inc.,                                        
     59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              
}

unit PuzzleMap;


interface

uses
    Crt, SysUtils, StrUtils, Quests, RandomArea;

var
    Landstring: array[1..150] of string;
    Part: array[1..90,1..11] of string;
    TotalParts: integer;

procedure CreatePuzzleLandscape (lvl: integer);


implementation

procedure CreatePuzzleMisc(lvl: integer);
var
    i, x, y, r, n: integer;
    LeftFromNPC, RightFromNPC, zeile: string;
begin
    // NPCs
//    GotoXY (2, 5);
//    write ('placing NPCs      ');
    for i:=1 to 9 do
        if Quest[lvl,i].strNPCname<>'-' then
        begin
//             writeln('Create NPC '+IntToStr(lvl)+'/'+IntToStr(i));
            n:=0;
            repeat
                inc(n);
                x:=2+trunc(random(40));  // 74
                y:=2+trunc(random(40));
                zeile:=Landstring[y];
            until (n=5000) or (zeile[x]='-') or (zeile[x]='S') or (zeile[x]='.');
            LeftFromNPC:=LeftStr(Landstring[y],x-1);
            r:=abs(length(Landstring[y])-length(LeftFromNPC))-1;
            RightFromNPC:=RightStr(Landstring[y],r);
            Landstring[y]:=LeftFromNPC+IntToStr(i)+RightFromNPC;
        end;

    // Unique Monster
//     if (lvl=1) or (lvl=3) or (lvl=5) or (lvl=9) or (lvl=15) or (lvl=19) or (lvl=20) then
//     begin
//         GotoXY (2, 5);
//         write ('unique monster    ');
    repeat
        x:=2+trunc(random(40)); // 74
        y:=2+trunc(random(40));
        zeile:=Landstring[y];
//             writeln(zeile[x]);
    until (zeile[x]='.') or (zeile[x]=',') or (zeile[x]='g') or (zeile[x]='W') or (zeile[x]='z') or (zeile[x]='I');
    LeftFromNPC:=LeftStr(Landstring[y],x-1);
    r:=abs(length(Landstring[y])-length(LeftFromNPC))-1;
    RightFromNPC:=RightStr(Landstring[y],r);
    Landstring[y]:=LeftFromNPC+'X'+RightFromNPC;
//     end;

//     GotoXY (2, 5);
//     write ('placing portals   ');

    // stairs to next area
    if (lvl<20) or (lvl=22) or (lvl=23) or (lvl=24) then
    begin
        repeat
            x:=2+trunc(random(40));   // 74
            y:=2+trunc(random(40));
            zeile:=Landstring[y];
//         until (zeile[x]='=');
        until (zeile[x]<>'^') and (zeile[x]<>'#') and (zeile[x]<>'$') and (zeile[x]<>'&') and (zeile[x]<>'?') and (zeile[x]<>'�') and (zeile[x]<>'-');
        LeftFromNPC:=LeftStr(Landstring[y],x-1);
        r:=abs(length(Landstring[y])-length(LeftFromNPC))-1;
        RightFromNPC:=RightStr(Landstring[y],r);
        Landstring[y]:=LeftFromNPC+'>'+RightFromNPC;
    end;


    // Portal to prev. area
    if ((lvl>1) and (lvl<21)) or (lvl=23) or (lvl=24) or (lvl=25) then
    begin
        repeat
            x:=2+trunc(random(40));  // 74
            y:=2+trunc(random(40));
            zeile:=Landstring[y];
//         until (zeile[x]='=');
        until (zeile[x]<>'^') and (zeile[x]<>'#') and (zeile[x]<>'$') and (zeile[x]<>'&') and (zeile[x]<>'?') and (zeile[x]<>'�') and (zeile[x]<>'-');
        LeftFromNPC:=LeftStr(Landstring[y],x-1);
        r:=abs(length(Landstring[y])-length(LeftFromNPC))-1;
        RightFromNPC:=RightStr(Landstring[y],r);
        Landstring[y]:=LeftFromNPC+'<'+RightFromNPC;
    end;
end;

procedure CreatePuzzleCorridorH(y, lvl: integer);
var
    n, i, x: integer;
begin

    if lvl=3 then
    begin
        Part[1, 1]:='#####.#####';
        Part[1, 2]:='#####.#####';
        Part[1, 3]:='####.######';
        Part[1, 4]:='#####.#####';
        Part[1, 5]:='===========';
        Part[1, 6]:='===========';
        Part[1, 7]:='===========';
        Part[1, 8]:='####.######';
        Part[1, 9]:='#####.#####';
        Part[1,10]:='#####.#####';
        Part[1,11]:='#####.#####';

        Part[2, 1]:='####...####';
        Part[2, 2]:='####...####';
        Part[2, 3]:='####...####';
        Part[2, 4]:='####...####';
        Part[2, 5]:='===##.##===';
        Part[2, 6]:='====*.*====';
        Part[2, 7]:='===##.##===';
        Part[2, 8]:='####...####';
        Part[2, 9]:='####...####';
        Part[2,10]:='####...####';
        Part[2,11]:='####...####';

        Part[3, 1]:='#####.#####';
        Part[3, 2]:='#####.#####';
        Part[3, 3]:='#####.#####';
        Part[3, 4]:='#####.#####';
        Part[3, 5]:='===========';
        Part[3, 6]:='=====,,,===';
        Part[3, 7]:='====,,,,,==';
        Part[3, 8]:='#####.#####';
        Part[3, 9]:='#####.#####';
        Part[3,10]:='#####.#####';
        Part[3,11]:='#####.#####';

        Part[4, 1]:='#####.#####';
        Part[4, 2]:='#####.#####';
        Part[4, 3]:='#####.#####';
        Part[4, 4]:='#####.#####';
        Part[4, 5]:='===,,,,====';
        Part[4, 6]:='====,,=====';
        Part[4, 7]:='===========';
        Part[4, 8]:='##.#####.##';
        Part[4, 9]:='##.......##';
        Part[4,10]:='#####.#####';
        Part[4,11]:='#####.#####';

        Part[5, 1]:='##.##.##.##';
        Part[5, 2]:='##.##.##.##';
        Part[5, 3]:='##.......##';
        Part[5, 4]:='##..#######';
        Part[5, 5]:='=#.##======';
        Part[5, 6]:='=*.*=======';
        Part[5, 7]:='=#.##======';
        Part[5, 8]:='##..#######';
        Part[5, 9]:='##.......##';
        Part[5,10]:='#####.#####';
        Part[5,11]:='#####.#####';

        Part[6, 1]:='#####.#####';
        Part[6, 2]:='#####.#####';
        Part[6, 3]:='#####.#####';
        Part[6, 4]:='#####.#####';
        Part[6, 5]:='####=======';
        Part[6, 6]:='=##========';
        Part[6, 7]:='===========';
        Part[6, 8]:='#####.#####';
        Part[6, 9]:='####.######';
        Part[6,10]:='####.######';
        Part[6,11]:='#####.#####';

        Part[7, 1]:='#####.#####';
        Part[7, 2]:='####.#.####';
        Part[7, 3]:='#...###..##';
        Part[7, 4]:='#.#########';
        Part[7, 5]:='===######==';
        Part[7, 6]:='====####===';
        Part[7, 7]:='===========';
        Part[7, 8]:='#####.#####';
        Part[7, 9]:='#####.#####';
        Part[7,10]:='#####.#####';
        Part[7,11]:='#####.#####';

        Part[8, 1]:='#####.#####';
        Part[8, 2]:='#####.#####';
        Part[8, 3]:='######...##';
        Part[8, 4]:='#########.#';
        Part[8, 5]:='===####====';
        Part[8, 6]:='===========';
        Part[8, 7]:='======##.#=';
        Part[8, 8]:='########.##';
        Part[8, 9]:='#######.###';
        Part[8,10]:='######.####';
        Part[8,11]:='#####.#####';

        Part[9, 1]:='#####.#####';
        Part[9, 2]:='#####.#####';
        Part[9, 3]:='#####.#####';
        Part[9, 4]:='##=====####';
        Part[9, 5]:='===========';
        Part[9, 6]:='===========';
        Part[9, 7]:='=====####==';
        Part[9, 8]:='##.########';
        Part[9, 9]:='##.########';
        Part[9,10]:='###...#####';
        Part[9,11]:='######.####';

        Part[10, 1]:='#####.#####';
        Part[10, 2]:='####...####';
        Part[10, 3]:='####...####';
        Part[10, 4]:='####...####';
        Part[10, 5]:='===========';
        Part[10, 6]:='===========';
        Part[10, 7]:='===========';
        Part[10, 8]:='#######..##';
        Part[10, 9]:='#####.....#';
        Part[10,10]:='#####.#..##';
        Part[10,11]:='#####.#####';

        Part[11, 1]:='#...#######';
        Part[11, 2]:='#.......###';
        Part[11, 3]:='#...###.###';
        Part[11, 4]:='#######.###';
        Part[11, 5]:='===========';
        Part[11, 6]:='===========';
        Part[11, 7]:='===========';
        Part[11, 8]:='##.########';
        Part[11, 9]:='##....#####';
        Part[11,10]:='#####.#####';
        Part[11,11]:='#####.#####';

        Part[12, 1]:='#####.#####';
        Part[12, 2]:='####.######';
        Part[12, 3]:='###.#######';
        Part[12, 4]:='###.##=====';
        Part[12, 5]:='=======,,,=';
        Part[12, 6]:='======,,,,=';
        Part[12, 7]:='===========';
        Part[12, 8]:='#####.#####';
        Part[12, 9]:='####...####';
        Part[12,10]:='#####.#####';
        Part[12,11]:='#####.#####';

        Part[13, 1]:='#####.#####';
        Part[13, 2]:='######.####';
        Part[13, 3]:='#######.###';
        Part[13, 4]:='#######.###';
        Part[13, 5]:='===========';
        Part[13, 6]:='===,,======';
        Part[13, 7]:='===========';
        Part[13, 8]:='#########.#';
        Part[13, 9]:='#########.#';
        Part[13,10]:='######...##';
        Part[13,11]:='#####.#####';

        TotalParts:=13;
    end;

    if lvl=6 then
    begin
        Part[1, 1]:='^^^^^.^^^^^';
        Part[1, 2]:='^---^.^^^^^';
        Part[1, 3]:='^---^.^^^^^';
        Part[1, 4]:='^^+^^.^^^^^';
        Part[1, 5]:='...........';
        Part[1, 6]:='...........';
        Part[1, 7]:='...........';
        Part[1, 8]:='^^^^^.^^^^^';
        Part[1, 9]:='^^^^^.^^^^^';
        Part[1,10]:='^^^^^.^^^^^';
        Part[1,11]:='^^^^^.^^^^^';

        Part[2, 1]:='^^^^^.^^^^^';
        Part[2, 2]:='^^^^^.^^^^^';
        Part[2, 3]:='^^^^^.^^^^^';
        Part[2, 4]:='^^^^^+^^^^^';
        Part[2, 5]:='...........';
        Part[2, 6]:='...........';
        Part[2, 7]:='...........';
        Part[2, 8]:='^^^^^.^^^^^';
        Part[2, 9]:='^^^^^.^^^^^';
        Part[2,10]:='^^^^^.^^^^^';
        Part[2,11]:='^^^^^.^^^^^';

        Part[3, 1]:='^^^^^.^^^^^';
        Part[3, 2]:='^^^^^.^^^^^';
        Part[3, 3]:='^^^^^.^^^^^';
        Part[3, 4]:='^^^^^.^^^^^';
        Part[3, 5]:='...........';
        Part[3, 6]:='...........';
        Part[3, 7]:='...........';
        Part[3, 8]:='^^^^^.^^^^^';
        Part[3, 9]:='^^^^^.^^^^^';
        Part[3,10]:='^^^^^.^^^^^';
        Part[3,11]:='^^^^^.^^^^^';

        Part[4, 1]:='^^^^^.^^^^^';
        Part[4, 2]:='^^^^^.^^^^^';
        Part[4, 3]:='^^^^^.^^^^^';
        Part[4, 4]:='^^^^^.^^^^^';
        Part[4, 5]:='...........';
        Part[4, 6]:='...........';
        Part[4, 7]:='...........';
        Part[4, 8]:='^^^^^.^^+^^';
        Part[4, 9]:='^^^^^.^---^';
        Part[4,10]:='^^^^^.^---^';
        Part[4,11]:='^^^^^.^^^^^';

        TotalParts:=4;
    end;

    if lvl=23 then
    begin
        Part[1, 1]:='#####.#####';
        Part[1, 2]:='...........';
        Part[1, 3]:='...........';
        Part[1, 4]:='...........';
        Part[1, 5]:='..#.....#..';
        Part[1, 6]:='.###...###.';
        Part[1, 7]:='..#.....#..';
        Part[1, 8]:='...........';
        Part[1, 9]:='...........';
        Part[1,10]:='...........';
        Part[1,11]:='#####.#####';

        Part[2, 1]:='#####.#####';
        Part[2, 2]:='...........';
        Part[2, 3]:='...........';
        Part[2, 4]:='.....-.....';
        Part[2, 5]:='....-#-....';
        Part[2, 6]:='...-###-...';
        Part[2, 7]:='....-#-....';
        Part[2, 8]:='.....-.....';
        Part[2, 9]:='...........';
        Part[2,10]:='...........';
        Part[2,11]:='#####.#####';

        Part[3, 1]:='#####.#####';
        Part[3, 2]:='#---#.#####';
        Part[3, 3]:='#---+.#####';
        Part[3, 4]:='#####.#####';
        Part[3, 5]:='...........';
        Part[3, 6]:='.#########.';
        Part[3, 7]:='...........';
        Part[3, 8]:='#####.#####';
        Part[3, 9]:='#####.#---#';
        Part[3,10]:='#####.+---#';
        Part[3,11]:='#####.#####';

        Part[4, 1]:='#####.#####';
        Part[4, 2]:='#..#...#..#';
        Part[4, 3]:='#.###.###.#';
        Part[4, 4]:='#..#...#.-#';
        Part[4, 5]:='...........';
        Part[4, 6]:='.#########.';
        Part[4, 7]:='...........';
        Part[4, 8]:='#..#...#..#';
        Part[4, 9]:='#-###.###.#';
        Part[4,10]:='#..#...#..#';
        Part[4,11]:='#####.#####';

        TotalParts:=4;
    end;

    x:=0;

    for i:=y to y+10 do
        Landstring[i]:='';

    while x<50 do  // 90
    begin
        n:=1+trunc(random(TotalParts));
        for i:=1 to 11 do
            Landstring[(y-1)+i]:=Landstring[(y-1)+i]+Part[n,i];

        inc(x,11);
    end;
end;

procedure CreatePuzzleCorridorV(x, lvl: integer);
var
    n, i, y, r: integer;
    LeftFromRiver, RightFromRiver: string;
begin

    if lvl=3 then
    begin
        Part[1, 1]:='#####=#####';
        Part[1, 2]:='#####=#####';
        Part[1, 3]:='#####=#####';
        Part[1, 4]:='####===####';
        Part[1, 5]:='####===####';
        Part[1, 6]:='....===....';
        Part[1, 7]:='####===####';
        Part[1, 8]:='####===####';
        Part[1, 9]:='#####=#####';
        Part[1,10]:='#####=#####';
        Part[1,11]:='#####=#####';

        Part[2, 1]:='###..=..###';
        Part[2, 2]:='###..=..###';
        Part[2, 3]:='###..=..###';
        Part[2, 4]:='###.===.###';
        Part[2, 5]:='###.===.###';
        Part[2, 6]:='....===....';
        Part[2, 7]:='###.===.###';
        Part[2, 8]:='###.===.###';
        Part[2, 9]:='###.===.###';
        Part[2,10]:='###.===.###';
        Part[2,11]:='###..==.###';

        Part[3, 1]:='#####==.###';
        Part[3, 2]:='#======.###';
        Part[3, 3]:='#======.###';
        Part[3, 4]:='#======.###';
        Part[3, 5]:='#####==.###';
        Part[3, 6]:='...........';
        Part[3, 7]:='#####.=.###';
        Part[3, 8]:='#####.=.###';
        Part[3, 9]:='#####===###';
        Part[3,10]:='#####===###';
        Part[3,11]:='#####===###';

        Part[4, 1]:='###..=..###';
        Part[4, 2]:='###..=..###';
        Part[4, 3]:='###..=..###';
        Part[4, 4]:='###..=..###';
        Part[4, 5]:='###..=..###';
        Part[4, 6]:='======.....';
        Part[4, 7]:='###...#####';
        Part[4, 8]:='###...#---#';
        Part[4, 9]:='###===+---#';
        Part[4,10]:='###===#---#';
        Part[4,11]:='###===#####';

        TotalParts:=4;
    end;


    if lvl=6 then
    begin
        Part[1, 1]:='^^^^^.^^^^^';
        Part[1, 2]:='^^^^^.^^^^^';
        Part[1, 3]:='^^^^^.^^^^^';
        Part[1, 4]:='^^^^...^^^^';
        Part[1, 5]:='^^^^...^^^^';
        Part[1, 6]:='...........';
        Part[1, 7]:='^^^^...^^^^';
        Part[1, 8]:='^^^^...^^^^';
        Part[1, 9]:='^^^^^.^^^^^';
        Part[1,10]:='^^^^^.^^^^^';
        Part[1,11]:='^^^^^.^^^^^';

        Part[2, 1]:='^^^.....^^^';
        Part[2, 2]:='^^^.....^^^';
        Part[2, 3]:='^^^.....^^^';
        Part[2, 4]:='^^^.....^^^';
        Part[2, 5]:='^^^.....^^^';
        Part[2, 6]:='...........';
        Part[2, 7]:='^^^.....^^^';
        Part[2, 8]:='^^^.....^^^';
        Part[2, 9]:='^^^.....^^^';
        Part[2,10]:='^^^.....^^^';
        Part[2,11]:='^^^.....^^^';

        Part[3, 1]:='^^^^^...^^^';
        Part[3, 2]:='^---^...^^^';
        Part[3, 3]:='^---+...^^^';
        Part[3, 4]:='^---^...^^^';
        Part[3, 5]:='^^^^^...^^^';
        Part[3, 6]:='...........';
        Part[3, 7]:='^^^^^...^^^';
        Part[3, 8]:='^^^^^...^^^';
        Part[3, 9]:='^^^^^...^^^';
        Part[3,10]:='^^^^^...^^^';
        Part[3,11]:='^^^^^...^^^';

        Part[4, 1]:='^^^.....^^^';
        Part[4, 2]:='^^^.....^^^';
        Part[4, 3]:='^^^.....^^^';
        Part[4, 4]:='^^^.....^^^';
        Part[4, 5]:='^^^.....^^^';
        Part[4, 6]:='...........';
        Part[4, 7]:='^^^...^^^^^';
        Part[4, 8]:='^^^...^---^';
        Part[4, 9]:='^^^...+---^';
        Part[4,10]:='^^^...^---^';
        Part[4,11]:='^^^...^^^^^';


        TotalParts:=4;
    end;

    if lvl=21 then
    begin
        Part[1, 1]:='S^^^^^^SSSS';
        Part[1, 2]:='SSS^^^^^^SS';
        Part[1, 3]:='SSSSS^^SSSS';
        Part[1, 4]:='SS^^^^SSSSS';
        Part[1, 5]:='SSSS^^^^SSS';
        Part[1, 6]:='SSSSS^^SSSS';
        Part[1, 7]:='SSSS^^^^SSS';
        Part[1, 8]:='SSSS^^^^^^S';
        Part[1, 9]:='SSS^^^^^^SS';
        Part[1,10]:='SS^^^^^^SSS';
        Part[1,11]:='SSSSS^^^^SS';

        Part[2, 1]:='S^^^^^^^^SS';
        Part[2, 2]:='SSS^^^^^^SS';
        Part[2, 3]:='SSS^^^^^^SS';
        Part[2, 4]:='SS^^^^^^SSS';
        Part[2, 5]:='SSS^^^^^^SS';
        Part[2, 6]:='SSS^^^^^^^S';
        Part[2, 7]:='S^^^^^^^SSS';
        Part[2, 8]:='SSSS^^^^^SS';
        Part[2, 9]:='SSS^^^^^SSS';
        Part[2,10]:='SS^^^^^^SSS';
        Part[2,11]:='SSSSS^^^^SS';

        Part[3, 1]:='SS^^^^^SSSS';
        Part[3, 2]:='SSS^^^^^^SS';
        Part[3, 3]:='SS^^^^^^^^S';
        Part[3, 4]:='S^^^^^^^^SS';
        Part[3, 5]:='SSS^^^^^^SS';
        Part[3, 6]:='S^^^^^^^^^^';
        Part[3, 7]:='SS^^^^^^^^S';
        Part[3, 8]:='S^^^^^^^^SS';
        Part[3, 9]:='^^^^^^^^SSS';
        Part[3,10]:='S^^^^^^SSSS';
        Part[3,11]:='SS^^^^^^^SS';

        Part[4, 1]:='SS^^^^^^^SS';
        Part[4, 2]:='SSS^^^^^SSS';
        Part[4, 3]:='SS^^^^^^^SS';
        Part[4, 4]:='SSS^^^^^^SS';
        Part[4, 5]:='SSSS^^^^^SS';
        Part[4, 6]:='S^^SSSS^^^^';
        Part[4, 7]:='SS^^^^SSSSS';
        Part[4, 8]:='S^^^^^^^^SS';
        Part[4, 9]:='SS^^^^^SSSS';
        Part[4,10]:='SS^^^^SSSSS';
        Part[4,11]:='SSS^^^^^^SS';

        Part[5, 1]:='SS^^^^^^^SS';
        Part[5, 2]:='SSS^^^^^SSS';
        Part[5, 3]:='SSSSS^^^^SS';
        Part[5, 4]:='SS^^^^^^SSS';
        Part[5, 5]:='SSSS^^^^^^S';
        Part[5, 6]:='SSS^S^^^^SS';
        Part[5, 7]:='SSS^^S^^^^S';
        Part[5, 8]:='SS^^^^S^^^S';
        Part[5, 9]:='S^^^^^S^^SS';
        Part[5,10]:='SS^^^^^S^^S';
        Part[5,11]:='SSS^^^^^SSS';

        TotalParts:=5;
    end;

    if lvl=23 then
    begin
        Part[1, 1]:='#####.#####';
        Part[1, 2]:='#####.#####';
        Part[1, 3]:='#####.#####';
        Part[1, 4]:='####...####';
        Part[1, 5]:='####.#.####';
        Part[1, 6]:='...........';
        Part[1, 7]:='####.#.####';
        Part[1, 8]:='####...####';
        Part[1, 9]:='#####.#####';
        Part[1,10]:='#####.#####';
        Part[1,11]:='#####.#####';

        Part[2, 1]:='###..#..###';
        Part[2, 2]:='###..#..###';
        Part[2, 3]:='###..#..###';
        Part[2, 4]:='###..#..###';
        Part[2, 5]:='###..#..###';
        Part[2, 6]:='.....#.....';
        Part[2, 7]:='###..#..###';
        Part[2, 8]:='###..#..###';
        Part[2, 9]:='###..#..###';
        Part[2,10]:='###..#..###';
        Part[2,11]:='###.....###';

        Part[3, 1]:='#----#----#';
        Part[3, 2]:='#---###---#';
        Part[3, 3]:='#----#----#';
        Part[3, 4]:='#---------#';
        Part[3, 5]:='#----#----#';
        Part[3, 6]:='----###----';
        Part[3, 7]:='#----#----#';
        Part[3, 8]:='#---------#';
        Part[3, 9]:='#----#----#';
        Part[3,10]:='#---###---#';
        Part[3,11]:='#----#----#';

        Part[4, 1]:='.#.......#.';
        Part[4, 2]:='###.....###';
        Part[4, 3]:='.#.......#.';
        Part[4, 4]:='...........';
        Part[4, 5]:='.#...#...#.';
        Part[4, 6]:='###.###.###';
        Part[4, 7]:='.#...#...#.';
        Part[4, 8]:='...........';
        Part[4, 9]:='.#.......#.';
        Part[4,10]:='###.....###';
        Part[4,11]:='.#.......#.';


        TotalParts:=4;
    end;


    y:=0;

//     while y<143 do
    while y<51 do  // 73
    begin
        n:=1+trunc(random(TotalParts));
        for i:=1 to 11 do
        begin
            LeftFromRiver:=LeftStr(Landstring[y+i],x-1);
            r:=length(Landstring[y+i])+1-(x+11);
            RightFromRiver:=RightStr(Landstring[y+i],r);
            Landstring[y+i]:=LeftFromRiver+Part[n,i]+RightFromRiver;
        end;
        inc(y,11);
    end;
end;


procedure CreatePuzzleRiverH(y: integer);
var
    n, i, x: integer;
begin
    Part[1, 1]:=',,,g,,,g,,g';
    Part[1, 2]:='g,ggggggggg';
    Part[1, 3]:='==========,';
    Part[1, 4]:='===========';
    Part[1, 5]:='===========';
    Part[1, 6]:='=========bt';
    Part[1, 7]:='g===,===,==';
    Part[1, 8]:='ggtggg,,ggg';
    Part[1, 9]:='ggggggggbgg';
    Part[1,10]:='gggg,,gggtg';
    Part[1,11]:='ggg,ggggtgg';

    Part[2, 1]:='gg,,,,tb,tb';
    Part[2, 2]:='gg,,ggggbbg';
    Part[2, 3]:='==========,';
    Part[2, 4]:='===========';
    Part[2, 5]:='===========';
    Part[2, 6]:='===========';
    Part[2, 7]:='gg,,,,,,===';
    Part[2, 8]:='tgtgg,,gggg';
    Part[2, 9]:=',gg,,gggbgg';
    Part[2,10]:='gggg,,gggtg';
    Part[2,11]:='ggg,ggggtgg';

    Part[3, 1]:='tg,tb,,,,g,';
    Part[3, 2]:='gg,,gggg,,g';
    Part[3, 3]:=',bgggbggggt';
    Part[3, 4]:='====,ggtggg';
    Part[3, 5]:='===========';
    Part[3, 6]:='===========';
    Part[3, 7]:='===========';
    Part[3, 8]:='tgtgg,,gggg';
    Part[3, 9]:=',gg,,gggbgg';
    Part[3,10]:='gggg,,gg,tg';
    Part[3,11]:='ggg,ggg,tgg';

    Part[4, 1]:='gg,,,,tb,tb';
    Part[4, 2]:='gg,,ggggbbg';
    Part[4, 3]:=',,ggtttbtbb';
    Part[4, 4]:='===bttbt===';
    Part[4, 5]:='===========';
    Part[4, 6]:='=========bt';
    Part[4, 7]:='ggg=====,,g';
    Part[4, 8]:='tgtgg,,gg,,';
    Part[4, 9]:='tggbggggbgg';
    Part[4,10]:='ggggttg,gtg';
    Part[4,11]:=',gg,g,,gtg,';

    Part[5, 1]:='gg,,,,tb,,b';
    Part[5, 2]:='gg,=====bbg';
    Part[5, 3]:='==========,';
    Part[5, 4]:='===========';
    Part[5, 5]:='===========';
    Part[5, 6]:='=========bt';
    Part[5, 7]:='gg=========';
    Part[5, 8]:='tgtg======g';
    Part[5, 9]:=',gg=====bg,';
    Part[5,10]:='g,g====,gt,';
    Part[5,11]:='ggb,==ggtgg';

    Part[6, 1]:='ggg,ggggtgg';
    Part[6, 2]:='gg,gtttggtg';
    Part[6, 3]:='g,ttttgttgg';
    Part[6, 4]:='gt==bbbbbgg';
    Part[6, 5]:='===========';
    Part[6, 6]:='=========,,';
    Part[6, 7]:='ggbbbbggggg';
    Part[6, 8]:='tgtgbbtgtgg';
    Part[6, 9]:='tttggtgggt,';
    Part[6,10]:='g,ggtgg,tt,';
    Part[6,11]:='gg,ggg,,tgg';

    Part[7, 1]:=',,g,gg,gtgg';
    Part[7, 2]:=',g,gt,,ggtg';
    Part[7, 3]:='g,tggtgttgg';
    Part[7, 4]:='gtggbbbbbgg';
    Part[7, 5]:='===,,,===,,';
    Part[7, 6]:='===========';
    Part[7, 7]:='ggbb==,,ggg';
    Part[7, 8]:='tgtgbbtg,gg';
    Part[7, 9]:='t,tgg,,,gt,';
    Part[7,10]:='g,ggtgg,tt,';
    Part[7,11]:=',g,ggg,ttgg';

    Part[8, 1]:='bgg,ggg==gg';
    Part[8, 2]:='gg,bgg==bgg';
    Part[8, 3]:='tg,g==,gggg';
    Part[8, 4]:='tg===,gggtg';
    Part[8, 5]:='========bbg';
    Part[8, 6]:='===========';
    Part[8, 7]:=',gg=======,';
    Part[8, 8]:=',gg,tggggbh';
    Part[8, 9]:='ggtbbgbbbgb';
    Part[8,10]:='gg,gtbbbbg,';
    Part[8,11]:='bgg,,,gthgg';

    Part[9, 1]:='b########gg';
    Part[9, 2]:='g#---+--+gg';
    Part[9, 3]:='t?#--#--#gg';
    Part[9, 4]:='tg#######tg';
    Part[9, 5]:='========bbg';
    Part[9, 6]:='===========';
    Part[9, 7]:=',gg=======,';
    Part[9, 8]:=',gg,tggggbh';
    Part[9, 9]:='ggtbbgbbbgb';
    Part[9,10]:='gg,gtbbbbg,';
    Part[9,11]:='bgg,,,gthgg';

    TotalParts:=9;

    x:=0;

    for i:=y to y+10 do
        Landstring[i]:='';

    while x<50 do  // 90
    begin
        n:=1+trunc(random(TotalParts));
        for i:=1 to 11 do
            Landstring[(y-1)+i]:=Landstring[(y-1)+i]+Part[n,i];

        inc(x,11);
    end;
end;



procedure CreatePuzzleRiverV(x: integer);
var
    n, i, y, r: integer;
    LeftFromRiver, RightFromRiver: string;
begin
    Part[1, 1]:=',g========g';
    Part[1, 2]:='g=======ggg';
    Part[1, 3]:='gg========g';
    Part[1, 4]:='gg=======gg';
    Part[1, 5]:='g=======ggt';
    Part[1, 6]:='gg=======gg';
    Part[1, 7]:='tgg=======g';
    Part[1, 8]:='g=======ggg';
    Part[1, 9]:='gg=======gg';
    Part[1,10]:='g,g=======g';
    Part[1,11]:='gg========g';

    Part[2, 1]:='gg========g';
    Part[2, 2]:='g========gg';
    Part[2, 3]:='gg=======g,';
    Part[2, 4]:=',========gg';
    Part[2, 5]:=',g========g';
    Part[2, 6]:='g========t,';
    Part[2, 7]:='gg====ggg,,';
    Part[2, 8]:='bgg=====g,,';
    Part[2, 9]:='gg=====gggg';
    Part[2,10]:=',gt=====tgg';
    Part[2,11]:='ggg=======g';

    Part[3, 1]:='gtg======gg';
    Part[3, 2]:=',t======bgg';
    Part[3, 3]:='gg======ggt';
    Part[3, 4]:='gg=====,gg,';
    Part[3, 5]:='g======,,,,';
    Part[3, 6]:='==========g';
    Part[3, 7]:='===========';
    Part[3, 8]:='gg=========';
    Part[3, 9]:='gbbg======g';
    Part[3,10]:=',,g=====gtg';
    Part[3,11]:='g,,g====ggg';

    Part[4, 1]:='t,,======t,';
    Part[4, 2]:='tt=======tg';
    Part[4, 3]:='tt=======tb';
    Part[4, 4]:='gt======,tt';
    Part[4, 5]:='tt,======gg';
    Part[4, 6]:=',tt=====tgt';
    Part[4, 7]:='tg=======tt';
    Part[4, 8]:='gt======t,g';
    Part[4, 9]:='gg=======g,';
    Part[4,10]:='tgt=======g';
    Part[4,11]:='gg=======gt';

    Part[5, 1]:='tg,g====tgt';
    Part[5, 2]:='tt,gg====tg';
    Part[5, 3]:='ttg,gg===tg';
    Part[5, 4]:='gtgg===gtgt';
    Part[5, 5]:=',tg===g,bbg';
    Part[5, 6]:=',ttg===gtgt';
    Part[5, 7]:='tg=gg===gtt';
    Part[5, 8]:='gt=g===gtgg';
    Part[5, 9]:='ggt=====tgg';
    Part[5,10]:='tggb======g';
    Part[5,11]:='gg=======gt';

    Part[6, 1]:='b,t,===,t,,';
    Part[6, 2]:='bb,,,,==,,,';
    Part[6, 3]:='gb,tt,==,,t';
    Part[6, 4]:='bgt,==,,,,g';
    Part[6, 5]:='tt,==,,,,,,';
    Part[6, 6]:=',,,t==,,b,,';
    Part[6, 7]:=',,===,,,,,g';
    Part[6, 8]:=',,==,,t,,gg';
    Part[6, 9]:=',t,==,,t,,g';
    Part[6,10]:=',,,==,,,,,,';
    Part[6,11]:='t,====,,,,g';

    Part[7, 1]:='t,,,==g,,,,';
    Part[7, 2]:=',,b==g,,,,,';
    Part[7, 3]:=',,t==tg,,g,';
    Part[7, 4]:='g,gg==,,ggg';
    Part[7, 5]:=',,,==,,t,,g';
    Part[7, 6]:='g,===b,,,,g';
    Part[7, 7]:='g,==,,g,,,,';
    Part[7, 8]:=',,,===,tg,g';
    Part[7, 9]:=',,===,,,gg,';
    Part[7,10]:=',====,b,,t,';
    Part[7,11]:='g====,,ggg,';

    Part[8, 1]:='bgg===gtggg';
    Part[8, 2]:='gggb==gtbg,';
    Part[8, 3]:='tgbg=gtbtg,';
    Part[8, 4]:='tggt=tgbbtg';
    Part[8, 5]:='g,gg,=ttbbg';
    Part[8, 6]:='gbg,g=,gg,g';
    Part[8, 7]:=',gbg=g,bggt';
    Part[8, 8]:=',gg=tgg,gbh';
    Part[8, 9]:='ggt=ggggggb';
    Part[8,10]:='gggg==ggbg,';
    Part[8,11]:='bgggg==thgg';

    Part[9, 1]:='b,,===,,g,g';
    Part[9, 2]:='gggb==gt,g,';
    Part[9, 3]:=',g======tg,';
    Part[9, 4]:='t,,,===,btg';
    Part[9, 5]:='g,,,,=tt,,,';
    Part[9, 6]:='gb,,g=,gg,g';
    Part[9, 7]:=',g,,g==bggt';
    Part[9, 8]:=',gg=,tg==bh';
    Part[9, 9]:='ggt=g,tg==b';
    Part[9,10]:='g,,t====bg,';
    Part[9,11]:='bg====,thgg';

    Part[10, 1]:='t,,,==g,,,,';
    Part[10, 2]:=',,b==g,,,,,';
    Part[10, 3]:=',,t==tg####';
    Part[10, 4]:='g,gg==,#--#';
    Part[10, 5]:=',,,==,,+--#';
    Part[10, 6]:='g,===b,####';
    Part[10, 7]:='g,==,,g,,,,';
    Part[10, 8]:=',,,===,tg,g';
    Part[10, 9]:=',,===,,,gg,';
    Part[10,10]:=',====,b,,t,';
    Part[10,11]:='g====,,ggg,';

    TotalParts:=10;
    

    y:=0;

//     while y<143 do
    while y<51 do  // 73
    begin
        n:=1+trunc(random(TotalParts));
        for i:=1 to 11 do
        begin
            LeftFromRiver:=LeftStr(Landstring[y+i],x-1);
            r:=length(Landstring[y+i])+1-(x+11);
            RightFromRiver:=RightStr(Landstring[y+i],r);
            Landstring[y+i]:=LeftFromRiver+Part[n,i]+RightFromRiver;
        end;
        inc(y,11);
    end;
end;

procedure CreatePuzzleBase(y, m, lvl: integer);
var
    n, i, j: integer;
begin
    if (lvl=1) or (lvl=22) then
    begin
        // standard set
        Part[1, 1]:='gg,ggg,ggbg';
        Part[1, 2]:='gggggggtggg';
        Part[1, 3]:=',gggggggggg';
        Part[1, 4]:='tggggggggg,';
        Part[1, 5]:='ggggggggggg';
        Part[1, 6]:='tt,gggggggt';
        Part[1, 7]:=',,ggggggggg';
        Part[1, 8]:='ggggggggggg';
        Part[1, 9]:='gtggggggg,g';
        Part[1,10]:='gggggtgggg,';
        Part[1,11]:=',gggggggggg';
    
        Part[2, 1]:='g,g,,gggtg,';
        Part[2, 2]:=',gtggggggg,';
        Part[2, 3]:='gggggggtggg';
        Part[2, 4]:='ggggggggggg';
        Part[2, 5]:='ggggtgggggg';
        Part[2, 6]:='ggggggtggtg';
        Part[2, 7]:='ggtgggggggb';
        Part[2, 8]:=',ggggggtggg';
        Part[2, 9]:=',gggtgggggg';
        Part[2,10]:='ggtgggggtgg';
        Part[2,11]:='gg,ggg,g,gg';
    
        Part[3, 1]:='gtgggtgg,,g';
        Part[3, 2]:='gtggggggbg,';
        Part[3, 3]:='gggtgggtggt';
        Part[3, 4]:='g,ggggggggg';
        Part[3, 5]:=',gtg,tggggg';
        Part[3, 6]:=',gggg,gtgbg';
        Part[3, 7]:='ggggtt,ggg,';
        Part[3, 8]:='gggggbgtgg,';
        Part[3, 9]:='ggbgtgggggg';
        Part[3,10]:='gbgggggggtg';
        Part[3,11]:='ghggtgggggg';
    
        Part[4, 1]:='tttgtt,bttt';
        Part[4, 2]:=',tgtttttttg';
        Part[4, 3]:='ttttttbtttt';
        Part[4, 4]:='gttbttttttt';
        Part[4, 5]:='ttbtttttggg';
        Part[4, 6]:='htttbttttgt';
        Part[4, 7]:='tgttttggttt';
        Part[4, 8]:='gttttttgtgg';
        Part[4, 9]:='gggttttttgg';
        Part[4,10]:='tgtttbt,ttg';
        Part[4,11]:='ggtgtt,ttgt';
    
        Part[5, 1]:='gggtgg,gg,g';
        Part[5, 2]:='gg=gggg,tgg';
        Part[5, 3]:=',g==tggtgg,';
        Part[5, 4]:='ggg==ggggt,';
        Part[5, 5]:='g,g==ggtgtg';
        Part[5, 6]:=',ggg==ggggg';
        Part[5, 7]:='gggtg==gtgg';
        Part[5, 8]:='ggg=====ggg';
        Part[5, 9]:='gtgg=====g,';
        Part[5,10]:='gggggt==gtg';
        Part[5,11]:='ggtgggggggg';
    
        Part[6, 1]:='ttggggbbbgg';
        Part[6, 2]:='ggtgggtggbg';
        Part[6, 3]:='gtgggggggg,';
        Part[6, 4]:='g,gbgtgtttg';
        Part[6, 5]:=',gggtggtttt';
        Part[6, 6]:='ggtttgbbggg';
        Part[6, 7]:='ggggggbbbgg';
        Part[6, 8]:='hggggbbtgbg';
        Part[6, 9]:='ghbggttgbgg';
        Part[6,10]:='gbgbbgb,gt,';
        Part[6,11]:='gggtgg,ggg,';
    
        Part[7, 1]:=',gggggghhhg';
        Part[7, 2]:='g,gggggghhg';
        Part[7, 3]:='ggg,ggggggg';
        Part[7, 4]:='ggggggggg,g';
        Part[7, 5]:='gggg,ggg,gg';
        Part[7, 6]:=',gggg,g,ggg';
        Part[7, 7]:=',tgggg,gggg';
        Part[7, 8]:='gggbgggtgg,';
        Part[7, 9]:='ggtggg,gg,,';
        Part[7,10]:='hhggggg,g,g';
        Part[7,11]:='gghhhgg,ggh';
    
        Part[8, 1]:='bgg,ggg,ggg';
        Part[8, 2]:='gggbggg,bgg';
        Part[8, 3]:='tgbggg,gggg';
        Part[8, 4]:='tggtg,gggtg';
        Part[8, 5]:='g,gg,gggbbg';
        Part[8, 6]:='g,g,gg,gg,g';
        Part[8, 7]:=',gggtggbgg,';
        Part[8, 8]:=',gg,tggggbh';
        Part[8, 9]:='ggt,ggggggb';
        Part[8,10]:='gggg,ggggg,';
        Part[8,11]:='bgggg,gthgg';
    
        Part[9, 1]:='ggtggg,g,tg';
        Part[9, 2]:='g,,,ggggbgg';
        Part[9, 3]:='ggg,,,ggggg';
        Part[9, 4]:='ggg,,,g,,gg';
        Part[9, 5]:='g,g,,,,gggg';
        Part[9, 6]:='gg^,,,g,,gg';
        Part[9, 7]:='gg,^,,,gggg';
        Part[9, 8]:='gggb,ggtgg,';
        Part[9, 9]:=',gtg,,,,g,g';
        Part[9,10]:='ggg,ggg,ggg';
        Part[9,11]:='ggggggg,gg,';
    
        Part[10, 1]:=',gg,,,g,gg,';
        Part[10, 2]:='g,,,g,,,bg,';
        Part[10, 3]:='ggb,,,,,,gg';
        Part[10, 4]:='ggg,,,g,gtg';
        Part[10, 5]:=',g,g,,,,b,,';
        Part[10, 6]:='gg,,gggg,,g';
        Part[10, 7]:='gg,,tg,,,gg';
        Part[10, 8]:='g,g,,,,g,,,';
        Part[10, 9]:='ggt,g,,,ggg';
        Part[10,10]:='gg,,,ggg,,g';
        Part[10,11]:=',,ggg,gtggg';
    
        Part[11, 1]:='gg,,g,,,tgg';
        Part[11, 2]:='gggggggg,g^';
        Part[11, 3]:='ggggggggggg';
        Part[11, 4]:='ggggggtgggg';
        Part[11, 5]:='ggg,,,ggg,g';
        Part[11, 6]:=',,,ggg,,,g,';
        Part[11, 7]:='gggggbggggg';
        Part[11, 8]:='ggggtgggggg';
        Part[11, 9]:='ghggggggggg';
        Part[11,10]:='gghgghhgggg';
        Part[11,11]:='ggggh,gtggg';
    
        Part[12, 1]:='ggtgghhhggg';
        Part[12, 2]:='gggbhhgg,gg';
        Part[12, 3]:='ggggggggggg';
        Part[12, 4]:='ggg^ggtgggg';
        Part[12, 5]:='ggggggggggg';
        Part[12, 6]:=',ggggggggg,';
        Part[12, 7]:='g,,,,,ggg,g';
        Part[12, 8]:='gggbbg,,,gg';
        Part[12, 9]:='hgggggggggg';
        Part[12,10]:='^gtbggggtgg';
        Part[12,11]:='gggggtgtthg';
    
        Part[13, 1]:='g,ggg,,,,gg';
        Part[13, 2]:=',,,,,,,,t,,';
        Part[13, 3]:=',,t,b,,,,b,';
        Part[13, 4]:='t,,,,,,,,,,';
        Part[13, 5]:=',,,,,,,,b,g';
        Part[13, 6]:='g,,,t,,,,gg';
        Part[13, 7]:='g,,,,,,,,,g';
        Part[13, 8]:='tt,b,,,,,,,';
        Part[13, 9]:='t,,,,,,b,,g';
        Part[13,10]:='g,,,,,t,,,,';
        Part[13,11]:=',b,,,,g,g,,';
    
        Part[14, 1]:='bt,,gggg,,t';
        Part[14, 2]:=',,t,,,g,tt,';
        Part[14, 3]:=',,,,,g,,,t,';
        Part[14, 4]:=',,g,,,,,,gg';
        Part[14, 5]:=',g,,,b,,b,g';
        Part[14, 6]:=',,,,b,,,,,,';
        Part[14, 7]:=',,t,,,,,,,g';
        Part[14, 8]:='b,,,,,,,,,^';
        Part[14, 9]:=',,,t,,,t,,g';
        Part[14,10]:='t,,,,bt,,g,';
        Part[14,11]:='bg,,,,,gggg';
    
        Part[15, 1]:='gbtbgttttgg';
        Part[15, 2]:='tggbtggttg,';
        Part[15, 3]:='bbtttggtg,g';
        Part[15, 4]:='bbbttgggggg';
        Part[15, 5]:='tbbtt,ggg,g';
        Part[15, 6]:='gg,ggggtbg,';
        Part[15, 7]:='ggg,gbttbgg';
        Part[15, 8]:='g,g,tttbbgg';
        Part[15, 9]:=',ggg,bttbbg';
        Part[15,10]:=',,tgg,gbtgg';
        Part[15,11]:='gg,gg,gtggh';
    
        Part[16, 1]:='gbt,,,t,,gg';
        Part[16, 2]:='g,,b,,gttgt';
        Part[16, 3]:='gttb,tgtg,g';
        Part[16, 4]:='gt,,,,,,g,g';
        Part[16, 5]:='t,,,,,g,,,g';
        Part[16, 6]:='bb,ggggtbgt';
        Part[16, 7]:='g,,,gtbttg,';
        Part[16, 8]:='g,g,tb,,,g,';
        Part[16, 9]:=',ggg,,,,,bg';
        Part[16,10]:=',,tg,,gbt,g';
        Part[16,11]:='gg,g,g,,gg,';
    
        Part[17, 1]:='hhhhgggggt^';
        Part[17, 2]:='ghh^hggg^gg';
        Part[17, 3]:='ggghhghgh^g';
        Part[17, 4]:='gghhh^g^ghh';
        Part[17, 5]:='hhhhhg^hghg';
        Part[17, 6]:='hgh^g^g^hgg';
        Part[17, 7]:='gghhhg^hhgg';
        Part[17, 8]:='ghhh^^gg^hg';
        Part[17, 9]:='ghhh^g^hhgt';
        Part[17,10]:='hhg^g^hhgtb';
        Part[17,11]:='hghh^hggbgg';
    
        Part[18, 1]:='gg,h^^^,,t^';
        Part[18, 2]:=',hh^h^^g^g,';
        Part[18, 3]:=',g,,h^hgh^,';
        Part[18, 4]:=',gh^^^,^,h,';
        Part[18, 5]:=',h^ggg^^,h,';
        Part[18, 6]:='hgh^g^^^,,t';
        Part[18, 7]:='gg,,,^^h,,g';
        Part[18, 8]:='g,hh^^^,^hg';
        Part[18, 9]:='g,,h^g^^h,,';
        Part[18,10]:='hg,^^^h^^t,';
        Part[18,11]:=',,hh^^^^^,g';
    
        Part[19, 1]:=',,^,^,,^,,,';
        Part[19, 2]:=',^^,,^^,b^,';
        Part[19, 3]:=',^,^^,^t,^,';
        Part[19, 4]:=',,^^^,^^^,,';
        Part[19, 5]:='^^,^,^^,^,,';
        Part[19, 6]:='^^,,^,^^,^,';
        Part[19, 7]:=',^^^,^^,^,,';
        Part[19, 8]:=',^,^^^b^^,,';
        Part[19, 9]:=',^^,,^^,^^,';
        Part[19,10]:=',t,^^,^^,^,';
        Part[19,11]:=',,,,^^^,,,,';
    
        Part[20, 1]:='t,,,,,,,,,,';
        Part[20, 2]:=',,^,,^,,,^,';
        Part[20, 3]:=',^,,^,,,,^,';
        Part[20, 4]:='==,^^,^,^,,';
        Part[20, 5]:=',=,^,^b,^,,';
        Part[20, 6]:='^,^,^,^b,^^';
        Part[20, 7]:=',,^^,^^,^^,';
        Part[20, 8]:=',t,,^,^t^^,';
        Part[20, 9]:=',bb,^^,,^^,';
        Part[20,10]:=',,b^^,,^^,,';
        Part[20,11]:=',,,,^,,,,,,';
    
        Part[21, 1]:='^,,,,,,,g,,';
        Part[21, 2]:=',,,,t,,,,,,';
        Part[21, 3]:=',,,,,,,,t,g';
        Part[21, 4]:=',,^,,,,,,,,';
        Part[21, 5]:='g^,,,,,,^,,';
        Part[21, 6]:='^,,,,t,,^,,';
        Part[21, 7]:=',,t,,,,,^,,';
        Part[21, 8]:=',,,,,,,,,^,';
        Part[21, 9]:=',,,t,,,,,^,';
        Part[21,10]:='bt,,,,,b,,g';
        Part[21,11]:='b,,gg,,,,,g';
    
        Part[22, 1]:='^,g,,,,,,,,';
        Part[22, 2]:=',,,,^,,,,h,';
        Part[22, 3]:=',,,,,^^^^,b';
        Part[22, 4]:=',,b,,,,,,,,';
        Part[22, 5]:='g,b,gtb,^,,';
        Part[22, 6]:='g,,,,gg=^,^';
        Part[22, 7]:=',g,,,==^,^^';
        Part[22, 8]:=',,,,^^^b,^g';
        Part[22, 9]:=',,t,,,^,,,,';
        Part[22,10]:=',,,t,^^^^,,';
        Part[22,11]:='hh,,,,^^^^^';
    
        Part[23, 1]:=',,,gt,,,t,,';
        Part[23, 2]:=',,,,g,,,,,,';
        Part[23, 3]:='g,,,,,,,,,,';
        Part[23, 4]:='g,,,,,,,,,,';
        Part[23, 5]:=',,,,,,,,,,g';
        Part[23, 6]:=',,,,,,,,,gb';
        Part[23, 7]:=',,,,,,,,,,b';
        Part[23, 8]:='t,,,,,,,,,,';
        Part[23, 9]:=',,,,,,,,,,,';
        Part[23,10]:=',,,,,,,,,,h';
        Part[23,11]:='g,,,,t,,,hh';
    
        Part[24, 1]:=',ggh,,,gg,,';
        Part[24, 2]:=',,t=,t,,,t,';
        Part[24, 3]:='tt=====,t,g';
        Part[24, 4]:=',,==t====,,';
        Part[24, 5]:=',==b,=gt==,';
        Part[24, 6]:='t,===t=b==,';
        Part[24, 7]:=',bt====,=,,';
        Part[24, 8]:='gb,,=====g,';
        Part[24, 9]:='g,,tgb==,tg';
        Part[24,10]:='bthg===b,,g';
        Part[24,11]:='b,,hg,,,,gg';
    
        Part[25, 1]:='hbtbht^ttgg';
        Part[25, 2]:=',ggggggttg,';
        Part[25, 3]:='tttbbbgtg,g';
        Part[25, 4]:='hhbtbggg,gg';
        Part[25, 5]:=',gtgttbgg,g';
        Part[25, 6]:='gbtb^bbtbg,';
        Part[25, 7]:=',gbbb^bbhbh';
        Part[25, 8]:='g,g,tbtthgh';
        Part[25, 9]:=',g,g,ttgtbg';
        Part[25,10]:=',,tgg,gbtgg';
        Part[25,11]:=',gbbh,t,,g,';
    
        Part[26, 1]:='bghtgg,gggb';
        Part[26, 2]:='gtggggg,tgg';
        Part[26, 3]:='t,,gg==tgg,';
        Part[26, 4]:='gggg===ggb,';
        Part[26, 5]:='gbgg=ggtgtg';
        Part[26, 6]:=',g,g==^ggg,';
        Part[26, 7]:='ggg===^gtgg';
        Part[26, 8]:='hgg=====ggh';
        Part[26, 9]:='gtg,==t==gh';
        Part[26,10]:=',bgg,ttggtg';
        Part[26,11]:='ghbgggbbggg';
    
        Part[27, 1]:=',ggh,,,gg,,';
        Part[27, 2]:=',,t,,t,=,t,';
        Part[27, 3]:='tbt=====t,b';
        Part[27, 4]:=',,=,t====,t';
        Part[27, 5]:=',===,,g===,';
        Part[27, 6]:='t,===t,===,';
        Part[27, 7]:=',bt,==,,=,,';
        Part[27, 8]:='gb,,,====g,';
        Part[27, 9]:=',,,tgb===tg';
        Part[27,10]:='gthg,,==t,g';
        Part[27,11]:='b,,hg,,,,gg';

        Part[28, 1]:=',tgt,,,gb,b';
        Part[28, 2]:=',,t,tt,=,t,';
        Part[28, 3]:='tbt==ttbt,b';
        Part[28, 4]:='gt======b,t';
        Part[28, 5]:='t=====bb==,';
        Part[28, 6]:='t,========,';
        Part[28, 7]:=',b=======,,';
        Part[28, 8]:='gbt====b=g,';
        Part[28, 9]:=',tbbgbg,ggg';
        Part[28,10]:='gthbt,gtt,g';
        Part[28,11]:='b,tt,,b,,gg';

        Part[29, 1]:='g,,,,,g,g,b';
        Part[29, 2]:=',,t,tt,=,,,';
        Part[29, 3]:='gbt==ttbt,,';
        Part[29, 4]:='bt=#####bb,';
        Part[29, 5]:=',=##---##=,';
        Part[29, 6]:='t,##+#--#,,';
        Part[29, 7]:=',b#--####,,';
        Part[29, 8]:='gb##---##gg';
        Part[29, 9]:=',tb##+##,tg';
        Part[29,10]:=',thbg,g=t,,';
        Part[29,11]:='g,bt,,b,,gg';

        Part[30, 1]:=',gg,,t,,,,g';
        Part[30, 2]:=',,bt,^,,,g,';
        Part[30, 3]:='^^^^^,^^^^^';
        Part[30, 4]:=',^,^,,,^,^,';
        Part[30, 5]:=',,^,,,,,^,,';
        Part[30, 6]:=',^,^,,,^,^,';
        Part[30, 7]:='^^^,^,^,^^^';
        Part[30, 8]:=',g,^,^,^,,,';
        Part[30, 9]:=',^,,^,^,,^t';
        Part[30,10]:='b^,,g^,,,^,';
        Part[30,11]:=',,^g,,,,^,g';

        Part[31, 1]:='h,b,,t,,h,g';
        Part[31, 2]:=',^gg,h^^,g,';
        Part[31, 3]:=',g^^^,hh^,h';
        Part[31, 4]:='h,hhhhhhhh,';
        Part[31, 5]:=',hh^^^^^^^h';
        Part[31, 6]:=',h^^,b^^,^h';
        Part[31, 7]:=',h^,bt,,^^h';
        Part[31, 8]:=',h^^,,,^^hh';
        Part[31, 9]:=',,h^^hh^hht';
        Part[31,10]:='h,,hhhhhhgb';
        Part[31,11]:='^g,,h,tggh,';

        Part[32, 1]:='ttgtggg=ggg';
        Part[32, 2]:='gggbggg=bg,';
        Part[32, 3]:='bgbggg=gg,g';
        Part[32, 4]:='bgg,g=gggtg';
        Part[32, 5]:='gtgg,=ggbbg';
        Part[32, 6]:='gtg,g=,gg,g';
        Part[32, 7]:=',gggtg=bgg,';
        Part[32, 8]:=',gg,tg=ggbh';
        Part[32, 9]:='ggt,,g=gggb';
        Part[32,10]:='ggg,,g=ggg,';
        Part[32,11]:='bg,gg=gthgg';

        Part[33, 1]:='ttgtggh^hgg';
        Part[33, 2]:='gg^bgh^hbg,';
        Part[33, 3]:='b^^ggh^^h,g';
        Part[33, 4]:='bg^^gh^^htg';
        Part[33, 5]:='gth^^h^^hbg';
        Part[33, 6]:='gtgh^^^^h,g';
        Part[33, 7]:=',gggh^^hgg,';
        Part[33, 8]:=',gg,th^^hbh';
        Part[33, 9]:='g,t,,h^^^hb';
        Part[33,10]:=',ggb,h^^hg,';
        Part[33,11]:='bg,gg=h^hgg';

        Part[34, 1]:='btgtgggbggg';
        Part[34, 2]:='gghbggg,bg,';
        Part[34, 3]:='bh^hhh=g,,g';
        Part[34, 4]:='bgh^^^hhhhg';
        Part[34, 5]:='gtghhh^^^^h';
        Part[34, 6]:='gtg,gbhhhh^';
        Part[34, 7]:=',gggtgtbggh';
        Part[34, 8]:='hhg,tgthhh^';
        Part[34, 9]:='h^hhhhh^^^h';
        Part[34,10]:='gh^^^^^hhh,';
        Part[34,11]:='bghhhhhhhgb';

        Part[35, 1]:='^,h,,,,hhh,';
        Part[35, 2]:=',^,hhh^^,gh';
        Part[35, 3]:=',g^^^,hhh,h';
        Part[35, 4]:='h,hh^^^^^h,';
        Part[35, 5]:=',hh^====^^h';
        Part[35, 6]:='hh^^===^,^h';
        Part[35, 7]:='hh^====,^^h';
        Part[35, 8]:=',h^^===^^hh';
        Part[35, 9]:=',,hh^hh^hht';
        Part[35,10]:='h,hhhhhhhgb';
        Part[35,11]:='^h,,ggggg,,';

        Part[36, 1]:='h,b^^t,,h,g';
        Part[36, 2]:=',^gg,hgg,g,';
        Part[36, 3]:=',g^^^ghh^,h';
        Part[36, 4]:='h,hhhhghhh,';
        Part[36, 5]:=',hh^^^h^^^h';
        Part[36, 6]:=',h^^,^,^,^h';
        Part[36, 7]:=',h^,,,,,^^h';
        Part[36, 8]:=',h^^,,,^^hh';
        Part[36, 9]:=',,h^^hh^hht';
        Part[36,10]:='h,,hhhhhhgb';
        Part[36,11]:='^g,,h,tggh,';

        Part[37, 1]:='h,h,,h,hhhh';
        Part[37, 2]:='hh,==^==,gh';
        Part[37, 3]:=',g===^===,h';
        Part[37, 4]:='h=====^===,';
        Part[37, 5]:=',======^=^h';
        Part[37, 6]:='h==^=====^h';
        Part[37, 7]:='hh^=====^^h';
        Part[37, 8]:='hh^^===^^hh';
        Part[37, 9]:=',h^h^hh^h^t';
        Part[37,10]:=',^hh,,,hhg^';
        Part[37,11]:='h^h,hhhh,h^';

        Part[38, 1]:=',,h,,h,,,,g';
        Part[38, 2]:=',h,,hhh,h,,';
        Part[38, 3]:=',,,,hhhh,,h';
        Part[38, 4]:='hh,hh=hh,,,';
        Part[38, 5]:=',,hh==hh,^,';
        Part[38, 6]:='h,h===h,,^,';
        Part[38, 7]:='hh==hhhh^^t';
        Part[38, 8]:='hhhhhhh^^h,';
        Part[38, 9]:='bh^hhhhhh^,';
        Part[38,10]:=',^,h,hhhhh,';
        Part[38,11]:='^hh,,,,h,,,';

        Part[39, 1]:=',,^,^,,^,,,';
        Part[39, 2]:=',^^,,^^,b^,';
        Part[39, 3]:=',^,^^,^t,^,';
        Part[39, 4]:=',,^^^,^^^,,';
        Part[39, 5]:='^,,,,,,,^,,';
        Part[39, 6]:='^,,yy,,,,^,';
        Part[39, 7]:=',,,,,,y,^,,';
        Part[39, 8]:='Y,,,,Y,,^,,';
        Part[39, 9]:='yY,,,,,,^^,';
        Part[39,10]:=',t,^^,^^,^,';
        Part[39,11]:=',,,,^^^,,,,';

        Part[40, 1]:='h,b^^t,,h,g';
        Part[40, 2]:=',^gg,h#####';
        Part[40, 3]:=',g^^^g#---#';
        Part[40, 4]:='h,hhhh#---#';
        Part[40, 5]:=',hh^^^##+##';
        Part[40, 6]:=',h^^,^,^,^h';
        Part[40, 7]:=',h^,,,,,^^h';
        Part[40, 8]:=',h^^,,,^^hh';
        Part[40, 9]:=',,h^^hh^hht';
        Part[40,10]:='h,,hhhhhhgb';
        Part[40,11]:='^g,,h,tggh,';

        Part[41, 1]:=',,,gt,,,t,,';
        Part[41, 2]:=',,,,g,,,,,,';
        Part[41, 3]:='g,,,,,,,,,,';
        Part[41, 4]:='g,,,,,,,,,,';
        Part[41, 5]:=',,####,,,,g';
        Part[41, 6]:=',,#--#,,,gb';
        Part[41, 7]:=',,#--#,,,,b';
        Part[41, 8]:='t,#+##,,,,,';
        Part[41, 9]:=',,,,,,,,,,,';
        Part[41,10]:=',,,,,,,,,,h';
        Part[41,11]:='g,,,,t,,,hh';

        Part[42, 1]:='GTGGGTTTGGB';
        Part[42, 2]:='GGGGBBgttgG';
        Part[42, 3]:='TGGGGGggggT';
        Part[42, 4]:='TG####G,GGB';
        Part[42, 5]:='GG#--+,,TGG';
        Part[42, 6]:='B,####G,G,B';
        Part[42, 7]:='BG,,,,,,GGT';
        Part[42, 8]:='GG,GG,,,,TG';
        Part[42, 9]:='B#+#GB,,yTG';
        Part[42,10]:='T#-#G,yyYBT';
        Part[42,11]:='G###TTBBGGG';

        Part[43, 1]:='GGGGGGGGTGG';
        Part[43, 2]:='GggggGGTBTG';
        Part[43, 3]:='GggggGGGTGG';
        Part[43, 4]:='BggggGggggg';
        Part[43, 5]:='GggggG,gggg';
        Part[43, 6]:='GG,GGG,gggg';
        Part[43, 7]:='GG,,,,,gGGG';
        Part[43, 8]:='TG#####,GGG';
        Part[43, 9]:='GG#---+,GTG';
        Part[43,10]:='TG#---#GGTG';
        Part[43,11]:='GG#####GGGG';

        Part[44, 1]:='GGG#####GTB';
        Part[44, 2]:='GBG#---#GGG';
        Part[44, 3]:='GGG##+##GGG';
        Part[44, 4]:='TTGB,,,,TTB';
        Part[44, 5]:='BG##+#B,###';
        Part[44, 6]:='BG#--#B,+-#';
        Part[44, 7]:='BG#--#T,#-#';
        Part[44, 8]:='TG####,,###';
        Part[44, 9]:='GGT,##+##T,';
        Part[44,10]:='GGGT#---#,T';
        Part[44,11]:='GBGG#####hh';

        Part[45, 1]:='g,gDgDDgDgg';
        Part[45, 2]:='DDDDDDDDtDD';
        Part[45, 3]:='DDPDLDDDDL,';
        Part[45, 4]:='tDDDDDDDDDD';
        Part[45, 5]:='DDDDDDDDL,g';
        Part[45, 6]:='gDD,tDLDDgg';
        Part[45, 7]:='gDDDLLPPD,g';
        Part[45, 8]:='tPDLDDDLLD,';
        Part[45, 9]:='tDDDDDDLDDg';
        Part[45,10]:='gDgDD,tDDDD';
        Part[45,11]:=',LDgDDg,gDD';

        Part[46, 1]:=',ggDDtDDgDg';
        Part[46, 2]:='DDLPD^DD,g,';
        Part[46, 3]:='^^DD^,^^^^^';
        Part[46, 4]:=',^,^DD,^,^,';
        Part[46, 5]:='DD^DDDD,^DD';
        Part[46, 6]:=',^,^DD,^,^,';
        Part[46, 7]:='^^^,^,^,^^^';
        Part[46, 8]:=',g,D,D,^DD,';
        Part[46, 9]:=',^DD^,^DD^t';
        Part[46,10]:='L^DDg^DD,^,';
        Part[46,11]:='DD^gDDgD^,g';

        Part[47, 1]:='PPgDtgDLPgt';
        Part[47, 2]:=',PDPPPPPPPD';
        Part[47, 3]:='PPPPPPLPPPP';
        Part[47, 4]:='gPPLPPPPPPt';
        Part[47, 5]:='PPLPPPPPDgg';
        Part[47, 6]:='hPPtLPPPPgt';
        Part[47, 7]:='PDPPPPggPPt';
        Part[47, 8]:='gPPPPPPgPDg';
        Part[47, 9]:='gggPPPPPPgg';
        Part[47,10]:='PDPPtLPDPPg';
        Part[47,11]:='ggPDtPDgPgt';

        Part[48, 1]:='PDgDtPgLPgL';
        Part[48, 2]:=',PDPDDPDDDD';
        Part[48, 3]:='gPPPDDDLLPP';
        Part[48, 4]:='gPPLPLPDPPt';
        Part[48, 5]:='PgLPLLLPDgg';
        Part[48, 6]:='gPPtLPPPPgt';
        Part[48, 7]:='PDPLPPgDPPt';
        Part[48, 8]:='gPDDDDPgPDg';
        Part[48, 9]:='gLgPLLPDDgg';
        Part[48,10]:='PDDPtDPDPPg';
        Part[48,11]:='DgPgtPDgPgt';

        Part[49, 1]:='^c^,b,g,,,g';
        Part[49, 2]:='^c^,bb,,g,,';
        Part[49, 3]:='^g^^,tb,,g,';
        Part[49, 4]:='^g,^^g^^,,g';
        Part[49, 5]:='gg,ggg,^^g,';
        Part[49, 6]:='ggg^g,g,^^,';
        Part[49, 7]:=',gg^^ggg,^g';
        Part[49, 8]:='^,gg^^bgg^g';
        Part[49, 9]:='^,,gg^^bg^g';
        Part[49,10]:='^^,,g,^bg^,';
        Part[49,11]:=',^^,,,^^^^,';

        Part[50, 1]:='^^,,^,^^,^^';
        Part[50, 2]:=',^^^^,,,,^^';
        Part[50, 3]:=',^^^^,^^^,^';
        Part[50, 4]:='^^^^^c^^^,^';
        Part[50, 5]:=',,^^ccc^^,^';
        Part[50, 6]:=',^^^c^Y^^^,';
        Part[50, 7]:='^,^^c^^yY^^';
        Part[50, 8]:='^,^^c^YYy^,';
        Part[50, 9]:='^,^^cc^y^,,';
        Part[50,10]:='^^,ccc^Yy^,';
        Part[50,11]:='^^^^^c^^^^^';

        Part[51, 1]:='gg^^^c^^^gg';
        Part[51, 2]:='g^^^ccccc^^';
        Part[51, 3]:='^^^^,,,^^c^';
        Part[51, 4]:='^^,^^,^^^c^';
        Part[51, 5]:='g^,^^,^^Yc^';
        Part[51, 6]:=',,,^,^^yy^,';
        Part[51, 7]:='^,,^,^^^^Y^';
        Part[51, 8]:='^,,,,,^^^^g';
        Part[51, 9]:='^,^^^,,^^^^';
        Part[51,10]:='^,,^^,,,,^^';
        Part[51,11]:='^^,^^,,,^gg';

        Part[52, 1]:='^^,,^,^^,^,';
        Part[52, 2]:='^^,g^c^^,^,';
        Part[52, 3]:='^,ggg,c^^,^';
        Part[52, 4]:='^,,g^^^^^^^';
        Part[52, 5]:='^,,^^.#.^^^';
        Part[52, 6]:=',,^^......^';
        Part[52, 7]:='^,^.#.N.#.^';
        Part[52, 8]:='^^^.......^';
        Part[52, 9]:='^^^^..#..^^';
        Part[52,10]:='gg^^^...^^,';
        Part[52,11]:='g,g^^�^^^,,';

        Part[53, 1]:=',,^ggg^^,,,';
        Part[53, 2]:='^^gggg^^,,,';
        Part[53, 3]:='gg,,,^^^^,,';
        Part[53, 4]:='^,,,,,^^^^,';
        Part[53, 5]:='^^g,,ggggg^';
        Part[53, 6]:='ggggggggggg';
        Part[53, 7]:='^^ggggggg^^';
        Part[53, 8]:='^ggggggg^^^';
        Part[53, 9]:='^^^^gggggg^';
        Part[53,10]:=',^^^gggg^,,';
        Part[53,11]:=',,^,^g^^,,,';

        Part[54, 1]:='bt,^^g^^,,t';
        Part[54, 2]:=',,t^^t^^tt,';
        Part[54, 3]:=',,^^tb^^,t,';
        Part[54, 4]:=',,^^bb^^^gg';
        Part[54, 5]:=',^^btbt^^^g';
        Part[54, 6]:=',^bbbtbt^^^';
        Part[54, 7]:=',^tttttbt^^';
        Part[54, 8]:='b^^bbtbtt^^';
        Part[54, 9]:=',,^tbtbtt^^';
        Part[54,10]:='t,^^,btb^^,';
        Part[54,11]:='bg,^^bb^^gg';
    
        Part[55, 1]:='gb^^^^^^t,g';
        Part[55, 2]:='tg,,t,,,^^^';
        Part[55, 3]:='bbtt,^^^^^g';
        Part[55, 4]:='bb^^^^gg,^^';
        Part[55, 5]:='^^^tgg,,g,g';
        Part[55, 6]:='gg,gt,gtbg,';
        Part[55, 7]:='^^^,gbtt^^g';
        Part[55, 8]:='g,^^^^tbb^^';
        Part[55, 9]:=',ttg,^^tbb^';
        Part[55,10]:='tbtgg,^btg^';
        Part[55,11]:='bg,gg,^^g^^';
    
        Part[56, 1]:='gbt^,^^^,gg';
        Part[56, 2]:='g,^^,,,^^^t';
        Part[56, 3]:='g^^,,^,,,^g';
        Part[56, 4]:='g^,,,^,,,^^';
        Part[56, 5]:='t^^^^^,,,^,';
        Part[56, 6]:='b^,,,^^^,^^';
        Part[56, 7]:='g^^,,,,,^g^';
        Part[56, 8]:='g,^^,,,,,^^';
        Part[56, 9]:=',gg^^^^,,^g';
        Part[56,10]:=',,tg,,^^^^g';
        Part[56,11]:='gg,g,g,,gg,';

        TotalParts:=56;
    end;

    if (lvl=2) or (lvl=3) then
    begin
        // stone dungeon
        Part[1, 1]:='#####.#####';
        Part[1, 2]:='#####.#####';
        Part[1, 3]:='#####.#####';
        Part[1, 4]:='#####.#####';
        Part[1, 5]:='#####.#####';
        Part[1, 6]:='...........';
        Part[1, 7]:='#####.#####';
        Part[1, 8]:='#####.#####';
        Part[1, 9]:='#####.#####';
        Part[1,10]:='#####.#####';
        Part[1,11]:='#####.#####';
    
        Part[2, 1]:='###########';
        Part[2, 2]:='###########';
        Part[2, 3]:='###########';
        Part[2, 4]:='###########';
        Part[2, 5]:='###########';
        Part[2, 6]:='...........';
        Part[2, 7]:='###########';
        Part[2, 8]:='###########';
        Part[2, 9]:='###########';
        Part[2,10]:='###########';
        Part[2,11]:='###########';
    
        Part[3, 1]:='#####.#####';
        Part[3, 2]:='#.....#####';
        Part[3, 3]:='#.#########';
        Part[3, 4]:='#.#########';
        Part[3, 5]:='#.#########';
        Part[3, 6]:='...........';
        Part[3, 7]:='#####.#####';
        Part[3, 8]:='#####.#####';
        Part[3, 9]:='#####.#####';
        Part[3,10]:='#####.#####';
        Part[3,11]:='#####.#####';
    
        Part[4, 1]:='#####.#####';
        Part[4, 2]:='#####.#####';
        Part[4, 3]:='#####.#####';
        Part[4, 4]:='#####.#####';
        Part[4, 5]:='#####.#####';
        Part[4, 6]:='..###......';
        Part[4, 7]:='#.###.#####';
        Part[4, 8]:='#.###.#####';
        Part[4, 9]:='#.....#####';
        Part[4,10]:='#####.#####';
        Part[4,11]:='#####+#####';
    
        Part[5, 1]:='#####.#####';
        Part[5, 2]:='#####.#####';
        Part[5, 3]:='#####.#####';
        Part[5, 4]:='#####.#####';
        Part[5, 5]:='#####.#####';
        Part[5, 6]:='..###......';
        Part[5, 7]:='#.###.#####';
        Part[5, 8]:='#.###.#####';
        Part[5, 9]:='#.....#####';
        Part[5,10]:='#####.#####';
        Part[5,11]:='#####.#####';
    
        Part[6, 1]:='#####.#####';
        Part[6, 2]:='#####.#####';
        Part[6, 3]:='#####.#####';
        Part[6, 4]:='#####.#####';
        Part[6, 5]:='#####.#####';
        Part[6, 6]:='...........';
        Part[6, 7]:='##.########';
        Part[6, 8]:='##.########';
        Part[6, 9]:='##.########';
        Part[6,10]:='##.########';
        Part[6,11]:='##....#####';
    
        Part[7, 1]:='#####.#####';
        Part[7, 2]:='#####...###';
        Part[7, 3]:='#######.###';
        Part[7, 4]:='#######.###';
        Part[7, 5]:='#######.###';
        Part[7, 6]:='......*....';
        Part[7, 7]:='#####.#####';
        Part[7, 8]:='#####.#####';
        Part[7, 9]:='#####.#####';
        Part[7,10]:='#####.#####';
        Part[7,11]:='#####.#####';
    
        Part[8, 1]:='####.######';
        Part[8, 2]:='#.##.######';
        Part[8, 3]:='#.##....###';
        Part[8, 4]:='#.#####.###';
        Part[8, 5]:='#.#####.###';
        Part[8, 6]:='+..........';
        Part[8, 7]:='#.#########';
        Part[8, 8]:='#.#....---#';
        Part[8, 9]:='#.+....---#';
        Part[8,10]:='###....---#';
        Part[8,11]:='#####.#####';
    
        Part[9, 1]:='#####.#####';
        Part[9, 2]:='#.........#';
        Part[9, 3]:='#.#######.#';
        Part[9, 4]:='#.#######.#';
        Part[9, 5]:='#.#######.#';
        Part[9, 6]:='+..........';
        Part[9, 7]:='#####+#####';
        Part[9, 8]:='#.#.#.#.#.#';
        Part[9, 9]:='##.#...#.##';
        Part[9,10]:='#.#.#.#.#.#';
        Part[9,11]:='#####+#####';
    
        Part[10, 1]:='#####.#####';
        Part[10, 2]:='#####.#####';
        Part[10, 3]:='#####.#####';
        Part[10, 4]:='#####.#####';
        Part[10, 5]:='#####.#####';
        Part[10, 6]:='..###.####.';
        Part[10, 7]:='#.###+####.';
        Part[10, 8]:='#..........';
        Part[10, 9]:='#.........#';
        Part[10,10]:='#####+#####';
        Part[10,11]:='#####.#####';
    
        Part[11, 1]:='#####......';
        Part[11, 2]:='#####.####.';
        Part[11, 3]:='#####.####.';
        Part[11, 4]:='#####.####.';
        Part[11, 5]:='#####.####.';
        Part[11, 6]:='...........';
        Part[11, 7]:='#####.####.';
        Part[11, 8]:='#####.####.';
        Part[11, 9]:='#####.####.';
        Part[11,10]:='#####.####.';
        Part[11,11]:='#####......';
    
        Part[12, 1]:='#####.#####';
        Part[12, 2]:='#####.#####';
        Part[12, 3]:='#####.#####';
        Part[12, 4]:='#####.#####';
        Part[12, 5]:='#####.#####';
        Part[12, 6]:='......##...';
        Part[12, 7]:='#####.##.##';
        Part[12, 8]:='#####.##..#';
        Part[12, 9]:='#####.###.#';
        Part[12,10]:='#####.###.#';
        Part[12,11]:='#####.....#';
    
        Part[13, 1]:='#####.#####';
        Part[13, 2]:='#####.#####';
        Part[13, 3]:='##.......##';
        Part[13, 4]:='##.#####.##';
        Part[13, 5]:='##.#####.##';
        Part[13, 6]:='...##.##...';
        Part[13, 7]:='##.##.##.##';
        Part[13, 8]:='##.##+##.##';
        Part[13, 9]:='##.......##';
        Part[13,10]:='#####.#####';
        Part[13,11]:='#####.#####';
    
        Part[14, 1]:='#####+#####';
        Part[14, 2]:='#####.#####';
        Part[14, 3]:='#####.#####';
        Part[14, 4]:='#####.#####';
        Part[14, 5]:='#####.#####';
        Part[14, 6]:='+....#....+';
        Part[14, 7]:='####.#.####';
        Part[14, 8]:='####.#.####';
        Part[14, 9]:='####.#.####';
        Part[14,10]:='####.#.####';
        Part[14,11]:='#####.#####';
    
        Part[15, 1]:='#####.#####';
        Part[15, 2]:='#####.#####';
        Part[15, 3]:='#####.#####';
        Part[15, 4]:='#.....#####';
        Part[15, 5]:='#.....#####';
        Part[15, 6]:='+.....+....';
        Part[15, 7]:='#.....#####';
        Part[15, 8]:='#####.#####';
        Part[15, 9]:='#####.#####';
        Part[15,10]:='#####.#####';
        Part[15,11]:='#####.#####';
    
        if lvl=2 then
        begin
            Part[16, 1]:='#####.#####';
            Part[16, 2]:='......#####';
            Part[16, 3]:='.##########';
            Part[16, 4]:='.####......';
            Part[16, 5]:='.#....####.';
            Part[16, 6]:='...#######.';
            Part[16, 7]:='####.....#.';
            Part[16, 8]:='.....###.#.';
            Part[16, 9]:='.#######...';
            Part[16,10]:='......#####';
            Part[16,11]:='#####.#####';
        end;

        if lvl=3 then
        begin
            Part[16, 1]:='#####.#####';
            Part[16, 2]:='#####.#####';
            Part[16, 3]:='###.....###';
            Part[16, 4]:='#....#....#';
            Part[16, 5]:='#....#....#';
            Part[16, 6]:='.#########.';
            Part[16, 7]:='#....#....#';
            Part[16, 8]:='#....#....#';
            Part[16, 9]:='##.......##';
            Part[16,10]:='#####+#####';
            Part[16,11]:='#####.#####';
        end;
    
        Part[17, 1]:='#####+#####';
        Part[17, 2]:='#..#-.--[]#';
        Part[17, 3]:='#..#------#';
        Part[17, 4]:='#..#)-.---#';
        Part[17, 5]:='#..###+####';
        Part[17, 6]:='...........';
        Part[17, 7]:='#..###+####';
        Part[17, 8]:='#..#--.---#';
        Part[17, 9]:='#..#------#';
        Part[17,10]:='#..#-.----#';
        Part[17,11]:='#####+#####';
    
        Part[18, 1]:='#####.#####';
        Part[18, 2]:='#####.#####';
        Part[18, 3]:='#####.#####';
        Part[18, 4]:='#####.#####';
        Part[18, 5]:='#####.#####';
        Part[18, 6]:='+.....+...+';
        Part[18, 7]:='#####.#####';
        Part[18, 8]:='#####.#####';
        Part[18, 9]:='#####.#####';
        Part[18,10]:='#####.#####';
        Part[18,11]:='#####.#####';
    
        Part[19, 1]:='#####.#####';
        Part[19, 2]:='#####.###.#';
        Part[19, 3]:='#####.###.#';
        Part[19, 4]:='#.......#.#';
        Part[19, 5]:='#.###.#.#.#';
        Part[19, 6]:='..###.#.#..';
        Part[19, 7]:='#####.#.#.#';
        Part[19, 8]:='#####.#...#';
        Part[19, 9]:='#####.#####';
        Part[19,10]:='#####.#####';
        Part[19,11]:='#####.#####';
    
        Part[20, 1]:='#####+#####';
        Part[20, 2]:='#####.#####';
        Part[20, 3]:='#####.#####';
        Part[20, 4]:='#####.#####';
        Part[20, 5]:='#####.#####';
        Part[20, 6]:='...........';
        Part[20, 7]:='#####.#####';
        Part[20, 8]:='#####.#####';
        Part[20, 9]:='#####.#####';
        Part[20,10]:='#####.#####';
        Part[20,11]:='#####.#####';
    
        Part[21, 1]:='#####.#####';
        Part[21, 2]:='#####.#####';
        Part[21, 3]:='###...#####';
        Part[21, 4]:='###.#######';
        Part[21, 5]:='###.#######';
        Part[21, 6]:='.+.........';
        Part[21, 7]:='#######.###';
        Part[21, 8]:='#######.###';
        Part[21, 9]:='#####...###';
        Part[21,10]:='#####.#####';
        Part[21,11]:='#####.#####';
    
        Part[22, 1]:='######+####';
        Part[22, 2]:='#......####';
        Part[22, 3]:='#.####.####';
        Part[22, 4]:='#.####.####';
        Part[22, 5]:='#.####.####';
        Part[22, 6]:='..####.###.';
        Part[22, 7]:='######.###.';
        Part[22, 8]:='######.###.';
        Part[22, 9]:='#####..###.';
        Part[22,10]:='#####......';
        Part[22,11]:='#####+#####';
    
        Part[23, 1]:='#####.#####';
        Part[23, 2]:='#####.#####';
        Part[23, 3]:='#)).*..####';
        Part[23, 4]:='#...##.####';
        Part[23, 5]:='#...##.####';
        Part[23, 6]:='�..)##.+...';
        Part[23, 7]:='#####..####';
        Part[23, 8]:='#####.#####';
        Part[23, 9]:='#####.#####';
        Part[23,10]:='#####.#####';
        Part[23,11]:='#####.#####';
    
        Part[24, 1]:='#####.#####';
        Part[24, 2]:='#####.#####';
        Part[24, 3]:='#####.#####';
        Part[24, 4]:='#####.#####';
        Part[24, 5]:='#####.#####';
        Part[24, 6]:='...........';
        Part[24, 7]:='#####.#####';
        Part[24, 8]:='#####.#####';
        Part[24, 9]:='#####.#####';
        Part[24,10]:='#####.#####';
        Part[24,11]:='#####.#####';

        if lvl=2 then
        begin
            Part[25, 1]:='#####+#####';
            Part[25, 2]:='#.........#';
            Part[25, 3]:='#.#.#.#.#.#';
            Part[25, 4]:='#.........#';
            Part[25, 5]:='#.#.....#.#';
            Part[25, 6]:='+.........+';
            Part[25, 7]:='#.#.....#.#';
            Part[25, 8]:='#.........#';
            Part[25, 9]:='#.#.#.#.#.#';
            Part[25,10]:='#.........#';
            Part[25,11]:='#####+#####';
        end;

        if lvl=3 then
        begin
            Part[25, 1]:='#####+#####';
            Part[25, 2]:='#.........#';
            Part[25, 3]:='#.((..(((.#';
            Part[25, 4]:='#......-..#';
            Part[25, 5]:='#.(((..((.#';
            Part[25, 6]:='+.........+';
            Part[25, 7]:='#.(.(.(((.#';
            Part[25, 8]:='#.........#';
            Part[25, 9]:='#.[]-..[].#';
            Part[25,10]:='#.........#';
            Part[25,11]:='#####+#####';
        end;

        Part[26, 1]:='#####+#####';
        Part[26, 2]:='#####.....#';
        Part[26, 3]:='##..####.##';
        Part[26, 4]:='##..#.##.##';
        Part[26, 5]:='##..#.##.##';
        Part[26, 6]:='�...#......';
        Part[26, 7]:='##.....#..#';
        Part[26, 8]:='#####.##.##';
        Part[26, 9]:='#####.#.###';
        Part[26,10]:='######.####';
        Part[26,11]:='#####.#####';
    
        Part[27, 1]:='#####.#####';
        Part[27, 2]:='#####.#####';
        Part[27, 3]:='#.####.####';
        Part[27, 4]:='#;#####.###';
        Part[27, 5]:='#.######.##';
        Part[27, 6]:='.......#...';
        Part[27, 7]:='#.######.##';
        Part[27, 8]:='##.#####.##';
        Part[27, 9]:='###.####.##';
        Part[27,10]:='####......#';
        Part[27,11]:='#####+#####';

        Part[28, 1]:='#####.#####';
        Part[28, 2]:='#####....##';
        Part[28, 3]:='#####.###.#';
        Part[28, 4]:='#####.###.#';
        Part[28, 5]:='#####.###.#';
        Part[28, 6]:='.###.#####.';
        Part[28, 7]:='#.##.######';
        Part[28, 8]:='#.##.######';
        Part[28, 9]:='#.###.#####';
        Part[28,10]:='##....#####';
        Part[28,11]:='#####.#####';

        Part[29, 1]:='#####.#####';
        Part[29, 2]:='#####.#####';
        Part[29, 3]:='#####.#####';
        Part[29, 4]:='#####.#####';
        Part[29, 5]:='#####.#####';
        Part[29, 6]:='...........';
        Part[29, 7]:='#####*#####';
        Part[29, 8]:='####...####';
        Part[29, 9]:='####...####';
        Part[29,10]:='####...####';
        Part[29,11]:='#####*#####';

        if lvl=2 then
        begin
            Part[30, 1]:='#####.#####';
            Part[30, 2]:='#####.#####';
            Part[30, 3]:='#####.#####';
            Part[30, 4]:='#####.#####';
            Part[30, 5]:='#####.#####';
            Part[30, 6]:='......+....';
            Part[30, 7]:='#####+#####';
            Part[30, 8]:='####...####';
            Part[30, 9]:='####---####';
            Part[30,10]:='####...####';
            Part[30,11]:='#####+#####';
        end;

        if lvl=3 then
        begin
            Part[30, 1]:='#####.#####';
            Part[30, 2]:='#####.#####';
            Part[30, 3]:='#####.#####';
            Part[30, 4]:='#####.#####';
            Part[30, 5]:='#####.#####';
            Part[30, 6]:='......+....';
            Part[30, 7]:='###.#######';
            Part[30, 8]:='###..((.###';
            Part[30, 9]:='###..-..###';
            Part[30,10]:='###.....###';
            Part[30,11]:='#####+#####';
        end;


        Part[31, 1]:='#####.#####';
        Part[31, 2]:='#####.#####';
        Part[31, 3]:='#####.#####';
        Part[31, 4]:='#####.#####';
        Part[31, 5]:='#####.#####';
        Part[31, 6]:='...........';
        Part[31, 7]:='#####.#####';
        Part[31, 8]:='#####.#####';
        Part[31, 9]:='#####.#####';
        Part[31,10]:='#####.#####';
        Part[31,11]:='#####.#####';

        Part[32, 1]:='#####.#####';
        Part[32, 2]:='#####.#####';
        Part[32, 3]:='#####.#####';
        Part[32, 4]:='#####+#####';
        Part[32, 5]:='#####.#####';
        Part[32, 6]:='...+...+...';
        Part[32, 7]:='#####.#####';
        Part[32, 8]:='#####+#####';
        Part[32, 9]:='#####.#####';
        Part[32,10]:='#####.#####';
        Part[32,11]:='#####.#####';

        Part[33, 1]:='#####.#####';
        Part[33, 2]:='#####.#####';
        Part[33, 3]:='#####.#####';
        Part[33, 4]:='#####+#####';
        Part[33, 5]:='#####.#####';
        Part[33, 6]:='...........';
        Part[33, 7]:='#####.#####';
        Part[33, 8]:='#####+#####';
        Part[33, 9]:='#####.#####';
        Part[33,10]:='#####.#####';
        Part[33,11]:='#####.#####';

        Part[34, 1]:='#####.#####';
        Part[34, 2]:='#####.#####';
        Part[34, 3]:='#####.#####';
        Part[34, 4]:='#####.#####';
        Part[34, 5]:='#####.#####';
        Part[34, 6]:='...+...+...';
        Part[34, 7]:='#####.#####';
        Part[34, 8]:='#####.#####';
        Part[34, 9]:='#####.#####';
        Part[34,10]:='#####.#####';
        Part[34,11]:='#####.#####';

        Part[35, 1]:='#####.#####';
        Part[35, 2]:='#####.#####';
        Part[35, 3]:='#####.#####';
        Part[35, 4]:='#####.#####';
        Part[35, 5]:='#####�#####';
        Part[35, 6]:='...........';
        Part[35, 7]:='###########';
        Part[35, 8]:='###########';
        Part[35, 9]:='#####.#####';
        Part[35,10]:='#####.#####';
        Part[35,11]:='#####.#####';

        Part[36, 1]:='#####.#####';
        Part[36, 2]:='#####.#####';
        Part[36, 3]:='#####.#####';
        Part[36, 4]:='#####.#####';
        Part[36, 5]:='#####*#####';
        Part[36, 6]:='......#####';
        Part[36, 7]:='#####.#####';
        Part[36, 8]:='#####�#####';
        Part[36, 9]:='#####.#####';
        Part[36,10]:='#####.#####';
        Part[36,11]:='#####.#####';

        Part[37, 1]:='#####.#####';
        Part[37, 2]:='#####.#####';
        Part[37, 3]:='#####.#####';
        Part[37, 4]:='#####.#####';
        Part[37, 5]:='#####.#####';
        Part[37, 6]:='+...+......';
        Part[37, 7]:='#####�#####';
        Part[37, 8]:='#####.#####';
        Part[37, 9]:='#####.#####';
        Part[37,10]:='#####.#####';
        Part[37,11]:='#####.#####';

        Part[38, 1]:='#####.#####';
        Part[38, 2]:='#####.#####';
        Part[38, 3]:='#####.#####';
        Part[38, 4]:='#####.#####';
        Part[38, 5]:='#####.#####';
        Part[38, 6]:='.......%...';
        Part[38, 7]:='#####.#####';
        Part[38, 8]:='#####%#####';
        Part[38, 9]:='#####.#####';
        Part[38,10]:='#####.#####';
        Part[38,11]:='#####.#####';

        Part[39, 1]:='#####.#####';
        Part[39, 2]:='#%%...#.%%#';
        Part[39, 3]:='#%%%..#..%#';
        Part[39, 4]:='#%.%......#';
        Part[39, 5]:='#.%%%.#...#';
        Part[39, 6]:='+.%%%%#%..%';
        Part[39, 7]:='#####.#####';
        Part[39, 8]:='#%%.#.#.))#';
        Part[39, 9]:='#%%#.....%#';
        Part[39,10]:='#%%%#.#.%)#';
        Part[39,11]:='#####+#####';

        if lvl=2 then
        begin
            Part[40, 1]:='#####.#####';
            Part[40, 2]:='#..)#.#%..#';
            Part[40, 3]:='#.###.#%#%#';
            Part[40, 4]:='#.%%..#)#.#';
            Part[40, 5]:='#####.###.#';
            Part[40, 6]:='...........';
            Part[40, 7]:='#.###.#####';
            Part[40, 8]:='#%#)#.#..%#';
            Part[40, 9]:='#%#)#.###.#';
            Part[40,10]:='#%%%#.%...#';
            Part[40,11]:='#####.#####';
        end;

        if lvl=3 then
        begin
            Part[40, 1]:='#####.#####';
            Part[40, 2]:='#####.#####';
            Part[40, 3]:='###....%###';
            Part[40, 4]:='#....#..%%#';
            Part[40, 5]:='#..%%#%..%#';
            Part[40, 6]:='.#########.';
            Part[40, 7]:='#....#)%..#';
            Part[40, 8]:='#%...#%...#';
            Part[40, 9]:='##.......##';
            Part[40,10]:='#####+#####';
            Part[40,11]:='#####.#####';
        end;

        Part[41, 1]:='#####+#####';
        Part[41, 2]:='####OOO####';
        Part[41, 3]:='####OOO####';
        Part[41, 4]:='####OOO####';
        Part[41, 5]:='#####+#####';
        Part[41, 6]:='...........';
        Part[41, 7]:='#####.#####';
        Part[41, 8]:='#####.#####';
        Part[41, 9]:='#####.#####';
        Part[41,10]:='#####.#####';
        Part[41,11]:='#####+#####';

        Part[42, 1]:='#####+#####';
        Part[42, 2]:='####...####';
        Part[42, 3]:='####...####';
        Part[42, 4]:='####...####';
        Part[42, 5]:='#####+#####';
        Part[42, 6]:='...........';
        Part[42, 7]:='#####.#####';
        Part[42, 8]:='###%....###';
        Part[42, 9]:='###....%###';
        Part[42,10]:='###...%%###';
        Part[42,11]:='#####.#####';

        Part[43, 1]:='#####+#####';
        Part[43, 2]:='#........)#';
        Part[43, 3]:='#.[][.....#';
        Part[43, 4]:='#......-..#';
        Part[43, 5]:='#.........#';
        Part[43, 6]:='.....[][...';
        Part[43, 7]:='#.-.......#';
        Part[43, 8]:='#.........#';
        Part[43, 9]:='#..[][....#';
        Part[43,10]:='#.......))#';
        Part[43,11]:='#####.#####';

        Part[44, 1]:='#####.#####';
        Part[44, 2]:='#OOOOOOOOO#';
        Part[44, 3]:='#O###O#+#O#';
        Part[44, 4]:='#O#.#O#.#O#';
        Part[44, 5]:='#O#.#O#.#O#';
        Part[44, 6]:='.O#+#O###OO';
        Part[44, 7]:='#OOOOOOOOO#';
        Part[44, 8]:='#O###O###O#';
        Part[44, 9]:='#O#.#O#.+O#';
        Part[44,10]:='#O+.#O#.#O#';
        Part[44,11]:='#####O#####';

        Part[45, 1]:='#####+#####';
        Part[45, 2]:='#.........#';
        Part[45, 3]:='#.#+#.###.#';
        Part[45, 4]:='#.#O#.#.#.#';
        Part[45, 5]:='#.#O#.#.#.#';
        Part[45, 6]:='..###.#.+..';
        Part[45, 7]:='#.....#.#.#';
        Part[45, 8]:='#.#####.#.#';
        Part[45, 9]:='#.#)....#.#';
        Part[45,10]:='#.#.....#.#';
        Part[45,11]:='#####+#####';

        Part[46, 1]:='#####*#####';
        Part[46, 2]:='#...#..#..#';
        Part[46, 3]:='#.#.##.#.##';
        Part[46, 4]:='#.........#';
        Part[46, 5]:='########*##';
        Part[46, 6]:='*.#.......*';
        Part[46, 7]:='#.#.#######';
        Part[46, 8]:='#...#.....#';
        Part[46, 9]:='###.###+###';
        Part[46,10]:='###.......#';
        Part[46,11]:='#####*#####';

        if lvl=2 then
        begin
            Part[47, 1]:='#####.#####';
            Part[47, 2]:='#.#...#...#';
            Part[47, 3]:='#.#...#...#';
            Part[47, 4]:='#.#+###...#';
            Part[47, 5]:='#.........#';
            Part[47, 6]:='.....####..';
            Part[47, 7]:='#....+..###';
            Part[47, 8]:='####.#....#';
            Part[47, 9]:='#..#.######';
            Part[47,10]:='#..+......#';
            Part[47,11]:='#####.#####';
        end;

        if lvl=3 then
        begin
            Part[47, 1]:='#####O#####';
            Part[47, 2]:='#O#OOO#OOO#';
            Part[47, 3]:='#O#OOO#OOO#';
            Part[47, 4]:='#O#+###OOO#';
            Part[47, 5]:='#O........#';
            Part[47, 6]:='..OOO####..';
            Part[47, 7]:='#OOOO+..###';
            Part[47, 8]:='####O#....#';
            Part[47, 9]:='#..#O######';
            Part[47,10]:='#..+OOOOOO#';
            Part[47,11]:='#####O#####';
        end;
    
        TotalParts:=47;
    end;


    if lvl=4 then
    begin
        // long straight paths
        Part[1, 1]:='^^^^^.^^^^^';
        Part[1, 2]:='^^^^^.^^^^^';
        Part[1, 3]:='^^^^^.^^^^^';
        Part[1, 4]:='^^^^^.^^^^^';
        Part[1, 5]:='^^^^^.^^^^^';
        Part[1, 6]:='........cc.';
        Part[1, 7]:='^^^^^.^^^^^';
        Part[1, 8]:='^^^^^.^^^^^';
        Part[1, 9]:='^^^^^.^^^^^';
        Part[1,10]:='^^^^^.^^^^^';
        Part[1,11]:='^^^^^.^^^^^';
    
        Part[2, 1]:='^^^^^^^^^^^';
        Part[2, 2]:='^^^^^^^^^^^';
        Part[2, 3]:='^^^^^^^^^^^';
        Part[2, 4]:='^^^^^^^^^^^';
        Part[2, 5]:='^^^^^^^^^^^';
        Part[2, 6]:='...........';
        Part[2, 7]:='^^^^^^^^^^^';
        Part[2, 8]:='^^^^^^^^^^^';
        Part[2, 9]:='^^^^^^^^^^^';
        Part[2,10]:='^^^^^^^^^^^';
        Part[2,11]:='^^^^^^^^^^^';
    
        Part[3, 1]:='^^^^^.^^^^^';
        Part[3, 2]:='^.....^^^^^';
        Part[3, 3]:='^.^^^^^^^^^';
        Part[3, 4]:='^.^^^^^^^^^';
        Part[3, 5]:='^.^^^^^^^^^';
        Part[3, 6]:='..ccc......';
        Part[3, 7]:='^^^^^c^^^^^';
        Part[3, 8]:='^^^^^c^^^^^';
        Part[3, 9]:='^^^^^.^^^^^';
        Part[3,10]:='^^^^^.^^^^^';
        Part[3,11]:='^^^^^.^^^^^';
    
        Part[4, 1]:='^^^^^.^^^^^';
        Part[4, 2]:='^^^^^.^^^^^';
        Part[4, 3]:='^^^^^.^^^^^';
        Part[4, 4]:='^^^^^.^^^^^';
        Part[4, 5]:='^^^^^.^^^^^';
        Part[4, 6]:='..^^^......';
        Part[4, 7]:='^.^^^.^^^^^';
        Part[4, 8]:='^.^^^.^^^^^';
        Part[4, 9]:='^.....^^^^^';
        Part[4,10]:='^^^^^.^^^^^';
        Part[4,11]:='^^^^^+^^^^^';
    
        Part[5, 1]:='^^^^^.^^^^^';
        Part[5, 2]:='^^^^^.^^^^^';
        Part[5, 3]:='^^^^^.^^^^^';
        Part[5, 4]:='^^^^^.^^^^^';
        Part[5, 5]:='^^^^^.^^^^^';
        Part[5, 6]:='..^^^......';
        Part[5, 7]:='^.^^^.^^^^^';
        Part[5, 8]:='^.^^^.^^^^^';
        Part[5, 9]:='^.....^^^^^';
        Part[5,10]:='^^^^^.^^^^^';
        Part[5,11]:='^^^^^.^^^^^';
    
        Part[6, 1]:='^^^^^.^^^^^';
        Part[6, 2]:='^^^^^.^^^^^';
        Part[6, 3]:='^^^^^.^^^^^';
        Part[6, 4]:='^^^^^.^^^^^';
        Part[6, 5]:='^^^^^.^^^^^';
        Part[6, 6]:='...c.......';
        Part[6, 7]:='^^c^^^^^^^^';
        Part[6, 8]:='^^.^^^^^^^^';
        Part[6, 9]:='^^.^^^^^^^^';
        Part[6,10]:='^^.^^^^^^^^';
        Part[6,11]:='^^....^^^^^';
    
        Part[7, 1]:='^^^^^.^^^^^';
        Part[7, 2]:='^^^^^...^^^';
        Part[7, 3]:='^^^^^^^.^^^';
        Part[7, 4]:='^^^^^^^.^^^';
        Part[7, 5]:='^^^^^^^.^^^';
        Part[7, 6]:='......^....';
        Part[7, 7]:='^^^^^.^^^^^';
        Part[7, 8]:='^^^^^.^^^^^';
        Part[7, 9]:='^^^^^.^^^^^';
        Part[7,10]:='^^^^^.^^^^^';
        Part[7,11]:='^^^^^.^^^^^';
    
        Part[8, 1]:='^^^^^.^^^^^';
        Part[8, 2]:='^.........^';
        Part[8, 3]:='^.^^^^^^^.^';
        Part[8, 4]:='^.^^^^^^^.^';
        Part[8, 5]:='^.^^^^^^^.^';
        Part[8, 6]:='+...cc.....';
        Part[8, 7]:='^^^^^+^^^^^';
        Part[8, 8]:='^.^.^.^.^.^';
        Part[8, 9]:='^^.^...^.^^';
        Part[8,10]:='^.^.^.^.^.^';
        Part[8,11]:='^^^^^+^^^^^';
    
        Part[9, 1]:='^^^^^.^^^^^';
        Part[9, 2]:='^^^^^.^^^^^';
        Part[9, 3]:='^^^^^.^^^^^';
        Part[9, 4]:='^^^^^.^^^^^';
        Part[9, 5]:='^^^^^.^^^^^';
        Part[9, 6]:='..^^^.^^^^.';
        Part[9, 7]:='^.^^^+^^^^.';
        Part[9, 8]:='^....cc....';
        Part[9, 9]:='^..ccc....^';
        Part[9,10]:='^^^^^+^^^^^';
        Part[9,11]:='^^^^^.^^^^^';
    
        Part[10, 1]:='^^^^^......';
        Part[10, 2]:='^^^^^.^^^^.';
        Part[10, 3]:='^^^^^.^^^^.';
        Part[10, 4]:='^^^^^.^^^^.';
        Part[10, 5]:='^^^^^.^^^^.';
        Part[10, 6]:='...........';
        Part[10, 7]:='^^^^^.^^^^.';
        Part[10, 8]:='^^^^^.^^^^.';
        Part[10, 9]:='^^^^^.^^^^.';
        Part[10,10]:='^^^^^.^^^^.';
        Part[10,11]:='^^^^^......';
    
        Part[11, 1]:='^^^^^.^^^^^';
        Part[11, 2]:='^^^^^.^^^^^';
        Part[11, 3]:='^^^^^.^^^^^';
        Part[11, 4]:='^^^^^.^^^^^';
        Part[11, 5]:='^^^^^.^^^^^';
        Part[11, 6]:='......^^^.c';
        Part[11, 7]:='^^^^^.^^^.^';
        Part[11, 8]:='^^^^^.^^^.^';
        Part[11, 9]:='^^^^^.^^^.^';
        Part[11,10]:='^^^^^.^^^.^';
        Part[11,11]:='^^^^^.....^';
    
        Part[12, 1]:='^^^^^.^^^^^';
        Part[12, 2]:='^...^.^...^';
        Part[12, 3]:='^...^.^....';
        Part[12, 4]:='^...^.^....';
        Part[12, 5]:='^^+^^.^^+^.';
        Part[12, 6]:='^^.^^.^^.^.';
        Part[12, 7]:='^^..+.^^.^^';
        Part[12, 8]:='^^^^^.^^.^^';
        Part[12, 9]:='^^^^^.^^.^^';
        Part[12,10]:='^^^^^.+..^^';
        Part[12,11]:='^^^^^.^^^^^';
    
        Part[13, 1]:='^^^^^+^^^^^';
        Part[13, 2]:='^^^^^.^^^^^';
        Part[13, 3]:='^^^^^.^^^^^';
        Part[13, 4]:='^^^^^.^^^^^';
        Part[13, 5]:='^^^^^.^^^^^';
        Part[13, 6]:='+....^....+';
        Part[13, 7]:='^^^^.^.^^^^';
        Part[13, 8]:='^^^^.^.^^^^';
        Part[13, 9]:='^^^^.^.^^^^';
        Part[13,10]:='^^^^.^.^^^^';
        Part[13,11]:='^^^^^.^^^^^';
    
        Part[14, 1]:='^^^^^.^^^^^';
        Part[14, 2]:='^^^^^.^^^^^';
        Part[14, 3]:='^^^^^.^^^^^';
        Part[14, 4]:='^.....^^^^^';
        Part[14, 5]:='^.....^^^^^';
        Part[14, 6]:='+.....+....';
        Part[14, 7]:='^.....^^^^^';
        Part[14, 8]:='^^^^^.^^^^^';
        Part[14, 9]:='^^^^^.^^^^^';
        Part[14,10]:='^^^^^.^^^^^';
        Part[14,11]:='^^^^^.^^^^^';
    
        Part[15, 1]:='^^^^^+^^^^^';
        Part[15, 2]:='^..^------^';
        Part[15, 3]:='^..^------^';
        Part[15, 4]:='^..^------^';
        Part[15, 5]:='^..^^^+^^^^';
        Part[15, 6]:='...........';
        Part[15, 7]:='^..^^^+^^^^';
        Part[15, 8]:='^..^------^';
        Part[15, 9]:='^..^------^';
        Part[15,10]:='^..^------^';
        Part[15,11]:='^^^^^+^^^^^';
    
        Part[16, 1]:='^^^^^.^^^^^';
        Part[16, 2]:='^^^^^.^^^^^';
        Part[16, 3]:='^^^^^.^^^^^';
        Part[16, 4]:='^^^^^.^^^^^';
        Part[16, 5]:='^^^^^.^^^^^';
        Part[16, 6]:='^^^^^.^^^^^';
        Part[16, 7]:='^^^^^.^^^^^';
        Part[16, 8]:='^^^^^.^^^^^';
        Part[16, 9]:='^^^^^.^^^^^';
        Part[16,10]:='^^^^^.^^^^^';
        Part[16,11]:='^^^^^.^^^^^';
    
        Part[17, 1]:='^^^^^.^^^^^';
        Part[17, 2]:='^^^^^.^^^^^';
        Part[17, 3]:='^^^^^.^^^^^';
        Part[17, 4]:='^^^^^.^^^^^';
        Part[17, 5]:='^^^^^.^^^^^';
        Part[17, 6]:='+.....+...+';
        Part[17, 7]:='^^^^^.^^^^^';
        Part[17, 8]:='^^^^^.^^^^^';
        Part[17, 9]:='^^^^^.^^^^^';
        Part[17,10]:='^^^^^.^^^^^';
        Part[17,11]:='^^^^^.^^^^^';

        Part[18, 1]:='^^^^^+^^^^^';
        Part[18, 2]:='^^^^^.^^^^^';
        Part[18, 3]:='^^^^^.^^^^^';
        Part[18, 4]:='^^^^^.^^^^^';
        Part[18, 5]:='^^^^^.^^^^^';
        Part[18, 6]:='...........';
        Part[18, 7]:='^^^^^.^^^^^';
        Part[18, 8]:='^^^^^.^^^^^';
        Part[18, 9]:='^^^^^.^^^^^';
        Part[18,10]:='^^^^^.^^^^^';
        Part[18,11]:='^^^^^.^^^^^';
    
        Part[19, 1]:='^^^^^�^^^^^';
        Part[19, 2]:='^......^^^^';
        Part[19, 3]:='^.^^^^.^^^^';
        Part[19, 4]:='^.^^^^.^^^^';
        Part[19, 5]:='^.^^^^.^^^^';
        Part[19, 6]:='..^^^^.^^^.';
        Part[19, 7]:='^^^^^^.^^^.';
        Part[19, 8]:='^^^^^^.^^^.';
        Part[19, 9]:='^^^^^^.^^^.';
        Part[19,10]:='^^^^^^.....';
        Part[19,11]:='^^^^^.^^^^^';
    
        Part[20, 1]:='^^^^^.^^^^^';
        Part[20, 2]:='^^^^^.^^^^^';
        Part[20, 3]:='^...�.^^^^^';
        Part[20, 4]:='^...^.^^^^^';
        Part[20, 5]:='^...^.^^^^^';
        Part[20, 6]:='�...^.+....';
        Part[20, 7]:='^^^^^.^^^^^';
        Part[20, 8]:='^^^^^.^^^^^';
        Part[20, 9]:='^^^^^.^^^^^';
        Part[20,10]:='^^^^^.^^^^^';
        Part[20,11]:='^^^^^.^^^^^';
    
        Part[21, 1]:='^^^^^.^^^^^';
        Part[21, 2]:='^^^^^.^^^^^';
        Part[21, 3]:='^^^^^.^^^^^';
        Part[21, 4]:='^^^^^.^^^^^';
        Part[21, 5]:='^^^^^.^^^^^';
        Part[21, 6]:='...........';
        Part[21, 7]:='^^^^^.^^^^^';
        Part[21, 8]:='^^^^^.^^^^^';
        Part[21, 9]:='^^^^^.^^^^^';
        Part[21,10]:='^^^^^.^^^^^';
        Part[21,11]:='^^^^^.^^^^^';

        Part[22, 1]:='^^^^^+^^^^^';
        Part[22, 2]:='^.........^';
        Part[22, 3]:='^.#.#.#.#.^';
        Part[22, 4]:='^.........^';
        Part[22, 5]:='^.#.....#.^';
        Part[22, 6]:='+.........+';
        Part[22, 7]:='^.#.....#.^';
        Part[22, 8]:='^.........^';
        Part[22, 9]:='^.#.#.#.#.^';
        Part[22,10]:='^.........^';
        Part[22,11]:='^^^^^+^^^^^';

        Part[23, 1]:='^^^^^.^^^^^';
        Part[23, 2]:='^^^^^.^^^^^';
        Part[23, 3]:='^^^^^.^^^^^';
        Part[23, 4]:='^^^^^�^^^^^';
        Part[23, 5]:='^^^^^.^^^^^';
        Part[23, 6]:='..+...^^^^^';
        Part[23, 7]:='^^^^^.^^^^^';
        Part[23, 8]:='^^^^^.^^^^^';
        Part[23, 9]:='^^^^^.^^^^^';
        Part[23,10]:='^^^^^.^^^^^';
        Part[23,11]:='^^^^^.^^^^^';

        Part[24, 1]:='^^^^^.^^^^^';
        Part[24, 2]:='^^^^^.^^^^^';
        Part[24, 3]:='^^^^^.^^^^^';
        Part[24, 4]:='^^^^^+^^^^^';
        Part[24, 5]:='^^^^^.^^^^^';
        Part[24, 6]:='...+...+...';
        Part[24, 7]:='^^^^^.^^^^^';
        Part[24, 8]:='^^^^^+^^^^^';
        Part[24, 9]:='^^^^^.^^^^^';
        Part[24,10]:='^^^^^.^^^^^';
        Part[24,11]:='^^^^^.^^^^^';

        Part[25, 1]:='^^^^^.^^^^^';
        Part[25, 2]:='^^^^^.^^^^^';
        Part[25, 3]:='^^^^^.^^^^^';
        Part[25, 4]:='^^^^^+^^^^^';
        Part[25, 5]:='^^^^^.^^^^^';
        Part[25, 6]:='...........';
        Part[25, 7]:='^^^^^.^^^^^';
        Part[25, 8]:='^^^^^+^^^^^';
        Part[25, 9]:='^^^^^.^^^^^';
        Part[25,10]:='^^^^^.^^^^^';
        Part[25,11]:='^^^^^.^^^^^';

        Part[26, 1]:='^^^^^.^^^^^';
        Part[26, 2]:='^^^^^.^^^^^';
        Part[26, 3]:='^^^^^.^^^^^';
        Part[26, 4]:='^^^^^.^^^^^';
        Part[26, 5]:='^^^^^.^^^^^';
        Part[26, 6]:='...+...+...';
        Part[26, 7]:='^^^^^.^^^^^';
        Part[26, 8]:='^^^^^.^^^^^';
        Part[26, 9]:='^^^^^.^^^^^';
        Part[26,10]:='^^^^^.^^^^^';
        Part[26,11]:='^^^^^.^^^^^';

        Part[27, 1]:='^^^^^.^^^^^';
        Part[27, 2]:='^^^^^.^^^^^';
        Part[27, 3]:='^^^^^.^^^^^';
        Part[27, 4]:='^^^^^.^^^^^';
        Part[27, 5]:='^^^^^�^^^^^';
        Part[27, 6]:='...........';
        Part[27, 7]:='^^^^^^^^^^^';
        Part[27, 8]:='^^^^^^^^^^^';
        Part[27, 9]:='^^^^^.^^^^^';
        Part[27,10]:='^^^^^.^^^^^';
        Part[27,11]:='^^^^^.^^^^^';

        Part[28, 1]:='^^^^^.^^^^^';
        Part[28, 2]:='^^^^^.^^^^^';
        Part[28, 3]:='^^^^^.^^^^^';
        Part[28, 4]:='^^^^^.^^^^^';
        Part[28, 5]:='^^^^^^^^^^^';
        Part[28, 6]:='......^^^^^';
        Part[28, 7]:='^^^^^.^^^^^';
        Part[28, 8]:='^^^^^�^^^^^';
        Part[28, 9]:='^^^^^.^^^^^';
        Part[28,10]:='^^^^^.^^^^^';
        Part[28,11]:='^^^^^.^^^^^';

        Part[29, 1]:='^^^^^.^^^^^';
        Part[29, 2]:='^^^^^.^^^^^';
        Part[29, 3]:='^^^^^.^^^^^';
        Part[29, 4]:='^^^^^.^^^^^';
        Part[29, 5]:='^^^^^.^^^^^';
        Part[29, 6]:='^^^^^......';
        Part[29, 7]:='^^^^^+^^^^^';
        Part[29, 8]:='^^^^^.^^^^^';
        Part[29, 9]:='^^^^^.^^^^^';
        Part[29,10]:='^^^^^.^^^^^';
        Part[29,11]:='^^^^^.^^^^^';
    
        TotalParts:=29;    
    end;


    if (lvl=6) or (lvl=7) then
    begin
        // straight paths and rectangular chambers
        Part[1, 1]:='^^^^^.^^^^^';
        Part[1, 2]:='^^^^^.^^^^^';
        Part[1, 3]:='^^^^^.^^^^^';
        Part[1, 4]:='^^^^^.^^^^^';
        Part[1, 5]:='^^^^^.^^^^^';
        Part[1, 6]:='...........';
        Part[1, 7]:='^^^^^.^^^^^';
        Part[1, 8]:='^^^^^.^^^^^';
        Part[1, 9]:='^^^^^.^^^^^';
        Part[1,10]:='^^^^^.^^^^^';
        Part[1,11]:='^^^^^.^^^^^';

        Part[2, 1]:='^^^^^^^^^^^';
        Part[2, 2]:='^^^^^^^^^^^';
        Part[2, 3]:='^^^^^^^^^^^';
        Part[2, 4]:='^^^^^^^^^^^';
        Part[2, 5]:='^^^^^^^^^^^';
        Part[2, 6]:='...........';
        Part[2, 7]:='^^^^^^^^^^^';
        Part[2, 8]:='^^^^^^^^^^^';
        Part[2, 9]:='^^^^^^^^^^^';
        Part[2,10]:='^^^^^^^^^^^';
        Part[2,11]:='^^^^^^^^^^^';

        Part[3, 1]:='^^^^^.^^^^^';
        Part[3, 2]:='^^^^^.^^^^^';
        Part[3, 3]:='^^^^^.^^^^^';
        Part[3, 4]:='^^.......^^';
        Part[3, 5]:='^^.......^^';
        Part[3, 6]:='...........';
        Part[3, 7]:='^^.......^^';
        Part[3, 8]:='^^.......^^';
        Part[3, 9]:='^^^^^.^^^^^';
        Part[3,10]:='^^^^^.^^^^^';
        Part[3,11]:='^^^^^.^^^^^';

        Part[4, 1]:='^^^^^.^^^^^';
        Part[4, 2]:='^^^^^+^^^^^';
        Part[4, 3]:='^.........^';
        Part[4, 4]:='^.........^';
        Part[4, 5]:='^.........^';
        Part[4, 6]:='...........';
        Part[4, 7]:='^.........^';
        Part[4, 8]:='^.........^';
        Part[4, 9]:='^.........^';
        Part[4,10]:='^^^^^.^^^^^';
        Part[4,11]:='^^^^^.^^^^^';

        Part[5, 1]:='^^^^^.^^^^^';
        Part[5, 2]:='^^^^^.^^^^^';
        Part[5, 3]:='^.........^';
        Part[5, 4]:='^.........^';
        Part[5, 5]:='^.........^';
        Part[5, 6]:='+.........+';
        Part[5, 7]:='^.........^';
        Part[5, 8]:='^.........^';
        Part[5, 9]:='^.........^';
        Part[5,10]:='^^^^^.^^^^^';
        Part[5,11]:='^^^^^.^^^^^';

        Part[6, 1]:='^^^^^.^^^^^';
        Part[6, 2]:='^^^^^.^^^^^';
        Part[6, 3]:='^...^.^...^';
        Part[6, 4]:='^...^.^...^';
        Part[6, 5]:='^^+^^.^^+^^';
        Part[6, 6]:='...........';
        Part[6, 7]:='^.........^';
        Part[6, 8]:='^.........^';
        Part[6, 9]:='^.........^';
        Part[6,10]:='^^^^^.^^^^^';
        Part[6,11]:='^^^^^.^^^^^';

        Part[7, 1]:='^^^^^.^^^^^';
        Part[7, 2]:='^---^.^---^';
        Part[7, 3]:='^---^.^---^';
        Part[7, 4]:='^---^.^---^';
        Part[7, 5]:='^^+^^.^^+^^';
        Part[7, 6]:='...........';
        Part[7, 7]:='^^+^^.^^+^^';
        Part[7, 8]:='^---^.^---^';
        Part[7, 9]:='^---^.^---^';
        Part[7,10]:='^---^.^---^';
        Part[7,11]:='^^^^^.^^^^^';


        Part[8, 1]:='^^^^^.^^^^^';
        Part[8, 2]:='^.........^';
        Part[8, 3]:='^.^^^^^^^.^';
        Part[8, 4]:='^.^^^^^^^.^';
        Part[8, 5]:='^.^^^^^^^.^';
        Part[8, 6]:='+..........';
        Part[8, 7]:='^^^^^+^^^^^';
        Part[8, 8]:='^.^.^.^.^.^';
        Part[8, 9]:='^^.^...^.^^';
        Part[8,10]:='^.^.^.^.^.^';
        Part[8,11]:='^^^^^+^^^^^';

        Part[9, 1]:='^^^^^.^^^^^';
        Part[9, 2]:='^^^^^.^^^^^';
        Part[9, 3]:='^^^^^.^^^^^';
        Part[9, 4]:='^^^^^.^^^^^';
        Part[9, 5]:='^^^^^.^^^^^';
        Part[9, 6]:='......^^^^^';
        Part[9, 7]:='^^^^^+^^^^^';
        Part[9, 8]:='^.........^';
        Part[9, 9]:='^.........^';
        Part[9,10]:='^^^^^+^^^^^';
        Part[9,11]:='^^^^^.^^^^^';

        Part[10, 1]:='^^^^^......';
        Part[10, 2]:='^^^^^.^^^^.';
        Part[10, 3]:='^^^^^.^^^^.';
        Part[10, 4]:='^^^^^.^^^^.';
        Part[10, 5]:='^^^^^.^^^^.';
        Part[10, 6]:='...........';
        Part[10, 7]:='^^^^^.^^^^.';
        Part[10, 8]:='^^^^^.^^^^.';
        Part[10, 9]:='^^^^^.^^^^.';
        Part[10,10]:='^^^^^.^^^^.';
        Part[10,11]:='^^^^^......';

        Part[11, 1]:='^^^^^.^^^^^';
        Part[11, 2]:='^^^^^.^^^^^';
        Part[11, 3]:='^^^^^+^^^^^';
        Part[11, 4]:='^^^^^.^^^^^';
        Part[11, 5]:='^^^^^.^^^^^';
        Part[11, 6]:='..........+';
        Part[11, 7]:='^^^^^.^^^^^';
        Part[11, 8]:='^^^^^.^^^^^';
        Part[11, 9]:='^^^^^.^^^^^';
        Part[11,10]:='^^^^^.^^^^^';
        Part[11,11]:='^^^^^.^^^^^';

        Part[12, 1]:='^^^^^.^^^^^';
        Part[12, 2]:='^^^^^.^^^^^';
        Part[12, 3]:='^^^^^�^^^^^';
        Part[12, 4]:='^^^^^.^^^^^';
        Part[12, 5]:='^^^^^.^^^^^';
        Part[12, 6]:='..�........';
        Part[12, 7]:='^^^^^.^^^^^';
        Part[12, 8]:='^^^^^.^^^^^';
        Part[12, 9]:='^^^^^.^^^^^';
        Part[12,10]:='^^^^^.^^^^^';
        Part[12,11]:='^^^^^.^^^^^';

        Part[13, 1]:='^^^^^+^^^^^';
        Part[13, 2]:='^^^^^.^^^^^';
        Part[13, 3]:='^^^^^.^^^^^';
        Part[13, 4]:='^^^^^.^^^^^';
        Part[13, 5]:='^^^^^.^^^^^';
        Part[13, 6]:='+....^....+';
        Part[13, 7]:='^^^^.^.^^^^';
        Part[13, 8]:='^^^^.^.^^^^';
        Part[13, 9]:='^^^^.^.^^^^';
        Part[13,10]:='^^^^.^.^^^^';
        Part[13,11]:='^^^^^.^^^^^';

        Part[14, 1]:='^^^^^.^^^^^';
        Part[14, 2]:='^^^^^.^^^^^';
        Part[14, 3]:='^^^^^.^^^^^';
        Part[14, 4]:='^.....^^^^^';
        Part[14, 5]:='^.....^^^^^';
        Part[14, 6]:='+.....+....';
        Part[14, 7]:='^.....^^^^^';
        Part[14, 8]:='^^^^^.^^^^^';
        Part[14, 9]:='^^^^^.^^^^^';
        Part[14,10]:='^^^^^.^^^^^';
        Part[14,11]:='^^^^^.^^^^^';

        Part[15, 1]:='^^^^^.^^^^^';
        Part[15, 2]:='^^^^^.^^^^^';
        Part[15, 3]:='^^^^^.^^^^^';
        Part[15, 4]:='^^^^^.^^^^^';
        Part[15, 5]:='^^^^^.^^^^^';
        Part[15, 6]:='...........';
        Part[15, 7]:='^^^^^^^^^^^';
        Part[15, 8]:='^^^^^^^^^^^';
        Part[15, 9]:='^^^^^^^^^^^';
        Part[15,10]:='^^^^^^^^^^^';
        Part[15,11]:='^^^^^^^^^^^';

        Part[16, 1]:='^^^^^^^^^^^';
        Part[16, 2]:='^^^^^^^^^^^';
        Part[16, 3]:='^^^^^^^^^^^';
        Part[16, 4]:='^^^^^^^^^^^';
        Part[16, 5]:='^^^^^^^^^^^';
        Part[16, 6]:='...........';
        Part[16, 7]:='^^^^^.^^^^^';
        Part[16, 8]:='^^^^^.^^^^^';
        Part[16, 9]:='^^^^^.^^^^^';
        Part[16,10]:='^^^^^.^^^^^';
        Part[16,11]:='^^^^^.^^^^^';

        Part[17, 1]:='^^^^^.^^^^^';
        Part[17, 2]:='^^^^^.^^^^^';
        Part[17, 3]:='^^^^^.^^^^^';
        Part[17, 4]:='^^^^^.^^^^^';
        Part[17, 5]:='^^^^^.^^^^^';
        Part[17, 6]:='+.....+...+';
        Part[17, 7]:='^^^^^.^^^^^';
        Part[17, 8]:='^^^^^.^^^^^';
        Part[17, 9]:='^^^^^.^^^^^';
        Part[17,10]:='^^^^^.^^^^^';
        Part[17,11]:='^^^^^.^^^^^';

        Part[18, 1]:='^^^^^�^^^^^';
        Part[18, 2]:='^^^^^.^^^^^';
        Part[18, 3]:='^^^^^.^^^^^';
        Part[18, 4]:='^^^^^.^^^^^';
        Part[18, 5]:='^^^^^.^^^^^';
        Part[18, 6]:='...........';
        Part[18, 7]:='^^^^^.^^^^^';
        Part[18, 8]:='^^^^^.^^^^^';
        Part[18, 9]:='^^^^^.^^^^^';
        Part[18,10]:='^^^^^.^^^^^';
        Part[18,11]:='^^^^^.^^^^^';

        Part[19, 1]:='^^^^^^^^^^^';
        Part[19, 2]:='^......^^^^';
        Part[19, 3]:='^.^^^^.^^^^';
        Part[19, 4]:='^.^^^^.^^^^';
        Part[19, 5]:='^.^^^^.^^^^';
        Part[19, 6]:='..^^^^.^^^.';
        Part[19, 7]:='^^^^^^.^^^.';
        Part[19, 8]:='^^^^^^.^^^.';
        Part[19, 9]:='^^^^^^.^^^.';
        Part[19,10]:='^^^^^^.....';
        Part[19,11]:='^^^^^^^^^^^';

        Part[20, 1]:='^^^^^.^^^^^';
        Part[20, 2]:='^^^^^.^^^^^';
        Part[20, 3]:='^---^.^^^^^';
        Part[20, 4]:='^---+.^^^^^';
        Part[20, 5]:='^---^.^^^^^';
        Part[20, 6]:='^---^......';
        Part[20, 7]:='^^^^^.^^+^^';
        Part[20, 8]:='^^^^^.^---^';
        Part[20, 9]:='^^^^^.^---^';
        Part[20,10]:='^^^^^.^---^';
        Part[20,11]:='^^^^^.^^^^^';

        Part[21, 1]:='^^^^^.^^^^^';
        Part[21, 2]:='^^^^^.^^^^^';
        Part[21, 3]:='^^^^^.^^^^^';
        Part[21, 4]:='^^^^^.^^^^^';
        Part[21, 5]:='^^^^^.^^^^^';
        Part[21, 6]:='^^^^^......';
        Part[21, 7]:='^^^^^.^^^^^';
        Part[21, 8]:='^^^^^.^^^^^';
        Part[21, 9]:='^^^^^.^^^^^';
        Part[21,10]:='^^^^^.^^^^^';
        Part[21,11]:='^^^^^.^^^^^';

        Part[22, 1]:='^^^^^+^^^^^';
        Part[22, 2]:='^.........^';
        Part[22, 3]:='^.#.#.#.#.^';
        Part[22, 4]:='^.........^';
        Part[22, 5]:='^.#.....#.^';
        Part[22, 6]:='+.........+';
        Part[22, 7]:='^.#.....#.^';
        Part[22, 8]:='^.........^';
        Part[22, 9]:='^.#.#.#.#.^';
        Part[22,10]:='^.........^';
        Part[22,11]:='^^^^^+^^^^^';

        Part[23, 1]:='^^^^^.^^^^^';
        Part[23, 2]:='^^^^^.^^^^^';
        Part[23, 3]:='^^^^^.^^^^^';
        Part[23, 4]:='^^^^^�^^^^^';
        Part[23, 5]:='^^^^^.^^^^^';
        Part[23, 6]:='^^^^^.^^^^^';
        Part[23, 7]:='^^^^^.^^^^^';
        Part[23, 8]:='^^^^^.^^^^^';
        Part[23, 9]:='^^^^^.^^^^^';
        Part[23,10]:='^^^^^.^^^^^';
        Part[23,11]:='^^^^^.^^^^^';

        Part[24, 1]:='^^^^^.^^^^^';
        Part[24, 2]:='^^^^^.^^^^^';
        Part[24, 3]:='^^^^^.^^^^^';
        Part[24, 4]:='^^^^^+^^^^^';
        Part[24, 5]:='^^^^^.^^^^^';
        Part[24, 6]:='...+...+...';
        Part[24, 7]:='^^^^^.^^^^^';
        Part[24, 8]:='^^^^^+^^^^^';
        Part[24, 9]:='^^^^^.^^^^^';
        Part[24,10]:='^^^^^.^^^^^';
        Part[24,11]:='^^^^^.^^^^^';

        Part[25, 1]:='^^^^^.^^^^^';
        Part[25, 2]:='^^^^^.^^^^^';
        Part[25, 3]:='^^^^^.^^^^^';
        Part[25, 4]:='^^^^^+^^^^^';
        Part[25, 5]:='^^^^^.^^^^^';
        Part[25, 6]:='...........';
        Part[25, 7]:='^^^^^.^^^^^';
        Part[25, 8]:='^^^^^+^^^^^';
        Part[25, 9]:='^^^^^.^^^^^';
        Part[25,10]:='^^^^^.^^^^^';
        Part[25,11]:='^^^^^.^^^^^';

        Part[26, 1]:='^^^^^.^^^^^';
        Part[26, 2]:='^^^^^.^^^^^';
        Part[26, 3]:='^^^^^.^^^^^';
        Part[26, 4]:='^^^^^.^^^^^';
        Part[26, 5]:='^^^^^.^^^^^';
        Part[26, 6]:='...+...+...';
        Part[26, 7]:='^^^^^.^^^^^';
        Part[26, 8]:='^^^^^.^^^^^';
        Part[26, 9]:='^^^^^.^^^^^';
        Part[26,10]:='^^^^^.^^^^^';
        Part[26,11]:='^^^^^.^^^^^';

        Part[27, 1]:='^^^^^.^^^^^';
        Part[27, 2]:='^^^^^.^^^^^';
        Part[27, 3]:='^^^^^.^^^^^';
        Part[27, 4]:='^^^^^.^^^^^';
        Part[27, 5]:='^^^^^�^^^^^';
        Part[27, 6]:='...........';
        Part[27, 7]:='^^^^^^^^^^^';
        Part[27, 8]:='^^^^^^^^^^^';
        Part[27, 9]:='^^^^^.^^^^^';
        Part[27,10]:='^^^^^.^^^^^';
        Part[27,11]:='^^^^^.^^^^^';

        Part[28, 1]:='^^^^^.^^^^^';
        Part[28, 2]:='^^^^^.^^^^^';
        Part[28, 3]:='^^^^^.^^^^^';
        Part[28, 4]:='^^^^^.^^^^^';
        Part[28, 5]:='^^^^^^^^^^^';
        Part[28, 6]:='......^^^^^';
        Part[28, 7]:='^^^^^.^^^^^';
        Part[28, 8]:='^^^^^�^^^^^';
        Part[28, 9]:='^^^^^.^^^^^';
        Part[28,10]:='^^^^^.^^^^^';
        Part[28,11]:='^^^^^.^^^^^';

        Part[29, 1]:='^^^^^.^^^^^';
        Part[29, 2]:='^^^^^.^^^^^';
        Part[29, 3]:='^^^^^.^^^^^';
        Part[29, 4]:='^^^^^.^^^^^';
        Part[29, 5]:='^^^^^.^^^^^';
        Part[29, 6]:='^^^^^......';
        Part[29, 7]:='^^^^^^^^^^^';
        Part[29, 8]:='^^^^^.^^^^^';
        Part[29, 9]:='^^^^^.^^^^^';
        Part[29,10]:='^^^^^.^^^^^';
        Part[29,11]:='^^^^^.^^^^^';

        Part[30, 1]:='^^^^^.^^^^^';
        Part[30, 2]:='^^^^^.^^^^^';
        Part[30, 3]:='^^^^^.^^^^^';
        Part[30, 4]:='^^^^^.^^^^^';
        Part[30, 5]:='^^^^^.^^^^^';
        Part[30, 6]:='^^^^^.%....';
        Part[30, 7]:='^^^^^.^^^^^';
        Part[30, 8]:='^^^^^.^^^^^';
        Part[30, 9]:='^^^^^.^^^^^';
        Part[30,10]:='^^^^^.^^^^^';
        Part[30,11]:='^^^^^.^^^^^';

        Part[31, 1]:='^^^^^+^^^^^';
        Part[31, 2]:='^.........^';
        Part[31, 3]:='^.#%#.#.#.^';
        Part[31, 4]:='^.%.......^';
        Part[31, 5]:='^.#%....#.^';
        Part[31, 6]:='+.........+';
        Part[31, 7]:='^.#.....#.^';
        Part[31, 8]:='^.......%.^';
        Part[31, 9]:='^.#.#%#%#.^';
        Part[31,10]:='^.....%...^';
        Part[31,11]:='^^^^^+^^^^^';

        TotalParts:=31;
    end;

    if lvl=5 then
    begin
        // cave town with contaminated ground and gas cylinders
        // alternate
        Part[1, 1]:='#####z#####';
        Part[1, 2]:='#####z#####';
        Part[1, 3]:='#####z#####';
        Part[1, 4]:='#####z#####';
        Part[1, 5]:='#####z#####';
        Part[1, 6]:='zzzzzzzzzzz';
        Part[1, 7]:='#####z#####';
        Part[1, 8]:='#####z#####';
        Part[1, 9]:='#####z#####';
        Part[1,10]:='#####z#####';
        Part[1,11]:='#####z#####';
    
        Part[2, 1]:='###########';
        Part[2, 2]:='##-----####';
        Part[2, 3]:='##-----####';
        Part[2, 4]:='##-----####';
        Part[2, 5]:='####+######';
        Part[2, 6]:='zzzzzzzzzzz';
        Part[2, 7]:='###########';
        Part[2, 8]:='###########';
        Part[2, 9]:='###########';
        Part[2,10]:='###########';
        Part[2,11]:='###########';
    
        Part[3, 1]:='#####z#####';
        Part[3, 2]:='#zzzzz#####';
        Part[3, 3]:='#z#########';
        Part[3, 4]:='#z#########';
        Part[3, 5]:='#z#########';
        Part[3, 6]:='#zzzzzzzzzz';
        Part[3, 7]:='#####z#####';
        Part[3, 8]:='#####z#####';
        Part[3, 9]:='#####z#####';
        Part[3,10]:='#####z#####';
        Part[3,11]:='#####z#####';
    
        Part[4, 1]:='#####z#####';
        Part[4, 2]:='#---#z#####';
        Part[4, 3]:='#---+z#####';
        Part[4, 4]:='#---#z#####';
        Part[4, 5]:='#####z#####';
        Part[4, 6]:='zz###zzzzzz';
        Part[4, 7]:='#z###z#####';
        Part[4, 8]:='#z###z#####';
        Part[4, 9]:='#zzzzz#####';
        Part[4,10]:='#####z#####';
        Part[4,11]:='#####+#####';
    
        Part[5, 1]:='#####z#####';
        Part[5, 2]:='#####z#####';
        Part[5, 3]:='#####z#####';
        Part[5, 4]:='#####z#####';
        Part[5, 5]:='#####z#####';
        Part[5, 6]:='zzzzzzzzzzz';
        Part[5, 7]:='#zzzzz#####';
        Part[5, 8]:='#zzzzz#####';
        Part[5, 9]:='#zzzzz#####';
        Part[5,10]:='#zzzzz#####';
        Part[5,11]:='#####z#####';
    
        Part[6, 1]:='#####z#####';
        Part[6, 2]:='#####z#####';
        Part[6, 3]:='#####z#####';
        Part[6, 4]:='#####z#####';
        Part[6, 5]:='#####z#####';
        Part[6, 6]:='zzzzzzzzzzz';
        Part[6, 7]:='##z########';
        Part[6, 8]:='##z########';
        Part[6, 9]:='##z########';
        Part[6,10]:='##z########';
        Part[6,11]:='##zzzz#####';
    
        Part[7, 1]:='#####z#####';
        Part[7, 2]:='#####zzz###';
        Part[7, 3]:='#######z###';
        Part[7, 4]:='#######z###';
        Part[7, 5]:='#######z###';
        Part[7, 6]:='zzzzzz+zzzz';
        Part[7, 7]:='#####z#####';
        Part[7, 8]:='#####z#####';
        Part[7, 9]:='#####z#####';
        Part[7,10]:='#####z#####';
        Part[7,11]:='#####z#####';
    
        Part[8, 1]:='####z######';
        Part[8, 2]:='#z##z######';
        Part[8, 3]:='#z##zzzz###';
        Part[8, 4]:='#z#####z###';
        Part[8, 5]:='#z#####z###';
        Part[8, 6]:='+zzzzzzzzzz';
        Part[8, 7]:='#z#########';
        Part[8, 8]:='#z#-------#';
        Part[8, 9]:='#z+-------#';
        Part[8,10]:='###-------#';
        Part[8,11]:='#####z#####';
    
        Part[9, 1]:='#####z#####';
        Part[9, 2]:='#zzzzzzzzz#';
        Part[9, 3]:='#z#######z#';
        Part[9, 4]:='#z#######z#';
        Part[9, 5]:='#z#######z#';
        Part[9, 6]:='+zzzzzzzzzz';
        Part[9, 7]:='#####+#####';
        Part[9, 8]:='#---#-#---#';
        Part[9, 9]:='#---+-+---#';
        Part[9,10]:='#---#-#---#';
        Part[9,11]:='#####+#####';
    
        Part[10, 1]:='#####z#####';
        Part[10, 2]:='#zzzzzzzzz#';
        Part[10, 3]:='#zzzzzzzzz#';
        Part[10, 4]:='#zzzzzzzzz#';
        Part[10, 5]:='#zzz###zzz#';
        Part[10, 6]:='zzzz{{#zzzz';
        Part[10, 7]:='#zzzzzzzzz#';
        Part[10, 8]:='#zzzzzzzzz#';
        Part[10, 9]:='#zzzzzzzzz#';
        Part[10,10]:='#zzzzzzzzz#';
        Part[10,11]:='#####z#####';

        Part[11, 1]:='#####+#####';
        Part[11, 2]:='#zzzzzzzzz#';
        Part[11, 3]:='#zzzzzzzzz#';
        Part[11, 4]:='#zz-----zz#';
        Part[11, 5]:='#zz-----zz#';
        Part[11, 6]:='+zz-----zz+';
        Part[11, 7]:='#zz-----zz#';
        Part[11, 8]:='#zz-----zz#';
        Part[11, 9]:='#zzzzzzzzz#';
        Part[11,10]:='#zzzzzzzzz#';
        Part[11,11]:='#####+#####';

        Part[12, 1]:='#####z#####';
        Part[12, 2]:='#####z#####';
        Part[12, 3]:='#####z#####';
        Part[12, 4]:='#####+#####';
        Part[12, 5]:='#####z#####';
        Part[12, 6]:='zzzzzzzzzzz';
        Part[12, 7]:='#####z#####';
        Part[12, 8]:='#####+#####';
        Part[12, 9]:='#####z#####';
        Part[12,10]:='#####z#####';
        Part[12,11]:='#####z#####';

        Part[13, 1]:='#####z#####';
        Part[13, 2]:='#####z#####';
        Part[13, 3]:='#####z#####';
        Part[13, 4]:='#####z#####';
        Part[13, 5]:='#####z#####';
        Part[13, 6]:='zzz+zzz+zzz';
        Part[13, 7]:='#####z#####';
        Part[13, 8]:='#####z#####';
        Part[13, 9]:='#####z#####';
        Part[13,10]:='#####z#####';
        Part[13,11]:='#####z#####';

        Part[14, 1]:='#####+#####';
        Part[14, 2]:='#zzzzzzzzz#';
        Part[14, 3]:='#zzzzzzzzz#';
        Part[14, 4]:='#zz--#--zz#';
        Part[14, 5]:='#zz-----zz#';
        Part[14, 6]:='+zz--#--zz+';
        Part[14, 7]:='#zz-----zz#';
        Part[14, 8]:='#zz--#--zz#';
        Part[14, 9]:='#zzzzzzzzz#';
        Part[14,10]:='#zzzzzzzzz#';
        Part[14,11]:='#####+#####';

        Part[15, 1]:='#####z#####';
        Part[15, 2]:='#zzzzzzzzz#';
        Part[15, 3]:='#zzzzzzzzz#';
        Part[15, 4]:='#zzzzzzzzz#';
        Part[15, 5]:='#zz###+##z#';
        Part[15, 6]:='zzz#----#zz';
        Part[15, 7]:='#zz#V---#z#';
        Part[15, 8]:='#zz#vV--#z#';
        Part[15, 9]:='#zz######z#';
        Part[15,10]:='#zzzzzzzzz#';
        Part[15,11]:='#####+#####';

        Part[16, 1]:='#####+#####';
        Part[16, 2]:='#z#-----#z#';
        Part[16, 3]:='#z#-----#z#';
        Part[16, 4]:='#z#-----#z#';
        Part[16, 5]:='#z###+###z#';
        Part[16, 6]:='+zzzzzzzzzz';
        Part[16, 7]:='#####+#####';
        Part[16, 8]:='#-----+---#';
        Part[16, 9]:='##+##-#####';
        Part[16,10]:='#---#-----#';
        Part[16,11]:='#####+#####';

        Part[17, 1]:='#####z#####';
        Part[17, 2]:='#zzzzzzzzz#';
        Part[17, 3]:='#zz##z##zz#';
        Part[17, 4]:='#zz##z##zz#';
        Part[17, 5]:='#zzzzzzzzz#';
        Part[17, 6]:='+zzzzzzzzzz';
        Part[17, 7]:='#zzzzz##+##';
        Part[17, 8]:='#######---#';
        Part[17, 9]:='#---+-+---#';
        Part[17,10]:='#---#+#---#';
        Part[17,11]:='#####z#####';

        Part[18, 1]:='#zzzzzzzzz#';
        Part[18, 2]:='zzz##+##zzz';
        Part[18, 3]:='zz##v--##zz';
        Part[18, 4]:='z##-----##z';
        Part[18, 5]:='##--#-#--##';
        Part[18, 6]:='+--#-|-#--+';
        Part[18, 7]:='##--#-#--##';
        Part[18, 8]:='z##----v##z';
        Part[18, 9]:='zz##--v##zz';
        Part[18,10]:='zzz##+##zzz';
        Part[18,11]:='#zzzzzzzzz#';

        Part[19, 1]:='zzzz##zzzzz';
        Part[19, 2]:='zzz##{zzzzz';
        Part[19, 3]:='zz##zzzzzzz';
        Part[19, 4]:='z##zzzzzzzz';
        Part[19, 5]:='##zzzzzzzzz';
        Part[19, 6]:='zzzz#######';
        Part[19, 7]:='##zzzzzzzzz';
        Part[19, 8]:='z##zzzzzzzz';
        Part[19, 9]:='zz##zzzzzzz';
        Part[19,10]:='zzz##zzzzzz';
        Part[19,11]:='zzzz##zzzzz';

        Part[20, 1]:='zzzz##zzzzz';
        Part[20, 2]:='zzzzz##zzzz';
        Part[20, 3]:='z##zzz##zzz';
        Part[20, 4]:='zz##zzz##zz';
        Part[20, 5]:='zzz##zzz##z';
        Part[20, 6]:='zzzz##zzzzz';
        Part[20, 7]:='##zz{##zzzz';
        Part[20, 8]:='z##zzz##zzz';
        Part[20, 9]:='zz##zzz##zz';
        Part[20,10]:='zzz##zzz##z';
        Part[20,11]:='zzzz##zzzzz';

        Part[21, 1]:='zzzz##zzzzz';
        Part[21, 2]:='zzz##zzzz##';
        Part[21, 3]:='zz##zzzz##z';
        Part[21, 4]:='z##zzzz##zz';
        Part[21, 5]:='##zzzzzzzzz';
        Part[21, 6]:='zzz##zzzzzz';
        Part[21, 7]:='zz##zzz##zz';
        Part[21, 8]:='z##zzz##zzz';
        Part[21, 9]:='##zzz##zzzz';
        Part[21,10]:='zzzz##zzz##';
        Part[21,11]:='zzzzzzzz##z';

        Part[22, 1]:='#####z#####';
        Part[22, 2]:='#{{zzzzzzz#';
        Part[22, 3]:='#zzzzzzzzz#';
        Part[22, 4]:='#zzzzzzzzz#';
        Part[22, 5]:='#zzzzzzzzz#';
        Part[22, 6]:='zzzzzzzzzzz';
        Part[22, 7]:='#zzzzzzzzz#';
        Part[22, 8]:='#zzzzzzzzz#';
        Part[22, 9]:='#zzzzzzzzz#';
        Part[22,10]:='#zzzzzzzz{#';
        Part[22,11]:='#####z#####';

        Part[23, 1]:='#####z#####';
        Part[23, 2]:='#zzzzzzzzz#';
        Part[23, 3]:='#zzzzzzzzz#';
        Part[23, 4]:='#zzzzzzzzz#';
        Part[23, 5]:='#z{######z#';
        Part[23, 6]:='zz{#----#zz';
        Part[23, 7]:='#zz#----#z#';
        Part[23, 8]:='#zz#----#z#';
        Part[23, 9]:='#zz###+##z#';
        Part[23,10]:='#zzzzzzzzz#';
        Part[23,11]:='#####+#####';

        Part[24, 1]:='#####z#####';
        Part[24, 2]:='#zzz#z#zzz#';
        Part[24, 3]:='#z###z###z#';
        Part[24, 4]:='#zzzzzzzzz#';
        Part[24, 5]:='#zz######z#';
        Part[24, 6]:='zzz#zz{{#zz';
        Part[24, 7]:='#zz#zzzz#z#';
        Part[24, 8]:='#zz#zzzz#z#';
        Part[24, 9]:='#z####+####';
        Part[24,10]:='#zzzzzzzzz#';
        Part[24,11]:='#####+#####';

        Part[25, 1]:='#####z#####';
        Part[25, 2]:='#####z#####';
        Part[25, 3]:='#####z#####';
        Part[25, 4]:='#####z#####';
        Part[25, 5]:='#####z#####';
        Part[25, 6]:='zz+zzzzzzzz';
        Part[25, 7]:='#####z##{{#';
        Part[25, 8]:='#####z#####';
        Part[25, 9]:='#####z#####';
        Part[25,10]:='#####z#####';
        Part[25,11]:='#####z#####';

        Part[26, 1]:='#####z#####';
        Part[26, 2]:='#####z#####';
        Part[26, 3]:='####{z#####';
        Part[26, 4]:='#####z#####';
        Part[26, 5]:='#####z#####';
        Part[26, 6]:='zzzzzzzzzzz';
        Part[26, 7]:='#####z#####';
        Part[26, 8]:='#####z#####';
        Part[26, 9]:='#####z#####';
        Part[26,10]:='#####z#####';
        Part[26,11]:='#####+#####';

        Part[27, 1]:='#####z#####';
        Part[27, 2]:='#zzzzzzzzz#';
        Part[27, 3]:='#z#######z#';
        Part[27, 4]:='#zzzzzzz#z#';
        Part[27, 5]:='#z#####z###';
        Part[27, 6]:='zz#zzz#zzzz';
        Part[27, 7]:='#z#zzz#####';
        Part[27, 8]:='#z#zzzzzzz#';
        Part[27, 9]:='#z##+######';
        Part[27,10]:='#zzzzzzzzz#';
        Part[27,11]:='#####z#####';

        Part[28, 1]:='#####z#####';
        Part[28, 2]:='#zzzzzzzzz#';
        Part[28, 3]:='#zzzzzzzzz#';
        Part[28, 4]:='#zzzzzzzzz#';
        Part[28, 5]:='#zz######z#';
        Part[28, 6]:='zzz#-VV-#zz';
        Part[28, 7]:='#zz+----+z#';
        Part[28, 8]:='#zz#---V#z#';
        Part[28, 9]:='#zz######z#';
        Part[28,10]:='#zzzzzzzzz#';
        Part[28,11]:='#####+#####';

        TotalParts:=28;
    end;

    if (lvl=8) or (lvl=9) then
    begin
        // rounded halls, twisted paths
        Part[1, 1]:='^^^^^.^^^^^';
        Part[1, 2]:='^^^^^.^^^^^';
        Part[1, 3]:='^^^^.^^^^^^';
        Part[1, 4]:='^^^.^^^^^^^';
        Part[1, 5]:='^^....^^^^^';
        Part[1, 6]:='^......^^..';
        Part[1, 7]:='^........^^';
        Part[1, 8]:='^^.cc.^^^^^';
        Part[1, 9]:='^^^^c^^^^^^';
        Part[1,10]:='^^^^^.^^^^^';
        Part[1,11]:='^^^^^.^^^^^';
    
        Part[2, 1]:='^^^^.^^^^^^';
        Part[2, 2]:='^.^^.^^^^^^';
        Part[2, 3]:='^.^^^..^^^^';
        Part[2, 4]:='^^.^^^^.^^^';
        Part[2, 5]:='.^^.^^^.^^.';
        Part[2, 6]:='...........';
        Part[2, 7]:='...^^^^^^..';
        Part[2, 8]:='^.^^....^^^';
        Part[2, 9]:='^.........^';
        Part[2,10]:='^^^......^^';
        Part[2,11]:='^^^^^.^^^^^';

        Part[3, 1]:='^^^^^.^^^^^';
        Part[3, 2]:='^^.......^^';
        Part[3, 3]:='^.........^';
        Part[3, 4]:='^..^......^';
        Part[3, 5]:='^..^^.....^';
        Part[3, 6]:='....^^^^...';
        Part[3, 7]:='^......^^^^';
        Part[3, 8]:='^^^^......^';
        Part[3, 9]:='^..^^.....^';
        Part[3,10]:='^^.......^^';
        Part[3,11]:='^^^^^.^^^^^';
    
        Part[4, 1]:='^^^^^.^^^^^';
        Part[4, 2]:='^^^^^+^^^^^';
        Part[4, 3]:='^^^....^^^^';
        Part[4, 4]:='^.c..^...^^';
        Part[4, 5]:='^.c.^^^^..^';
        Part[4, 6]:='..c.^^^^^..';
        Part[4, 7]:='^..c^^^^..^';
        Part[4, 8]:='^....^^...^';
        Part[4, 9]:='^^.......^^';
        Part[4,10]:='^^^^^+^^^^^';
        Part[4,11]:='^^^^^.^^^^^';
    
        Part[5, 1]:='^^^^^c^^^^^';
        Part[5, 2]:='^^^^^c^^^^^';
        Part[5, 3]:='^^^.....^^^';
        Part[5, 4]:='^....^^...^';
        Part[5, 5]:='^..^^^^^..^';
        Part[5, 6]:='..^^^^^^^..';
        Part[5, 7]:='^..^^^^^..^';
        Part[5, 8]:='^...^^^...^';
        Part[5, 9]:='^^......^^^';
        Part[5,10]:='^^^^^+^^^^^';
        Part[5,11]:='^^^^^.^^^^^';
    
        Part[6, 1]:='^^^^^.^^^^^';
        Part[6, 2]:='^^.......^^';
        Part[6, 3]:='^.........^';
        Part[6, 4]:='^.^.....^.^';
        Part[6, 5]:='^.^^...^^.^';
        Part[6, 6]:='...^^^^^...';
        Part[6, 7]:='^.........^';
        Part[6, 8]:='^.........^';
        Part[6, 9]:='^.....cc..^';
        Part[6,10]:='^^.......^^';
        Part[6,11]:='^^^^^.^^^^^';
    
        Part[7, 1]:='^^^^^.^^^^^';
        Part[7, 2]:='^^^^^.^^^^^';
        Part[7, 3]:='^......^^^^';
        Part[7, 4]:='^.......^^^';
        Part[7, 5]:='^....c...^^';
        Part[7, 6]:='.....c.....';
        Part[7, 7]:='^.........^';
        Part[7, 8]:='^^..c.....^';
        Part[7, 9]:='^^^.......^';
        Part[7,10]:='^^^^^.....^';
        Part[7,11]:='^^^^^.^^^^^';
    
        Part[8, 1]:='^^^^^+^^^^^';
        Part[8, 2]:='^^^^^...^^^';
        Part[8, 3]:='^^..^^^..^^';
        Part[8, 4]:='^^..^.^^.^^';
        Part[8, 5]:='^^..^.^^.^^';
        Part[8, 6]:='^^..^......';
        Part[8, 7]:='^^^....^..^';
        Part[8, 8]:='^^^^^.^^.^^';
        Part[8, 9]:='^^^^^.^.^^^';
        Part[8,10]:='^^^^^^.^^^^';
        Part[8,11]:='^^^^^.^^^^^';
    
        Part[9, 1]:='^^^^^.^^^^^';
        Part[9, 2]:='^^^^^.^^^^^';
        Part[9, 3]:='^.^^^^.^^^^';
        Part[9, 4]:='^.^^^^^.^^^';
        Part[9, 5]:='^.^^^^^^.^^';
        Part[9, 6]:='.......^...';
        Part[9, 7]:='^.^^^^^^.^^';
        Part[9, 8]:='^^.^^^^^.^^';
        Part[9, 9]:='^^^.^^^^.^^';
        Part[9,10]:='^^^^......^';
        Part[9,11]:='^^^^^+^^^^^';
    
        Part[10, 1]:='^^^^^+^^^^^';
        Part[10, 2]:='^^^^^.^^^^^';
        Part[10, 3]:='^......^^^^';
        Part[10, 4]:='^.......^^^';
        Part[10, 5]:='^........^^';
        Part[10, 6]:='+.........+';
        Part[10, 7]:='^.........^';
        Part[10, 8]:='^^........^';
        Part[10, 9]:='^^^.......^';
        Part[10,10]:='^^^^^.....^';
        Part[10,11]:='^^^^^+^^^^^';

        Part[11, 1]:='^c^........';
        Part[11, 2]:='^c^........';
        Part[11, 3]:='^.^^.......';
        Part[11, 4]:='^..^^.^^...';
        Part[11, 5]:='.......^^..';
        Part[11, 6]:='...^....^^.';
        Part[11, 7]:='...^^....^.';
        Part[11, 8]:='^...^^...^.';
        Part[11, 9]:='^....^^..^.';
        Part[11,10]:='^^....^..^.';
        Part[11,11]:='^^^...^^^^.';

        Part[12, 1]:='^^^^^+^^^^^';
        Part[12, 2]:='^^^^^cc..^^';
        Part[12, 3]:='^^^^^c^^^.^';
        Part[12, 4]:='^^^^^.^^^.^';
        Part[12, 5]:='^^^^...^^.^';
        Part[12, 6]:='.^^^.^^^^^.';
        Part[12, 7]:='^.^^+^^^^^^';
        Part[12, 8]:='^.^^.^^^^^^';
        Part[12, 9]:='^.^^..^^^^^';
        Part[12,10]:='^^....^^^^^';
        Part[12,11]:='^^^^^.^^^^^';

        Part[13, 1]:='^^^^^.^^^^^';
        Part[13, 2]:='^^^^^....^^';
        Part[13, 3]:='^^^^^.^^^.^';
        Part[13, 4]:='^^^^^.^^^.^';
        Part[13, 5]:='^^^^^.^^^.^';
        Part[13, 6]:='.^^^.^^^^^.';
        Part[13, 7]:='^.^^.^^^^^^';
        Part[13, 8]:='^.^^.^^^^^^';
        Part[13, 9]:='^.^^^.^^^^^';
        Part[13,10]:='^^....^^^^^';
        Part[13,11]:='^^^^^.^^^^^';

        Part[14, 1]:='^^^^^.^^^^^';
        Part[14, 2]:='^^^^^c^^^^^';
        Part[14, 3]:='^^^...c^^^^';
        Part[14, 4]:='^....^^^^^^';
        Part[14, 5]:='^..^^^...^^';
        Part[14, 6]:='..^^......+';
        Part[14, 7]:='^.+.......^';
        Part[14, 8]:='^^^.......^';
        Part[14, 9]:='^^^......^^';
        Part[14,10]:='^^^^^...^^^';
        Part[14,11]:='^^^^^+^^^^^';

        Part[15, 1]:='^^^...^^^^^';
        Part[15, 2]:='^^....^^^^^';
        Part[15, 3]:='..ccc^^^^^^';
        Part[15, 4]:='^ccccc^^^^^';
        Part[15, 5]:='^^.cc.....^';
        Part[15, 6]:='...........';
        Part[15, 7]:='^^.......^^';
        Part[15, 8]:='^.......^^^';
        Part[15, 9]:='^^^^......^';
        Part[15,10]:='^^^^....^^^';
        Part[15,11]:='^^^^^.^^^^^';

        Part[16, 1]:='^^^......^^';
        Part[16, 2]:='^^.........';
        Part[16, 3]:='...........';
        Part[16, 4]:='^.......^^^';
        Part[16, 5]:='^^........^';
        Part[16, 6]:='.....^.....';
        Part[16, 7]:='^^.^^....^^';
        Part[16, 8]:='^..........';
        Part[16, 9]:='^^^.......^';
        Part[16,10]:='^^^^....^^^';
        Part[16,11]:='^^^......^^';

        Part[17, 1]:='^^^......^^';
        Part[17, 2]:='^^.........';
        Part[17, 3]:='...........';
        Part[17, 4]:='^....%..^^^';
        Part[17, 5]:='^^....%...^';
        Part[17, 6]:='.....^.....';
        Part[17, 7]:='^^.%^....^^';
        Part[17, 8]:='^..........';
        Part[17, 9]:='^^^..^^^%%^';
        Part[17,10]:='^^^^%...^^^';
        Part[17,11]:='^^^......^^';
    
        TotalParts:=17;
    end;


    if (lvl=10) or (lvl=11) then
    begin
        // rounded halls, twisted paths
        Part[1, 1]:='^^^^^.^^^^^';
        Part[1, 2]:='^^^^^.^^^^^';
        Part[1, 3]:='^^^^.^^^^^^';
        Part[1, 4]:='^^^.^^^^^^^';
        Part[1, 5]:='^^....^^^^^';
        Part[1, 6]:='^......^^..';
        Part[1, 7]:='^........^^';
        Part[1, 8]:='^^cc..^^^^^';
        Part[1, 9]:='^^^^c^^^^^^';
        Part[1,10]:='^^^^^c^^^^^';
        Part[1,11]:='^^^^^c^^^^^';
    
        Part[2, 1]:='^^^^.^^^^^^';
        Part[2, 2]:='^.^^.^^^^^^';
        Part[2, 3]:='^.^^^..^^^^';
        Part[2, 4]:='^^.^^^^.^^^';
        Part[2, 5]:='.^^.^^^.^^.';
        Part[2, 6]:='...........';
        Part[2, 7]:='...^^^^^^..';
        Part[2, 8]:='^.^^....^^^';
        Part[2, 9]:='^.........^';
        Part[2,10]:='^^^......^^';
        Part[2,11]:='^^^^^.^^^^^';

        Part[3, 1]:='^^^^^,^^^^^';
        Part[3, 2]:='^^,,,,,,,^^';
        Part[3, 3]:='^,,,,,,,,,^';
        Part[3, 4]:='^,,^,,,,,,^';
        Part[3, 5]:='^,,^^,,,,,^';
        Part[3, 6]:=',,,,^^^^,,,';
        Part[3, 7]:='^,,,,,,^^^^';
        Part[3, 8]:='^^^^,,cc,,^';
        Part[3, 9]:='^,,^^,,,,,^';
        Part[3,10]:='^^,,,,,,,^^';
        Part[3,11]:='^^^^^,^^^^^';
    
        Part[4, 1]:='^^^^^,^^^^^';
        Part[4, 2]:='^^^^^+^^^^^';
        Part[4, 3]:='^^^,,,,^^^^';
        Part[4, 4]:='^,,,,^,,,^^';
        Part[4, 5]:='^,,,^^^^,,^';
        Part[4, 6]:=',cc,^^^^^,,';
        Part[4, 7]:='^,,,^^^^,,^';
        Part[4, 8]:='^,,,,^^,,,^';
        Part[4, 9]:='^^,,,,,,,^^';
        Part[4,10]:='^^^^^+^^^^^';
        Part[4,11]:='^^^^^,^^^^^';
    
        Part[5, 1]:='^^^^^.^^^^^';
        Part[5, 2]:='^^^^^.^^^^^';
        Part[5, 3]:='^^^.....^^^';
        Part[5, 4]:='^....^^...^';
        Part[5, 5]:='^..^^^^^..^';
        Part[5, 6]:='..^^^^^^^..';
        Part[5, 7]:='^..^^^^^..^';
        Part[5, 8]:='^...^^^...^';
        Part[5, 9]:='^^......^^^';
        Part[5,10]:='^^^^^+^^^^^';
        Part[5,11]:='^^^^^.^^^^^';
    
        Part[6, 1]:='^^^^^.^^^^^';
        Part[6, 2]:='^^.......^^';
        Part[6, 3]:='^.........^';
        Part[6, 4]:='^.^..w..^.^';
        Part[6, 5]:='^.^^...^^.^';
        Part[6, 6]:='...^^^^^...';
        Part[6, 7]:='^.........^';
        Part[6, 8]:='^.cc......^';
        Part[6, 9]:='^..cc.....^';
        Part[6,10]:='^^.......^^';
        Part[6,11]:='^^^^^.^^^^^';
    
        Part[7, 1]:='^^^^^,^^^^^';
        Part[7, 2]:='^^^^^,^^^^^';
        Part[7, 3]:='^,,,,,,^^^^';
        Part[7, 4]:='^,,,,,,,^^^';
        Part[7, 5]:='^,,,.,,,,^^';
        Part[7, 6]:=',,,,,.,,,,,';
        Part[7, 7]:='^,,,,.,,c,^';
        Part[7, 8]:='^^,,,.,c,,^';
        Part[7, 9]:='^^^,,,,c,,^';
        Part[7,10]:='^^^^^,,,,,^';
        Part[7,11]:='^^^^^,^^^^^';
    
        Part[8, 1]:='^^^^^+^^^^^';
        Part[8, 2]:='^^^^^...^^^';
        Part[8, 3]:='^^..^^^..^^';
        Part[8, 4]:='^^..^.^^.^^';
        Part[8, 5]:='^^..^.^^.^^';
        Part[8, 6]:='^^..^......';
        Part[8, 7]:='^^^....^..^';
        Part[8, 8]:='^^^^^.^^.^^';
        Part[8, 9]:='^^^^^.^.^^^';
        Part[8,10]:='^^^^^^.^^^^';
        Part[8,11]:='^^^^^.^^^^^';
    
        Part[9, 1]:='^^^^^.^^^^^';
        Part[9, 2]:='^^^^^.^^^^^';
        Part[9, 3]:='^.^^^^.^^^^';
        Part[9, 4]:='^.^^^^^.^^^';
        Part[9, 5]:='^.^^^^^^.^^';
        Part[9, 6]:='.......^...';
        Part[9, 7]:='^.^^^^^^.^^';
        Part[9, 8]:='^^.^^^^^.^^';
        Part[9, 9]:='^^^.^^^^.^^';
        Part[9,10]:='^^^^......^';
        Part[9,11]:='^^^^^+^^^^^';
    
        Part[10, 1]:='^^^^^+^^^^^';
        Part[10, 2]:='^^^^^.^^^^^';
        Part[10, 3]:='^^.....^^^^';
        Part[10, 4]:='^...,...^^^';
        Part[10, 5]:='^...,....^^';
        Part[10, 6]:='+..,---,..+';
        Part[10, 7]:='^...--,,..^';
        Part[10, 8]:='^^..--,...^';
        Part[10, 9]:='^^.......^^';
        Part[10,10]:='^^^^^..^^^^';
        Part[10,11]:='^^^^^+^^^^^';

        Part[11, 1]:='^.^.....c..';
        Part[11, 2]:='^.^...ccc..';
        Part[11, 3]:='^.^^....ccc';
        Part[11, 4]:='^..^^,^^...';
        Part[11, 5]:=',..,.,,^^..';
        Part[11, 6]:='.,.^,,,,^^.';
        Part[11, 7]:='...^^,,,,^.';
        Part[11, 8]:='^.,.^^,,,^.';
        Part[11, 9]:='^...-^^,,^.';
        Part[11,10]:='^^.---^,,^.';
        Part[11,11]:='^^^.-.^^^^.';

        Part[12, 1]:='^^^^^+^^^^^';
        Part[12, 2]:='^^^^^,,,,^^';
        Part[12, 3]:='^^^^^,^^^,^';
        Part[12, 4]:='^^^^^,^^^,^';
        Part[12, 5]:='^^^^,,,^^,^';
        Part[12, 6]:=',^^^,^^^^^,';
        Part[12, 7]:='^,^^+^^^^^^';
        Part[12, 8]:='^,^^,^^^^^^';
        Part[12, 9]:='^,^^,,^^^^^';
        Part[12,10]:='^^,,,,^^^^^';
        Part[12,11]:='^^^^^,^^^^^';

        Part[13, 1]:='^^^^^,^^^^^';
        Part[13, 2]:='^^^^^,,,,^^';
        Part[13, 3]:='^^^^^,^^^,^';
        Part[13, 4]:='^^^^^,^^^,^';
        Part[13, 5]:='^^^^^,^^^,^';
        Part[13, 6]:=',^^^,^^^^^,';
        Part[13, 7]:='^,^^,^^^^^^';
        Part[13, 8]:='^,^^,^^^^^^';
        Part[13, 9]:='^,^^^,^^^^^';
        Part[13,10]:='^^,,,,^^^^^';
        Part[13,11]:='^^^^^,^^^^^';

        Part[14, 1]:='^^^^^.^^^^^';
        Part[14, 2]:='^^^^^.^^^^^';
        Part[14, 3]:='^^^....^^^^';
        Part[14, 4]:='^....^^^^^^';
        Part[14, 5]:='^..^^^...^^';
        Part[14, 6]:='..^^..,...+';
        Part[14, 7]:='^.+...,...^';
        Part[14, 8]:='^^^..,....^';
        Part[14, 9]:='^^^...,..^^';
        Part[14,10]:='^^^^^..,^^^';
        Part[14,11]:='^^^^^+^^^^^';

        Part[15, 1]:='^^^...^^^^^';
        Part[15, 2]:='^^....^^^^^';
        Part[15, 3]:='.....^^^^^^';
        Part[15, 4]:='^.....^^^^^';
        Part[15, 5]:='^^..,,....^';
        Part[15, 6]:='....,,,,...';
        Part[15, 7]:='^^.,,,,,.^^';
        Part[15, 8]:='^....,,,^^^';
        Part[15, 9]:='^^^^..,...^';
        Part[15,10]:='^^^^....^^^';
        Part[15,11]:='^^^^^.^^^^^';

        Part[16, 1]:='^^^......^^';
        Part[16, 2]:='^^.....,.--';
        Part[16, 3]:='.....,,..--';
        Part[16, 4]:='^....,..^^^';
        Part[16, 5]:='^^........^';
        Part[16, 6]:='.....^.....';
        Part[16, 7]:='^^.^^....^^';
        Part[16, 8]:='^.....,....';
        Part[16, 9]:='^^^...,...^';
        Part[16,10]:='^^^^.,..^^^';
        Part[16,11]:='^^^...,..^^';

        Part[17, 1]:='^^^,,.,,,^^';
        Part[17, 2]:='^^,,,.,^^,,';
        Part[17, 3]:=',,,,.,,,,,,';
        Part[17, 4]:='^,,,.,,,^^^';
        Part[17, 5]:='^^,..,,,,,^';
        Part[17, 6]:=',,,,,^^,,,,';
        Part[17, 7]:='^^,^^,,.,^^';
        Part[17, 8]:='^,,,,,.,,,,';
        Part[17, 9]:='^^^,,.,,,,^';
        Part[17,10]:='^^^^,.,,^^^';
        Part[17,11]:='^^^,,.,,,^^';

        Part[18, 1]:='^^^^^+^^^^^';
        Part[18, 2]:='^^^^^.^^^^^';
        Part[18, 3]:='^^%....^^^^';
        Part[18, 4]:='^%%.,..%^^^';
        Part[18, 5]:='^%..,.%%%^^';
        Part[18, 6]:='+....,.%%.+';
        Part[18, 7]:='^%.%..,...^';
        Part[18, 8]:='^^%%..%%.%^';
        Part[18, 9]:='^^%%%..%%^^';
        Part[18,10]:='^^^^^..^^^^';
        Part[18,11]:='^^^^^+^^^^^';

        Part[19, 1]:='^^^...^^^^^';
        Part[19, 2]:='^^....^^^^^';
        Part[19, 3]:='.....^^^^^^';
        Part[19, 4]:='^.....^^^^^';
        Part[19, 5]:='^^..,,....^';
        Part[19, 6]:='....,Y,,...';
        Part[19, 7]:='^^.,Yyy,.^^';
        Part[19, 8]:='^..,,,Y,^^^';
        Part[19, 9]:='^^,,,.,...^';
        Part[19,10]:='^^^^,,,.^^^';
        Part[19,11]:='^^^^^,^^^^^';

        Part[20, 1]:='^^^^^,,^^^^';
        Part[20, 2]:='^^^^,,^^^^^';
        Part[20, 3]:='^^^,^^,^^^^';
        Part[20, 4]:='^,,,,,^^^^^';
        Part[20, 5]:='^^,,,,,,yy^';
        Part[20, 6]:=',,,,,,,,,,y';
        Part[20, 7]:='^^.,,,,,.^^';
        Part[20, 8]:='^Yy^^^,^^..';
        Part[20, 9]:='^^^^,,.,,^^';
        Part[20,10]:='^^,,,..,,,^';
        Part[20,11]:='^.........^';

        Part[21, 1]:='^,^,,,,,,..';
        Part[21, 2]:='^,^,yy,,,,.';
        Part[21, 3]:='^,^^YyyY,,,';
        Part[21, 4]:='^,,^^,^^,..';
        Part[21, 5]:=',,,,.,,^^..';
        Part[21, 6]:=',,,^,,,,^^.';
        Part[21, 7]:=',,,^^,,,,^.';
        Part[21, 8]:='^,,,^^,,,^.';
        Part[21, 9]:='^,,,y^^,,^.';
        Part[21,10]:='^^,Yyy^,,^.';
        Part[21,11]:='^^^,,,^^^^.';
    
        TotalParts:=21;
    end;

    if (lvl=12) or (lvl=13) then
    begin
        // labyrinth
        Part[1, 1]:='^^^^^.^^^^^';
        Part[1, 2]:='^^^^^....^^';
        Part[1, 3]:='^^^^^.^^.^^';
        Part[1, 4]:='^^....^^.^^';
        Part[1, 5]:='^^.^^.^^^^^';
        Part[1, 6]:='...^^.....*';
        Part[1, 7]:='^^^^^.^^.^^';
        Part[1, 8]:='^.^^^.^^.^^';
        Part[1, 9]:='^.^^^....^^';
        Part[1,10]:='^.....^^^^^';
        Part[1,11]:='^^^^^.^^^^^';

        Part[2, 1]:='^^^^^....^^';
        Part[2, 2]:='^^....^^.^^';
        Part[2, 3]:='^^.^^.^^^^^';
        Part[2, 4]:='^^.^^.^^.^^';
        Part[2, 5]:='^^.^^.^^.^^';
        Part[2, 6]:='....^......';
        Part[2, 7]:='^^.^^.^^.^^';
        Part[2, 8]:='^^.^^.^^.^.';
        Part[2, 9]:='^^.^^.^^.^.';
        Part[2,10]:='^..^^.^^...';
        Part[2,11]:='^^^^^.^^^^^';

        Part[3, 1]:='^^.^^.^^^^^';
        Part[3, 2]:='^^.^^.^^^^^';
        Part[3, 3]:='^^V^^.^^^^^';
        Part[3, 4]:='^^.^^.^^^^^';
        Part[3, 5]:='^^.^^*^^^^^';
        Part[3, 6]:='...........';
        Part[3, 7]:='^^^^^.^^^^^';
        Part[3, 8]:='^^^^^.^^^^^';
        Part[3, 9]:='^^^^^.^^^^^';
        Part[3,10]:='^^^^^.^^^^^';
        Part[3,11]:='^^^^^.^^^^^';

        Part[4, 1]:='^^.^^.^^.^^';
        Part[4, 2]:='^^.^^.^^.^^';
        Part[4, 3]:='^^.......^^';
        Part[4, 4]:='^^.^^.^^.^^';
        Part[4, 5]:='^^.^^.^^.^^';
        Part[4, 6]:='...........';
        Part[4, 7]:='^^.^^.^^.^^';
        Part[4, 8]:='^^.^^.^^.^^';
        Part[4, 9]:='^^.^^.^^.^^';
        Part[4,10]:='^^.^^.^^.^^';
        Part[4,11]:='^^.^^.^^.^^';

        Part[5, 1]:='^^^^^*^^^^^';
        Part[5, 2]:='....^......';
        Part[5, 3]:='^^^^^.^^^^^';
        Part[5, 4]:='..........V';
        Part[5, 5]:='^^^^^.^^^^^';
        Part[5, 6]:='......^....';
        Part[5, 7]:='^^.^^^^^^^^';
        Part[5, 8]:='...........';
        Part[5, 9]:='^^.^^.^^^^^';
        Part[5,10]:='...........';
        Part[5,11]:='^^.^^.^^^^^';

        Part[6, 1]:='^^.^^.^^.^^';
        Part[6, 2]:='^^.^^.^^.^^';
        Part[6, 3]:='^^.^^....^^';
        Part[6, 4]:='^^.^^.^^.^^';
        Part[6, 5]:='^^*^^.^^.^^';
        Part[6, 6]:='...........';
        Part[6, 7]:='^.^^^.^.^.^';
        Part[6, 8]:='^.^^^.^.^.^';
        Part[6, 9]:='^.....^...^';
        Part[6,10]:='^.^^^.^^^.^';
        Part[6,11]:='^.^^^.^^^.^';

        Part[7, 1]:='^^^^^.^^^^^';
        Part[7, 2]:='^...^.^...^';
        Part[7, 3]:='^^^.^.^.^^^';
        Part[7, 4]:='^^^.^.^.^^^';
        Part[7, 5]:='^^^.^.^.^^^';
        Part[7, 6]:='...........';
        Part[7, 7]:='^^^.^.^.^^^';
        Part[7, 8]:='^^^.^.^.^^^';
        Part[7, 9]:='^^^.^.^.^^^';
        Part[7,10]:='^...^.^...^';
        Part[7,11]:='^^^^^.^^^^^';

        Part[8, 1]:='.^^^^.^^^^.';
        Part[8, 2]:='^.^^^.^^^.^';
        Part[8, 3]:='^^.^^.^^.^^';
        Part[8, 4]:='^^^.^.^.^^^';
        Part[8, 5]:='^^^^...^^^^';
        Part[8, 6]:='...........';
        Part[8, 7]:='^^^^...^^^^';
        Part[8, 8]:='^^^.^.^.^^^';
        Part[8, 9]:='^^.^^.^^.^^';
        Part[8,10]:='^.^^^.^^^.^';
        Part[8,11]:='.^^^^.^^^^.';

        Part[9, 1]:='^^^^^......';
        Part[9, 2]:='^^^^^.^^^^^';
        Part[9, 3]:='^^^^^.^^^^^';
        Part[9, 4]:='^.........^';
        Part[9, 5]:='^^^^^.^^^^^';
        Part[9, 6]:='...........';
        Part[9, 7]:='^^^^^.^^^^^';
        Part[9, 8]:='^.........^';
        Part[9, 9]:='^^^^^.^^^.^';
        Part[9,10]:='^^^^^.^^^.^';
        Part[9,11]:='^^^^^.^^^.^';

        Part[10, 1]:='^^^^^.^^.^^';
        Part[10, 2]:='^^^......^^';
        Part[10, 3]:='^^^^^.^^.^^';
        Part[10, 4]:='^^^......^^';
        Part[10, 5]:='^^^^^.^^.^^';
        Part[10, 6]:='...........';
        Part[10, 7]:='^^^^^.^^.^^';
        Part[10, 8]:='^^^.....^^^';
        Part[10, 9]:='^^^^^.^^^^^';
        Part[10,10]:='^^^......^^';
        Part[10,11]:='^^^^^.^^.^^';

        Part[11, 1]:='^^....^^.^^';
        Part[11, 2]:='^^^^^.^^.^^';
        Part[11, 3]:='^^^^^.^..^^';
        Part[11, 4]:='^^^^^.^.^^^';
        Part[11, 5]:='^^.^^.^.^^^';
        Part[11, 6]:='...........';
        Part[11, 7]:='^^^.^.^^.^^';
        Part[11, 8]:='^^^.^.^^.^^';
        Part[11, 9]:='^^^^^.^^.^^';
        Part[11,10]:='^^....^^.^^';
        Part[11,11]:='^^.^^.^^^^^';

        Part[12, 1]:='^^.^^.^^.^^';
        Part[12, 2]:='^^.^..^^.^^';
        Part[12, 3]:='^^.^^....^^';
        Part[12, 4]:='^^....^^^^^';
        Part[12, 5]:='^^^^^.....^';
        Part[12, 6]:='...........';
        Part[12, 7]:='^^^^..^^^^.';
        Part[12, 8]:='^^^^^...^^.';
        Part[12, 9]:='^^....^^^^.';
        Part[12,10]:='^^^^^......';
        Part[12,11]:='^^^^^.^^.^^';

        Part[13, 1]:='^^^^^.^^.^^';
        Part[13, 2]:='^.....^^.^^';
        Part[13, 3]:='^^^^^....^^';
        Part[13, 4]:='^^^...^^^^^';
        Part[13, 5]:='^^^^^..^^^^';
        Part[13, 6]:='...........';
        Part[13, 7]:='^^^^..^^^^^';
        Part[13, 8]:='^^^^^...^^^';
        Part[13, 9]:='^^....^^^^^';
        Part[13,10]:='^^^^^..^...';
        Part[13,11]:='^^^^^.^^.^^';

        Part[14, 1]:='^^....^^.^^';
        Part[14, 2]:='^^^^^.^^.^^';
        Part[14, 3]:='^^^^^.^%%^^';
        Part[14, 4]:='^^^^^.^.^^^';
        Part[14, 5]:='^^.^^.^.^^^';
        Part[14, 6]:='...........';
        Part[14, 7]:='^^^.^.^^.^^';
        Part[14, 8]:='^^^.^.^^.^^';
        Part[14, 9]:='^^^^^.^^.^^';
        Part[14,10]:='^^....^^.^^';
        Part[14,11]:='^^.^^.^^^^^';

        Part[15, 1]:='^^^^^.^^.^^';
        Part[15, 2]:='^^^......^^';
        Part[15, 3]:='^^^^^.^^.^^';
        Part[15, 4]:='^^^......^^';
        Part[15, 5]:='^^^^^*^^^^^';
        Part[15, 6]:='...........';
        Part[15, 7]:='^^^^^*^^^^^';
        Part[15, 8]:='^^^.....^^^';
        Part[15, 9]:='^^^^^.^^^^^';
        Part[15,10]:='^^^......^^';
        Part[15,11]:='^^^^^.^^.^^';

        Part[16, 1]:='^^.^^.^^.^^';
        Part[16, 2]:='^^.^..^^.^^';
        Part[16, 3]:='^^.^^V..v^^';
        Part[16, 4]:='^^..vV^^^^^';
        Part[16, 5]:='^^^^^VV...^';
        Part[16, 6]:='.....Vv....';
        Part[16, 7]:='^^^^.V^^^^.';
        Part[16, 8]:='^^^^^...^^.';
        Part[16, 9]:='^^....^^^^.';
        Part[16,10]:='^^^^^......';
        Part[16,11]:='^^^^^.^^.^^';

        Part[17, 1]:='^^^^^^v^^^^';
        Part[17, 2]:='^^^^^..^^^^';
        Part[17, 3]:='^^^^^...^^^';
        Part[17, 4]:='^^^.^.V..^^';
        Part[17, 5]:='^^^^.VV.^^^';
        Part[17, 6]:='...VVVVV...';
        Part[17, 7]:='^^...VV^^^^';
        Part[17, 8]:='^^^^^^..v^^';
        Part[17, 9]:='^^^^^^^^.vv';
        Part[17,10]:='^^^^^^...^^';
        Part[17,11]:='^^^^^.^^^^^';

        Part[18, 1]:='^^^^^^.^^^^';
        Part[18, 2]:='^^^^^..^^^^';
        Part[18, 3]:='^^^^^...^^^';
        Part[18, 4]:='^^^.^....^^';
        Part[18, 5]:='^^^^....^^^';
        Part[18, 6]:='.....^^....';
        Part[18, 7]:='^^.....^^^^';
        Part[18, 8]:='^^^^^^...^^';
        Part[18, 9]:='^^^^...^...';
        Part[18,10]:='^^^..^^^^^^';
        Part[18,11]:='^^^^^.^^^^^';

        TotalParts:=18;
    end;


    if (lvl=14) or (lvl=15) then
    begin
        // rounded halls, twisted paths
        Part[1, 1]:='^^^^^i^^^^^';
        Part[1, 2]:='^^^^^i^^^^^';
        Part[1, 3]:='^^^^i^^^^^^';
        Part[1, 4]:='^^^i^^^^^^^';
        Part[1, 5]:='^^iiii^^^^^';
        Part[1, 6]:='^ii.iii^^ii';
        Part[1, 7]:='^iii...ii^^';
        Part[1, 8]:='^^iiii^^^^^';
        Part[1, 9]:='^^^^i^^^^^^';
        Part[1,10]:='^^^^^i^^^^^';
        Part[1,11]:='^^^^^i^^^^^';
    
        Part[2, 1]:='^^^^i^^^^^^';
        Part[2, 2]:='^i^^i^^^^^^';
        Part[2, 3]:='^i^^^ii^^^^';
        Part[2, 4]:='^^i^^^^i^^^';
        Part[2, 5]:='i^^i^^^i^^i';
        Part[2, 6]:='iiicccc..ii';
        Part[2, 7]:='iii^^^^^^ii';
        Part[2, 8]:='^i^^iiii^^^';
        Part[2, 9]:='^iiiiiiiii^';
        Part[2,10]:='^^^iiiiii^^';
        Part[2,11]:='^^^^^i^^^^^';

        Part[3, 1]:='^^^^^i^^^^^';
        Part[3, 2]:='^^iiiiiii^^';
        Part[3, 3]:='^iiiii..ii^';
        Part[3, 4]:='^ii^iiii.i^';
        Part[3, 5]:='^ii^^iiii.^';
        Part[3, 6]:='iiii^^^^iii';
        Part[3, 7]:='^iiiiii^^^^';
        Part[3, 8]:='^^^^iiiiii^';
        Part[3, 9]:='^ii^^iiiii^';
        Part[3,10]:='^^iiiiiii^^';
        Part[3,11]:='^^^^^i^^^^^';
    
        Part[4, 1]:='^^^^^i^^^^^';
        Part[4, 2]:='^^^^^+^^^^^';
        Part[4, 3]:='^^^cc..^^^^';
        Part[4, 4]:='^.cc.^...^^';
        Part[4, 5]:='^...^^^^..^';
        Part[4, 6]:='....^^^^^..';
        Part[4, 7]:='^...^^^^..^';
        Part[4, 8]:='^....^^...^';
        Part[4, 9]:='^^.......^^';
        Part[4,10]:='^^^^^+^^^^^';
        Part[4,11]:='^^^^^i^^^^^';
    
        Part[5, 1]:='^^^^^i^^^^^';
        Part[5, 2]:='^^^^^.^^^^^';
        Part[5, 3]:='^^^i..ii^^^';
        Part[5, 4]:='^iiii^^iii^';
        Part[5, 5]:='^ii^^^^^ii^';
        Part[5, 6]:='ii^^^^^^^ii';
        Part[5, 7]:='^ii^^^^^ii^';
        Part[5, 8]:='^iii^^^iii^';
        Part[5, 9]:='^^iiiiii^^^';
        Part[5,10]:='^^^^^+^^^^^';
        Part[5,11]:='^^^^^i^^^^^';
    
        Part[6, 1]:='^^^^^i^^^^^';
        Part[6, 2]:='^^iiiiiii^^';
        Part[6, 3]:='^iiiiiiiii^';
        Part[6, 4]:='^i^iiiii^i^';
        Part[6, 5]:='^i^^iii^^i^';
        Part[6, 6]:='iii^^^^^iii';
        Part[6, 7]:='^ii.....ii^';
        Part[6, 8]:='^i......ii^';
        Part[6, 9]:='^iii.ii.ii^';
        Part[6,10]:='^^i.....i^^';
        Part[6,11]:='^^^^^i^^^^^';
    
        Part[7, 1]:='^^^^^i^^^^^';
        Part[7, 2]:='^^^^^i^^^^^';
        Part[7, 3]:='^iiiiii^^^^';
        Part[7, 4]:='^iiiiiii^^^';
        Part[7, 5]:='^ii..iiii^^';
        Part[7, 6]:='iiiiicciiii';
        Part[7, 7]:='^iiiiiicci^';
        Part[7, 8]:='^^iiii.iii^';
        Part[7, 9]:='^^^iii.iii^';
        Part[7,10]:='^^^^^i.iii^';
        Part[7,11]:='^^^^^i^^^^^';
    
        Part[8, 1]:='^^^^^+^^^^^';
        Part[8, 2]:='^^^^^ii.^^^';
        Part[8, 3]:='^^ii^^^.i^^';
        Part[8, 4]:='^^ii^i^^i^^';
        Part[8, 5]:='^^ii^i^^i^^';
        Part[8, 6]:='^^ii^iiiiii';
        Part[8, 7]:='^^^iiii^ii^';
        Part[8, 8]:='^^^^^i^^i^^';
        Part[8, 9]:='^^^^^i^i^^^';
        Part[8,10]:='^^^^^^i^^^^';
        Part[8,11]:='^^^^^i^^^^^';
    
        Part[9, 1]:='^^^^^i^^^^^';
        Part[9, 2]:='^^^^^i^^^^^';
        Part[9, 3]:='^.^^^^i^^^^';
        Part[9, 4]:='^.^^^^^i^^^';
        Part[9, 5]:='^.^^^^^^i^^';
        Part[9, 6]:='.......^iii';
        Part[9, 7]:='^i^^^^^^i^^';
        Part[9, 8]:='^^i^^^^^i^^';
        Part[9, 9]:='^^^i^^^^i^^';
        Part[9,10]:='^^^^iiiiii^';
        Part[9,11]:='^^^^^+^^^^^';
    
        Part[10, 1]:='^^^^^+^^^^^';
        Part[10, 2]:='^^^^^.^^^^^';
        Part[10, 3]:='^i.i^ii^^^^';
        Part[10, 4]:='^i.^iii^^^^';
        Part[10, 5]:='^ci^ii^^i^^';
        Part[10, 6]:='+cii^^^iii+';
        Part[10, 7]:='^cii^^iiii^';
        Part[10, 8]:='^^ii^^iiii^';
        Part[10, 9]:='^^^ii.iii^^';
        Part[10,10]:='^^^^^.iii^^';
        Part[10,11]:='^^^^^+^^^^^';

        Part[11, 1]:='^i^iiiiiiii';
        Part[11, 2]:='^i^iiiiiiii';
        Part[11, 3]:='^i^^iiiiiii';
        Part[11, 4]:='^ii^^i^^iii';
        Part[11, 5]:='iiiiiii^^ii';
        Part[11, 6]:='iii^iiii^^i';
        Part[11, 7]:='iii^^iiii^i';
        Part[11, 8]:='^iii^^iii^i';
        Part[11, 9]:='^iiii^^ii^i';
        Part[11,10]:='^^iiii^ii^i';
        Part[11,11]:='^^^iii^^^^i';

        Part[12, 1]:='^^^^^+^^^^^';
        Part[12, 2]:='^^^^^iiii^^';
        Part[12, 3]:='^^^^^i^^^i^';
        Part[12, 4]:='^^^^^i^^^i^';
        Part[12, 5]:='^^^^icc^^i^';
        Part[12, 6]:='i^^^i^^^^^i';
        Part[12, 7]:='^i^^+^^^^^^';
        Part[12, 8]:='^i^^i^^^^^^';
        Part[12, 9]:='^i^^ii^^^^^';
        Part[12,10]:='^^iiii^^^^^';
        Part[12,11]:='^^^^^i^^^^^';

        Part[13, 1]:='^^^^^i^^^^^';
        Part[13, 2]:='^^^^^iiii^^';
        Part[13, 3]:='^^^^^i^^^i^';
        Part[13, 4]:='^^^^^i^^^i^';
        Part[13, 5]:='^^^^^i^^^i^';
        Part[13, 6]:='i^^^i^^^^^i';
        Part[13, 7]:='^i^^i^^^^^^';
        Part[13, 8]:='^i^^i^^^^^^';
        Part[13, 9]:='^i^^^i^^^^^';
        Part[13,10]:='^^iiii^^^^^';
        Part[13,11]:='^^^^^i^^^^^';

        Part[14, 1]:='^^^^^i^^^^^';
        Part[14, 2]:='^^^^^i^^^^^';
        Part[14, 3]:='^^^iiii^^^^';
        Part[14, 4]:='^iiii^^^^^^';
        Part[14, 5]:='^ii^^^iii^^';
        Part[14, 6]:='ii^^ii..ii+';
        Part[14, 7]:='^i+ii....i^';
        Part[14, 8]:='^^^iii..ii^';
        Part[14, 9]:='^^^ii...i^^';
        Part[14,10]:='^^^^^i.i^^^';
        Part[14,11]:='^^^^^+^^^^^';

        Part[15, 1]:='^^^iii^^^^^';
        Part[15, 2]:='^^iiii^^^^^';
        Part[15, 3]:='iiiii^^^^^^';
        Part[15, 4]:='^iiiii^^^^^';
        Part[15, 5]:='^^iiiiiiii^';
        Part[15, 6]:='iiiiiiiiiii';
        Part[15, 7]:='^^iiiiiii^^';
        Part[15, 8]:='^iiiiiii^^^';
        Part[15, 9]:='^^^^iiiiii^';
        Part[15,10]:='^^^^iiii^^^';
        Part[15,11]:='^^^^^i^^^^^';

        Part[16, 1]:='^^^iiiiii^^';
        Part[16, 2]:='^^ii.iiiiii';
        Part[16, 3]:='iiii.iiiiii';
        Part[16, 4]:='^iiii.ii^^^';
        Part[16, 5]:='^^iiii.iii^';
        Part[16, 6]:='iiiii^.....';
        Part[16, 7]:='^^i^^i.ii^^';
        Part[16, 8]:='^iiii.iiiii';
        Part[16, 9]:='^^^ii.iiii^';
        Part[16,10]:='^^^^i.ii^^^';
        Part[16,11]:='^^^ii.iii^^';

        Part[17, 1]:='^^^^^i^^^^^';
        Part[17, 2]:='^^^^^i^^^^^';
        Part[17, 3]:='^^^iiii^^^^';
        Part[17, 4]:='^iiii^^^^^^';
        Part[17, 5]:='^ii^^^iii^^';
        Part[17, 6]:='il^^llllii+';
        Part[17, 7]:='^lllllllll^';
        Part[17, 8]:='^^^lllllll^';
        Part[17, 9]:='^^^llllll^^';
        Part[17,10]:='^^^^^lll^^^';
        Part[17,11]:='^^^^^l^^^^^';

        Part[18, 1]:='^^^^^ll^^^^';
        Part[18, 2]:='^^lliilll^^';
        Part[18, 3]:='^lilllllll^';
        Part[18, 4]:='^l^iiiii^i^';
        Part[18, 5]:='^il^iii^^i^';
        Part[18, 6]:='iiilll^^lll';
        Part[18, 7]:='^ii..llllil';
        Part[18, 8]:='^i.lllllll^';
        Part[18, 9]:='^iiilll.ii^';
        Part[18,10]:='^^i..l..i^^';
        Part[18,11]:='^^^^llll^^^';

        Part[19, 1]:='^^^^^l^^^^^';
        Part[19, 2]:='^^iiiliii^^';
        Part[19, 3]:='^iiiiliiii^';
        Part[19, 4]:='^i^iliii^i^';
        Part[19, 5]:='^i^^lii^^i^';
        Part[19, 6]:='iii^l^^^iii';
        Part[19, 7]:='^iii.l..ii^';
        Part[19, 8]:='^i.i.l..ii^';
        Part[19, 9]:='^iii.li.ii^';
        Part[19,10]:='^^i...l.i^^';
        Part[19,11]:='^^^^^ll^^^^';

        if lvl=15 then
        begin
            Part[20, 1]:='^^^iii^^^^^';
            Part[20, 2]:='^^iiii^^^^^';
            Part[20, 3]:='iiiii^^^^^^';
            Part[20, 4]:='^i###+###^^';
            Part[20, 5]:='^^#-----#i^';
            Part[20, 6]:='ii+-----+ii';
            Part[20, 7]:='^^#-----#^^';
            Part[20, 8]:='^i###+##^^^';
            Part[20, 9]:='^^^^iiiiii^';
            Part[20,10]:='^^^^iiii^^^';
            Part[20,11]:='^^^^^i^^^^^';

            Part[21, 1]:='^iii^^#####';
            Part[21, 2]:='^^iii^#---#';
            Part[21, 3]:='iiiii^#---#';
            Part[21, 4]:='^iiiii#---#';
            Part[21, 5]:='^^ii^^###+#';
            Part[21, 6]:='iii^^^^^iii';
            Part[21, 7]:='^^iiiiiii^^';
            Part[21, 8]:='^^^iiii^^^^';
            Part[21, 9]:='^^iii^^^^^^';
            Part[21,10]:='^iiiiii^^^^';
            Part[21,11]:='^^^^^i^^^^^';

            Part[22, 1]:='^^^iii^^ii^';
            Part[22, 2]:='^^iiii^^iii';
            Part[22, 3]:='iiiii^^^iii';
            Part[22, 4]:='^^####ii^i^';
            Part[22, 5]:='^^#--+iiii^';
            Part[22, 6]:='ii####iiiii';
            Part[22, 7]:='iiiiiiiiiii';
            Part[22, 8]:='iiii^^iiiii';
            Part[22, 9]:='^ii^ii##+##';
            Part[22,10]:='^iiiii#---#';
            Part[22,11]:='^^^^^i#####';

            TotalParts:=22;
        end
        else
                TotalParts:=19;
    end;

    if (lvl=16) or (lvl=17) or (lvl=18) then
    begin
        // labyrinth
        Part[1, 1]:='#####.#####';
        Part[1, 2]:='#...#.#####';
        Part[1, 3]:='#........##';
        Part[1, 4]:='########.##';
        Part[1, 5]:='########.##';
        Part[1, 6]:='...........';
        Part[1, 7]:='##.##.##.##';
        Part[1, 8]:='#...#.##.##';
        Part[1, 9]:='#...#.##.##';
        Part[1,10]:='#####.##.##';
        Part[1,11]:='#####.##.##';

        Part[2, 1]:='...........';
        Part[2, 2]:='.####.#####';
        Part[2, 3]:='.#....#####';
        Part[2, 4]:='##.########';
        Part[2, 5]:='##.########';
        Part[2, 6]:='.....#####.';
        Part[2, 7]:='##.#.#####.';
        Part[2, 8]:='##.#.......';
        Part[2, 9]:='##.########';
        Part[2,10]:='##....#####';
        Part[2,11]:='#####.#####';

        Part[3, 1]:='...........';
        Part[3, 2]:='.####.####.';
        Part[3, 3]:='.####.####.';
        Part[3, 4]:='#####.#####';
        Part[3, 5]:='...........';
        Part[3, 6]:='.#.#####.#.';
        Part[3, 7]:='.#.#####.#.';
        Part[3, 8]:='.#.......#.';
        Part[3, 9]:='.#########.';
        Part[3,10]:='...........';
        Part[3,11]:='#####.#####';

        Part[4, 1]:='#####.#####';
        Part[4, 2]:='.####.#####';
        Part[4, 3]:='.####.....#';
        Part[4, 4]:='.#..#.###.#';
        Part[4, 5]:='.#..#.###.#';
        Part[4, 6]:='.#..#......';
        Part[4, 7]:='.##.#.###.#';
        Part[4, 8]:='......#...#';
        Part[4, 9]:='.##.#.#####';
        Part[4,10]:='.#..#.#####';
        Part[4,11]:='.#..#.#####';

        Part[5, 1]:='#.#...#####';
        Part[5, 2]:='..#.###...#';
        Part[5, 3]:='###.#.###.#';
        Part[5, 4]:='..........#';
        Part[5, 5]:='#####.#####';
        Part[5, 6]:='......#....';
        Part[5, 7]:='#.###.###.#';
        Part[5, 8]:='#.#.#.#....';
        Part[5, 9]:='#.#.###.###';
        Part[5,10]:='#.....#.#..';
        Part[5,11]:='#####.....#';

        Part[6, 1]:='.....#.#...';
        Part[6, 2]:='.#####.####';
        Part[6, 3]:='.#.........';
        Part[6, 4]:='.#########.';
        Part[6, 5]:='.......#.#.';
        Part[6, 6]:='####...#.#.';
        Part[6, 7]:='####.###.#.';
        Part[6, 8]:='####.....#.';
        Part[6, 9]:='####.#####.';
        Part[6,10]:='####.......';
        Part[6,11]:='#####.#####';

        Part[7, 1]:='........###';
        Part[7, 2]:='.######.#..';
        Part[7, 3]:='.#....#.##.';
        Part[7, 4]:='.#..#.#....';
        Part[7, 5]:='.####.#####';
        Part[7, 6]:='......#....';
        Part[7, 7]:='.###..#.#.#';
        Part[7, 8]:='.#.#..###.#';
        Part[7, 9]:='.#.#......#';
        Part[7,10]:='.#.##.#####';
        Part[7,11]:='...##......';

        Part[8, 1]:='####.......';
        Part[8, 2]:='####.######';
        Part[8, 3]:='......#####';
        Part[8, 4]:='.####.#####';
        Part[8, 5]:='.#..#.#....';
        Part[8, 6]:='....#.#..#.';
        Part[8, 7]:='#####.####.';
        Part[8, 8]:='...........';
        Part[8, 9]:='#########.#';
        Part[8,10]:='#.........#';
        Part[8,11]:='#####.#####';

        Part[9, 1]:='####...####';
        Part[9, 2]:='...#.#.....';
        Part[9, 3]:='.#########.';
        Part[9, 4]:='.#...#...#.';
        Part[9, 5]:='.#.#.#.#.#.';
        Part[9, 6]:='...#.#.#...';
        Part[9, 7]:='.#.#.###.#.';
        Part[9, 8]:='.#.#.....#.';
        Part[9, 9]:='.#.#.#...#.';
        Part[9,10]:='.#.#######.';
        Part[9,11]:='##.........';


        TotalParts:=9;
    end;


    if (lvl=19) or (lvl=23) or (lvl=24) or (lvl=25) then
    begin
        // halls of the ancients
        Part[1, 1]:='$$$$$W$$$$$';
        Part[1, 2]:='$$$$$W$$$$$';
        Part[1, 3]:='$$$$$W$$$$$';
        Part[1, 4]:='$$$$$W$$$$$';
        Part[1, 5]:='$$$$$W$$$$$';
        Part[1, 6]:='WWWWWWWWWWW';
        Part[1, 7]:='$$$$$W$$$$$';
        Part[1, 8]:='$$$$$W$$$$$';
        Part[1, 9]:='$$$$$W$$$$$';
        Part[1,10]:='$$$$$W$$$$$';
        Part[1,11]:='$$$$$W$$$$$';
    
        Part[2, 1]:='$$$$$$$$$$$';
        Part[2, 2]:='$$-----$$$$';
        Part[2, 3]:='$$-----$$$$';
        Part[2, 4]:='$$-----$$$$';
        Part[2, 5]:='$$$$+$$$$$$';
        Part[2, 6]:='WWWWWWWWWWW';
        Part[2, 7]:='$$$$$$$$$$$';
        Part[2, 8]:='$$$$$$$$$$$';
        Part[2, 9]:='$$$$$$$$$$$';
        Part[2,10]:='$$$$$$$$$$$';
        Part[2,11]:='$$$$$$$$$$$';
    
        Part[3, 1]:='$$$$$W$$$$$';
        Part[3, 2]:='$WWWWW$$$$$';
        Part[3, 3]:='$W$$$$$$$$$';
        Part[3, 4]:='$W$$$$$$$$$';
        Part[3, 5]:='$W$$$$$$$$$';
        Part[3, 6]:='$WWWWwWWWWW';
        Part[3, 7]:='$$$$$W$$$$$';
        Part[3, 8]:='$$$$$W$$$$$';
        Part[3, 9]:='$$$$$W$$$$$';
        Part[3,10]:='$$$$$W$$$$$';
        Part[3,11]:='$$$$$W$$$$$';
    
        Part[4, 1]:='$$$$$W$$$$$';
        Part[4, 2]:='$---$W$$$$$';
        Part[4, 3]:='$---+W$$$$$';
        Part[4, 4]:='$---$W$$$$$';
        Part[4, 5]:='$$$$$W$$$$$';
        Part[4, 6]:='WW$$$WWWWWW';
        Part[4, 7]:='$W$$$W$$$$$';
        Part[4, 8]:='$W$$$W$$$$$';
        Part[4, 9]:='$WWWWW$$$$$';
        Part[4,10]:='$$$$$W$$$$$';
        Part[4,11]:='$$$$$+$$$$$';
    
        Part[5, 1]:='$$$$$W$$$$$';
        Part[5, 2]:='$$$$$W$$$$$';
        Part[5, 3]:='$$$$$W$$$$$';
        Part[5, 4]:='$$$$$W$$$$$';
        Part[5, 5]:='$$$$$W$$$$$';
        Part[5, 6]:='WWWWWWWWWWW';
        Part[5, 7]:='$W---W$$$$$';
        Part[5, 8]:='$W---W$$$$$';
        Part[5, 9]:='$W---W$$$$$';
        Part[5,10]:='$WWWWW$$$$$';
        Part[5,11]:='$$$$$W$$$$$';
    
        Part[6, 1]:='$$$$$.$$$$$';
        Part[6, 2]:='$$$$$.$$$$$';
        Part[6, 3]:='$$$$$.$$$$$';
        Part[6, 4]:='$$$$$.$$$$$';
        Part[6, 5]:='$$$$$.$$$$$';
        Part[6, 6]:='...........';
        Part[6, 7]:='$$.$$$$$$$$';
        Part[6, 8]:='$$.$$$$$$$$';
        Part[6, 9]:='$$.$$$$$$$$';
        Part[6,10]:='$$.$$$$$$$$';
        Part[6,11]:='$$....$$$$$';
    
        Part[7, 1]:='$$$$$W$$$$$';
        Part[7, 2]:='$$$$$WWW$$$';
        Part[7, 3]:='$$$$$$$W$$$';
        Part[7, 4]:='$$$$$$$W$$$';
        Part[7, 5]:='$$$$$$$W$$$';
        Part[7, 6]:='WWWWWW$WWWW';
        Part[7, 7]:='$$$$$W$$$$$';
        Part[7, 8]:='$$$$$W$$$$$';
        Part[7, 9]:='$$$$$W$$$$$';
        Part[7,10]:='$$$$$W$$$$$';
        Part[7,11]:='$$$$$W$$$$$';
    
        Part[8, 1]:='$$$$.$$$$$$';
        Part[8, 2]:='$W$$.$$$$$$';
        Part[8, 3]:='$W$$...W$$$';
        Part[8, 4]:='$W$$$$$W$$$';
        Part[8, 5]:='$W$$$$$.$$$';
        Part[8, 6]:='+WWW....WWW';
        Part[8, 7]:='$W$$$$$$$$$';
        Part[8, 8]:='$W$-------$';
        Part[8, 9]:='$W+-------$';
        Part[8,10]:='$$$-------$';
        Part[8,11]:='$$$$$W$$$$$';
    
        Part[9, 1]:='$$$$$W$$$$$';
        Part[9, 2]:='$WWWWWWWWW$';
        Part[9, 3]:='$W$$$$$$$W$';
        Part[9, 4]:='$W$$$$$$$W$';
        Part[9, 5]:='$W$$$$$$$W$';
        Part[9, 6]:='+WWWWWWWWWW';
        Part[9, 7]:='$$$$$+$$$$$';
        Part[9, 8]:='$---$-$---$';
        Part[9, 9]:='$---+-+---$';
        Part[9,10]:='$---$-$---$';
        Part[9,11]:='$$$$$+$$$$$';
    
        Part[10, 1]:='$$$$$W$$$$$';
        Part[10, 2]:='$WWWWWWWWW$';
        Part[10, 3]:='$WWWWWWWWW$';
        Part[10, 4]:='$WW...W.WW$';
        Part[10, 5]:='$WW.....WW$';
        Part[10, 6]:='WWWW....WWW';
        Part[10, 7]:='$WW.....WW$';
        Part[10, 8]:='$WW.W.WWWW$';
        Part[10, 9]:='$WW.....WW$';
        Part[10,10]:='$WWWWWWWWW$';
        Part[10,11]:='$$$$$W$$$$$';

        Part[11, 1]:='$$$$$+$$$$$';
        Part[11, 2]:='$WWWWWWWWW$';
        Part[11, 3]:='$WWWWWWWWW$';
        Part[11, 4]:='$WW-----WW$';
        Part[11, 5]:='$WW-----WW$';
        Part[11, 6]:='+WW-----WW+';
        Part[11, 7]:='$WW-----WW$';
        Part[11, 8]:='$WW-----WW$';
        Part[11, 9]:='$WWWWWWWWW$';
        Part[11,10]:='$WWWWWWWWW$';
        Part[11,11]:='$$$$$+$$$$$';

        Part[12, 1]:='$$$$$W$$$$$';
        Part[12, 2]:='$$$$$W$$$$$';
        Part[12, 3]:='$$$$$W$$$$$';
        Part[12, 4]:='$$$$$+$$$$$';
        Part[12, 5]:='$$$$$W$$$$$';
        Part[12, 6]:='WWWWWWWWWWW';
        Part[12, 7]:='$$$$$W$$$$$';
        Part[12, 8]:='$$$$$+$$$$$';
        Part[12, 9]:='$$$$$W$$$$$';
        Part[12,10]:='$$$$$W$$$$$';
        Part[12,11]:='$$$$$W$$$$$';

        Part[13, 1]:='$$$$$W$$$$$';
        Part[13, 2]:='$$$$$W$$$$$';
        Part[13, 3]:='$$$$$.$$$$$';
        Part[13, 4]:='$$$$$.$$$$$';
        Part[13, 5]:='$$$$$.$$$$$';
        Part[13, 6]:='WWW+...+WWW';
        Part[13, 7]:='$$$$$W$$$$$';
        Part[13, 8]:='$$$$$.$$$$$';
        Part[13, 9]:='$$$$$W$$$$$';
        Part[13,10]:='$$$$$W$$$$$';
        Part[13,11]:='$$$$$W$$$$$';

        Part[14, 1]:='$$$$$+$$$$$';
        Part[14, 2]:='$WWWWWWWWW$';
        Part[14, 3]:='$WWWWWWWWW$';
        Part[14, 4]:='$WW--$--WW$';
        Part[14, 5]:='$WW-----WW$';
        Part[14, 6]:='+WW--$--WW+';
        Part[14, 7]:='$WW-----WW$';
        Part[14, 8]:='$WW--$--WW$';
        Part[14, 9]:='$WWWWWWWWW$';
        Part[14,10]:='$WWWWWWWWW$';
        Part[14,11]:='$$$$$+$$$$$';

        Part[15, 1]:='$$$$$W$$$$$';
        Part[15, 2]:='$WWWWWWWWW$';
        Part[15, 3]:='$WWWWWWWWW$';
        Part[15, 4]:='$WWWWWWWWW$';
        Part[15, 5]:='$WW$$$+$$W$';
        Part[15, 6]:='WWW$----$WW';
        Part[15, 7]:='$WW$----$W$';
        Part[15, 8]:='$WW$----$W$';
        Part[15, 9]:='$WW$$$$$$W$';
        Part[15,10]:='$WWWWWWWWW$';
        Part[15,11]:='$$$$$+$$$$$';

        Part[16, 1]:='$$$$$+$$$$$';
        Part[16, 2]:='$W$-----$W$';
        Part[16, 3]:='$W$-----$W$';
        Part[16, 4]:='$W$-----$W$';
        Part[16, 5]:='$W$$$+$$$W$';
        Part[16, 6]:='+WWWWWWWWWW';
        Part[16, 7]:='$$$$$+$$$$$';
        Part[16, 8]:='$-----+---$';
        Part[16, 9]:='$$+$$-$$$$$';
        Part[16,10]:='$---$-----$';
        Part[16,11]:='$$$$$+$$$$$';

        Part[17, 1]:='$$$$$+$$$$$';
        Part[17, 2]:='$.........$';
        Part[17, 3]:='$.WWWWWWW.$';
        Part[17, 4]:='$.WWWWWWW.$';
        Part[17, 5]:='$.WWWWWWW.$';
        Part[17, 6]:='+.WWWwWWW.+';
        Part[17, 7]:='$.WWWWWWW.$';
        Part[17, 8]:='$.WWWWWWW.$';
        Part[17, 9]:='$.WWWWWWW.$';
        Part[17,10]:='$.........$';
        Part[17,11]:='$$$$$+$$$$$';

        Part[18, 1]:='$$$$$+$$$$$';
        Part[18, 2]:='$$$$$O$$$$$';
        Part[18, 3]:='$$$$$O$$$$$';
        Part[18, 4]:='$$$$$O$$$$$';
        Part[18, 5]:='$$$$$O$$$$$';
        Part[18, 6]:='+OOOOOOOOO+';
        Part[18, 7]:='$$$$$O$$$$$';
        Part[18, 8]:='$$$$$O$$$$$';
        Part[18, 9]:='$$$$$O$$$$$';
        Part[18,10]:='$$$$$O$$$$$';
        Part[18,11]:='$$$$$+$$$$$';

        Part[19, 1]:='$$$$$+$$$$$';
        Part[19, 2]:='$$$OOOOO$$$';
        Part[19, 3]:='$$$OOOOO$$$';
        Part[19, 4]:='$$$OOOOO$$$';
        Part[19, 5]:='$$$$$+$$$$$';
        Part[19, 6]:='...........';
        Part[19, 7]:='$$$$$+$$$$$';
        Part[19, 8]:='$$$$OOO$$$$';
        Part[19, 9]:='$$$$OOO$$$$';
        Part[19,10]:='$$$$OOO$$$$';
        Part[19,11]:='$$$$$+$$$$$';

        Part[20, 1]:='$$$$$.$$$$$';
        Part[20, 2]:='$$$$$.$$$$$';
        Part[20, 3]:='$$$$$+$$$$$';
        Part[20, 4]:='$$$OOOOO$$$';
        Part[20, 5]:='$$$OOOOO$$$';
        Part[20, 6]:='..+OOOOO+..';
        Part[20, 7]:='$$$OOOOO$$$';
        Part[20, 8]:='$$$OOOOO$$$';
        Part[20, 9]:='$$$$$+$$$$$';
        Part[20,10]:='$$$$$.$$$$$';
        Part[20,11]:='$$$$$.$$$$$';

        TotalParts:=20;
    end;

    if lvl=20 then
    begin
        // lava, rock and great halls
        Part[1, 1]:='lllllllllll';
        Part[1, 2]:='lllllllllll';
        Part[1, 3]:='ll^^lllllll';
        Part[1, 4]:='lll^^llllll';
        Part[1, 5]:='lllllllllll';
        Part[1, 6]:='lllllllllll';
        Part[1, 7]:='lllll^^llll';
        Part[1, 8]:='llllll^llll';
        Part[1, 9]:='lllllll^lll';
        Part[1,10]:='lllllllllll';
        Part[1,11]:='lllllllllll';
    
        Part[2, 1]:='llll^+^llll';
        Part[2, 2]:='ll^^^.^^^ll';
        Part[2, 3]:='l^^.....^^l';
        Part[2, 4]:='l^.......^l';
        Part[2, 5]:='^^.......^^';
        Part[2, 6]:='+.........+';
        Part[2, 7]:='^^.......^^';
        Part[2, 8]:='l^^.....^^l';
        Part[2, 9]:='ll^^^+^^^ll';
        Part[2,10]:='llll^.^llll';
        Part[2,11]:='llll^.^llll';
    
        Part[3, 1]:='lll^^.^^lll';
        Part[3, 2]:='ll^^...^^ll';
        Part[3, 3]:='l^^.....^^l';
        Part[3, 4]:='l^.......^l';
        Part[3, 5]:='^^.......^^';
        Part[3, 6]:='...........';
        Part[3, 7]:='^^.......^^';
        Part[3, 8]:='l^.......^l';
        Part[3, 9]:='l^.......^l';
        Part[3,10]:='l^^.....^^l';
        Part[3,11]:='ll^^^.^^^ll';
    
        Part[4, 1]:='ll^......^^';
        Part[4, 2]:='l^^.......^';
        Part[4, 3]:='^^.......^^';
        Part[4, 4]:='^^........^';
        Part[4, 5]:='l^^^^....^^';
        Part[4, 6]:='ll^^^^..^^^';
        Part[4, 7]:='l^^^.....^l';
        Part[4, 8]:='l^.....^^^l';
        Part[4, 9]:='^^......^^^';
        Part[4,10]:='l^^^.....^^';
        Part[4,11]:='ll^^^.^^^^l';
    
        Part[5, 1]:='lllll.lllll';
        Part[5, 2]:='llll..lllll';
        Part[5, 3]:='llll..lllll';
        Part[5, 4]:='lllll.lllll';
        Part[5, 5]:='lllll..ll.l';
        Part[5, 6]:='...........';
        Part[5, 7]:='ll....ll...';
        Part[5, 8]:='lllll.lllll';
        Part[5, 9]:='lllll.lllll';
        Part[5,10]:='lllll..llll';
        Part[5,11]:='lllll.lllll';

        Part[6, 1]:='llll^.^llll';
        Part[6, 2]:='ll^^^.^^^ll';
        Part[6, 3]:='l^^.....^^l';
        Part[6, 4]:='l^...^...^l';
        Part[6, 5]:='^^...^...^^';
        Part[6, 6]:='...^.^^....';
        Part[6, 7]:='^^....^..^^';
        Part[6, 8]:='l^^...^.^^l';
        Part[6, 9]:='ll^^^.^^^ll';
        Part[6,10]:='llll^.^llll';
        Part[6,11]:='llll^.^llll';
    
        Part[7, 1]:='lll^^.^^lll';
        Part[7, 2]:='ll^^...^^ll';
        Part[7, 3]:='l^^..^..^^l';
        Part[7, 4]:='l^...^.^.^l';
        Part[7, 5]:='^^^^^....^^';
        Part[7, 6]:='....^^^^...';
        Part[7, 7]:='^^...^...^^';
        Part[7, 8]:='l^.^.^...^l';
        Part[7, 9]:='l^...^^..^l';
        Part[7,10]:='l^^.....^^l';
        Part[7,11]:='ll^^^.^^^ll';

        Part[8, 1]:='lll^^.^^^ll';
        Part[8, 2]:='l^^^^...^^l';
        Part[8, 3]:='^^..^^^..^^';
        Part[8, 4]:='^^..^.^^.^^';
        Part[8, 5]:='^^..^.^^.^^';
        Part[8, 6]:='^^..^......';
        Part[8, 7]:='^^^....^..^';
        Part[8, 8]:='l^^^^.^^.^^';
        Part[8, 9]:='ll^^^.^.^^l';
        Part[8,10]:='lll^^^.^^ll';
        Part[8,11]:='llll^.^^lll';
    
        Part[9, 1]:='l^^^^.^^^ll';
        Part[9, 2]:='^^^^^.^^^^l';
        Part[9, 3]:='^.^^^^.^^^^';
        Part[9, 4]:='^.^^^^^.^^^';
        Part[9, 5]:='^.^^^^^^.^^';
        Part[9, 6]:='.......^...';
        Part[9, 7]:='^.^^^^^^.^^';
        Part[9, 8]:='^^.^^^^^.^^';
        Part[9, 9]:='^^^.^^^^.^^';
        Part[9,10]:='l^^^......^';
        Part[9,11]:='lll^^.^^^^^';

        Part[10, 1]:='llll^.^llll';
        Part[10, 2]:='l^^^^.^^^ll';
        Part[10, 3]:='^^.....^^^l';
        Part[10, 4]:='^....l..^^l';
        Part[10, 5]:='^...lll..^^';
        Part[10, 6]:='..lllllll..';
        Part[10, 7]:='^..lllll..^';
        Part[10, 8]:='^^..lll.^^';
        Part[10, 9]:='^^^...l..^l';
        Part[10,10]:='l^^^....^^l';
        Part[10,11]:='lll^^.^^^ll';

        Part[11, 1]:='^.^llllllll';
        Part[11, 2]:='^.^llllllll';
        Part[11, 3]:='^.^^lllllll';
        Part[11, 4]:='^..^^.^^lll';
        Part[11, 5]:='.......^^ll';
        Part[11, 6]:='...^....^^l';
        Part[11, 7]:='...^^....^l';
        Part[11, 8]:='^...^^...^l';
        Part[11, 9]:='^....^^..^l';
        Part[11,10]:='^^....^.^^l';
        Part[11,11]:='l^^...^^^ll';

        Part[12, 1]:='lllllll.lll';
        Part[12, 2]:='llllll.llll';
        Part[12, 3]:='lllll.lllll';
        Part[12, 4]:='lllll.lllll';
        Part[12, 5]:='llllll....l';
        Part[12, 6]:='lll...l.ll.';
        Part[12, 7]:='...lll..lll';
        Part[12, 8]:='lllllll.lll';
        Part[12, 9]:='lllll..llll';
        Part[12,10]:='lllll.lllll';
        Part[12,11]:='lllll.lllll';

        Part[13, 1]:='llllllll^.^';
        Part[13, 2]:='llllllll^.^';
        Part[13, 3]:='ll^^^ll^..^';
        Part[13, 4]:='l^^.^^l^.^l';
        Part[13, 5]:='^.....^^.^l';
        Part[13, 6]:='..^^.......';
        Part[13, 7]:='^...^^^^.^l';
        Part[13, 8]:='^........^l';
        Part[13, 9]:='^...^^...^l';
        Part[13,10]:='^^....^.^^l';
        Part[13,11]:='l^^^^.^^^ll';

        TotalParts:=13;
    end;

    if lvl=21 then
    begin
        // ice level
        Part[1, 1]:='SSSSSSSSSSS';
        Part[1, 2]:='SSSSSSSISSS';
        Part[1, 3]:='SSSSSSSSSSS';
        Part[1, 4]:='SSSISSSSIIS';
        Part[1, 5]:='SSSISSSIISS';
        Part[1, 6]:='SSSSSSSSSSS';
        Part[1, 7]:='SSSSSSSSSSS';
        Part[1, 8]:='SSSSSIIISSS';
        Part[1, 9]:='ISSSIISSSSS';
        Part[1,10]:='SISSSIIISSS';
        Part[1,11]:='SSSSSSSSSSS';

        Part[2, 1]:='SSSSSSSSSSS';
        Part[2, 2]:='SSSSSSSSSSS';
        Part[2, 3]:='SSSSSSSSSSS';
        Part[2, 4]:='SSS########';
        Part[2, 5]:='SSS#...S.SS';
        Part[2, 6]:='SSS#..SSSSS';
        Part[2, 7]:='SSS#.SS.SSS';
        Part[2, 8]:='SSS#S.SS.S.';
        Part[2, 9]:='SSSSSSSSS..';
        Part[2,10]:='SSSSSS#####';
        Part[2,11]:='SSSSSSSSSSS';

        Part[3, 1]:='SSSSSSSS.SS';
        Part[3, 2]:='SSSSSSISS.S';
        Part[3, 3]:='S##SIII.##S';
        Part[3, 4]:='SSSIIII..IS';
        Part[3, 5]:='SSII..II.SS';
        Part[3, 6]:='S##I..II##S';
        Part[3, 7]:='SSI..IIIIIS';
        Part[3, 8]:='SSSII.IIISS';
        Part[3, 9]:='S##SI.II##S';
        Part[3,10]:='SSSIIIISSSS';
        Part[3,11]:='SSSSS.SSSSS';

        Part[4, 1]:='SSSS^^SSSSS';
        Part[4, 2]:='SSSS^^^^SSS';
        Part[4, 3]:='SS^^^^^^^^S';
        Part[4, 4]:='S^^^^^^SSSS';
        Part[4, 5]:='SSSS^^^^SSS';
        Part[4, 6]:='SSSSSS^^^^S';
        Part[4, 7]:='SSSSSSSS^^S';
        Part[4, 8]:='SS^^SSSSSSS';
        Part[4, 9]:='S^^^^SSSSSS';
        Part[4,10]:='SS^^^^SSSSS';
        Part[4,11]:='SSSS^^^^^SS';

        Part[5, 1]:='SSS^SS^^^SS';
        Part[5, 2]:='SSSSS^^SSSS';
        Part[5, 3]:='SSSSSSSSSSS';
        Part[5, 4]:='SSSSSSSSSSS';
        Part[5, 5]:='SSSSS#SSSSS';
        Part[5, 6]:='SSS..#SSSSS';
        Part[5, 7]:='SS####SSSSS';
        Part[5, 8]:='SSSSSSSSSSS';
        Part[5, 9]:='SSSSS#####S';
        Part[5,10]:='SSSSS#..SSS';
        Part[5,11]:='SSSSS#SS.SS';

        Part[6, 1]:='SSSSSSSSSSS';
        Part[6, 2]:='SSSSSS^SSSS';
        Part[6, 3]:='SSSSS^^^^SS';
        Part[6, 4]:='SSSSSSII^^S';
        Part[6, 5]:='SS,,SSSII^^';
        Part[6, 6]:='S,PP,SSS^^S';
        Part[6, 7]:='SS,,L,SSSSS';
        Part[6, 8]:='SSSS^^SSSSS';
        Part[6, 9]:='SSSSS^^SSSS';
        Part[6,10]:='SSSSSSSSSSS';
        Part[6,11]:='SSSSSSSSSSS';

        Part[7, 1]:='SSSSSSSSSSS';
        Part[7, 2]:='SSS^^^^SSSS';
        Part[7, 3]:='SSSSSS^^^^S';
        Part[7, 4]:='SSSSSSSSSSS';
        Part[7, 5]:='SSSSSSSSSSS';
        Part[7, 6]:='SSSSSSSSSSS';
        Part[7, 7]:='SSS^^SSSSS#';
        Part[7, 8]:='SS^^SSSSSS#';
        Part[7, 9]:='SSSSSSSISS#';
        Part[7,10]:='SSSSSSIISS#';
        Part[7,11]:='SSSSSSSSSSS';

        Part[8, 1]:='SSSSSSSSSSS';
        Part[8, 2]:='S######SSSS';
        Part[8, 3]:='S.....#SSSS';
        Part[8, 4]:='SSSS..#SSSS';
        Part[8, 5]:='SSSSS.#S,,S';
        Part[8, 6]:='SSSSSSS,PL,';
        Part[8, 7]:='SSSS.S,PL,S';
        Part[8, 8]:='SSSS.SS,,SS';
        Part[8, 9]:='SSSS...ISSS';
        Part[8,10]:='SSSSSSIISSS';
        Part[8,11]:='SSSSSSSSSSS';

        Part[9, 1]:='SSSSSSSSSSS';
        Part[9, 2]:='S###SSSSSSS';
        Part[9, 3]:='S#....SSSSS';
        Part[9, 4]:='S#....SSSSS';
        Part[9, 5]:='SS.SS.##SSS';
        Part[9, 6]:='SS..SS....S';
        Part[9, 7]:='SS...SS..SS';
        Part[9, 8]:='S#...SSSSSS';
        Part[9, 9]:='S#.....SSSS';
        Part[9,10]:='S#.SSSSSSSS';
        Part[9,11]:='SSSSSSSSSSS';

        Part[10, 1]:='SS,,,S,SSSS';
        Part[10, 2]:='S,PPL,P,,SS';
        Part[10, 3]:='SS,####,L,S';
        Part[10, 4]:='SSS#..#S,SS';
        Part[10, 5]:='SSS#..#SSSS';
        Part[10, 6]:='SSS#..SSSSS';
        Part[10, 7]:='SSS......SS';
        Part[10, 8]:='SS#####..SS';
        Part[10, 9]:='SS....#.SSS';
        Part[10,10]:='SS....#SSSS';
        Part[10,11]:='SSSSSSSSSSS';

        Part[11, 1]:='SSSSSSSSSSS';
        Part[11, 2]:='SSS#SSSS..S';
        Part[11, 3]:='SSS#SSS....';
        Part[11, 4]:='SSSSSS##S.S';
        Part[11, 5]:='SSSSSS#.SSS';
        Part[11, 6]:='SSS#SSSSSSS';
        Part[11, 7]:='SSSSSSS#SSS';
        Part[11, 8]:='SSSSSSSS,,S';
        Part[11, 9]:='#.SSS#S,LL,';
        Part[11,10]:='#..SS,PPP,L';
        Part[11,11]:='####SS,L,P,';

        Part[12, 1]:='^^^^^.^^^^^';
        Part[12, 2]:='^^^^^.^^^^^';
        Part[12, 3]:='^^^^^.^^^^^';
        Part[12, 4]:='^^^^^.^^^^^';
        Part[12, 5]:='^^^^^.^^^^^';
        Part[12, 6]:='...........';
        Part[12, 7]:='^^^^^.^^^^^';
        Part[12, 8]:='^^^^^.^^^^^';
        Part[12, 9]:='^^^^^.^^^^^';
        Part[12,10]:='^^^^^.^^^^^';
        Part[12,11]:='^^^^^.^^^^^';

        Part[13, 1]:='^^^^^.^^^^^';
        Part[13, 2]:='^^^^^.^^^^^';
        Part[13, 3]:='^^^^^.^^^^^';
        Part[13, 4]:='^^.......^^';
        Part[13, 5]:='^^..SSSS.^^';
        Part[13, 6]:='......SSSSS';
        Part[13, 7]:='^^.......SS';
        Part[13, 8]:='^^......SSS';
        Part[13, 9]:='^^^^^.^^^^^';
        Part[13,10]:='^^^^^.^^^^^';
        Part[13,11]:='^^^^^.^^^^^';

        Part[14, 1]:='^^^^^.^^^^^';
        Part[14, 2]:='^^^^^.^^^^^';
        Part[14, 3]:='^.....%%%.^';
        Part[14, 4]:='^.......%.^';
        Part[14, 5]:='^....SSS..^';
        Part[14, 6]:='..SS..SSS..';
        Part[14, 7]:='SSSSS.....^';
        Part[14, 8]:='SSSS......^';
        Part[14, 9]:='SSS.......^';
        Part[14,10]:='^^^^^.^^^^^';
        Part[14,11]:='^^^^^.^^^^^';

        Part[15, 1]:='^^^^^.^^^^^';
        Part[15, 2]:='^^^^^.^^^^^';
        Part[15, 3]:='^..S^.^...^';
        Part[15, 4]:='^SSS^.^...^';
        Part[15, 5]:='^^S^^.^^S^^';
        Part[15, 6]:='%%%SSSSSS..';
        Part[15, 7]:='^I%I%%%...^';
        Part[15, 8]:='^%I%%..II.^';
        Part[15, 9]:='^IIIII.III^';
        Part[15,10]:='^^^^^.^^^^^';
        Part[15,11]:='^^^^^.^^^^^';

        Part[16, 1]:='^^^^^.^^^^^';
        Part[16, 2]:='^.,,L,,,,,^';
        Part[16, 3]:='^.^,,,PL,.^';
        Part[16, 4]:='^.^^^,,PP.^';
        Part[16, 5]:='S.SS^^^^^.^';
        Part[16, 6]:='SSS........';
        Part[16, 7]:='^^^^^.^^^^^';
        Part[16, 8]:='^SSSS.SSSSS';
        Part[16, 9]:='^SS..I.SSSS';
        Part[16,10]:='^S...I^SSSS';
        Part[16,11]:='^^^^^I^^^^^';

        Part[17, 1]:='^^^^^I^^^^^';
        Part[17, 2]:='^^^^^.^^^^^';
        Part[17, 3]:='^^^^^.^^^^^';
        Part[17, 4]:='^^^^^.^^^^^';
        Part[17, 5]:='^^^^^.^^^^^';
        Part[17, 6]:='....SS^^^^^';
        Part[17, 7]:='^^^^^S^^^^^';
        Part[17, 8]:='^...%%%.%.^';
        Part[17, 9]:='^.SS.%.%..^';
        Part[17,10]:='^^^^^S^^^^^';
        Part[17,11]:='^^^^^.^^^^^';

        Part[18, 1]:='^^^^^......';
        Part[18, 2]:='^^^^^.^^^^.';
        Part[18, 3]:='^^^^^.^^^^.';
        Part[18, 4]:='^^^^^.^^^^.';
        Part[18, 5]:='^^^^^.^^^^.';
        Part[18, 6]:='...........';
        Part[18, 7]:='^^^^^.^^^^.';
        Part[18, 8]:='^^^^^.^^^^.';
        Part[18, 9]:='^^^^^.^^^^.';
        Part[18,10]:='^^^^^.^^^^.';
        Part[18,11]:='^^^^^......';

        Part[19, 1]:='^^^^^.^^^^^';
        Part[19, 2]:='^^^^^S^^^^^';
        Part[19, 3]:='^^^^^SS^^^^';
        Part[19, 4]:='^^^^SSSS^^^';
        Part[19, 5]:='^^^^S.^^^^^';
        Part[19, 6]:='..%....SSSS';
        Part[19, 7]:='^^^^^.^^^SS';
        Part[19, 8]:='^^^^^.^^^^^';
        Part[19, 9]:='^^^^^.^^^^^';
        Part[19,10]:='^^^^^.^^^^^';
        Part[19,11]:='^^^^^.^^^^^';

        Part[20, 1]:='^^^^^.^^^^^';
        Part[20, 2]:='^^^^^.^^^^^';
        Part[20, 3]:='^^^^^.^^^^^';
        Part[20, 4]:='^^^^^.^^^^^';
        Part[20, 5]:='^^^^^.^^^^^';
        Part[20, 6]:='.....^.....';
        Part[20, 7]:='^^^^.^.^^^^';
        Part[20, 8]:='^^^^.^.^^^^';
        Part[20, 9]:='^^^^.^.^^^^';
        Part[20,10]:='^^^^.^.^^^^';
        Part[20,11]:='^^^^^.^^^^^';

        Part[21, 1]:='^^^^^.^^^^^';
        Part[21, 2]:='^^^^^.^^^^^';
        Part[21, 3]:='^^^^^.^^^^^';
        Part[21, 4]:='^.%%..^^^^^';
        Part[21, 5]:='^.....^^^^^';
        Part[21, 6]:='...........';
        Part[21, 7]:='^.....^^^^^';
        Part[21, 8]:='^^^^^.^^^^^';
        Part[21, 9]:='^^^^^.^^^^^';
        Part[21,10]:='^^^^^.^^^^^';
        Part[21,11]:='^^^^^.^^^^^';

        Part[22, 1]:='^^^^^.^^^^^';
        Part[22, 2]:='^^^^^.^^S^^';
        Part[22, 3]:='^^^^^.^^S^^';
        Part[22, 4]:='^^^^^.^^S^^';
        Part[22, 5]:='^^^^^.^S^^^';
        Part[22, 6]:='...........';
        Part[22, 7]:='^SS^^.^^^^^';
        Part[22, 8]:='^SSS^.^^^^^';
        Part[22, 9]:='SSS^^.^^^^^';
        Part[22,10]:='^SSS^.^^^^^';
        Part[22,11]:='^^^^^.^^^^^';

        Part[23, 1]:='^^^^^.^^^^^';
        Part[23, 2]:='^^^^^.^^^^^';
        Part[23, 3]:='^^^^^SSS^^^';
        Part[23, 4]:='^^^SSSS^^^^';
        Part[23, 5]:='^^^^^.^^^^^';
        Part[23, 6]:='...........';
        Part[23, 7]:='^^^^^I^^^^^';
        Part[23, 8]:='^^^ISSS^^^^';
        Part[23, 9]:='^^^^IISSSI^';
        Part[23,10]:='^^^^^.^^^^^';
        Part[23,11]:='^^^^^.^^^^^';

        Part[24, 1]:='^^^^^.^^^^^';
        Part[24, 2]:='^^^^^.^^^^^';
        Part[24, 3]:='^^^^^.^^^^^';
        Part[24, 4]:='^^^^^.^^^^^';
        Part[24, 5]:='^^^^^.^^^^^';
        Part[24, 6]:='...........';
        Part[24, 7]:='^^^^^.^^^^^';
        Part[24, 8]:='^^^^^.^^^^^';
        Part[24, 9]:='^^^^^.^^^^^';
        Part[24,10]:='^^^^^.^^^^^';
        Part[24,11]:='^^^^^.^^^^^';

        Part[25, 1]:='^^^^^.^^^^^';
        Part[25, 2]:='^^^^^.^^^^^';
        Part[25, 3]:='^^^^^.^^^^^';
        Part[25, 4]:='^^^^^.^^^^^';
        Part[25, 5]:='^^^^^.^^^^^';
        Part[25, 6]:='^^^^^......';
        Part[25, 7]:='^^^^^^^^^^^';
        Part[25, 8]:='^^^^^.^^^^^';
        Part[25, 9]:='^^^^^.^^^^^';
        Part[25,10]:='^^^^^.^^^^^';
        Part[25,11]:='^^^^^.^^^^^';

        Part[26, 1]:='^^^^^.^^^^^';
        Part[26, 2]:='^^^^^.^^^^^';
        Part[26, 3]:='^^^^^.^^^^^';
        Part[26, 4]:='^^^^^.^^^^^';
        Part[26, 5]:='^^^^^.^^^^^';
        Part[26, 6]:='^^^^^.%....';
        Part[26, 7]:='^^^^^.^^^^^';
        Part[26, 8]:='^^^^^.^^^^^';
        Part[26, 9]:='^^^^^.^^^^^';
        Part[26,10]:='^^^^^.^^^^^';
        Part[26,11]:='^^^^^.^^^^^';

        TotalParts:=26;
    end;

    i:=0;

    while i<50 do  // 90
    begin
        inc(i,11);

        n:=1+trunc(random(TotalParts));
        for j:=1 to 11 do
            Landstring[y+j]:=Landstring[y+j]+Part[n,j];
    end;
end;

procedure CreatePuzzleLandscape (lvl: integer);
var
    i, x, y: integer;
    MapFile: textfile;
    tl, tr: String;
begin

//     writeln (' ');

    for i:=1 to 150 do            // 150 only to clear the max. available array
        Landstring[i]:='';

//     GotoXY(2, 2);
//     Write('DLV ' + IntToStr(lvl));

//     GotoXY(20+lvl, 2);
//     Write('#');

    // base landscape
//     GotoXY(2, 5);
//     writeln ('basic landscape   ');
    y:=0;
    while y<51 do  // 73
    begin
        CreatePuzzleBase(y,0,lvl);
        inc(y,11);
    end;

     // river
    if (lvl=1) or (lvl=22) then
    begin
//          GotoXY(2, 5);
//          writeln ('river             ');
        x:=11*trunc(1+random(4));  // 7 (alle Korridore unten auch)
        if random(400)>200 then
            CreatePuzzleRiverV(x)
        else
            CreatePuzzleRiverH(x);
    end;

    // big corridor
    if lvl=3 then
    begin
        x:=11*trunc(1+random(4));
        CreatePuzzleCorridorV(x, lvl);
        x:=11*trunc(1+random(4));
        CreatePuzzleCorridorH(x, lvl);
    end;

    if lvl=6 then
    begin
        x:=11*trunc(1+random(4));
        CreatePuzzleCorridorV(x, lvl);
        x:=11*trunc(1+random(4));
        CreatePuzzleCorridorH(x, lvl);
    end;

    if lvl=21 then
    begin
        x:=11*trunc(1+random(4));
        CreatePuzzleCorridorV(x, lvl);
        //x:=11*trunc(1+random(7));
        //CreatePuzzleCorridorH(x, lvl);
    end;

    if lvl=23 then
    begin
        x:=11*trunc(1+random(4));
        CreatePuzzleCorridorV(x, lvl);
        x:=11*trunc(1+random(4));
        CreatePuzzleCorridorH(x, lvl);
    end;


    // optimize
    for i:=1 to y do
    begin
        // replace double doors by one door
        Landstring[i] := AnsiReplaceText(Landstring[i], '++', '+-');
        Landstring[i] := AnsiReplaceText(Landstring[i], '��', '�-');

        // smooth beaches
        Landstring[i] := AnsiReplaceText(Landstring[i], ',==', ',A=');
        Landstring[i] := AnsiReplaceText(Landstring[i], 'g==', 'gA=');
        Landstring[i] := AnsiReplaceText(Landstring[i], 'b==', 'bA=');
        Landstring[i] := AnsiReplaceText(Landstring[i], 't==', 'tA=');
        Landstring[i] := AnsiReplaceText(Landstring[i], 'h==', 'hA=');

        Landstring[i] := AnsiReplaceText(Landstring[i], '==,', '=J,');
        Landstring[i] := AnsiReplaceText(Landstring[i], '==g', '=Jg');
        Landstring[i] := AnsiReplaceText(Landstring[i], '==b', '=Jb');
        Landstring[i] := AnsiReplaceText(Landstring[i], '==t', '=Jt');
        Landstring[i] := AnsiReplaceText(Landstring[i], '==h', '=Jh');

        Landstring[i] := AnsiReplaceText(Landstring[i], 'A=,', 'AJ,');
        Landstring[i] := AnsiReplaceText(Landstring[i], 'A=g', 'AJg');
        Landstring[i] := AnsiReplaceText(Landstring[i], 'A=h', 'AJh');

        Landstring[i] := AnsiReplaceText(Landstring[i], '^=,', '^j,');
        Landstring[i] := AnsiReplaceText(Landstring[i], '^=g', '^jg');
        Landstring[i] := AnsiReplaceText(Landstring[i], '^=b', '^jb');
        Landstring[i] := AnsiReplaceText(Landstring[i], '^=t', '^jt');
        Landstring[i] := AnsiReplaceText(Landstring[i], '^=h', '^jh');

        Landstring[i] := AnsiReplaceText(Landstring[i], ',=J', ',AJ');
        Landstring[i] := AnsiReplaceText(Landstring[i], ',=^', ',j^');
        Landstring[i] := AnsiReplaceText(Landstring[i], ',=,', ',j,');
        Landstring[i] := AnsiReplaceText(Landstring[i], ',=g', ',jg');
        Landstring[i] := AnsiReplaceText(Landstring[i], ',=t', ',jt');
        Landstring[i] := AnsiReplaceText(Landstring[i], ',=b', ',jb');
        Landstring[i] := AnsiReplaceText(Landstring[i], ',=h', ',jh');

        Landstring[i] := AnsiReplaceText(Landstring[i], 'g=J', 'gAJ');
        Landstring[i] := AnsiReplaceText(Landstring[i], 'g=^', 'gj^');
        Landstring[i] := AnsiReplaceText(Landstring[i], 'g=g', 'gjg');
        Landstring[i] := AnsiReplaceText(Landstring[i], 'g=,', 'gj,');
        Landstring[i] := AnsiReplaceText(Landstring[i], 'g=b', 'gjb');
        Landstring[i] := AnsiReplaceText(Landstring[i], 'g=t', 'gjt');
        Landstring[i] := AnsiReplaceText(Landstring[i], 'g=h', 'gjh');

        Landstring[i] := AnsiReplaceText(Landstring[i], 't=,', 'tj,');
        Landstring[i] := AnsiReplaceText(Landstring[i], 't=t', 'tjt');
        Landstring[i] := AnsiReplaceText(Landstring[i], 't=b', 'tjb');
        Landstring[i] := AnsiReplaceText(Landstring[i], 't=h', 'tjh');
        Landstring[i] := AnsiReplaceText(Landstring[i], 't=g', 'tjg');

        Landstring[i] := AnsiReplaceText(Landstring[i], 'b=,', 'bj,');
        Landstring[i] := AnsiReplaceText(Landstring[i], 'b=b', 'bjb');
        Landstring[i] := AnsiReplaceText(Landstring[i], 'b=t', 'bjt');
        Landstring[i] := AnsiReplaceText(Landstring[i], 'b=h', 'bjh');
        Landstring[i] := AnsiReplaceText(Landstring[i], 'b=g', 'bjg');

        Landstring[i] := AnsiReplaceText(Landstring[i], 'h=J', 'hAJ');
        Landstring[i] := AnsiReplaceText(Landstring[i], 'h=h', 'hjh');
        Landstring[i] := AnsiReplaceText(Landstring[i], 'h=,', 'hj,');
        Landstring[i] := AnsiReplaceText(Landstring[i], 'h=g', 'hjg');
        Landstring[i] := AnsiReplaceText(Landstring[i], 'h=b', 'hjb');
        Landstring[i] := AnsiReplaceText(Landstring[i], 'h=t', 'hjt');
        Landstring[i] := AnsiReplaceText(Landstring[i], 'h=^', 'hA^');
        Landstring[i] := AnsiReplaceText(Landstring[i], 'h=#', 'hA#');

        // smooth border between grass and sand
        Landstring[i] := AnsiReplaceText(Landstring[i], 'g,,', 'gE,');
        Landstring[i] := AnsiReplaceText(Landstring[i], ',gg', ',Fg');
        Landstring[i] := AnsiReplaceText(Landstring[i], 'g,', 'FE');
        Landstring[i] := AnsiReplaceText(Landstring[i], ',g', 'EF');

        // make border left and right
        tl:=LeftStr(Landstring[i], length(Landstring[i])-1);
        Landstring[i]:=tl+'^';
        tr:=RightStr(Landstring[i], length(Landstring[i])-1);
        Landstring[i]:='^'+tr;
    end;
    Landstring[1] := AnsiReplaceText(Landstring[1], '.', '^');
    Landstring[1] := AnsiReplaceText(Landstring[1], '+', '^');
    Landstring[1] := AnsiReplaceText(Landstring[1], '�', '^');
    Landstring[y] := AnsiReplaceText(Landstring[y], '.', '^');
    Landstring[y] := AnsiReplaceText(Landstring[y], '+', '^');
    Landstring[y] := AnsiReplaceText(Landstring[y], '�', '^');


    // Miscellanous things (portals, NPCs and Unique Monster)
//     writeln ('- misc');
    CreatePuzzleMisc(lvl);


    // save level file

    Assign(MapFile, PathUserWritable+'random_levels/'+IntToStr(lvl)+'.txt');
    Rewrite(MapFile);
//     for i:=1 to 150 do
    for i:=1 to 50 do   // 80
        writeln(MapFile, Landstring[i]);
    Close(MapFile);
end;

end.
