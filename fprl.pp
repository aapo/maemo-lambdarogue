{
     Copyright (C) 2006-2009 by Mario Donick
     mario.donick@gmail.com
                                                                           
     This program is free software; you can redistribute it and/or modify   
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation; either version 2 of the License, or      
     (at your option) any later version.                                    
                                                                            
     This program is distributed in the hope that it will be useful,        
     but WITHOUT ANY WARRANTY; without even the implied warranty of         
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
     GNU General Public License for more details.                           
                                                                            
     You should have received a copy of the GNU General Public License      
     along with this program; if not, write to the                          
     Free Software Foundation, Inc.,                                        
     59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              
}

program fprl;

uses
    Constants, SDL, SDL_image, Crt, Video, VidUtil, Keyboard, SysUtils, StrUtils, Player, Items, Quests, Chants,
    RandomArea, LineOfSight, Dungeon, Plot, Input, CollectData, BaseOutput, Output, PuzzleMap, ExternMusic,
    ExternSFX;


var
    i, j, vx, vy, k: integer;
    blQuit, blStartGame: Boolean;
    chKey: Char;
    msg1, FunKey: string;
    intCenterX, intCenterY: integer;
    blAllRunes: Boolean;
    TitleSelection: integer;
    Knoepfe, KX, KY: longint;
    TempAnsi: Ansistring;

    DecoIcon, DecoIconS: SDL_RECT;

    CONST_MONSTERRECDIST, CONST_MONSTERFIRERATE, CONST_SPAWN: integer;

//Game uses strings and shortstrings, but SDL's IMG_LOAD needs PChar
function LR_IMG_LOAD(path: String): pSDL_SURFACE;
var 
  TempString: String;
  TempPChar: PChar;
begin
  TempString := Path+''#0;
  tempPChar := @(TempString)[1];
  LR_IMG_LOAD := IMG_LOAD(tempPChar);
end;

function ShowSavegames: string;
    var
        Info : TSearchRec;
        x, y, n, sn: integer;
        strCharaName, dummy, tmp: String;
        stop: Boolean;
        strFileList: array[97..162] of String;
        ch: Char;
begin
    ClearScreenSDL;

    for i:=97 to 162 do
        strFileList[i]:='-';

    if UseSDL=true then
    begin
        TempAnsi := PathData+'graphics/decobg.jpg';
        LoadImage_Title(PathData+'graphics/decobg.jpg');
    end;

    dummy:='-';
    tmp:='-';
    ch:='-';

    repeat

        if UseSDL=true then
            BlitImage_Title
        else
            StatusDeco;

        TransTextXY(1,1,'EXISTING CHARACTERS');

        y:=3;
        x:=1;
        n:=0;
        stop:=false;

        If FindFirst(PathUserWritable+'saves/*.lambdarogue', faAnyFile and faDirectory, Info) = 0 then
        begin
            repeat
                Inc(y);
                Inc(n);
                strCharaName := ExtractFileName(Info.Name);
                strCharaName := LeftStr(strCharaName, NPos('.lambdarogue', strCharaName, 1)-1);

                TransTextXY(x, y, chr(96+n)+'  '+strCharaName);
                strFileList[96+n]:=strCharaName;
                if y=18 then
                begin
                    y:=3;
                    case x of
                        1:
                            x:=21;
                        21:
                            x:=41;
                        41:
                            x:=61;
                        61:
                         
                            stop:=true;
                    end;
                end;
            until (stop=true) or (FindNext(info)<>0);
        end;
        FindClose(Info);

        if (x=1) and (y=3) then
        begin
            TransTextXY(1, 4, 'There are no games to continue.');
            dummy:=GetKeyInput('Press any key to return to main menu.', false);
        end
        else
            dummy:=GetKeyInput('Select a character by pressing the related key. Any other key for main menu.', false);

        ch:=dummy[1];

        if (dummy<>'ESC') and (ord(ch)>96) and (ord(ch)<=96+n) then
        begin
            sn:=ord(ch);
            tmp:=strFileList[sn];
        end
        else
            tmp:='ESC';

    until tmp<>'-';

    ShowSavegames:=tmp;

end;


procedure InitGraphics (blNew: boolean);
    var
        VideoInitResult: integer;
        FullscreenFlag: longword;
begin
    FullscreenFlag:=0;
    GlobalConColor:=-1;
    SetCursorType(crHidden); //MAEMO: hide cursor always

    if UseSDL=true then
    begin
        if UseFullScreen=true then
            FullscreenFlag := SDL_Fullscreen;

        if blNew=true then
        begin
            VideoInitResult := SDL_INIT(SDL_INIT_VIDEO);
            if VideoInitResult < 0 then
            begin
                Writeln('Error: Could not initialize SDL video. However, you can play this game');
                Writeln('in console mode by setting the option UseSDL to "= False" (without the');
                Writeln('quotation marks). To do so, edit ');
                Writeln(PathUserWritable+'lambdarogue.cfg');
                halt;
            end;

            SDL_ENABLEUNICODE(1);
            SDL_ENABLEKEYREPEAT(SDL_DEFAULT_REPEAT_DELAY, SDL_DEFAULT_REPEAT_INTERVAL);

            SDL_WM_SETCAPTION('LambdaRogue '+strVersion, 'LambdaRogue '+strVersion);
            //SDL_WM_SETICON(SDL_LOADBMP(PathData+'graphics/icon.bmp'),0); //SDL-bug on maemo5
       end;

        //With 800x600:
        //PlaceOfBottomBar:=26;

        if UseHiRes=false then
        begin
            screen := SDL_SETVIDEOMODE(800,480,24,SDL_HWSURFACE + FullscreenFlag);
            HiResOffsetX:=0;
            HiResOffsetY:=0;
            HiResTextOffsetX:=0;
            HiResTextOffsetY:=0;
            PlaceOfBottomBar:=20;
            ChantBarX:=167;
            ChantBarY:=4;
            LoadImage_Back(PathData+'graphics/bg-800.png');
        end
        else
        begin
            screen := SDL_SETVIDEOMODE(1024,768,24,SDL_HWSURFACE + FullscreenFlag);

            HiResOffsetX:=110;
            HiResTextOffsetX:=-12;
            ChantBarX:=219;
            ChantBarY:=9;
            LoadImage_Back(PathData+'graphics/bg-1024.png');

            if Netbook=true then
            begin
                HiResOffsetY:=0;
                HiResTextOffsetY:=0;
            end
            else
            begin
                HiResOffsetY:=80;
                HiResTextOffsetY:=3;
            end;
        end;

        if screen = nil then
            halt;
    
        if backgroundGraph = nil then
            halt;

        if blNew=true then
        begin
            if UseSmallTiles=true then
                tilesetASCII := LR_IMG_LOAD(PathData+'graphics/tileset-2-small.png')
            else
                tilesetASCII := LR_IMG_LOAD(PathData+'graphics/tileset-2-big.png');
            if tilesetASCII = nil then
                halt;
    
            tilesetGraph := LR_IMG_LOAD(PathData+'graphics/tileset-1.png');
            if tilesetGraph = nil then
                halt;

            LoadImage_Title(PathData+'graphics/title.jpg');

            if title = nil then
                halt;

            extratiles := LR_IMG_LOAD(PathData+'graphics/extra.png');
            if extratiles = nil then
                halt;

            // initiate "pixels" to draw with
            PixelWhiteRect.x:=300;
            PixelWhiteRect.y:=230;
            PixelWhiteRect.w:=1;
            PixelWhiteRect.h:=1;

            PixelGreyRect.x:=301;
            PixelGreyRect.y:=230;
            PixelGreyRect.w:=1;
            PixelGreyRect.h:=1;

            PixelRedRect.x:=302;
            PixelRedRect.y:=230;
            PixelRedRect.w:=1;
            PixelRedRect.h:=1;

            PixelBlueRect.x:=303;
            PixelBlueRect.y:=230;
            PixelBlueRect.w:=1;
            PixelBlueRect.h:=1;

            PixelGreenRect.x:=304;
            PixelGreenRect.y:=230;
            PixelGreenRect.w:=1;
            PixelGreenRect.h:=1;

            PixelBrownRect.x:=305;
            PixelBrownRect.y:=230;
            PixelBrownRect.w:=1;
            PixelBrownRect.h:=1;

            PixelOrangeRect.x:=306;
            PixelOrangeRect.y:=230;
            PixelOrangeRect.w:=1;
            PixelOrangeRect.h:=1;
        end
    end
    else
    begin
        HiResOffsetX:=0;
        HiResOffsetY:=0;
        HiResTextOffsetX:=0;
        HiResTextOffsetY:=0;
        PlaceOfBottomBar:=26;
        InitVideo;
        SetCursorType(crHidden);
        InitKeyboard;
    end;
end;

procedure StopGraphics;
begin
    if UseSDL=true then
    begin
        SDL_FREESURFACE(tilesetASCII);
        SDL_FREESURFACE(tilesetGraph);
        SDL_FREESURFACE(title);
        SDL_FREESURFACE(backgroundGraph);
        SDL_FREESURFACE(screen);
        SDL_FREESURFACE(extratiles);
        SDL_QUIT;
    end
    else
    begin
        DoneKeyboard;
        SetCursorType(crUnderline);
        DoneVideo;
    end;
end;

// returns true if the player has a quest to destroy the given level's monster hive
function IsTargetHive (level: integer): boolean;
    var i, j: Integer;
begin
    IsTargetHive:=false;
    for i:=1 to WinLevel do
        for j:=1 to 9 do
            if Quest[i,j].intTargetHive=level then
                if ThePlayer.intQuestState[i,j]=1 then
                    IsTargetHive:=true;
end;

// hive destroyed message
procedure HiveDestroyed;
begin
    DngLvl[HiveX,HiveY].intIntegrity:=0;
    HiveX:=-1;
    HiveY:=-1;
    ShowTransMessage('You destroy the monster'+chr(39)+'s hive.', false);
    Inc(ThePlayer.longEXP,DungeonLevel*100);
    Inc(ThePlayer.longScore,DungeonLevel*10);

    // check if player has "DestroyHive" Quest and this is the correct level
    // then set intNoHivesAnymore[level] to 1
    if IsTargetHive(DungeonLevel)=true then
    begin
        intNoHivesAnymore[DungeonLevel]:=1;
        ShowTransMessage('You have destroyed this hive forever. Return to your client.', false);
    end;
end;

// gas cylinder destroyed
procedure CylinderDestroyed (x, y: integer);
begin
    PlaySFX('explosion.mp3');

    ShowTransMessage('You destroy a gas cylinder; it explodes.', false);

    DngLvl[x,y].intIntegrity:=0;
    DngLvl[x,y].intFloorType:=31;
    DngLvl[x,y].intLight:=2;
    DngLvl[x,y].intAirType:=1;
    DngLvl[x,y].intAirRange:=8+trunc(random(10));

    DngLvl[x+1,y].intIntegrity:=0;
    DngLvl[x+1,y].intFloorType:=31;
    DngLvl[x+1,y].intLight:=2;
    DngLvl[x+1,y].intAirType:=1;
    DngLvl[x+1,y].intAirRange:=8+trunc(random(10));
    if DngLvl[x+1,y].intFloorType=56 then
        CylinderDestroyed(x+1,y);

    DngLvl[x+1,y+1].intIntegrity:=0;
    DngLvl[x+1,y+1].intFloorType:=31;
    DngLvl[x+1,y+1].intLight:=2;
    DngLvl[x+1,y+1].intAirType:=1;
    DngLvl[x+1,y+1].intAirRange:=8+trunc(random(10));
    if DngLvl[x+1,y+1].intFloorType=56 then
        CylinderDestroyed(x+1,y+1);

    DngLvl[x,y+1].intIntegrity:=0;
    DngLvl[x,y+1].intFloorType:=31;
    DngLvl[x,y+1].intLight:=2;
    DngLvl[x,y+1].intAirType:=1;
    DngLvl[x,y+1].intAirRange:=8+trunc(random(10));
    if DngLvl[x,y+1].intFloorType=56 then
        CylinderDestroyed(x,y+1);

    DngLvl[x-1,y+1].intIntegrity:=0;
    DngLvl[x-1,y+1].intFloorType:=31;
    DngLvl[x-1,y+1].intLight:=2;
    DngLvl[x-1,y+1].intAirType:=1;
    DngLvl[x-1,y+1].intAirRange:=8+trunc(random(10));
    if DngLvl[x-1,y+1].intFloorType=56 then
        CylinderDestroyed(x-1,y+1);

    DngLvl[x-1,y].intIntegrity:=0;
    DngLvl[x-1,y].intFloorType:=31;
    DngLvl[x-1,y].intLight:=2;
    DngLvl[x-1,y].intAirType:=1;
    DngLvl[x-1,y].intAirRange:=8+trunc(random(10));
    if DngLvl[x-1,y].intFloorType=56 then
        CylinderDestroyed(x-1,y);

    DngLvl[x-1,y-1].intIntegrity:=0;
    DngLvl[x-1,y-1].intFloorType:=31;
    DngLvl[x-1,y-1].intLight:=2;
    DngLvl[x-1,y-1].intAirType:=1;
    DngLvl[x-1,y-1].intAirRange:=8+trunc(random(10));
    if DngLvl[x-1,y-1].intFloorType=56 then
        CylinderDestroyed(x-1,y-1);

    DngLvl[x,y-1].intIntegrity:=0;
    DngLvl[x,y-1].intFloorType:=31;
    DngLvl[x,y-1].intLight:=2;
    DngLvl[x,y-1].intAirType:=1;
    DngLvl[x,y-1].intAirRange:=8+trunc(random(10));
    if DngLvl[x,y-1].intFloorType=56 then
        CylinderDestroyed(x,y-1);

end;

// barrel destroyed
procedure BarrelDestroyed (x, y: integer);
    var
        i, j, n: integer;
begin
    PlaySFX('destroy-barrel.mp3');
    DngLvl[x,y].intIntegrity:=0;
    ShowTransMessage('You destroy the barrel.', false);

    n:=240;

    if DngLvl[x,y].intItem=0 then
    begin
        if random(CONST_CHESTISEMPTY) > n then
        begin
            i:=0;
            repeat
                inc(i);
                j:=trunc(1+random(ItemCount));
                if (Thing[j].blUnique=false) and (Thing[j].blRare=false) and (Thing[j].blShopOnly=false) and (DungeonLevel>=Thing[j].intMinLvl) then
                begin
                    DngLvl[x,y].intItem:=j;
                    msg1:='You find something';
                end;
            until (i=2000) or (msg1='You find something');
        end;
    end;

end;


// this procedure is just for debugging
procedure CheatCodes;
    var
        strCommandline: string;
        strCmd: string;
        strCmdPara1, strCmdPara2: string;
        i, j: integer;
        longCmdPara1, longCmdPara2: longint;
begin
    strCommandline := GetTextInput(strVersion+'#'+IntToStr(longTotalTurns)+'>', 50);
    strCommandline:=strCommandline+' ';

    strCmd:='';
    strCmdPara1:='';
    strCmdPara2:='';
    longCmdPara1:=0;
    longCmdPara2:=0;

    // Get command
    i:=0;
    repeat
        strCmd:=strCmd+strCommandline[i];
        inc(i);
    until (strCommandline[i]=' ') or (i=length(strCommandline));
    strCmd:=trim(uppercase(strCmd));

    // Get parameters
    repeat
        strCmdPara1:=strCmdPara1+strCommandline[i];
        inc(i);
    until (strCommandline[i]=' ') or (i=length(strCommandline));
    strCmdPara1:=trim(uppercase(strCmdPara1));
    val(strCmdPara1, longCmdPara1);

    repeat
        strCmdPara2:=strCmdPara2+strCommandline[i];
        inc(i);
    until (strCommandline[i]=' ') or (i=length(strCommandline));
    strCmdPara2:=trim(uppercase(strCmdPara2));
//     writeln(strCmdPara2);
    val(strCmdPara2, longCmdPara2);

    // sets different status values
    if strCmd='ME.SET' then
        if strCmdPara1='HP' then
            ThePlayer.intHP:=longCmdPara2
        else
        if strCmdPara1='MAXHP' then
            ThePlayer.intMaxHP:=longCmdPara2
        else
        if strCmdPara1='PP' then
            ThePlayer.intPP:=longCmdPara2
        else
        if strCmdPara1='MAXPP' then
            ThePlayer.intMaxPP:=longCmdPara2
        else
        if strCmdPara1='STR' then
            ThePlayer.intStrength:=longCmdPara2
        else
        if strCmdPara1='EXP' then
            ThePlayer.longEXP:=longCmdPara2
        else
        if strCmdPara1='LEVEL' then
            ThePlayer.intLvl:=longCmdPara2
        else
        if strCmdPara1='FOOD' then
            ThePlayer.longFood:=longCmdPara2
        else
        if strCmdPara1='GOLD' then
            ThePlayer.longGold:=longCmdPara2;

    // shows map and sets status values to extreme powerful
    if strCmd='ME.GOD' then
    begin
        ThePlayer.intHP:=999;
        ThePlayer.intMaxHP:=999;
        ThePlayer.intPP:=999;
        ThePlayer.intMaxPP:=999;
        ThePlayer.intFight:=15;
        ThePlayer.intMove:=15;
        ThePlayer.longFood:=9999;
        ThePlayer.longGold:=9999;
        for i:=1 to DngMaxWidth do
            for j:=1 to DngMaxHeight do
                DngLvl[i,j].blKnown:=true;
    end;


    // re-reads data files
    if strCmd='REALITY.INIT' then
    begin
        MonsterInit;
        BonesInit;
        ClassInit;
        ChantInit;
        ItemInit;
        QuestInit;
        InitStatements;
    end;


    // commits suicide
    if strCmd='ME.SUICIDE' then
        ThePlayer.intHP:=0;

    // uncurses
    if strCmd='ME.UNCURSE' then
        ThePlayer.blCursed:=false;

    // blesses
    if strCmd='ME.BLESS' then
        ThePlayer.blBlessed:=true;

    // unblesses
    if strCmd='ME.UNBLESS' then
        ThePlayer.blBlessed:=false;

    // curses
    if strCmd='ME.CURSE' then
        ThePlayer.blCursed:=true;

    // quests
    if strCmd='QUESTS.OPENALL' then
        for i:=1 to WinLevel do
            for j:=1 to 9 do
                if Quest[i,j].strDescri<>'-' then
                    ThePlayer.intQuestState[i,j]:=1;

    if strCmd='QUESTS.SOLVEALL' then
        for i:=1 to WinLevel do
            for j:=1 to 9 do
                if Quest[i,j].strDescri<>'-' then
                    ThePlayer.intQuestState[i,j]:=2;

    if strCmd='QUESTS.RESETALL' then
        for i:=1 to WinLevel do
            for j:=1 to 9 do
                if Quest[i,j].strDescri<>'-' then
                    ThePlayer.intQuestState[i,j]:=0;

    if strCmd='QUESTS.SOLVE' then
        ThePlayer.intQuestState[longCmdPara1, longCmdPara2] := 2;

    if strCmd='QUESTS.OPEN' then
        ThePlayer.intQuestState[longCmdPara1, longCmdPara2] := 1;

    if strCmd='QUESTS.RESET' then
        ThePlayer.intQuestState[longCmdPara1, longCmdPara2] := 0;

    // puts certain items on certain positions in inventory
    if strCmd='INVENTORY.SETITEM' then
        for i:=1 to ItemCount do
            if uppercase(Thing[i].strRealName)=strCmdPara2 then
            begin
                Inventory[longCmdPara1].intType:=i;
                Inventory[longCmdPara1].longNumber:=1;
            end;

    // sets number of items of an inventory type
    if strCmd='INVENTORY.SETAMOUNT' then
        Inventory[longCmdPara1].longNumber:=longCmdPara2;

    // create barrier around player
    if strCmd='ME.PROTECT' then
        ThePlayer.intWall := longCmdPara1;

    // makes map known
    if strCmd='MAP.SHOW' then
        for i:=1 to DngMaxWidth do
            for j:=1 to DngMaxHeight do
                DngLvl[i,j].blKnown:=true;

    // creates portal down
    if strCmd='MAP.DOWNSTAIRS' then
        DngLvl[ThePlayer.intX,ThePlayer.intY].intFloorType:=9;

    // creates portal up
    if strCmd='MAP.UPSTAIRS' then
        DngLvl[ThePlayer.intX,ThePlayer.intY].intFloorType:=8;

    // creates rare item
    if strCmd='MAP.RAREITEM' then
        DngLvl[ThePlayer.intX,ThePlayer.intY].intItem:=ReturnRareItem;

    // shows plot scene
    if strCmd='PLOT.SHOW' then
        ShowPlot(longCmdPara1);

    ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
    if UseSDL=true then
        SDL_UPDATERECT(screen,0,0,0,0)
    else
        UpdateScreen(true);

end;


procedure CharacterDump(reason: string);
    var
        dumpfile: Textfile;
        i, j, solved: integer;
        equi, strSex, strProf, levelname: string;
        LogRowsToShow: integer;
begin
    ClearScreenSDL;

    Assign(DumpFile, PathUserWritable+'saves/' + ThePlayer.strName + '-dump.txt');
    Rewrite(DumpFile);

    case ThePlayer.intSex of
        1:
            strSex:='male';
        2:
            strSex:='female';
    end;

    case ThePlayer.intProf of
        1:
            strProf:='Constructor';
        2:
            strProf:='Enchanter';
        3:
            strProf:='Thief';
        4:
            strProf:='Archer';
        5:
            strProf:='Soldier';
    end;


    if ThePlayer.blEvil=true then
        WriteLn(DumpFile, ThePlayer.strName, ', ' + strSex + ', devoted to death')
    else
        WriteLn(DumpFile, ThePlayer.strName, ', ' + strSex + ', believer in ' + ThePlayer.strReli);


    WriteLn(DumpFile, '----------------------------------------------------------------------');
    WriteLn(DumpFile, ' ');

    WriteLn(DumpFile, 'LambdaRogue version: ' + strVersion);
    WriteLn(DumpFile, ' ');

    WriteLn(DumpFile, ThePlayer.strVita);
    WriteLn(DumpFile, ThePlayer.strDescription);

    WriteLn(DumpFile, ' ');
    WriteLn(DumpFile, 'HP: ' + IntToStr(ThePlayer.intHP) + '/' + IntToStr(ThePlayer.intMaxHP));
    WriteLn(DumpFile, 'PP: ' + IntToStr(ThePlayer.intPP) + '/' + IntToStr(ThePlayer.intMaxPP));
    WriteLn(DumpFile, ' ');
    WriteLn(DumpFile, 'Strength   : ' + IntToStr(ThePlayer.intStrength) + '%/100%');
    WriteLn(DumpFile, 'Divine Rage: ' + IntToStr(ThePlayer.intLimit) + '%/100%');
    WriteLn(DumpFile, ' ');

    case DungeonLevel of
        1:
            levelname:='the Temple of Enoa';
        2:
            levelname:='the Catacombs of Enoa';
        3:
            levelname:='the Sewers of Enoa';
        4:
            levelname:='the Old Mine';
        5:
            levelname:='the Lost Outpost';
        6:
            levelname:='the Dungeon, level 1';
        7:
            levelname:='the Dungeon, level 2';
        8:
            levelname:='the Caves, level 1';
        9:
            levelname:='the Caves, level 2';
        10:
            levelname:='the Caves, level 3';
        11:
            levelname:='the Caves, level 4';
        12:
            levelname:='the Old Maze, level 1';
        13:
            levelname:='the Old Maze, level 2';
        14:
            levelname:='the Ash Caves, level 1';
        15:
            levelname:='the Ash Caves, level 2';
        16:
            levelname:='the New Maze, level 1';
        17:
            levelname:='the New Maze, level 2';
        18:
            levelname:='Krice'+chr(39)+'s hideout';
        19:
            levelname:='the Dark Temple';
        20:
            levelname:='the Halls of Eris';
        21:
            levelname:='the Ice Ruins of Noldarur';
        22:
            levelName:='the Outskirts of Enoa';
        23:
            levelName:='the Forgotten Realm, outer part';
        24:
            levelName:='the Forgotten Realm, inner part';
        25:
            levelName:='the Forgotten Realm, King'+chr(39)+'s estate';
    end;

    if DungeonLevel<20 then
        WriteLn(DumpFile, reason + ' on dungeon level ' + IntToStr(DungeonLevel) + ' ('+levelname+').');
    if (DungeonLevel=20) or (DungeonLevel=21) then
        WriteLn(DumpFile, reason + ' on dungeon level ? ('+levelname+').');
    if DungeonLevel>21 then
        WriteLn(DumpFile, reason + ' on dungeon level ' + IntToStr(DungeonLevel-21) + ' ('+levelname+').');

    WriteLn(DumpFile, 'Date: ' + IntToStr(intDayTime) + '-' + IntToStr(intDay) + '-' + IntToStr(longYear));
    WriteLn(DumpFile, ' ');
    WriteLn(DumpFile, ThePlayer.strName+' was active for '+IntToStr(longTotalTurns)+' turns.');
    WriteLn(DumpFile, 'In this time, '+ThePlayer.strName+' achieved a score of '+IntToStr(ThePlayer.longScore)+' points.');
    WriteLn(DumpFile, ' ');
    WriteLn(DumpFile, 'Abilities');
    WriteLn(DumpFile, '  Fight       : ' + IntToStr(ThePlayer.intFight));
    WriteLn(DumpFile, '  View        : ' + IntToStr(ThePlayer.intView));
    WriteLn(DumpFile, '  Chant       : ' + IntToStr(ThePlayer.intChant));
    WriteLn(DumpFile, '  Move        : ' + IntToStr(ThePlayer.intMove));
    WriteLn(DumpFile, '  Steal       : ' + IntToStr(ThePlayer.intBurgle));
    WriteLn(DumpFile, ' ');
    WriteLn(DumpFile, 'Skills');
    WriteLn(DumpFile, '  Sword       : ' + IntToStr(ThePlayer.intSword));
    WriteLn(DumpFile, '  Axe         : ' + IntToStr(ThePlayer.intAxe));
    WriteLn(DumpFile, '  Lance       : ' + IntToStr(ThePlayer.intWhip));
    WriteLn(DumpFile, '  Fire-arm    : ' + IntToStr(ThePlayer.intGun));
    WriteLn(DumpFile, '  Tool        : ' + IntToStr(ThePlayer.intTool));
    WriteLn(DumpFile, ' ');
    WriteLn(DumpFile, 'Talents');
    WriteLn(DumpFile, '  Humility    : ' + IntToStr(ThePlayer.intHumility));
    WriteLn(DumpFile, '  Trade       : ' + IntToStr(ThePlayer.intTrade));
    WriteLn(DumpFile, ' ');

    Writeln(DumpFile, 'Melee/Long-range/Defense: '+IntToStr(CollectTP)+'/'+IntToStr(CollectGunTP)+'/'+IntToStr(CollectDP));

    WriteLn(DumpFile, ' ');

    WriteLn(DumpFile, 'Experience    : ' + IntToStr(ThePlayer.longEXP));
    WriteLn(DumpFile, 'Character Lvl.: ' + IntToStr(ThePlayer.intLvl));

    WriteLn(DumpFile, ' ');

    WriteLn(DumpFile, 'Profession: '+strProf);

    if ThePlayer.intDipl[1]=1 then
        WriteLn(DumpFile, '- Centurio');
    if ThePlayer.intDipl[2]=1 then
        WriteLn(DumpFile, '  +- Hastatus');
    if ThePlayer.intDipl[3]=1 then
        WriteLn(DumpFile, '     +- Princeps');
    if ThePlayer.intDipl[4]=1 then
        WriteLn(DumpFile, '        +- Pilus');
    if ThePlayer.intDipl[5]=1 then
        WriteLn(DumpFile, '           +- Primus Pilus');

    if ThePlayer.intDipl[6]=1 then
        WriteLn(DumpFile, '- Mage');
    if ThePlayer.intDipl[7]=1 then
        WriteLn(DumpFile, '  +- Battlemage');
    if ThePlayer.intDipl[8]=1 then
        WriteLn(DumpFile, '- Believer');
    if ThePlayer.intDipl[9]=1 then
        WriteLn(DumpFile, '  +- Monk');
    if ThePlayer.intDipl[10]=1 then
        WriteLn(DumpFile, '     +- Holy Warrior');

    if ThePlayer.intDipl[11]=1 then
        WriteLn(DumpFile, '- Assassin');
    if ThePlayer.intDipl[12]=1 then
        WriteLn(DumpFile, '  +- Agent');
    if ThePlayer.intDipl[13]=1 then
        WriteLn(DumpFile, '- Master Thief');
    if ThePlayer.intDipl[14]=1 then
        WriteLn(DumpFile, '  +- Guild Leader');

    if ThePlayer.intDipl[15]=1 then
        WriteLn(DumpFile, '- Hunter');
    if ThePlayer.intDipl[16]=1 then
        WriteLn(DumpFile, '  +- Ranger');
    if ThePlayer.intDipl[17]=1 then
        WriteLn(DumpFile, '- Marksman');
    if ThePlayer.intDipl[18]=1 then
        WriteLn(DumpFile, '  +- Sergeant');

    WriteLn(DumpFile, ' ');
    solved:=0;
    for i:=1 to WinLevel do
        for j:=1 to 9 do
            if (ThePlayer.intQuestState[i,j]=2) and (Quest[i,j].strDescri<>'-') then
                inc(solved);

    WriteLn(DumpFile, ThePlayer.strName + ' has solved ' + IntToStr(solved) + ' quest(s).');

    if solved>0 then
    begin
        WriteLn(DumpFile, 'Solved quests:');
        for i:=1 to WinLevel do
            for j:=1 to 9 do
                if (ThePlayer.intQuestState[i,j]=2) and (Quest[i,j].strDescri<>'-') then
                    WriteLn(DumpFile, '    '+Quest[i,j].strDescri);
        WriteLn(DumpFile, ' ');
    end;


    WriteLn(DumpFile, ' ');
    WriteLn(DumpFile, ' ');

    if ThePlayer.longContCleared > 0 then
    begin
        WriteLn(DumpFile, ThePlayer.strName + ' cleared ' + IntToStr(ThePlayer.longContCleared) + ' contaminated areas.');
        WriteLn(DumpFile, ' ');
    end;

    if ThePlayer.intTotalVitari > 0 then
        WriteLn(DumpFile, ThePlayer.strName + ' was addicted to drugs.');

    if ThePlayer.longPrayers > 0 then
    begin
        if ThePlayer.longLifeIns > 0 then
            WriteLn(DumpFile, 'Although ' + ThePlayer.strName + ' prayed '+IntToStr(ThePlayer.longPrayers)+' and saved '+IntToStr(ThePlayer.longLifeIns)+' time(s), '+ThePlayer.strName+' is dead.')
        else
            WriteLn(DumpFile, 'Although ' + ThePlayer.strName + ' prayed '+IntToStr(ThePlayer.longPrayers)+' time(s), '+ThePlayer.strName+' is dead.');
        WriteLn(DumpFile, ' ');
    end
    else
    if ThePlayer.longLifeIns > 0 then
    begin
        WriteLn(DumpFile, 'Although ' + ThePlayer.strName + ' saved '+IntToStr(ThePlayer.longLifeIns)+' time(s), '+ThePlayer.strName+' is dead.');
        WriteLn(DumpFile, ' ');
    end;

    WriteLn(DumpFile, ' ');
    WriteLn(DumpFile, 'Inventory:');
    WriteLn(DumpFile, '----------');

    WriteLn(DumpFile, 'Credits: ' + IntToStr(ThePlayer.longGold));
    WriteLn(DumpFile, ' ');
    for i:=1 to 16 do
    begin
        equi := '';
        if inventory[i].intType > 0 then
        begin
            if (thing[inventory[i].intType].blWield) and (ThePlayer.intWeapon=inventory[i].intType) then
                equi:=' [wielded]';
            if (thing[inventory[i].intType].blWear) and (ThePlayer.intArmour=inventory[i].intType) then
                equi:=' [worn]';
            if (thing[inventory[i].intType].blHat) and (ThePlayer.intHat=inventory[i].intType) then
                equi:=' [worn]';
            if (thing[inventory[i].intType].blExtra) and (ThePlayer.intExtra=inventory[i].intType) then
                equi:=' [worn]';
            if (thing[inventory[i].intType].blShoes) and (ThePlayer.intFeet=inventory[i].intType) then
                equi:=' [worn]';
            if (thing[inventory[i].intType].blRing) and (ThePlayer.intRingLeft=inventory[i].intType) then
                equi:=' [wielded]';
            if (thing[inventory[i].intType].blRing) and (ThePlayer.intRingRight=inventory[i].intType) then
                equi:=equi + ' [worn]';

            WriteLn(DumpFile, Thing[inventory[i].intType].strRealName + ' x' + IntToStr(Inventory[i].longNumber) + equi);
        end;
    end;

    WriteLn(DumpFile, ' ');
    WriteLn(DumpFile, ' ');

    WriteLn(DumpFile, 'Monsters killed:');
    WriteLn(DumpFile, '----------------');
    WriteLn(DumpFile, ' ');
    for i:=1 to MonsterTemplates do
        if ThePlayer.longKilled[i]>0 then
            WriteLn(DumpFile, IntToStr(ThePlayer.longKilled[i]) + ' ' + MonTe[i].strName + '(s)');

    WriteLn(DumpFile, ' ');
    WriteLn(DumpFile, ' ');

    WriteLn(DumpFile, 'Last Messages:');
    WriteLn(DumpFile, '--------------');
    WriteLn(DumpFile, ' ');
    LogRowsToShow := 15;
    if (ThePlayer.intNumberOfLogs < LogRowsToShow) then
        LogRowsToShow := ThePlayer.intNumberOfLogs;
    for i:= ThePlayer.intNumberOfLogs-LogRowsToShow to ThePlayer.intNumberOfLogs do
        WriteLn(DumpFile, ThePlayer.strMessageLog[i]);

    WriteLn(DumpFile, ' ');
    WriteLn(DumpFile, ' ');

    WriteLn(DumpFile, 'Achievements:');
    WriteLn(DumpFile, '-------------');
    WriteLn(DumpFile, ' ');
    for i:=1 to intAchieved do
        if Achievements[i]<>'-' then
            WriteLn(DumpFile, Achievements[i]);


    Close(DumpFile);

    GetKeyInput('A character dump has been created as '+PathUserWritable+'saves/' + ThePlayer.strName + '-dump.txt.', true);
end;

// player gained a level
procedure LevelUp;
begin
    if ThePlayer.intHP>0 then
        if ThePlayer.intLvl<CONST_MAXCLV then
            if ThePlayer.longExp > (ThePlayer.intLvl * CONST_LVL_MULTI) * (ThePlayer.intLvl * CONST_LVL_MULTI) * ThePlayer.intLvl then
            begin
                PlaySFX('levelup.mp3');
                inc (ThePlayer.intLvl);
                inc(ThePlayer.intMaxHP, 1+((CONST_HP_MULTI*ThePlayer.intMaxHP) div 100));
                inc(ThePlayer.intMaxPP, 1+((CONST_PP_MULTI*ThePlayer.intMaxPP) div 100));
                ThePlayer.intHP:=ThePlayer.intMaxHP;
                ThePlayer.intPP:=ThePlayer.intMaxPP;
                inc(ThePlayer.longSkillPoints);
                inc(ThePlayer.longScore, 200);
                ShowTransMessage('Welcome to level ' + IntToStr(ThePlayer.intLvl) + ', ' + ThePlayer.strName + '. You'+chr(39)+'ve earned 1 skill point.', false);
                StoreAchievement('Gained a character level (Now: '+IntToStr(ThePlayer.intLvl)+')');
                if ThePlayer.intLvl=CONST_MAXCLV then
                    ShowTransMessage('You have reached the highest possible level.', false);
            end;
end;

procedure WinGame(blRegular: Boolean);
    var
        dummy: string;
begin
    PlayMusic('win.mp3');

    StoreAchievement('Completed the journey successfully.');

    inc(ThePlayer.longScore, 2000);

    DeleteFile(PathUserWritable+'saves/' + ThePlayer.strName + '.lambdarogue');

    ClearScreenSDL;

    if UseSDL=true then
        LoadImage_Title(PathData+'graphics/decobg.jpg');


    repeat
        if UseSDL=true then
            BlitImage_Title;

        TransTextXY(1, 1, 'CONGRATULATIONS!');

        TransTextXY(1, 3, 'Your final score is '+IntToStr(ThePlayer.longScore)+'.');

        dummy := GetKeyInput('[d] to create a character dump, [q] to quit.', false);
    until (dummy='d') or (dummy='q') or (dummy='D') or (dummy='Q');

    if (dummy='d') or (dummy='D') then
        CharacterDump(ThePlayer.strName + ' has retired after a successful life');

    StopMusic;
    FreeMusic;
    StopGraphics;
    Halt;
end;

procedure GameOver(reason: string);
    var
        dummy: string;
        i: integer;
        LogRowsToShow: integer;
        counter: integer;
begin

    if ThePlayer.blDead = false then
    begin

        ThePlayer.blDead := true;

        StoreAchievement('Died before the journey could be completed.');

        PlayMusic('death.mp3');

        ClearScreenSDL;

        if UseSDL=true then
        begin
            LoadImage_Title(PathData+'graphics/decobg.jpg');
            BlitImage_Title;
        end;

        TransTextXY(1, 1, uppercase(reason)+'.');
        LogRowsToShow := 16;
        if (ThePlayer.intNumberOfLogs < LogRowsToShow) then
            LogRowsToShow := ThePlayer.intNumberOfLogs;
        counter:= 0;
        for i:= ThePlayer.intNumberOfLogs-LogRowsToShow to ThePlayer.intNumberOfLogs do
            TransTextXY(1, 1+i, ThePlayer.strMessageLog[i]);

        If (ThePlayer.longLifeIns=0) or (fileexists(PathUserWritable+'saves/' + ThePlayer.strName + '.lambdarogue')=false) then
        begin
            if fileexists(PathUserWritable+'saves/' + ThePlayer.strName + '.lambdarogue') then
                DeleteFile(PathUserWritable+'saves/' + ThePlayer.strName + '.lambdarogue');
            SaveBones;

            repeat

                if UseSDL=true then
                    BlitImage_Title;

                TransTextXY(1, 1, uppercase(reason)+'.');

                counter:= 0;
                for i:= ThePlayer.intNumberOfLogs-LogRowsToShow to ThePlayer.intNumberOfLogs do
                begin
                  //Writeln('i='+IntToStr(i)+'  .row='+ ThePlayer.strMessageLog[i]);
                  TransTextXY(1, 3+counter, ThePlayer.strMessageLog[i]);
                  inc(counter);
                end;

                dummy := GetKeyInput('[d] to create a character dump, [q] to quit.', false);
            until (dummy='d') or (dummy='q') or (dummy='D') or (dummy='Q');

            if (dummy='d') or (dummy='D') then
                CharacterDump(reason);

        end
        else
            GetKeyInput('You have been knocked out, but will awake at hospital after restarting.', false);

    end;
end;


procedure ShowDamage (n: longint);
var
    k, x, y: integer;
begin

    exit;

    if UseHiRes=true then
    begin
        if UseSmallTiles=true then
        begin
            if Netbook=false then
            begin
                x:=38;
                y:=14;
            end
            else
            begin
                x:=38;
                y:=18;
            end;
        end
        else
        begin
            if Netbook=false then
            begin
                x:=36;
                y:=12;
            end
            else
            begin
                x:=36;
                y:=16;
            end;
        end;
    end
    else
    begin
        if UseSmallTiles=true then
        begin
            x:=39;
            y:=14;
        end
        else
        begin
            x:=39;
            y:=16;
        end;
    end;

    if UseSDL=true then
        if n>0 then
        begin
            for k:=1 to 10 do
            begin
                TransTextXY(x, y, RedNumbers(n));
                SDL_UPDATERECT(screen,0,0,0,0);
                delay(5);
            end;
        end;
end;


// returns a string containing lvl and id, if the given monster name is object of assassination in any quest
function IsTargetMonster (MonsterName: string): string;
    var
        i, j: Integer;
begin
    IsTargetMonster:='false';
    for i:=1 to WinLevel do
        for j:=1 to 9 do
            if Quest[i,j].strTargetName=MonsterName then
                if ThePlayer.intQuestState[i,j]=1 then
                    IsTargetMonster:=IntToStr(i)+','+IntToStr(j);
end;


procedure MonsterShoot(MonsterID: integer);
    var
        DistanceX, DistanceY, i, k, intDP, intMTP: Integer;
        vx, vy, GunX, GunY, GunBX, GunBY: Integer;
        strMsg: string;
begin
    DistanceX := abs(ThePlayer.intX - Monster[MonsterID].intX);
    DistanceY := abs(ThePlayer.intY - Monster[MonsterID].intY);

    vx:=0;
    vy:=0;

    if ((DistanceX <= CONST_MONSTERRECDIST) and (DistanceY <= CONST_MONSTERRECDIST)) and ((DistanceX > 2) or (DistanceY > 2)) then
    begin
        // monster in the same row as player
        if Monster[MonsterID].intY = ThePlayer.intY then
        begin
            // monster is left from player
            if Monster[MonsterID].intX < ThePlayer.intX then
            begin
                for i:=Monster[MonsterID].intX to ThePlayer.intX do
                    if (DngLvl[i,Monster[MonsterID].intY].intIntegrity>0) or (DngLvl[i,Monster[MonsterID].intY].intFloorType=13) or (DngLvl[i,Monster[MonsterID].intY].intFloorType=22) then
                        break;
                if i=ThePlayer.intX then
                    vx:=1;
            end;

            // monster is right from player
            if Monster[MonsterID].intX > ThePlayer.intX then
            begin
                for i:=Monster[MonsterID].intX downto ThePlayer.intX do
                    if (DngLvl[i,Monster[MonsterID].intY].intIntegrity>0) or (DngLvl[i,Monster[MonsterID].intY].intFloorType=13) or (DngLvl[i,Monster[MonsterID].intY].intFloorType=22) then
                        break;
                if i=ThePlayer.intX then
                    vx:=-1;
            end;
        end;

        // monster in the same column as player
        if (Monster[MonsterID].intX = ThePlayer.intX) then
        begin

            // monster is beyond player
            if Monster[MonsterID].intY < ThePlayer.intY then
            begin
                for i:=Monster[MonsterID].intY to ThePlayer.intY do
                    if (DngLvl[Monster[MonsterID].intX,i].intIntegrity>0) or (DngLvl[Monster[MonsterID].intX,i].intFloorType=13) or (DngLvl[Monster[MonsterID].intX,i].intFloorType=22) then
                        break;
                if i=ThePlayer.intY then
                    vy:=1;
            end;

            // monster is below player
            if Monster[MonsterID].intY > ThePlayer.intY then
            begin
                for i:=Monster[MonsterID].intY downto ThePlayer.intY do
                    if (DngLvl[Monster[MonsterID].intX,i].intIntegrity>0) or (DngLvl[Monster[MonsterID].intX,i].intFloorType=13) or (DngLvl[Monster[MonsterID].intX,i].intFloorType=22) then
                        break;
                if i=ThePlayer.intY then
                    vy:=-1;
            end;
        end;
    end;

    // if a vector (vx or vy) is set, the monster really shoots
    if (vx<>0) or (vy<>0) then
    begin

        // starting position of monster's bullet
        GunX:=Monster[MonsterID].intX;
        GunY:=Monster[MonsterID].intY;

        GunBX:=ThePlayer.intBX;

        if UseSDL=true then
            GunBY:=ThePlayer.intBY+1
        else
            GunBY:=ThePlayer.intBY;

        if Monster[MonsterID].intX > ThePlayer.intX then
            inc(GunBX, DistanceX)
        else
            dec(GunBX, DistanceX);

        if Monster[MonsterID].intY > ThePlayer.intY then
            inc(GunBY, DistanceY)
        else
            dec(GunBY, DistanceY);
    
        ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);

        if (ord(Monster[MonsterID].chBulletLetter)<>129) and (ord(Monster[MonsterID].chBulletLetter)<>160) then
            PlaySFX('magic-combat.mp3');

        // let the bullet fly
        repeat
            Inc(GunX, vx);
            Inc(GunY, vy);
    
            Inc(GunBX, vx);
            Inc(GunBY, vy);
    
            // show bullet
            if (GunBX > 1) and (GunBX < ThePlayer.intBX*2) and (GunBY > 1) and (GunBY < ThePlayer.intBY*2) then
            begin
                AnyCharXY(GunBX, GunBY, Monster[MonsterID].chBulletLetter, 0);
                if UseSDL=true then
                    SDL_UPDATERECT(screen,0,0,0,0)
                else
                    UpdateScreen(true);
                delay(50);
            end;
        until (GunX=ThePlayer.intX) and (GunY=ThePlayer.intY);

        // calculate effects of battle
        intDP := CollectDP;
        intMTP := Monster[MonsterID].intGP - intDP;
        if intMTP <= 1 then
            intMTP := 0;

        // barrier?
        if ThePlayer.intWall>0 then
            intMTP:=0;


        ShowDamage(intMTP);

        if random(400)>Monster[MonsterID].intHitRate then
        begin
            if intMTP>0 then
                ShowTransMessage ('The '+Monster[MonsterID].strName + ' ' + Monster[MonsterID].strShootVerb+' [-'+IntToStr(intMTP)+' HP].', false)
            else
            begin
                if ThePlayer.intWall>0 then
                    ShowTransMessage ('The attack bounces off your barrier.', false)
                else
                    ShowTransMessage ('The '+Monster[MonsterID].strName + ' ' + Monster[MonsterID].strShootVerb+', but does no damage.', false);
            end;
        end
        else
        begin
            ShowTransMessage ('The '+Monster[MonsterID].strName + ' ' + Monster[MonsterID].strShootVerb + ', but the shot misses.', false);
            intMTP:=0;
        end;

        ThePlayer.intHP := ThePlayer.intHP - intMTP;
        if ThePlayer.intHP < 1 then
        begin
            if Monster[i].intInvis>0 then
                GameOver('Shot by ... something')
            else
            begin
                strMsg:=lowercase(LeftStr(Monster[MonsterID].strName, 1));
                if (strMsg='a') or (strMsg='e') or (strMsg='u') or (strMsg='o') or (strMsg='i') then
                    GameOver('Shot by an '+Monster[MonsterID].strName)
                else
                    GameOver('Shot by a '+Monster[MonsterID].strName);
            end;
        end;
    end;
end;

// This function returns, if a player or a monster hits or misses the enemy
function HitOrMiss(MonsterID: integer): boolean;
    var
        CompareMoveMonster, CompareMovePlayer: integer;
begin
    HitOrMiss:=true;

    // player base is Move-skill
    CompareMovePlayer:=ThePlayer.intMove;

    // monster base is Move-skill; if not available, player skill +/-
    if Monster[MonsterID].intMove=0 then
    begin
        CompareMoveMonster:=CompareMovePlayer;
        if random(400) > CONST_MONSTERISFASTER then
            inc(CompareMoveMonster, 1+trunc(random(6)))
        else
            dec(CompareMoveMonster, 1+trunc(random(6)));
    end
    else
        CompareMoveMonster:=Monster[MonsterID].intMove;

    // items with influence on move-skill
    inc(CompareMovePlayer, ReturnStatusIncRate(25));

    // negative influences on player
    // -- a little bit hunger
    if ThePlayer.longFood < 300 then
        dec(CompareMovePlayer);

    // -- hunger
    if ThePlayer.longFood < 200 then
        dec(CompareMovePlayer);

    // -- fainting
    if ThePlayer.longFood < 61 then
        dec(CompareMovePlayer);

    // -- starving
    if ThePlayer.longFood < 21 then
        dec(CompareMovePlayer);

    // -- confusion
    if ThePlayer.intConfusion > 0 then
        CompareMovePlayer := CompareMovePlayer div 2;

    // -- blindness
    if ThePlayer.intBlind > 0 then
        CompareMovePlayer:=0;

    // -- paralization or curse
    if (ThePlayer.intPara > 0) or (ThePlayer.blCursed=true) then
        CompareMovePlayer:=0;

    // -- bad luck
    if random(400)>CONST_BADHITLUCK then
        CompareMovePlayer := 0;



    // negative influences on monster
    // -- player is blessed
    if ThePlayer.blBlessed=true then
        CompareMoveMonster := CompareMoveMonster div 2;

    // -- bad luck
    if random(400)>CONST_BADHITLUCK then
        CompareMoveMonster := 0;


    // both have the same Move-skill -> mostly the player hits
    if CompareMoveMonster=CompareMovePlayer then
        if random(400)>350 then
            inc(CompareMoveMonster)
        else
            dec(CompareMoveMonster);


    if CompareMoveMonster > CompareMovePlayer then
        HitOrMiss:=true
    else
        HitOrMiss:=false;
end;


// check if a monster is dead (after battle)
procedure IsMonsterDead (i: integer);
    var
        longAddEXP, longAddGold: longint;
        strBaseMonsterName, dummy: string;
        lvl,npcid: integer;
begin
    if Monster[i].intHP = 0 then
    begin

        // increase killed counter
        inc(ThePlayer.longKilled[Monster[i].intType]);

        // remove blessed state, except for ares
        if (ThePlayer.intReli<>5) and (ThePlayer.blBlessed=true) then
        begin
            ThePlayer.blBlessed:=false;
            ShowTransMessage('Due to the killing, you are not longer blessed.', false);
        end;

        // draw blood...
        CreateBlood(Monster[i].intX,Monster[i].intY);

        // monster loses item when enough space in dungeon and not killed SO often
//         writeln('MonTE ID: '+IntToStr(ReturnMonTeByName(Monster[i].strName))+': '+MonTe[ReturnMonTeByName(Monster[i].strName)].strName);
        if ThePlayer.longKilled[ReturnMonTeByName(Monster[i].strName)]<CONST_MAXKILLEDFORDROP then
            if DngLvl[Monster[i].intX,Monster[i].intY].intItem=0 then
                if random(500) > CONST_MONSTERDROPSITEM then
                    if random(500)>150 then
                        DngLvl[Monster[i].intX,Monster[i].intY].intItem:=ReturnRandomItem;

        // unique monster: Items, Score and Disappear
        if Monster[i].blUnique=true then
        begin
            inc(ThePlayer.longScore,DungeonLevel*10);
            DngLvl[Monster[i].intX,Monster[i].intY].intItem:=ReturnUniqueItem;
            ThePlayer.blUnKilled[Monster[i].intTemplate]:=true;

            StoreAchievement('Defeated '+Monster[i].strName+'.');

            if Monster[i].strName='Undying King' then
                KillAllMonsters;

            // this line allows for winning the game without need of a quest
            // however, it's more elegant to use a quest and give WinGame as reward
            if Monster[i].blLastEnemy=true then
                WinGame(false);
        end;

        // make sure that also big, strong and mighty variants of the monsters are counted for hunt-counter
        strBaseMonsterName:=Monster[i].strName;
        if GrowingMonsters=true then
        begin
            if LeftStr(strBaseMonsterName,4)='big ' then
                strBaseMonsterName:=RightStr(strBaseMonsterName,length(strBaseMonsterName)-4);
    
            if LeftStr(strBaseMonsterName,7)='strong ' then
                strBaseMonsterName:=RightStr(strBaseMonsterName,length(strBaseMonsterName)-7);
    
            if LeftStr(strBaseMonsterName,7)='mighty ' then
                strBaseMonsterName:=RightStr(strBaseMonsterName,length(strBaseMonsterName)-7);
        end;

        ShowTransMessage('You kill the ' + Monster[i].strName + '.', false);

        // hide monster object by moving out of the dungeon
        Monster[i].intX := DngMaxWidth+10;
        Monster[i].intY := DngMaxHeight+10;

        // show killplot file
        if Monster[i].intKillPlot>0 then
            ShowPlot(Monster[i].intKillPlot);

        // make sure that ares characters don't get experience for killing caveworms
        if ((Monster[i].strName='caveworm') or (Monster[i].strName='worm mother')) and (ThePlayer.intReli=5) then
        begin
            ShowTransMessage('Your code of conduct forbids you to kill caveworms!', false);
            longAddEXP := 0;
            ThePlayer.blBlessed:=false;
        end
        else
        begin
            longAddEXP := 0;

            // make sure that characters of class 1 get less experience (how much is determined by CONST_BATTLEEXP_REDUCED)
            if ThePlayer.intReli=1 then
                longAddEXP := ((CONST_BATTLEEXP_REDUCED * MonTe[Monster[i].intType].intHP) div 100) + Monster[i].longExp
            else
            begin
                longAddEXP := ((CONST_BATTLEEXP * MonTe[Monster[i].intType].intHP) div 100) + Monster[i].longExp;
                if CheckEffect(6)=true then
                    inc(longAddEXP, Monster[i].longExp * 2);
            end;

            longAddGold := Monster[i].longGold;
            if CheckEffect(7)=true then
                inc(longAddGold, (Monster[i].longGold div 2) + 1);

            inc(ThePlayer.longExp, longAddEXP);
            inc (ThePlayer.longGold, Monster[i].longGold);
            ShowTransMessage('You get ' + IntToStr(longAddGold) + ' Credits and ' + IntToStr(longAddEXP)+' EXP.', false);

            // has player gained a level?
            LevelUp;
        end;

        // is the killed enemy a monster that should be killed in one of the player's quests?
        dummy:=IsTargetMonster(strBaseMonsterName);
//         writeln(strBaseMonsterName+': '+dummy);
        if dummy<>'false' then
        begin
            lvl:=StrToInt(LeftStr(dummy, NPos(',', dummy, 1)-1));
            //npcID:=StrToInt(RightStr(dummy, NPos(',', dummy, 1)-1));
            npcID:=StrToInt(RightStr(dummy, NPos(',', dummy, 1)-strlen(PChar(IntToStr(lvl)))));

//             writeln('level: '+IntToStr(lvl));
//             writeln('npcID: '+IntToStr(npcID));

            inc(ThePlayer.intQuestHunt[lvl,npcID]); // increase counter of killed monsters for quest

            // show message if monsters of this type still need to be killed
            if ThePlayer.intQuestHunt[lvl,npcID]<Quest[lvl,npcID].intHuntAmount then
               ShowTransMessage('Kill '+IntToStr(Quest[lvl,npcID].intHuntAmount-ThePlayer.intQuestHunt[lvl,npcID])+' more '+strBaseMonsterName+'(s) to solve quest '+IntToStr(lvl)+'/'+IntToStr(npcID)+'.', true);

            // show message if enough monsters of this type have been killed
            if ThePlayer.intQuestHunt[lvl,npcID]=Quest[lvl,npcID].intHuntAmount then
                ShowTransMessage('You have killed enough '+strBaseMonsterName+'s to solve quest '+IntToStr(lvl)+'/'+IntToStr(npcID)+'.', true);
        end;

        ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);

        if UseSDL=true then
            SDL_UPDATERECT(screen,0,0,0,0)
        else
            UpdateScreen(true);

    end;
end;



// Move Monsters
function MoveMonsters(): string;
    var
        i, j, intDP, intMTP, oldx, oldy: integer;
        strMsg: string;
        DistanceX, DistanceY: integer;
        blDone: boolean;
begin
    MoveMonsters:='';

    intDP := CollectDP;

    for i:=1 to 510 do
    begin

        // long-range-attack
        if (Monster[i].intGP>0) and (random(400)>CONST_MONSTERFIRERATE) then
            if Monster[i].blHuman=false then
                if ThePlayer.intInvis < 1 then
                    MonsterShoot(i);

        oldx:=Monster[i].intX;
        oldy:=Monster[i].intY;

        // run away, if: 1. not frozen/trapped & low hp & near to player or 2. invisible
        if (DngLvl[Monster[i].intX,Monster[i].intY].intAirType<>2) and (DngLvl[Monster[i].intX,Monster[i].intY].intAirType<>6) and ((Monster[i].intHP<5) or (Monster[i].intInvis>0)) then
        begin
            // run away
            if ((abs(ThePlayer.intX - Monster[i].intX) < 7) and (abs(ThePlayer.intX - Monster[i].intX) > 1)) or ((abs(ThePlayer.intY - Monster[i].intY) < 7) and (abs(ThePlayer.intY - Monster[i].intY) > 1)) then
            begin
                if (random(500)>120) and (IsInvisible=false) then
                begin
                    if (Monster[i].intX < ThePlayer.intX) and (DngLvl[Monster[i].intX-1,Monster[i].intY].intIntegrity=0)  and (CheckForMonster(Monster[i].intX-1,Monster[i].intY)=false) then
                        dec(Monster[i].intX)
                    else
                    if (Monster[i].intX > ThePlayer.intX) and (DngLvl[Monster[i].intX+1,Monster[i].intY].intIntegrity=0)  and (CheckForMonster(Monster[i].intX+1,Monster[i].intY)=false) then
                        inc(Monster[i].intX);
    
                    if (Monster[i].intY < ThePlayer.intY) and (DngLvl[Monster[i].intX,Monster[i].intY-1].intIntegrity=0)  and (CheckForMonster(Monster[i].intX,Monster[i].intY-1)=false) then
                        dec(Monster[i].intY)
                    else
                    if (Monster[i].intY > ThePlayer.intY) and (DngLvl[Monster[i].intX,Monster[i].intY+1].intIntegrity=0)  and (CheckForMonster(Monster[i].intX,Monster[i].intY+1)=false) then
                        inc(Monster[i].intY);
                end;
            end;
        end
        else
        begin
            if Monster[i].blHuman=false then
            begin
                // next to player?
                if (DngLvl[Monster[i].intX,Monster[i].intY].intAirType<>2) and (DngLvl[Monster[i].intX,Monster[i].intY].intAirType<>6) and ((abs(ThePlayer.intX - Monster[i].intX) < 2) and (abs(ThePlayer.intY - Monster[i].intY) < 2)) then
                begin
                    if (DngLvl[ThePlayer.intX,ThePlayer.intY].intFloorType<>26) and (DngLvl[ThePlayer.intX,ThePlayer.intY].intFloorType<>27) then
                    begin
                        if (random(500)>420) and (IsInvisible=false) then
                        begin
                            if HitOrMiss(i)=false then
                            begin

                                intMTP := Monster[i].intWP - intDP;
                                if intMTP < 1 then
                                    intMTP := 0;

                                ShowDamage(intMTP);

                                if intMTP > 0 then
                                    MoveMonsters := 'The ' + Monster[i].strName + ' ' + Monster[i].strVerb + ' [-' + IntToStr(intMTP) + 'HP].'
                                else
                                    MoveMonsters := 'The ' + Monster[i].strName + ' ' + Monster[i].strVerb + ', but does no damage.';

                                if ThePlayer.intWall > 0 then
                                begin
                                    intMTP:=0;
                                    MoveMonsters := 'The ' + Monster[i].strName + ' ' + Monster[i].strVerb + ', but your barrier blocks the attack.';
                                end;

                                ThePlayer.intHP := ThePlayer.intHP - intMTP;

                                if ThePlayer.intLimit<100 then
                                    inc(ThePlayer.intLimit);

                                if ThePlayer.intHP < 1 then
                                begin
                                    strMsg:=lowercase(LeftStr(Monster[i].strName, 1));
                                    if (strMsg='a') or (strMsg='e') or (strMsg='u') or (strMsg='o') or (strMsg='i') then
                                        GameOver('Killed by an '+Monster[i].strName)
                                    else
                                        GameOver('Killed by a '+Monster[i].strName);
                                end;
                            end
                            else
                                MoveMonsters := 'The ' + Monster[i].strName + ' tries to attack you, but misses.';
                        end;
                    end;
                end
                else
                begin
                    // move to player    
                    if (DngLvl[Monster[i].intX,Monster[i].intY].intAirType<>2) and (DngLvl[Monster[i].intX,Monster[i].intY].intAirType<>6) and (random(500) > 220) and (IsInvisible=false) then
                    begin
                        DistanceX := abs(ThePlayer.intX - Monster[i].intX);
                        DistanceY := abs(ThePlayer.intY - Monster[i].intY);

                        if ((DistanceX <= CONST_MONSTERRECDIST) and (DistanceY <= CONST_MONSTERRECDIST)) and ((DistanceX > 0) or (DistanceY > 0)) then
                        begin

                            blDone:=false;

                            if (Monster[i].intY < ThePlayer.intY) and (Monster[i].intX < ThePlayer.intX) and (DngLvl[Monster[i].intX+1,Monster[i].intY+1].intIntegrity=0) and (CheckForMonster(Monster[i].intX+1,Monster[i].intY+1)=false) then
                                if blDone=false then
                                begin
                                    inc(Monster[i].intX); // to SE
                                    inc(Monster[i].intY);
                                    blDone:=true;
                                end;

                            if (Monster[i].intY < ThePlayer.intY) and (Monster[i].intX > ThePlayer.intX) and (DngLvl[Monster[i].intX-1,Monster[i].intY+1].intIntegrity=0) and (CheckForMonster(Monster[i].intX-1,Monster[i].intY+1)=false) then
                                if blDone=false then
                                begin
                                    dec(Monster[i].intX); // to SW
                                    inc(Monster[i].intY);
                                    blDone:=true;
                                end;

                            if (Monster[i].intY > ThePlayer.intY) and (Monster[i].intX < ThePlayer.intX) and (DngLvl[Monster[i].intX+1,Monster[i].intY-1].intIntegrity=0) and (CheckForMonster(Monster[i].intX+1,Monster[i].intY-1)=false) then
                                if blDone=false then
                                begin
                                    inc(Monster[i].intX); // to NE
                                    dec(Monster[i].intY);
                                    blDone:=true;
                                end;

                            if (Monster[i].intY > ThePlayer.intY) and (Monster[i].intX > ThePlayer.intX) and (DngLvl[Monster[i].intX-1,Monster[i].intY-1].intIntegrity=0) and (CheckForMonster(Monster[i].intX-1,Monster[i].intY-1)=false) then
                                if blDone=false then
                                begin
                                    dec(Monster[i].intX); // to NW
                                    dec(Monster[i].intY);
                                    blDone:=true;
                                end;

                            if (Monster[i].intX < ThePlayer.intX) and (DngLvl[Monster[i].intX+1,Monster[i].intY].intIntegrity=0) and (CheckForMonster(Monster[i].intX+1,Monster[i].intY)=false) then
                                if blDone=false then
                                begin
                                   inc(Monster[i].intX);  // W to E
                                    blDone:=true;
                                end;

                            if (Monster[i].intX > ThePlayer.intX) and (DngLvl[Monster[i].intX-1,Monster[i].intY].intIntegrity=0) and (CheckForMonster(Monster[i].intX-1,Monster[i].intY)=false) then
                                if blDone=false then
                                begin
                                    dec(Monster[i].intX); // E to W
                                    blDone:=true;
                                end;
        
                            if (Monster[i].intY < ThePlayer.intY) and (DngLvl[Monster[i].intX,Monster[i].intY+1].intIntegrity=0) and (CheckForMonster(Monster[i].intX,Monster[i].intY+1)=false) then
                                if blDone=false then
                                begin
                                    inc(Monster[i].intY);  // N to S
                                    blDone:=true;
                                end;

                            if (Monster[i].intY > ThePlayer.intY) and (DngLvl[Monster[i].intX,Monster[i].intY-1].intIntegrity=0) and (CheckForMonster(Monster[i].intX,Monster[i].intY-1)=false) then
                                if blDone=false then
                                begin
                                    dec(Monster[i].intY); // S to N
                                    blDone:=true;
                                end;
                        end;
                    end;
                end;
            end
            else
            begin
                // randomly move humans and non-hostile enemies
                j:=trunc(random(10));
                if (j=0) and (DngLvl[Monster[i].intX,Monster[i].intY+1].intIntegrity=0)  and (CheckForMonster(Monster[i].intX,Monster[i].intY+1)=false) then
                    inc(Monster[i].intY);
                if (j=1) and (DngLvl[Monster[i].intX,Monster[i].intY-1].intIntegrity=0)  and (CheckForMonster(Monster[i].intX,Monster[i].intY-1)=false) then
                    dec(Monster[i].intY);
                if (j=2) and (DngLvl[Monster[i].intX+1,Monster[i].intY].intIntegrity=0)  and (CheckForMonster(Monster[i].intX+1,Monster[i].intY)=false) then
                    inc(Monster[i].intX);
                if (j=3) and (DngLvl[Monster[i].intX-1,Monster[i].intY].intIntegrity=0)  and (CheckForMonster(Monster[i].intX-1,Monster[i].intY)=false) then
                    dec(Monster[i].intX);
            end;
        end;

        // water monsters only on water
        if (DngLvl[Monster[i].intX,Monster[i].intY].intFloorType<>5) and (Monster[i].blWater=true) then
        begin
            Monster[i].intX := oldx;
            Monster[i].intY := oldy;
        end;

        // non-water monsters only on land
        if (DngLvl[Monster[i].intX,Monster[i].intY].intFloorType=5) and (Monster[i].blWater=false) then
        begin
            Monster[i].intX := oldx;
            Monster[i].intY := oldy;
        end;

        // trap
        if DngLvl[Monster[i].intX,Monster[i].intY].intFloorType=35 then
        begin
            Monster[i].intHP:=Monster[i].intHP div 2;
            if Monster[i].intHP<1 then
                Monster[i].intHP:=1;
            strMsg:=lowercase(LeftStr(Monster[i].strName, 1));
            if (strMsg='a') or (strMsg='e') or (strMsg='u') or (strMsg='o') or (strMsg='i') then
                ShowTransMessage('One of your traps hit an ' + Monster[i].strName + '.', false)
            else
                ShowTransMessage('One of your traps hit a ' + Monster[i].strName + '.', false);
        end;

        // lava
        if DngLvl[Monster[i].intX,Monster[i].intY].intFloorType=20 then
        begin
            dec(Monster[i].intHP,1+trunc(random(3)));
            if Monster[i].intHP<1 then
                Monster[i].intHP:=1;
        end;

        // air effects
        // - fire
        if DngLvl[Monster[i].intX,Monster[i].intY].intAirType=1 then
        begin
            if Monster[i].blFire=true then
                inc(Monster[i].intHP, DngLvl[Monster[i].intX,Monster[i].intY].intAirRange)
            else
            begin
                dec(Monster[i].intHP, DngLvl[Monster[i].intX,Monster[i].intY].intAirRange);
                if Monster[i].intHP<1 then
                    IsMonsterDead(i);
            end;
        end;

        // - ice (yes, ice monsters get freezed, but regain new powers because of this)
        if DngLvl[Monster[i].intX,Monster[i].intY].intAirType=2 then
        begin
            if Monster[i].blIce=true then
                inc(Monster[i].intHP, DngLvl[Monster[i].intX,Monster[i].intY].intAirRange div 2)
            else
            begin
                dec(Monster[i].intHP, DngLvl[Monster[i].intX,Monster[i].intY].intAirRange div 2);
                if Monster[i].intHP<1 then
                    IsMonsterDead(i);
            end;
        end;

        // - water
        if DngLvl[Monster[i].intX,Monster[i].intY].intAirType=3 then
        begin
            if Monster[i].blWater=true then
                inc(Monster[i].intHP, DngLvl[Monster[i].intX,Monster[i].intY].intAirRange div 3)
            else
            begin
                dec(Monster[i].intHP, DngLvl[Monster[i].intX,Monster[i].intY].intAirRange div 3);
                if Monster[i].intHP<1 then
                    IsMonsterDead(i);
            end;
        end;

        // - heal aura
        if DngLvl[Monster[i].intX,Monster[i].intY].intAirType=4 then
            inc(Monster[i].intHP, DngLvl[Monster[i].intX,Monster[i].intY].intAirRange);

        // - reduce effects
        if Monster[i].intInvis>0 then
            dec(Monster[i].intInvis);

    end;
end;


// Here all special effects (confusion, poison, curse etc.) are ticking
procedure EffectTicker;
begin

    // freeze time
    if ThePlayer.intFreeze > 0 then
        dec(ThePlayer.intFreeze);


    if ThePlayer.intFreeze = 0 then
    begin

        // confusion
        if ThePlayer.intConfusion > 0 then
            dec(ThePlayer.intConfusion);


        // poison
        if ThePlayer.intPoison > 0 then
        begin
            dec(ThePlayer.intPoison);
            ShowDamage(1);
        end;


        // paralyze
        if ThePlayer.intPara > 0 then
            dec(ThePlayer.intPara);
    
    
        // calm
        if ThePlayer.intCalm > 0 then
            dec(ThePlayer.intCalm);

    
        // invisibility
        if ThePlayer.intInvis > 0 then
            dec(ThePlayer.intInvis);


        // blindness
        if ThePlayer.intBlind > 0 then
            dec(ThePlayer.intBlind);

    
        // wall
        if ThePlayer.intWall > 0 then
            dec(ThePlayer.intWall);

        // strength drug addiction symptoms
        if ThePlayer.intNextVitari>0 then
        begin
            inc(ThePlayer.intNeedVitari);
            if ThePlayer.intNeedVitari >= ThePlayer.intNextVitari then
            begin
                if (random(100)>50) and (ThePlayer.intPP>0) then
                    dec(ThePlayer.intPP);
                if random(100)>50 then
                begin
                    dec(ThePlayer.intHP);
                    ShowDamage(1);
                end;
                if random(100)>50 then
                    inc(ThePlayer.intStrength);
            end;
        end;

    end;
end;



// actions of gods (depending on number of prayers, humility skill and number of turns)
procedure GodAction;
begin

    if ThePlayer.blEvil=false then
    begin
    if longTotalTurns > 100 then
        begin

            // curse player
            if (longTotalTurns > CONST_MINTURNFORCURSE) and (ThePlayer.longPrayers = 0) then
            begin
                GetKeyInput('You never honored your deity; now ' + ThePlayer.strReli + ' is angry with you!', true);
                inc(ThePlayer.longPrayers, 1); // increase prayers to avoid repetition of this action
                dec(ThePlayer.intHumility, 3);
                if ThePlayer.intHumility<1 then
                begin
                    ThePlayer.blCurse:=true;
                    ThePlayer.blBlessed:=false;
                end;
            end;

            // TODO: don't hardcode this; instead use the "AffectedStatus" / "AffectedSkill" properties of the class

            // give player bonus 1
            if (ThePlayer.longPrayers>10) and (ThePlayer.intHumility > 4) and (ThePlayer.intHumility < 7) then
            begin
                case ThePlayer.intReli of
                    1:
                    begin   // Aphrodite
                        inc(ThePlayer.intMaxPP, ThePlayer.intHumility + trunc(random(ThePlayer.intHumility)));
                        ThePlayer.intPP := ThePlayer.intMaxPP;
                    end;
                    2:
                    begin   // Hermes
                        inc(ThePlayer.longExp, ThePlayer.intHumility * 30);
                    end;
                    3:
                    begin   // Apoll
                        inc(ThePlayer.longGold, ThePlayer.intHumility * 30);
                    end;
                    4:
                    begin   // Dionysos
                        inc(ThePlayer.longFood, ThePlayer.intHumility * 50);
                    end;
                    5:
                    begin   // Ares
                        inc(ThePlayer.intMaxHP, ThePlayer.intHumility + trunc(random(ThePlayer.intHumility)));
                        ThePlayer.intHP := ThePlayer.intMaxHP;
                    end;
                end;

                ThePlayer.intHumility := 7;
                GetKeyInput('Your relation to ' + ThePlayer.strReli + ' improves.', true);
            end;

            // give player bonus 2
            if (ThePlayer.longPrayers>500) and (ThePlayer.intHumility > 12) and (ThePlayer.intHumility < 15) then
            begin
                case ThePlayer.intReli of
                    1:
                    begin   // Aphrodite
                        inc(ThePlayer.intChant, 2);
                    end;
                    2:
                    begin   // Hermes
                        inc(ThePlayer.intBurgle, 2);
                    end;
                    3:
                    begin   // Apoll
                        inc(ThePlayer.intView, 2);
                    end;
                    4:
                    begin   // Dionysos
                        inc(ThePlayer.intMove, 2);
                    end;
                    5:
                    begin   // Ares
                        inc(ThePlayer.intFight, 2);
                    end;
                end;

                ThePlayer.intHumility := 15;
                GetKeyInput('Your relation to ' + ThePlayer.strReli + ' improves again.', true);
            end;

        end;
    end;
end;



// here the next turn after a player action is calculated
function NextTurn(): boolean;
    var
        n, i, j: integer;
        TraderTypeOK: boolean;
        dummy: string;
begin

    NextTurn := false;        // true if player is dead
    dummy:='';
    inc(longTotalTurns);    // the total number of turns the game is lasting is increased by 1

    // make current map position known to player
    if DngLvl[ThePlayer.intX,ThePlayer.intY].intAirType<>5 then
        DngLvl[ThePlayer.intX,ThePlayer.intY].blKnown := true;

    // do the following only if time is not frozen
    if ThePlayer.intFreeze=0 then
    begin
        // effects (like poison, barrier, confusion...)
        // temporary resistances are processed further below
        EffectTicker;

        // if player has low humility and is not yet evil, make him so
        if ThePlayer.intHumility<-5 then
            ThePlayer.blEvil:=true;

        // move monsters
        dummy:=MoveMonsters;

        // if monster hive stands within fire aura, decrease its integrity
        if (HiveX>-1) and (HiveY>-1) then
            if DngLvl[HiveX,HiveY].intAirType=1 then
                if DngLvl[HiveX,HiveY].intIntegrity>0 then
                begin
                    dec(DngLvl[HiveX,HiveY].intIntegrity, DngLvl[HiveX,HiveY].intAirRange);
                    if DngLvl[HiveX,HiveY].intIntegrity<1 then
                    begin
                        DngLvl[HiveX,HiveY].intFloorType:=2;
                        HiveDestroyed;
                    end;
                end;

        // regain HP, if item with HP regeneration (.intEffect=4) is worn
        if dummy='' then
            if CheckEffect(4)=true then
                if ThePlayer.intHP < ThePlayer.intMaxHP then
                    inc(ThePlayer.intHP);

        // regain PP, if item with PP regeneration (.intEffect=3) is worn
        if dummy='' then
            if CheckEffect(3)=true then
                if ThePlayer.intPP < ThePlayer.intMaxPP then
                    inc(ThePlayer.intPP);

        // regain PP, if sitting on a stool
        if DngLvl[ThePlayer.intX,ThePlayer.intY].intFloorType=50 then
            if ThePlayer.intPP < ThePlayer.intMaxPP then
                inc(ThePlayer.intPP);

        // reduce temporary resistances
        for i:=1 to 100 do
            if ThePlayer.intTempResist[i]>0 then
                dec(ThePlayer.intTempResist[i]);

        // refresh chants
        for i:=1 to 12 do
            if spellbook[i].intType>0 then
                if spellbook[i].intRefresh < spell[spellbook[i].intType].intRefresh then
                    inc(spellbook[i].intRefresh);

        // decrease food
        dec (ThePlayer.longFood);

        // if food is below 200, decrease strength until it is 0
        if (ThePlayer.longFood < 200) and (ThePlayer.intStrength>0) then
            dec(ThePlayer.intStrength);

        // if food is greater than 199 and strength is less then 100, increase strength
        if (ThePlayer.longFood>199) and (ThePlayer.intStrength<100) then
            inc(ThePlayer.intStrength);

        // if strength is less than the minimum needed strength for the current weapon, unequip the weapon
        if ThePlayer.intWeapon>0 then
            if (ThePlayer.intStrength<Thing[ThePlayer.intWeapon].intStrength) then
            begin
                ThePlayer.intWeapon:=0;
                GetKeyInput('You are too weak to carry your weapon.', true);
            end;

        // gods
        GodAction;

        // if food < 1 then let the player die from starving
        if ThePlayer.longFood < 1 then
        begin
            GetKeyInput('You die from starving.', true);
            ThePlayer.intHP:=0;
            GameOver('Died from starving');
        end;
    
        // if the player is poisoned, decrease HP by current poison value
        if ThePlayer.intPoison > 0 then
        begin
            dec(ThePlayer.intHP, ThePlayer.intPoison);
            if ThePlayer.intHP<1 then
            begin
                GetKeyInput('You die from poison.', true);
                GameOver('Poisoned to death');
            end;
        end;

        // if strength is greater than 100, a strength drug is used
        if ThePlayer.intStrength > 100 then
        begin
            // do negative effects on HP, if drug is used to extensivly
            if trunc(random(ThePlayer.intStrength-100))>trunc(random(ThePlayer.intStrength)) then
                dec(ThePlayer.intHP, 1+trunc(random(3)));
            dec(ThePlayer.intStrength);
        end;
    
        // if player is walking on lava, decrease player's HP
        if DngLvl[ThePlayer.intX,ThePlayer.intY].intFloorType = 20 then
            if ThePlayer.blEvil=false then
                if CheckEffect(13)=false then
                begin
                    dec(ThePlayer.intHP,2);
                    ShowDamage(2);
                    if ThePlayer.intHP<1 then
                    begin
                        GetKeyInput('The lava hurts you to death.', true);
                        GameOver('Died while walking through lava');
                    end;
                end;

        // if the player is walking through heal aura air, increase his HP
        if DngLvl[ThePlayer.intX,ThePlayer.intY].intAirType=4 then
            if ThePlayer.blEvil=false then
                if ThePlayer.intHP<ThePlayer.intMaxHP-2 then
                    inc(ThePlayer.intHP, DngLvl[ThePlayer.intX,ThePlayer.intY].intAirRange);

        if ThePlayer.intHP < 1 then
            NextTurn := true;

        // if air is affected by auras (except darkness), reduce the amount (1 per turn, until it's cleaned)
        for i:=1 to DngMaxWidth do
            for j:=1 to DngMaxHeight do
                if DngLvl[i,j].intAirRange>0 then
                    if DngLvl[i,j].intAirType<>5 then
                    begin
                        dec(DngLvl[i,j].intAirRange);
                        if DngLvl[i,j].intAirRange<1 then
                            DngLvl[i,j].intAirType:=0;
                    end;


        // refill shops (1-4)
        if (longTotalTurns=1) or (random(500) > 480) then
            for i:=1 to 4 do  // shop number ...
                for j:=1 to 16 do  // shop items
                begin
                    TraderTypeOK:=false;
                    repeat

                        repeat
                            n := trunc(random(ItemCount)+1);
//                             writeln('Item '+IntToStr(n));
                        until DungeonLevel>Thing[n].intMinLvl-1;

                        if n=0 then
                            n:=ItemCount;
        
                        // add items to shop depending on the current shop number (which is the trader type)

                        if (i=1) and (Thing[n].blRing=false) and (Thing[n].blWield=false) and (Thing[n].blWear=false) and (Thing[n].blHat=false) and ((Thing[n].blEat=true) or (Thing[n].blBarricade=true) or (Thing[n].blDrink=true)) then
                            TraderTypeOK := true;
    
                        if (i=2) and (TraderTypeOK=false) and (Thing[n].blEat=false) and (Thing[n].blDrink=false) and (Thing[n].blWear=false) and (Thing[n].blHat=false) and ((Thing[n].blWield=true) or (Thing[n].blRing=true) or (Thing[n].intIsAmmu>0) or (Thing[n].chLetter=chr(191))) then
                            TraderTypeOK := true;
    
                        if (i=3) and (TraderTypeOK=false) and (Thing[n].blEat=false) and (Thing[n].blDrink=false) and (Thing[n].blWield=false) and ((Thing[n].blWear=true) or (Thing[n].blShoes=true) or (Thing[n].blHat=true) or (Thing[n].blExtra=true)) then
                            TraderTypeOK := true;
    
                        if (i=4) and(TraderTypeOK=false) and ((Thing[n].chLetter=chr(161)) or (Thing[n].chLetter=chr(193))) then
                            TraderTypeOK := true;
                    until TraderTypeOK = true;

                    // make sure that we only put non-unique and non-rare items in the shop
                    if (Thing[n].blUnique = false) and (Thing[n].blRare = false) then
                        MyShop[i].Inventory[j].intType := n
                    else
                        MyShop[i].Inventory[j].intType := 0;
                end;
    
        // calculate time, day and year
        if intDayTime<250 then
            inc(intDayTime);

        if intDayTime=250 then
        begin
            if intDay<21 then
                inc(intDay);
            intDayTime:=1;
        end;

        if intDay=21 then
        begin
            inc(longYear);
            intDay:=1;
        end;

        // respawn monsters
        if DungeonLevel>1 then
            if (GetTotalMonsters<MaxMonster) and (GetFirstFreeMonsterID>-1) and (random(500)>CONST_SPAWN) then
                CreateMonster(GetFirstFreeMonsterID, '-', DungeonLevel);

    end;

    ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
    ShowTransMessage(dummy, false);

end;


// rest
procedure Rest;
    var
        i, n: integer;
begin
    n := GetNumericalInput('How long do you want to rest?', 4);

    i:=1;

    while i<=n do
    begin
        ShowTransMessage('Resting (turn ' + IntToStr(i) + ' of ' + IntToStr(n) + ') ...', false);
        if NextTurn then break; //NextTurn returns true when/if player dies
        inc(i);
    end;
end;


// identify tile
procedure IdentifyTile;
    var
        chDir: char;
        lookX, lookY: integer;
        strDir: string;
begin
    chDir := GetDirection(0);

    ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);

    if chDir=KeyNorth then
    begin
        lookX:=ThePlayer.intX;
        lookY:=ThePlayer.intY-1;
        strDir:='north';
    end;

    if chDir=KeySouth then
    begin
        lookX:=ThePlayer.intX;
        lookY:=ThePlayer.intY+1;
        strDir:='south';
    end;

    if chDir=KeyEast then
    begin
        lookX:=ThePlayer.intX+1;
        lookY:=ThePlayer.intY;
        strDir:='east';
    end;

    if chDir=KeyWest then
    begin
        lookX:=ThePlayer.intX-1;
        lookY:=ThePlayer.intY;
        strDir:='west';
    end;

    if chDir=KeyNorthEast then
    begin
        lookX:=ThePlayer.intX+1;
        lookY:=ThePlayer.intY-1;
        strDir:='northeast';
    end;

    if chDir=KeySouthEast then
    begin
        lookX:=ThePlayer.intX+1;
        lookY:=ThePlayer.intY+1;
        strDir:='southeast';
    end;

    if chDir=KeyNorthWest then
    begin
        lookX:=ThePlayer.intX-1;
        lookY:=ThePlayer.intY-1;
        strDir:='northwest';
    end;

    if chDir=KeySouthWest then
    begin
        lookX:=ThePlayer.intX-1;
        lookY:=ThePlayer.intY+1;
        strDir:='southwest';
    end;

    case DngLvl[lookX,lookY].intFloorType of
        1:
            msg1:='a wall';
        2:
            msg1:='plain floor';
        3:
            msg1:='a closed door';
        4:
            msg1:='an open door';
        5:
            msg1:='water';
        6:
        begin
            if DungeonLevel=1 then
                msg1:='a mountain'
            else
                msg1:='solid rock';
        end;
        7:
            msg1:='a closed treasure chest';
        8:
            msg1:='a staircase to the previous dungeon level';
        9:
            msg1:='a staircase to a deeper dungeon level';
        10:
            msg1:='a small tree';
        11:
            msg1:='worm excrements';
        12:
            msg1:='mud or sand';
        13:
            msg1:='a big tree';
        14:
            msg1:='an open treasure chest';
        15:
            msg1:='an altar';
        16:
            msg1:='plain floor';
        17:
            msg1:='grass';
        18:
            msg1:='a hill';
        19:
            msg1:='ash';
        20:
            msg1:='lava';
        21:
            msg1:='a small dry tree';
        22:
            msg1:='a big dry tree';
        23:
            msg1:='dry grass';
        24:
            msg1:='a locked door';
        25:
            msg1:='a wall';
        26:
            msg1:='a crypt';
        27:
            msg1:='a wall';
        28:
            msg1:='an enchanted well';
        29:
            msg1:='a terminal';
        30:
            msg1:='plain floor';
        31:
            msg1:='contaminated ground';
        32:
            msg1:='a wall';
        33:
            msg1:='a crypt';
        34:
            msg1:='a monster'+chr(39)+'s hive [Integrity: ' + IntToStr(DngLvl[lookX,lookY].intIntegrity)+']';
        35:
            msg1:='one of your traps';
        36:
            msg1:='sand';
        37:
            msg1:='sand';
        38:
            msg1:='shore';
        39:
            msg1:='shore';
        40:
            msg1:='shallow water';
        41:
            msg1:='sand';
        42:
            msg1:='sand';
        48:
            msg1:='just a wall';
        49:
            msg1:='a bookshelf';
        50:
            msg1:='a stool';
        51:
            msg1:='a table';
        52:
            msg1:='a barrel';
        53:
            msg1:='a big mushroom';
        54:
            msg1:='a small mushroom';
        55:
            msg1:='wooden garbage';
        56:
            msg1:='a gas cylinder';
        57:
            msg1:='machine oil';
        58:
            msg1:='old metal parts';
        59:
            msg1:='a magical portal';
        60:
            msg1:='snow';
        61:
            msg1:='ice';
    end;

    ShowTransMessage('In the '+strDir+', you see ' + msg1 + '.', false);

end;

procedure ItemInfo(id: integer);
var
   strSet, effect, strProf, dummy: string;
   IconRect, IconRectS: SDL_RECT;

begin

    if Thing[id].intEffect>-1 then
        IconRect.x:=38*(Thing[id].intEffect-1)
    else
        IconRect.x:=0;

    IconRect.y:=415;
    IconRect.w:=38;
    IconRect.h:=38;

    IconRectS.x:=735+HiResOffsetX;
    IconRectS.y:=12+HiResOffsetY;
    IconRectS.w:=38;
    IconRectS.h:=38;

    if UseSDL=true then
    begin
        TempAnsi := PathData+'graphics/invbg.jpg';
        LoadImage_Title(PathData+'graphics/invbg.jpg');
    end;

    dummy:='-';
    repeat

        if UseSDL=true then
        begin
            BlitImage_Title;
            if Thing[id].intEffect>-1 then
                SDL_BLITSURFACE(extratiles, @IconRect, screen, @IconRectS);
        end
        else
        begin
            DialogWin;
            BottomBar;
        end;

        if Thing[id].strName<>'-' then
        begin

            if Thing[id].blTwoHands=false then
                TransTextXY(1,1, uppercase(Thing[id].strRealName))
            else
                TransTextXY(1,1, uppercase(Thing[id].strRealName) + ' (two-handed)');

            if Thing[id].strDescri<>'-' then
                TransTextXY(8,7, Thing[id].strDescri+'.');

            strSet:='';

            if Thing[id].blRare=true then
                strSet:='rare item';
            if Thing[id].blUnique=true then
                strSet:='unique item';

            if Thing[id].intRuneSet>0 then
            begin
                case Thing[id].intRuneSet of
                    1:
                        strSet:='set "Runes of the Drekh'+chr(39)+'Nar" (resurrection from death)';
                    2:
                        strSet:='set "Redemption" (20% defense bonus)';
                    3:
                        strSet:='set "Minor Books of Yron"';
                    4:
                        strSet:='set "History of NeoTerr"';
                    5:
                        strSet:='set "Minor Gear of Thagor" (20% melee bonus)';
                    6:
                        strSet:='set "Major Gear of Thagor" (20% long-range bonus)';
                    7:
                        strSet:='set "Combat Gear by Adrian Smith"';
                end;
            end;

            TransTextXY(8,8, strSet);

            TransTextXY(8,10, 'Standard Price: ' + IntToStr(Thing[id].intPrice));
            TransTextXY(30, 10, 'Retail Price: ' + IntToStr(CollectSellPrice(id)));

            if Thing[id].intWP > 0 then
                if Thing[id].blWield = true then
                    TransTextXY(8,12, 'WP: ' + IntToStr(Thing[id].intWP))
                else
                    TransTextXY(8,12, 'Melee bonus: ' + IntToStr(Thing[id].intWP));

            if Thing[id].intGP > 0 then
                if Thing[id].blWield = true then
                    TransTextXY(8,13, 'GP: ' + IntToStr(Thing[id].intGP))
                else
                    TransTextXY(8,13, 'Long-range bonus: ' + IntToStr(Thing[id].intGP));

            if Thing[id].intAP > 0 then
                if Thing[id].blWear = true then
                    TransTextXY(8,14, 'AP: ' + IntToStr(Thing[id].intAP))
                else
                    TransTextXY(8,14, 'Defense bonus: ' + IntToStr(Thing[id].intAP));

            if Thing[id].intSP > 0 then
                TransTextXY(8,15, 'SP: ' + IntToStr(Thing[id].intSP));

            if Thing[id].intLight > 0 then
                TransTextXY(8,16, 'Light bonus: ' + IntToStr(Thing[id].intLight));

            effect:='none';
            strProf:='all professions';

            case Thing[id].intEffect of
                5:
                    effect:='makes invisible';
                6:
                    effect:='gives extra EXP';
                7:
                    effect:='gives extra Credits';
                10:
                    effect:='protects from poison';
                11:
                    effect:='protects from confusion';
                13:
                    effect:='protects from fire';
                14:
                    effect:='protects from ice';
                17:
                    effect:='protects from blindness';
                25:
                    effect:='adds '+IntToStr(Thing[id].intRange)+' to the Move-skill during battles';
                26:
                    effect:='protects from paralization';
                28:
                    effect:='protects from calm';
                31:
                    effect:='protects from curse';
                35:
                    effect:='protects from water';
                36:
                    effect:='will not get lost';
                37:
                    effect:='maximizes the efficiency of magical chants';
                38:
                    effect:='attacks with fire';
                39:
                    effect:='attacks with ice';
                40:
                    effect:='attacks with water';
                41:
                    effect:='protects from traps';
                43:
                    effect:='works as compass';
                47:
                    effect:='identifies unknown items';
                48:
                    effect:='enlightens dark areas';
                49:
                    effect:='adds '+IntToStr(Thing[id].intRange)+' to DRG collection rate';
                50:
                    effect:='wipes out all monsters';
                51:
                    effect:='preserves PP';
                52:
                    effect:='preserves STR';
            end;

            case Thing[id].intProf of
                2:
                    strProf:='enchanter';
                3:
                    strProf:='thief';
                4:
                    strProf:='archer';
                5:
                    strProf:='soldier';
            end;

            TransTextXY(30,12, 'Profession      : ' + strProf);
            TransTextXY(30,13, 'Minimum level   : ' + IntToStr(Thing[id].intCharLvl));
            if Thing[id].blWield=true then
                TransTextXY(30,14, 'Minimum STR     : ' + IntToStr(Thing[id].intStrength));
            if (Thing[id].blEat=true) or (Thing[id].blDrink=true) then
                if Thing[id].blWield=false then
                    TransTextXY(30,15, 'Efficiency      : ' + IntToStr(Thing[id].intRange))
                else
                    TransTextXY(30,16, 'Efficiency      : ' + IntToStr(Thing[id].intRange));

            if effect<>'none' then
                TransTextXY(8,18, 'The '+Thing[id].strRealName+' '+ effect + '.');

            dummy:=GetKeyInput('[ESC] Resume game', false);
        end;
    until dummy='ESC';
end;



// select which item to steal
procedure StealSelection(intID: integer);
    var
        MouseItem, i, t: integer;
        n, p: longint;
        ch, dummy, equi: string;
        blRemItem: boolean;
begin
    DecoIcon.w:=80;
    DecoIcon.y:=80;
    DecoIcon.h:=80;

    case intID of
        1:
            DecoIcon.x:=360;
        2:
            DecoIcon.x:=280;
        3:
        begin
            DecoIcon.x:=220;
            DecoIcon.w:=60;
        end;
        4:
            DecoIcon.x:=0;
    end;

    DecoIconS.x:=711+HiResOffsetX;
    DecoIconS.y:=400+HiResOffsetY;
    DecoIconS.w:=72;
    DecoIconS.h:=72;

    ClearScreenSDL;

    if UseSDL=true then
    begin
        TempAnsi := PathData+'graphics/invbg.jpg';
        LoadImage_Title(PathData+'graphics/invbg.jpg');
    end;

    repeat
        if UseSDL=true then
        begin
            BlitImage_Title;
            SDL_BLITSURFACE(extratiles, @DecoIcon, screen, @DecoIconS);
        end
        else
        begin
            DialogWin;
            StatusDeco;
        end;


        TransTextXY(1, 1, 'STEAL FROM ' + uppercase(Shops[MyShop[intID].intType]) + '.');

        for i:=1 to 16 do
        begin
            if MyShop[intID].Inventory[i].intType > 0 then
            begin

                p := CollectBuyPrice(MyShop[intID].Inventory[i].intType);

                TransTextXY(6, 4+i, IntToStr(i));
                TransTextXY(10, 4+i, thing[MyShop[intID].Inventory[i].intType].chLetter + ' ' + thing[MyShop[intID].Inventory[i].intType].strRealName);
                TransTextXY(40, 4+i, '$' + IntToStr(p));

                equi:='';

                // mark items with + - =
                if ThePlayer.intArmour > 0 then
                begin
                    if thing[MyShop[intID].Inventory[i].intType].intAP > 0 then
                        if thing[MyShop[intID].Inventory[i].intType].blWear = true then
                        begin
                            if thing[MyShop[intID].Inventory[i].intType].intAP > thing[ThePlayer.intArmour].intAP then
                                equi:=' '+chr(214);
                            if thing[MyShop[intID].Inventory[i].intType].intAP < thing[ThePlayer.intArmour].intAP then
                                equi:=' '+chr(215);
                            if thing[MyShop[intID].Inventory[i].intType].intAP = thing[ThePlayer.intArmour].intAP then
                                equi:=' '+chr(216);
                        end;

                    if thing[MyShop[intID].Inventory[i].intType].blIdentified = false then
                        if thing[MyShop[intID].Inventory[i].intType].blWear = true then
                            equi:=' '+chr(217);
                end;

                if ThePlayer.intHat > 0 then
                begin
                    if thing[MyShop[intID].Inventory[i].intType].intAP > 0 then
                        if thing[MyShop[intID].Inventory[i].intType].blHat = true then
                        begin
                            if thing[MyShop[intID].Inventory[i].intType].intAP > thing[ThePlayer.intHat].intAP then
                                equi:=' '+chr(214);
                            if thing[MyShop[intID].Inventory[i].intType].intAP < thing[ThePlayer.intHat].intAP then
                                equi:=' '+chr(215);
                            if thing[MyShop[intID].Inventory[i].intType].intAP = thing[ThePlayer.intHat].intAP then
                                equi:=' '+chr(216);
                        end;

                    if thing[MyShop[intID].Inventory[i].intType].blIdentified = false then
                        if thing[MyShop[intID].Inventory[i].intType].blHat = true then
                            equi:=' '+chr(217);
                end;

                if ThePlayer.intWeapon > 0 then
                begin
                    if thing[MyShop[intID].Inventory[i].intType].intWP > 0 then
                        if thing[ThePlayer.intWeapon].intWP > 0 then
                            if thing[MyShop[intID].Inventory[i].intType].blWield = true then
                            begin
                                if thing[MyShop[intID].Inventory[i].intType].intWP > thing[ThePlayer.intWeapon].intWP then
                                    equi:=' '+chr(214);
                                if thing[MyShop[intID].Inventory[i].intType].intWP < thing[ThePlayer.intWeapon].intWP then
                                    equi:=' '+chr(215);
                                if thing[MyShop[intID].Inventory[i].intType].intWP = thing[ThePlayer.intWeapon].intWP then
                                    equi:=' '+chr(216);
                            end;

                    if thing[MyShop[intID].Inventory[i].intType].intGP > 0 then
                        if thing[ThePlayer.intWeapon].intGP > 0 then
                            if thing[MyShop[intID].Inventory[i].intType].blWield = true then
                            begin
                                if thing[MyShop[intID].Inventory[i].intType].intGP > thing[ThePlayer.intWeapon].intGP then
                                    equi:=' '+chr(214);
                                if thing[MyShop[intID].Inventory[i].intType].intGP < thing[ThePlayer.intWeapon].intGP then
                                    equi:=' '+chr(215);
                                if thing[MyShop[intID].Inventory[i].intType].intGP = thing[ThePlayer.intWeapon].intGP then
                                    equi:=' '+chr(216);
                            end;

                    if thing[MyShop[intID].Inventory[i].intType].blIdentified = false then
                        if thing[MyShop[intID].Inventory[i].intType].blWield = true then
                            equi:=' '+chr(217);
                end;

                // mark items with a higher clvl
                if ThePlayer.intLvl<Thing[MyShop[intID].Inventory[i].intType].intCharLvl then
                    equi:=' '+chr(218);

                TransTextXY(45, 4+i, equi);

                // mark items with a higher skill
                if (Thing[MyShop[intID].Inventory[i].intType].chLetter=chr(156)) then
                   if ThePlayer.intSword < Thing[MyShop[intID].Inventory[i].intType].intWP then
                       TransTextXY(52, 4+i, 'Skill!');
                if (Thing[MyShop[intID].Inventory[i].intType].chLetter=chr(159)) then
                   if ThePlayer.intAxe < Thing[MyShop[intID].Inventory[i].intType].intWP then
                       TransTextXY(52, 4+i, 'Skill!');
                if (Thing[MyShop[intID].Inventory[i].intType].chLetter=chr(164)) then
                   if ThePlayer.intWhip < Thing[MyShop[intID].Inventory[i].intType].intWP then
                       TransTextXY(52, 4+i, 'Skill!');
                if (Thing[MyShop[intID].Inventory[i].intType].chLetter=chr(158)) then
                   if ThePlayer.intGun < Thing[MyShop[intID].Inventory[i].intType].intGP then
                       TransTextXY(52, 4+i, 'Skill!');

                // mark useless items
                if (ThePlayer.intProf<>Thing[MyShop[intID].Inventory[i].intType].intProf) and (Thing[MyShop[intID].Inventory[i].intType].intProf>0) then
                begin
                    case Thing[MyShop[intID].Inventory[i].intType].intProf of
                        1:
                            TransTextXY(59, 4+i, 'Constructor!');
                        2:
                            TransTextXY(59, 4+i, 'Enchanter!');
                        3:
                            TransTextXY(59, 4+i, 'Thief!');
                        4:
                            TransTextXY(59, 4+i, 'Archer!');
                        5:
                            TransTextXY(59, 4+i, 'Soldier!');
                    end;
                end;

            end;
        end;

        n := -1;

        dummy := GetKeyInput('[s]teal an item  [l]ook at an item   Press [ESC] to leave '+ Shops[MyShop[intID].intType]+'.', false);

    until (dummy='s') or (dummy='l') or (dummy='ESC');

    ch := dummy;

    if MouseItem>0 then
        n:=MouseItem;

    if ch='l' then
    begin
        if (UseSDL=false) or (UseMouseToMove=false) then
            repeat
                n := GetNumericalInput('Enter the number of the item you wish to see [ENTER to cancel]:', 2);
            until (n=0) or ((n>0) and (n<17) and (MyShop[intID].Inventory[n].intType>0));

        if n>0 then
            ItemInfo(MyShop[intID].Inventory[n].intType);
    end;

    if ch='s' then
    begin
        if (UseSDL=false) or (UseMouseToMove=false) then
            repeat
                n := GetNumericalInput('Enter the number of the item you wish to steal [ENTER to cancel]:', 2);
            until (n=0) or ((n>0) and (n<17) and (MyShop[intID].Inventory[n].intType>0));

        if n>0 then
        begin

            t := MyShop[intID].Inventory[n].intType;

            blRemItem:=false;

            // check if player already has one item of this type
            for i:=1 to 16 do
                if inventory[i].intType = t then
                begin
                    inc(inventory[i].longNumber, Thing[Inventory[i].intType].intAmount);
                    blRemItem:=true;
                    break;
                end;

            // if item not bought, check if free space available
            if blRemItem=false then
                for i:=1 to 16 do
                    if (inventory[i].intType = 0) and (blRemItem=false) then
                    begin
                        inventory[i].intType := t;
                        inventory[i].longNumber := Thing[Inventory[i].intType].intAmount;
                        blRemItem:=true;
                        break;
                    end;

            // if item taken, identify item type and remove item from store
            if blRemItem then
            begin
                Thing[MyShop[intID].Inventory[n].intType].blIdentified := true;
                Thing[MyShop[intID].Inventory[n].intType].strName := Thing[MyShop[intID].Inventory[n].intType].strRealName;
                MyShop[intID].Inventory[n].intType := 0;
                blTodayStolenTrader:=true;
                GetKeyInput('You steal the ' + Thing[t].strName + '.', true);
            end
            else
                GetKeyInput('You want to steal the ' + Thing[t].strName + ', but your inventory is full.', true);

        end;
    end;

    ClearScreenSDL;
end;


procedure TrainSkill(points: integer);
    var
        dummy: string;
        ch: char;
begin

    if UseSDL=true then
    begin
        TempAnsi := PathData+'graphics/invbg.jpg';
        LoadImage_Title(PathData+'graphics/invbg.jpg');
    end;

    while points>0 do
    begin

        repeat
            if UseSDL=true then
                BlitImage_Title
            else
            begin
                ClearScreenSDL;
                DialogWin;
                StatusDeco;
            end;

            TransTextXY(1, 1, 'SKILL TRAINING');
            TransTextXY(8, 4,  IntToStr(points)+' skill point(s) left.');

            TransTextXY(8, 6,  'a  Fight     influences your general skills in combat      [' + IntToStr(ThePlayer.intFight) + ']');
            TransTextXY(8, 7, 'b  View      determines if long-range attacks hit or miss  [' + IntToStr(ThePlayer.intView) + ']');
            TransTextXY(8, 8, 'c  Chant     determines the success of casting a chant     [' + IntToStr(ThePlayer.intChant) + ']');
            TransTextXY(8, 9, 'd  Move      influences your ability to evade attacks      [' + IntToStr(ThePlayer.intMove) + ']');
            TransTextXY(8, 10, 'e  Steal     allows stealing without being caught          [' + IntToStr(ThePlayer.intBurgle) + ']');
            TransTextXY(8, 11, 'f  Sword     determines the efficiency in using swords     [' + IntToStr(ThePlayer.intSword) + ']');
            TransTextXY(8, 12, 'g  Axe       determines the efficiency in using axes       [' + IntToStr(ThePlayer.intAxe) + ']');
            TransTextXY(8, 13, 'h  Lance     determines the efficiency in using lances     [' + IntToStr(ThePlayer.intWhip) + ']');
            TransTextXY(8, 14, 'i  Fire-arm  determines the efficiency in using bows       [' + IntToStr(ThePlayer.intGun) + ']');
            TransTextXY(8, 15, 'j  Tool      determines the effeciency in using spades     [' + IntToStr(ThePlayer.intTool) + ']');
            TransTextXY(8, 16, 'k  Humility  affects your relation to your deity           [' + IntToStr(ThePlayer.intHumility) + ']');
            TransTextXY(8, 17, 'l  Trade     gives you advantages when buying or selling   [' + IntToStr(ThePlayer.intTrade) + ']');

            ch:=' ';
            dummy := GetKeyInput('Select the skill to train by pressing the related button.', false);
        until (dummy='a') or (dummy='b') or (dummy='c') or (dummy='d') or (dummy='e') or (dummy='f')  or (dummy='g') or (dummy='h') or (dummy='i') or (dummy='j') or (dummy='k') or (dummy='l');
        ch:=dummy[1];

        case ch of
            'a':
                inc(ThePlayer.intFight, 1);
            'b':
                inc(ThePlayer.intView, 1);
            'c':
                inc(ThePlayer.intChant, 1);
            'd':
                inc(ThePlayer.intMove, 1);
            'e':
                inc(ThePlayer.intBurgle, 1);
            'f':
                inc(ThePlayer.intSword, 1);
            'g':
                inc(ThePlayer.intAxe, 1);
            'h':
                inc(ThePlayer.intWhip, 1);
            'i':
                inc(ThePlayer.intGun, 1);
            'j':
                inc(ThePlayer.intTool, 1);
            'k':
                inc(ThePlayer.intHumility, 1);
            'l':
                inc(ThePlayer.intTrade, 1);
        end;

        dec(points);

    end;


    // Check for promotions; ranks not listed here are gained by solving quests

    // can centurio become hastatus?
    if ThePlayer.intDipl[1]=1 then  // ok, is centurio
        if ThePlayer.intWhip > 10 then  // ok, minimum lance requirement is met
            if ThePlayer.intDipl[2]=0 then  // ok, not yet promoted to Hastatus
            begin
                ThePlayer.intDipl[2]:=1;
                ShowTransMessage('You have been promoted to the rank "Hastatus".', true);
            end;

    // can princeps become pilus?
    if ThePlayer.intDipl[3]=1 then  // ok, is princeps
        if (ThePlayer.intSword > 14) and (ThePlayer.intAxe > 14) and (ThePlayer.intWhip > 14) then  // ok, minimum lance requirement is met
            if ThePlayer.intDipl[4]=0 then  // ok, not yet promoted to Pilus
            begin
                ThePlayer.intDipl[4]:=1;
                ShowTransMessage('You have been promoted to the rank "Pilus".', true);
            end;

    // can mage become battlemage?
    if ThePlayer.intDipl[6]=1 then  // ok, is mage
        if ThePlayer.intFight > 12 then  // ok, minimum fight requirement is met
            if (ThePlayer.intSword > 12) or (ThePlayer.intAxe > 12) then // ok, minimum sword/axe req. met
                if ThePlayer.intDipl[7]=0 then  // ok, not yet promoted to battlemage
                begin
                    ThePlayer.intDipl[7]:=1;
                    ShowTransMessage('You have been promoted to the rank "Battlemage".', true);

                end;

    // can believer become monk?
    if ThePlayer.intDipl[8]=1 then  // ok, is believer
        if ThePlayer.intChant > 12 then  // ok, minimum fight requirement is met
            if ThePlayer.intDipl[9]=0 then  // ok, not yet promoted to monk
            begin
                ThePlayer.intDipl[9]:=1;
                ShowTransMessage('You have been promoted to the rank "Monk".', true);
            end;

    // can monk become holy warrior?
    if ThePlayer.intDipl[9]=1 then  // ok, is monk
        if ThePlayer.intGun > 14 then  // ok, minimum gun requirement is met
            if ThePlayer.intDipl[10]=0 then  // ok, not yet promoted to holy warrior
            begin
                ThePlayer.intDipl[10]:=1;
                ShowTransMessage('You have been promoted to the rank "Holy Warrior".', true);

            end;

    // can hunter become ranger?
    if ThePlayer.intDipl[15]=1 then  // ok, is hunter
        if ThePlayer.intGun > 14 then  // ok, minimum gun requirement is met
            if ThePlayer.intDipl[16]=0 then  // ok, not yet promoted to ranger
            begin
                ThePlayer.intDipl[16]:=1;
                ShowTransMessage('You have been promoted to the rank "Ranger".', true);

            end;

    // can archer become marksman?
    if ThePlayer.intProf=4 then  // ok, is archer
        if ThePlayer.intGun > 12 then  // ok, minimum gun requirement is met
            if ThePlayer.intDipl[17]=0 then  // ok, not yet promoted to ranger
            begin
                ThePlayer.intDipl[17]:=1;
                ShowTransMessage('You have been promoted to the rank "Marksman".', true);

            end;

    // can master thief become guild leader?
    if ThePlayer.intDipl[13]=1 then  // ok, is master thief
        if ThePlayer.intBurgle > 14 then  // ok, minimum steal requirement is met
            if ThePlayer.intDipl[14]=0 then  // ok, not yet promoted to Guild leader
            begin
                ThePlayer.intDipl[14]:=1;
                ShowTransMessage('You have been promoted to the rank "Guild Leader".', true);

            end;

    // can assassin become agent?
    if ThePlayer.intDipl[11]=1 then  // ok, is assassin
        if (ThePlayer.intFight > 14) and (ThePlayer.intTrade >= 10) then  // ok, minimum requirements are met
            if ThePlayer.intDipl[12]=0 then  // ok, not yet promoted to Agent
            begin
                ThePlayer.intDipl[12]:=1;
                ShowTransMessage('You have been promoted to the rank "Agent".', true);

            end;
end;


// search for secrets, steal from shops (item) and npcs (money)
procedure SearchAndSteal;
    var
        vx, vy, i, m: integer;
        intShop: integer;
        blHasStolen: boolean;
begin

    // search for secret
    for i:=1 to 8 do
    begin
        case i of
            1:
            begin
                vx:=0;
                vy:=-1;
            end;
            2:
            begin
                vx:=0;
                vy:=1;
            end;
            3:
            begin
                vx:=1;
                vy:=0;
            end;
            4:
            begin
                vx:=-1;
                vy:=0;
            end;
            5:
            begin
                vx:=1;
                vy:=-1;
            end;
            6:
            begin
                vx:=-1;
                vy:=-1;
            end;
            7:
            begin
                vx:=1;
                vy:=1;
            end;
            8:
            begin
                vx:=-1;
                vy:=1;
            end;
        end;

        // hidden door
        if DngLvl[ThePlayer.intX+vx, ThePlayer.intY+vy].intFloorType=48 then
            if random(500)>CONST_SECRETSEARCH then
            begin
                DngLvl[ThePlayer.intX+vx, ThePlayer.intY+vy].intFloorType := 3;
                ShowTransMessage('You reveal a hidden door.', false);
            end;

        // bookshelf to study
        if DngLvl[ThePlayer.intX+vx, ThePlayer.intY+vy].intFloorType=49 then
            if ThePlayer.longSkillPoints > 0 then
            begin
                GetKeyInput('You study some of the books in the shelf ...', true);
                TrainSkill(ThePlayer.longSkillPoints);
                ThePlayer.longSkillPoints:=0;
            end;
    end;


    blHasStolen := false;

    // if thief, steal something
    if ThePlayer.intProf = 3 then
    begin
        intShop := DngLvl[ThePlayer.intX,ThePlayer.intY].intBuilding;

        // in front of a trader?
        if intShop>0 then
        begin
            if blTodayStolenTrader=false then
            begin
                if intShop<5 then
                begin
                    StealSelection(intShop);
                    if blTodayStolenTrader=true then
                    begin
                        blShopStolen[intShop]:=true;
                        blHasStolen := true;
                    end;
                end
                else
                    ShowTransMessage('This trader has nothing interesting to steal.', false);
            end
            else
                ShowTransMessage('You have already robbed a trader today.', false);
        end
        else
            // simple NPC?
        if CheckForNPC(ThePlayer.intX,ThePlayer.intY) = true then
        begin
            if blTodayStolenNPC=false then
            begin
                ShowTransMessage('You steal some money ...', false);
                inc(ThePlayer.longGold, ThePlayer.intBurgle);
                blTodayStolenNPC:=true;
                blHasStolen := true;
            end
            else
                ShowTransMessage('You have already robbed a peaceful citizen today.', false);
        end;

        // create guard?
        if blHasStolen=true then
            if (100 * ThePlayer.intBurgle) + trunc(random(100 * ThePlayer.intBurgle)) < CONST_SAFEBURGLE then
            begin
                ShowTransMessage('You have alarmed the guards!', false);
                m := GetFirstFreeMonsterID;
                if m > -1 then
                begin
                    CreateMonster(m, 'guard', 0);
                    Monster[m].intX := ThePlayer.intX+1;
                    Monster[m].intY := ThePlayer.intY;
                end;
            end;

    end;

end;


// dig through walls
procedure Dig;
    var
        chDir: char;
        strDecont: string;
        intShovel: integer;
        i, n, vx, vy: integer;
        blDecont: boolean;
        intDecontLvl: integer;
        intCurrentDecont: integer;
        intDecontGold: integer;
begin
    intShovel := ThePlayer.intTool;
    if ThePlayer.intWeapon>0 then
        inc(intShovel, Thing[ThePlayer.intWeapon].intSP);

    intDecontLvl := DungeonLevel * (20 + trunc(random(DungeonLevel*40)));
    intCurrentDecont := intDecontLvl;
    intDecontGold := 1 + (intDecontLvl div 16);

    blDecont := false;
    if (DngLvl[ThePlayer.intX, ThePlayer.intY].intFloorType=31) and ((ThePlayer.intProf=1) or (ThePlayer.intProf=4))  then
    begin
        repeat
            strDecont := GetTextInput('Do you want to decontaminate the ground (level ' + IntToStr(intDecontLvl) + '; y/n)?', 1);
        until (strDecont='Y') or (strDecont='N') or (strDecont='y') or (strDecont='n');
        if (strDecont='Y') or (strDecont='y') then
            blDecont:=true;
    end;


    if blDecont=true then
    // decontaminate ground
    begin
        n := GetNumericalInput('How many turns do you want to work?', 4);

        ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);

        for i:=1 to n do
        begin

            dec (intCurrentDecont, intShovel);
            if intCurrentDecont < 1 then
                intCurrentDecont := 0;

            ShowTransMessage('Turn: '+IntToStr(i)+', current decontamination level: '+IntToStr(intCurrentDecont), false);
            if NextTurn then break; //NextTurn returns true when/if player dies
        end;

        if intCurrentDecont = 0 then
        begin
            DngLvl[ThePlayer.intX,ThePlayer.intY].intFloorType := 12;
            ShowTransMessage('You have cleared this place from contamination and earn '+IntToStr(intDecontGold)+' Credits.', false);
            inc(ThePlayer.longGold, intDecontGold);
            inc(ThePlayer.longContCleared);
        end;

    end
    else
    // dig
    begin
        n := GetNumericalInput('How long do you want to dig?', 4);
        chDir := GetDirection(0);

        vx:=0;
        vy:=0;
        if chDir=KeyNorth then
        begin
            vx:=0;
            vy:=-1;
        end;
        if chDir=KeySouth then
        begin
            vx:=0;
            vy:=1;
        end;
        if chDir=KeyEast then
        begin
            vx:=1;
            vy:=0;
        end;
        if chDir=KeyWest then
        begin
            vx:=-1;
            vy:=0;
        end;
        if chDir=KeyNorthEast then
        begin
            vx:=1;
            vy:=-1;
        end;
        if chDir=KeyNorthWest then
        begin
            vx:=-1;
            vy:=-1;
        end;
        if chDir=KeySouthEast then
        begin
            vx:=1;
            vy:=1;
        end;
        if chDir=KeySouthWest then
        begin
            vx:=-1;
            vy:=1;
        end;

        ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
        i:=0;
        repeat
            inc(i);
            ShowTransMessage('Digging; turn '+IntToStr(i)+'  Integrity: '+IntToStr(DngLvl[ThePlayer.intX+vx, ThePlayer.intY+vy].intIntegrity), false);

            if DngLvl[ThePlayer.intX+vx, ThePlayer.intY+vy].intFloorType=6 then
            begin
                dec(DngLvl[ThePlayer.intX+vx, ThePlayer.intY+vy].intIntegrity, intShovel);
                if DngLvl[ThePlayer.intX+vx, ThePlayer.intY+vy].intIntegrity < 1 then
                begin
                    DngLvl[ThePlayer.intX+vx, ThePlayer.intY+vy].intFloorType := 2;
                    DngLvl[ThePlayer.intX+vx, ThePlayer.intY+vy].intLight := 4;
                    DngLvl[ThePlayer.intX+vx, ThePlayer.intY+vy].intIntegrity := 0;
                    if (random(1000)>980) and (DngLvl[ThePlayer.intX,ThePlayer.intY].intItem=0) then
                        DngLvl[ThePlayer.intX,ThePlayer.intY].intItem:=ReturnRareItem;
                    if random(500)>450 then
                        DngLvl[ThePlayer.intX+vx,ThePlayer.intY+vy].intItem:=ReturnItemByName('Rock');
                end;
            end
            else
            begin
                ShowTransMessage('You can only dig through rock!', false);
                i:=n;
            end;

            NextTurn;
        until (ThePlayer.intHP<1) or (i=n) or (DngLvl[ThePlayer.intX+vx, ThePlayer.intY+vy].intIntegrity<1);
    end;
end;

// check if the player is dead (and resurrect, if runes of beithe in inventory)
function IsPlayerDead(): boolean;
begin
    IsPlayerDead:=false;
    if ThePlayer.intHP < 1 then
    begin    
        ThePlayer.intHP:=0;
        IsPlayerDead := true;
        // if the player has a complete "Beithe" rune set, resurrect him from death
        blAllRunes := RuneSet('Rune of Beithe', 'Rune of hUath', 'Rune of Muin', 'Rune of Ailm', 'Rune of Ailm', true);
        if blAllRunes = true then
        begin
            ThePlayer.intHP := ThePlayer.intMaxHP div 2;
            IsPlayerDead:=false;
            GetKeyInput('Beithe resurrects you from death instantly, but you lose the runes.', true);
        end;
    end;
end;


// show and calculate effects monsters suffer from player spells
function DoEffectOnMonster(intEffectRange: integer; intSpellType: integer): integer;
    var
        ch1, chDir: char;
        GunX, GunY, GunBY, GunBX, GunBYborder, vx, vy, i: integer;
        blMonsterHit: boolean;
begin

    // choose visual representation of spell
    case intSpellType of
        1:
            ch1:=chr(145);        // fire
        2:
            ch1:=chr(132);        // ice
        3:
            ch1:=chr(195);        // water
        19:
            ch1:=chr(207);        // force
    end;

    // if an item with effect MaxMagic is worn, double the effect of the spell
    if CheckEffect(37)=true then
        intEffectRange:=intEffectRange*2;

    // direction
    chDir := GetDirection(0);
    ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);

    if chDir=KeyNorth then
    begin
        vx:=0;
        vy:=-1;
    end;

    if chDir=KeySouth then
    begin
        vx:=0;
        vy:=1;
    end;

    if chDir=KeyNorthEast then
    begin
        vx:=1;
        vy:=-1;
    end;

    if chDir=KeySouthEast then
    begin
        vx:=1;
        vy:=1;
    end;

    if chDir=KeyNorthWest then
    begin
        vx:=-1;
        vy:=-1;
    end;

    if chDir=KeySouthWest then
    begin
        vx:=-1;
        vy:=1;
    end;

    if chDir=KeyWest then
    begin
        vx:=-1;
        vy:=0;
    end;

    if chDir=KeyEast then
    begin
        vx:=1;
        vy:=0;
    end;

    GunX:=ThePlayer.intX;
    GunY:=ThePlayer.intY;
    GunBX:=ThePlayer.intBX;

    if UseSDL=true then
        GunBY:=ThePlayer.intBY+1
    else
        GunBY:=ThePlayer.intBY;

    PlaySFX('magic-combat.mp3');

    // as long as there is no wall etc. let the bullet fly. At least for ThePlayer.intView+2 tiles
    blMonsterHit:=false;
    while (DngLvl[GunX,GunY].intIntegrity=0) and (abs(ThePlayer.intX-GunX)<ThePlayer.intView+2) and (abs(ThePlayer.intY-GunY)<ThePlayer.intView+2) and (blMonsterHit=false) do
    begin
        Inc(GunX, vx);
        Inc(GunY, vy);

        Inc(GunBX, vx);
        Inc(GunBY, vy);

        if (UseSmallTiles=true) or (UseSDL=false) then
            GunBYborder := 1
        else
            GunBYborder := 0;

        // show bullet
        if (GunBX > 1) and (GunBX < ThePlayer.intBX*2) and (GunBY > GunBYborder) and (GunBY < ThePlayer.intBY*2) then
        begin
            AnyCharXY(GunBX, GunBY, ch1, 0);
//             TextXY(GunBX-1, GunBY, ch1);

            if UseSDL=true then
                SDL_UPDATERECT(screen,0,0,0,0)
            else
                UpdateScreen(true);
            delay(50);
        end;

        // special effects of fire spells
        if intSpellType=1 then
        begin
            // was a monster's hive hit?
            if (HiveX>-1) and (HiveY>-1) then
                if (HiveX=GunX) and (HiveY=GunY) then
                begin
                    if DngLvl[HiveX,HiveY].intIntegrity>0 then
                    begin
                        dec(DngLvl[HiveX,HiveY].intIntegrity, 10*intEffectRange);
                        ShowTransMessage('You damage a monster'+chr(39)+'s hive.', false);
                        if DngLvl[HiveX,HiveY].intIntegrity<1 then
                        begin
                            DngLvl[HiveX,HiveY].intFloorType:=2;
                            HiveDestroyed;
                        end;
                    end;
                end;

            // was a spider's web hit?
            if DngLvl[GunX,GunY].intAirType=6 then
            begin
                DngLvl[GunX,GunY].intAirType:=0;
                DngLvl[GunX,GunY].intAirRange:=0;
                ShowTransMessage('You destroy a spider'+chr(39)+'s web.', false);
            end;

            // was a gas cylinder hit?
            if DngLvl[GunX,GunY].intFloorType = 56 then
                CylinderDestroyed(GunX,GunY);

            // was machine oil hit?
            if DngLvl[GunX,GunY].intFloorType = 57 then
            begin
                DngLvl[GunX,GunY].intFloorType := 2;
                DngLvl[GunX,GunY].intAirType := 1;
                inc(DngLvl[GunX,GunY].intAirRange,5+trunc(random(10)));
                ShowTransMessage('You emblaze oil on the ground.', false);
            end;
        end;

        // was a monster hit?
        for i:=1 to 510 do
        begin
            if (Monster[i].intX = GunX) and (Monster[i].intY = GunY) then
            begin

                Monster[i].blHuman := false;    // even men and peaceful monsters will now be angry
                Monster[i].blAttacked := true;  // set attacked flag

                blMonsterHit:=true;

                // if the monster is immune against spell, show message
                // - water immunity?
                if (intSpellType=3) and ((Monster[i].blWater=true) or (Monster[i].blWaterC=true)) then
                begin
                    intEffectRange:=0;
                    ShowTransMessage('The '+Monster[i].strName+' is immune against water magic.', false);
                end;

                // - fire immunity?
                if (intSpellType=1) and (Monster[i].blFire=true) then
                begin
                    intEffectRange:=0;
                    ShowTransMessage('The '+Monster[i].strName+' is immune against fire magic.', false);
                end;

                // - ice immunity?
                if (intSpellType=2) and (Monster[i].blIce=true) then
                begin
                    intEffectRange:=0;
                    ShowTransMessage('The '+Monster[i].strName+' is immune against ice magic.', false);
                end;

                // decrease monster's HP by intEffectRange
                if (intSpellType<>19) then
                    dec(Monster[i].intHP, intEffectRange)
                else
                begin
                    if ThePlayer.intStrength >= intEffectRange then
                    begin
                        dec(ThePlayer.intStrength, intEffectRange);
                        dec(Monster[i].intHP, intEffectRange);
                    end
                    else
                        GetKeyInput('You feel too weak; the force attack has no effect.', true);
                end;

                if Monster[i].intHP<1 then
                    Monster[i].intHP:=0;

                // check if monster's dead
                IsMonsterDead(i);

            end;
        end;
    end;

    DoEffectOnMonster:=1;
    if blMonsterHit=false then
        DoEffectOnMonster:=0;

end;


// shoot with weapon
procedure Shoot;
    var
        chDir, chBullet: char;
        GunX, GunY, GunBY, GunBX, GunBYborder, vx, vy, i, n: integer;
        intTP, intMDP: integer;
        blMonsterHit: boolean;
begin

    intTP := CollectGunTP;

    // check if player has wielded a long range weapon
    if ThePlayer.intWeapon>0 then
    begin

        if Thing[ThePlayer.intWeapon].intGP > 0 then
        begin
            // check if there is enough ammu for the weapon in the player's inventory
            n:=0;
            for i:=1 to 16 do
                // if item[i] is ammu for current weapon, we have it
                if Inventory[i].intType>0 then
                    if Thing[Inventory[i].intType].intIsAmmu = Thing[ThePlayer.intWeapon].intNeedsAmmu then
                        n:=i;

            // if we have ammu, process the shoot
            if n>0 then
            begin
                chBullet := Thing[Inventory[n].intType].chLetter;

                // add ammu-wp to total tp
                inc (intTP, Thing[Inventory[n].intType].intWP);

                dec(Inventory[n].longNumber);  // decrease total number of bullets of this type in inventory
                if Inventory[n].longNumber = 0 then
                    Inventory[n].intType := 0; // if number of bullets=0, delete this item from inventory

                // magic?
                if (Thing[ThePlayer.intWeapon].intEffect=19) or (Thing[ThePlayer.intWeapon].intEffect=38) or (Thing[ThePlayer.intWeapon].intEffect=39) or (Thing[ThePlayer.intWeapon].intEffect=40) then
                begin
                    case Thing[ThePlayer.intWeapon].intEffect of
                        38:
                            DoEffectOnMonster(Thing[ThePlayer.intWeapon].intRange, 1);        // fire
                        39:
                            DoEffectOnMonster(Thing[ThePlayer.intWeapon].intRange, 2);        // ice
                        40:
                            DoEffectOnMonster(Thing[ThePlayer.intWeapon].intRange, 3);        // water
                        19:
                            DoEffectOnMonster(Thing[ThePlayer.intWeapon].intRange, 19);       // force
                    end;
                end
                else
                begin
                    // direction
                    chDir := GetDirection(0);
                    ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);

                    if chDir=KeyNorth then
                    begin
                        vx:=0;
                        vy:=-1;
                    end;
                
                    if chDir=KeySouth then
                    begin
                        vx:=0;
                        vy:=1;
                    end;

                    if chDir=KeyNorthEast then
                    begin
                        vx:=1;
                        vy:=-1;
                    end;
                
                    if chDir=KeySouthEast then
                    begin
                        vx:=1;
                        vy:=1;
                    end;

                    if chDir=KeyNorthWest then
                    begin
                        vx:=-1;
                        vy:=-1;
                    end;
                
                    if chDir=KeySouthWest then
                    begin
                        vx:=-1;
                        vy:=1;
                    end;

                    if chDir=KeyWest then
                    begin
                        vx:=-1;
                        vy:=0;
                    end;
                
                    if chDir=KeyEast then
                    begin
                        vx:=1;
                        vy:=0;
                    end;

                    GunX:=ThePlayer.intX;
                    GunY:=ThePlayer.intY;

                    //writeln ('PlayerBX: ' + inttostr(ThePlayer.intBX) + '  PlayerBY: ' + Inttostr(ThePlayer.intBY));
                    GunBX:=ThePlayer.intBX;

                    if UseSDL=true then
                        GunBY:=ThePlayer.intBY+1
                    else
                        GunBY:=ThePlayer.intBY;

                    if (UseSmallTiles=true) or (UseSDL=false) then
                        GunBYborder := 1
                    else
                        GunBYborder := 0;
            
                    // as long as there is no wall etc. let the ammu fly. At least for ThePlayer.intView+2 tiles
                    blMonsterHit:=false;
                    while (DngLvl[GunX,GunY].intIntegrity=0) and (DngLvl[GunX,GunY].blLOS=true) and (blMonsterHit=false) do
                    begin
                        Inc(GunX, vx);
                        Inc(GunY, vy);
            
                        Inc(GunBX, vx);
                        Inc(GunBY, vy);
            
                        // show ammu
                        //writeln ('GunBX: ' + inttostr(GunBX) + '  GunBY: ' + Inttostr(GunBY));
                        if (GunBX > 1) and (GunBX < ThePlayer.intBX*2) and (GunBY > GunBYborder) and (GunBY < ThePlayer.intBY*2) then
                        begin
                            AnyCharXY(GunBX, GunBY, chBullet, 0);
                            if UseSDL=true then
                                SDL_UPDATERECT(screen,0,0,0,0)
                            else
                                UpdateScreen(true);
                            delay(50);
                        end;
            
                        // was a monster hit?
                        for i:=1 to 510 do
                        begin
                            if (Monster[i].intX = GunX) and (Monster[i].intY = GunY) then
                            begin

                                Monster[i].blHuman := false;    // even men and peaceful monsters will now be angry
                                Monster[i].blAttacked := true;  // set attacked flag

                                // check if the monster was hit
                                if random(CONST_LONGRANGE_HITRATE) > ThePlayer.intView then
                                //if HitOrMiss(i)=true then
                                    ShowTransMessage('You miss the ' + Monster[i].strName + ' [HP: ' + IntToStr(Monster[i].intHP) + '].', false)
                                else
                                begin
                                    blMonsterHit:=true;
                                    
                                    intMDP := Monster[i].intAP;
                        
                                    intTP := intTP - intMDP;
                                    if intTP < 1 then
                                        intTP := trunc(random(2));

                                    ShowTransMessage('You shoot the ' + Monster[i].strName + ' [HP: ' + IntToStr(Monster[i].intHP) + '].', false);
                
                                    Monster[i].intHP := Monster[i].intHP - intTP;
                                    if Monster[i].intHP < 0 then
                                        Monster[i].intHP := 0;
                                end;
                
                                IsMonsterDead(i);
                            end;
                        end;
                    end;
                end;
            end
            else
                GetKeyInput('You need the proper ammunition to use the ' + Thing[ThePlayer.intWeapon].strName + '.', true);
        end
        else
            GetKeyInput('Your current weapon is no long-range weapon.', true);
    end
    else
        GetKeyInput('You need to wield a long-range weapon in order to shoot.', true);
end;


procedure OpenDoor(chDir: Char);
    var
        vx, vy: integer;
begin
    if chDir='-' then
        chDir := GetDirection(0);

//     ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);

    vx:=0;
    vy:=0;
    if chDir=KeyNorth then
    begin
        vx:=0;
        vy:=-1;
    end;
    if chDir=KeySouth then
    begin
        vx:=0;
        vy:=1;
    end;
    if chDir=KeyEast then
    begin
        vx:=1;
        vy:=0;
    end;
    if chDir=KeyWest then
    begin
        vx:=-1;
        vy:=0;
    end;
    if chDir=KeyNorthEast then
    begin
        vx:=1;
        vy:=-1;
    end;
    if chDir=KeyNorthWest then
    begin
        vx:=-1;
        vy:=-1;
    end;
    if chDir=KeySouthEast then
    begin
        vx:=1;
        vy:=1;
    end;
    if chDir=KeySouthWest then
    begin
        vx:=-1;
        vy:=1;
    end;


    // standard door
    if DngLvl[ThePlayer.intX+vx, ThePlayer.intY+vy].intFloorType = 3 then
    begin
        PlaySFX('door-open.mp3');
        DngLvl[ThePlayer.intX+vx, ThePlayer.intY+vy].intFloorType := 4;
        DngLvl[ThePlayer.intX+vx, ThePlayer.intY+vy].intIntegrity := 0;
        ShowTransMessage('You open a door.', false);
    end;
    // locked door which needs a key to open
    if DngLvl[ThePlayer.intX+vx, ThePlayer.intY+vy].intFloorType = 24 then
    begin
        if ((PlayerHasKey>0) and (PlayerHasKey<17)) or (PlayerHasKey=22) then
        begin
            PlaySFX('door-unlock.mp3');
            // TODO: Fix range check error an dieser stelle:

            if PlayerHasKey<22 then
            begin
                dec(Inventory[PlayerHasKey].longNumber);
                if Inventory[PlayerHasKey].longNumber < 1 then
                    Inventory[PlayerHasKey].intType:=0;
            end;
            DngLvl[ThePlayer.intX+vx, ThePlayer.intY+vy].intFloorType := 4;
            DngLvl[ThePlayer.intX+vx, ThePlayer.intY+vy].intIntegrity := 0;
            ShowTransMessage('You unlock a door.', false);
        end
    end;
end;

procedure CloseDoor;
    var
        chDir: char;
        vx, vy: integer;
begin

    chDir := GetDirection(0);
    ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);

    vx:=0;
    vy:=0;
    if chDir=KeyNorth then
    begin
        vx:=0;
        vy:=-1;
    end;
    if chDir=KeySouth then
    begin
        vx:=0;
        vy:=1;
    end;
    if chDir=KeyEast then
    begin
        vx:=1;
        vy:=0;
    end;
    if chDir=KeyWest then
    begin
        vx:=-1;
        vy:=0;
    end;
    if chDir=KeyNorthEast then
    begin
        vx:=1;
        vy:=-1;
    end;
    if chDir=KeyNorthWest then
    begin
        vx:=-1;
        vy:=-1;
    end;
    if chDir=KeySouthEast then
    begin
        vx:=1;
        vy:=1;
    end;
    if chDir=KeySouthWest then
    begin
        vx:=-1;
        vy:=1;
    end;

    if DngLvl[ThePlayer.intX+vx, ThePlayer.intY+vy].intFloorType = 4 then
    begin
        PlaySFX('door-close.mp3');
        ShowTransMessage('You close the door.', false);
        DngLvl[ThePlayer.intX+vx, ThePlayer.intY+vy].intFloorType := 3;
        DngLvl[ThePlayer.intX+vx, ThePlayer.intY+vy].intIntegrity := 100;
    end;
end;

// returns: 1 = New Game ; 2 = Quickstart; 3 = Load Game
function TitleScreen(): Integer;
    var
        dummy: String;
begin

    ClearScreenSDL;

    dummy:='-';
    repeat

        if UseSDL=true then
            BlitImage_Title
        else
        begin

            GlobalConColor:=yellow;
            TextXY(28,4, 'L a m b d a R o g u e');

            GlobalConColor:=lightred;
            TextXY(30,6, 'The Book of Stars');

            GlobalConColor:=lightgray;
            TextXY(21,13, 'Copyright (c) Mario Donick 2006-2009.');

            GlobalConColor:=darkgray;
            TextXY(13,15, '"Du musst doch das Gras implementieren!!" - Soraya');

            TextXY(7, 17, 'This program can be copied, modified and distributed under the');
            TextXY(11,18, 'terms of the GNU General Public License, version 2.x.');

            GlobalConColor:=cyan;
            TextXY(18,20, 'Please ensure that NumLock is switched ON.');

            GlobalConColor:=-1;

            TextXY(26, 8,  '   1 Start a New Game');
            TextXY(26, 9,  '      2 Quickstart');
            TextXY(26, 10, '3 Continue Existing Game');
            TextXY(26, 11, '         4 Quit');

        end;

        if UseSDL=true then
            SmallTextXY(1,1, strVersion, false)
        else
            TransTextXY(79-length(strVersion)-2, 1, strVersion);

        dummy:=GetKeyInput('Please select an option.', false);
    until (dummy='1') or (dummy='2') or (dummy='3') or (dummy='4');

    BottomBar;
    TitleScreen:=StrToInt(dummy);
end;



procedure SaveGame(charname: string; lvl: integer);
    var
        SaveFile: Textfile;
        i, j: Integer;
begin

    Assign(SaveFile, PathUserWritable+'saves/' + charname + '.lambdarogue');
    Rewrite(SaveFile);
    WriteLn(SaveFile, strVersion);

    // display mode
    WriteLn(SaveFile, IntToStr(ThePlayer.intTileset));

    // number of saves
    WriteLn(SaveFile, IntToStr(ThePlayer.longLifeIns));

    // save dungeon
    WriteLn(SaveFile, IntToStr(lvl));
    for i:=1 to 100 do
    begin
        for j:=1 to 100 do
        begin
            with DngLvl[i,j] do
            begin
                WriteLn(SaveFile, IntToStr(intFloorType));
                WriteLn(SaveFile, IntToStr(intAirType));
                WriteLn(SaveFile, IntToStr(intAirRange));
                WriteLn(SaveFile, IntToStr(intIntegrity));
                WriteLn(SaveFile, IntToStr(intLight));
                WriteLn(SaveFile, IntToStr(intItem));
                WriteLn(SaveFile, IntToStr(intBuilding));
                if blKnown=true then
                    WriteLn(SaveFile, '1')
                else
                    WriteLn(SaveFile,'0');
                if blTown=true then
                    WriteLn(SaveFile, '1')
                else
                    WriteLn(SaveFile,'0');

            end;
        end;
    end;

    // save traders
    for i:=1 to 8 do
    begin
        WriteLn(SaveFile, IntToStr(MyShop[i].intType));
        for j:=1 to 16 do
            WriteLn(SaveFile, IntToStr(MyShop[i].Inventory[j].intType));
    end;

    // save NPCs
    for i:=1 to 9 do
    begin
        WriteLn(SaveFile, IntToStr(NPC[i].intX));
        WriteLn(SaveFile, IntToStr(NPC[i].intY));
    end;

    // save hive status
    for i:=1 to 30 do
        WriteLn(SaveFile, IntToStr(intNoHivesAnymore[i]));

    // save achievements
    WriteLn(SaveFile, IntToStr(intAchieved));
    for i:=1 to 500 do
        WriteLn(SaveFile, Achievements[i]);

    // save quest info
    for i:=1 to WinLevel do
        for j:=1 to 9 do
        begin
            Writeln(SaveFile, IntToStr(ThePlayer.intQuestState[i,j]));
            Writeln(SaveFile, IntToStr(ThePlayer.intQuestHunt[i,j]));
        end;

    // save plot info
    for i:=1 to 80 do
        if ThePlayer.blStory[i]=true then
            WriteLn(SaveFile, '1')
        else
            WriteLn(SaveFile, '0');

    // save diplomas
    for i:=1 to 30 do
        WriteLn(SaveFile, IntToStr(ThePlayer.intDipl[i]));

    // save current temporary resistances
    for i:=1 to 100 do
        WriteLn(SaveFile, IntToStr(ThePlayer.intTempResist[i]));

    // save ressources
    Writeln(SaveFile, IntToStr(Storage.longWood));
    Writeln(SaveFile, IntToStr(Storage.longMetal));
    Writeln(SaveFile, IntToStr(Storage.longStone));
    Writeln(SaveFile, IntToStr(Storage.longLeather));
    Writeln(SaveFile, IntToStr(Storage.longPlastics));
    Writeln(SaveFile, IntToStr(Storage.longPaper));

    // save time and day
    WriteLn(SaveFile, IntToStr(intDayTime));
    WriteLn(SaveFile, IntToStr(intDay));
    WriteLn(SaveFile, IntToStr(longYear));

    // save quickbar position
    WriteLn(SaveFile, IntToStr(ChantBarX));
    WriteLn(SaveFile, IntToStr(ChantBarY));

    // save player
    with ThePlayer do
    begin
        WriteLn(SaveFile, strName);
        WriteLn(SaveFile, strVita);
        WriteLn(SaveFile, strDescription);
        WriteLn(SaveFile, IntToStr(longTotalTurns));
        WriteLn(SaveFile, IntToStr(longSkillPoints));
        WriteLn(SaveFile, IntToStr(intMaxHP));
        WriteLn(SaveFile, IntToStr(intHP));
        WriteLn(SaveFile, IntToStr(intMaxHP));
        WriteLn(SaveFile, IntToStr(intStrength));
        WriteLn(SaveFile, IntToStr(intLastSong));
        WriteLn(SaveFile, IntToStr(longExp));
        WriteLn(SaveFile, IntToStr(intLvl));
        WriteLn(SaveFile, IntToStr(longGold));
        WriteLn(SaveFile, IntToStr(longScore));
        WriteLn(SaveFile, IntToStr(intX));
        WriteLn(SaveFile, IntToStr(intY));
        WriteLn(SaveFile, IntToStr(intSex));
        WriteLn(SaveFile, IntToStr(intPP));
        WriteLn(SaveFile, IntToStr(intMaxPP));
        WriteLn(SaveFile, IntToStr(longFood));
        WriteLn(SaveFile, IntToStr(intBX));
        WriteLn(SaveFile, IntToStr(intBY));
        WriteLn(SaveFile, strReli);
        WriteLn(SaveFile, IntToStr(UnX));
        WriteLn(SaveFile, IntToStr(UnY));
        WriteLn(SaveFile, IntToStr(intReli));
        WriteLn(SaveFile, IntToStr(intProf));
        WriteLn(SaveFile, IntToStr(intFight));
        WriteLn(SaveFile, IntToStr(intMove));
        WriteLn(SaveFile, IntToStr(intChant));
        WriteLn(SaveFile, IntToStr(intView));
        WriteLn(SaveFile, IntToStr(intBurgle));
        WriteLn(SaveFile, IntToStr(intSword));
        WriteLn(SaveFile, IntToStr(intAxe));
        WriteLn(SaveFile, IntToStr(intWhip));
        WriteLn(SaveFile, IntToStr(intGun));
        WriteLn(SaveFile, IntToStr(intTool));
        WriteLn(SaveFile, IntToStr(intHumility));
        WriteLn(SaveFile, IntToStr(intTrade));
        WriteLn(SaveFile, IntToStr(intWeapon));
        WriteLn(SaveFile, IntToStr(intArmour));
        WriteLn(SaveFile, IntToStr(intHat));
        WriteLn(SaveFile, IntToStr(intFeet));
        WriteLn(SaveFile, IntToStr(intExtra));
        WriteLn(SaveFile, IntToStr(intRingLeft));
        WriteLn(SaveFile, IntToStr(intRingRight));
        WriteLn(SaveFile, IntToStr(intPoison));
        WriteLn(SaveFile, IntToStr(intConfusion));
        WriteLn(SaveFile, IntToStr(intInvis));
        WriteLn(SaveFile, IntToStr(intBlind));
        WriteLn(SaveFile, IntToStr(intSleep));
        WriteLn(SaveFile, IntToStr(intWall));
        WriteLn(SaveFile, IntToStr(longPrayers));
        WriteLn(SaveFile, IntToStr(intPara));
        WriteLn(SaveFile, IntToStr(intCalm));
        WriteLn(SaveFile, IntToStr(longContCleared));
        WriteLn(SaveFile, IntToStr(intTotalVitari));
        WriteLn(SaveFile, IntToStr(intNextVitari));
        WriteLn(SaveFile, IntToStr(intNeedVitari));
        WriteLn(SaveFile, IntToStr(DownX));
        WriteLn(SaveFile, IntToStr(DownY));
        WriteLn(SaveFile, IntToStr(UpX));
        WriteLn(SaveFile, IntToStr(UpY));
        WriteLn(SaveFile, IntToStr(HiveX));
        WriteLn(SaveFile, IntToStr(HiveY));
        WriteLn(SaveFile, IntToStr(DiffLevel));
        WriteLn(SaveFile, IntToStr(LastSong));
        WriteLn(SaveFile, IntToStr(intLimit));
        if blCursed = true then
            WriteLn(SaveFile, '1')
        else
            WriteLn(SaveFile, '0');
        if blBlessed = true then
            WriteLn(SaveFile, '1')
        else
            WriteLn(SaveFile, '0');
        if blEvil = true then
            WriteLn(SaveFile, '1')
        else
            WriteLn(SaveFile, '0');
    end;

    // save killed monsters
    for i:=1 to 200 do
        WriteLn(SaveFile, IntToStr(ThePlayer.longKilled[i]));

    // save level visits
    for i:=1 to WinLevel do
        WriteLn(SaveFile, IntToStr(ThePlayer.longLevelVisits[i]));

    // save inventory
    for i:=1 to 16 do
        with inventory[i] do
        begin
            WriteLn(SaveFile, intType);
            WriteLn(SaveFile, longNumber);
        end;


    // save songbook and quickkeys
    for i:=1 to 12 do
    begin
        with spellbook[i] do
        begin
            WriteLn(SaveFile, intType);
            WriteLn(SaveFile, intKnown);
            WriteLn(SaveFile, intRefresh);
        end;
        WriteLn(SaveFile, strQuickKey[i]);
    end;

    // save memory of killed unique monsters
    for i:=1 to 200 do
        if ThePlayer.blUnKilled[i]=true then
            WriteLn(SaveFile, '1')
        else
            WriteLn(SaveFile,'0');

    // save memory of identified items
    for i:=1 to ItemCount do
        if Thing[i].blIdentified=true then
            WriteLn(SaveFile, '1')
        else
            WriteLn(SaveFile, '0');


    Close(SaveFile);
end;

procedure LoadGame(charname: string);
    var
        SaveFile: Textfile;
        i, j, n: Integer;
        savever: string;
begin

    // empty messagelog
    SetLength(ThePlayer.strMessageLog, 0);
    SetLength(ThePlayer.strMessageLog, 1000);
    ThePlayer.intNumberOfLogs := 0;

    ShowTransMessage('Log started.', false); //Hack. All log handling starts at index 1.

    Assign(SaveFile, PathUserWritable+'saves/' + charname + '.lambdarogue');
    Reset(SaveFile);

    // load dungeon
    ReadLn(SaveFile, savever);
    //if savever <> strVersion then
    //begin
        //Close(SaveFile);
        //ClearScreenSDL;
        //GetKeyInput('Error: Wrong file version.', true);
        //StopGraphics;
        //halt;
    //end;

    // display mode
    ReadLn(SaveFile, ThePlayer.intTileset);

    // number of saves
    ReadLn(SaveFile, ThePlayer.longLifeIns);

    ReadLn(SaveFile, DungeonLevel);
    for i:=1 to 100 do
    begin
        for j:=1 to 100 do
        begin
            with DngLvl[i,j] do
            begin
                ReadLn(SaveFile, intFloorType);
                ReadLn(SaveFile, intAirType);
                ReadLn(SaveFile, intAirRange);
                ReadLn(SaveFile, intIntegrity);
                ReadLn(SaveFile, intLight);
                ReadLn(SaveFile, intItem);
                ReadLn(SaveFile, intBuilding);
                ReadLn(SaveFile, n);
                if n=1 then
                    blKnown:=true
                else
                    blKnown:=false;
                ReadLn(SaveFile, n);
                if n=1 then
                    blTown:=true
                else
                    blTown:=false;
            end;
        end;
    end;

    // load traders
    for i:=1 to 8 do
    begin
        ReadLn(SaveFile, MyShop[i].intType);
        for j:=1 to 16 do
            ReadLn(SaveFile, MyShop[i].Inventory[j].intType);
    end;


    // load NPCs
    for i:=1 to 9 do
    begin
        ReadLn(SaveFile, NPC[i].intX);
        ReadLn(SaveFile, NPC[i].intY);
    end;

    // load hive status
    for i:=1 to 30 do
        ReadLn(SaveFile, intNoHivesAnymore[i]);

    // load achievements
    ReadLn(SaveFile, intAchieved);
    for i:=1 to 500 do
        ReadLn(SaveFile, Achievements[i]);

    // load quest info
    for i:=1 to WinLevel do
        for j:=1 to 9 do
        begin
            ReadLn(SaveFile, ThePlayer.intQuestState[i,j]);
            ReadLn(SaveFile, ThePlayer.intQuestHunt[i,j]);
        end;

    // load plot info
    for i:=1 to 80 do
    begin
        ReadLn(SaveFile, n);
        if n=1 then
            ThePlayer.blStory[i]:=true
        else
            ThePlayer.blStory[i]:=false;
    end;

    // load diplomas
    for i:=1 to 30 do
        ReadLn(SaveFile, ThePlayer.intDipl[i]);

    // load temporary resistances
    for i:=1 to 100 do
        ReadLn(SaveFile, ThePlayer.intTempResist[i]);

    // load ressources
    Readln(SaveFile, Storage.longWood);
    Readln(SaveFile, Storage.longMetal);
    Readln(SaveFile, Storage.longStone);
    Readln(SaveFile, Storage.longLeather);
    Readln(SaveFile, Storage.longPlastics);
    Readln(SaveFile, Storage.longPaper);


    // load time and day
    ReadLn(SaveFile, intDayTime);
    ReadLn(SaveFile, intDay);
    ReadLn(SaveFile, longYear);

    // quickbar position
    ReadLn(SaveFile, ChantBarX);
    ReadLn(SaveFile, ChantBarY);

    // load player
    with ThePlayer do
    begin
        ReadLn(SaveFile, strName);
        ReadLn(SaveFile, strVita);
        ReadLn(SaveFile, strDescription);
        ReadLn(SaveFile, longTotalTurns);
        ReadLn(SaveFile, longSkillPoints);
        ReadLn(SaveFile, intMaxHP);
        ReadLn(SaveFile, intHP);
        ReadLn(SaveFile, intMaxHP);
        ReadLn(SaveFile, intStrength);
        ReadLn(SaveFile, intLastSong);
        ReadLn(SaveFile, longExp);
        ReadLn(SaveFile, intLvl);
        ReadLn(SaveFile, longGold);
        ReadLn(SaveFile, longScore);
        ReadLn(SaveFile, intX);
        ReadLn(SaveFile, intY);
        ReadLn(SaveFile, intSex);
        ReadLn(SaveFile, intPP);
        ReadLn(SaveFile, intMaxPP);
        ReadLn(SaveFile, longFood);
        ReadLn(SaveFile, intBX);
        ReadLn(SaveFile, intBY);
        ReadLn(SaveFile, strReli);
        ReadLn(SaveFile, UnX);
        ReadLn(SaveFile, UnY);
        ReadLn(SaveFile, intReli);
        ReadLn(SaveFile, intProf);
        ReadLn(SaveFile, intFight);
        ReadLn(SaveFile, intMove);
        ReadLn(SaveFile, intChant);
        ReadLn(SaveFile, intView);
        ReadLn(SaveFile, intBurgle);
        ReadLn(SaveFile, intSword);
        ReadLn(SaveFile, intAxe);
        ReadLn(SaveFile, intWhip);
        ReadLn(SaveFile, intGun);
        ReadLn(SaveFile, intTool);
        ReadLn(SaveFile, intHumility);
        ReadLn(SaveFile, intTrade);
        ReadLn(SaveFile, intWeapon);
        ReadLn(SaveFile, intArmour);
        ReadLn(SaveFile, intHat);
        ReadLn(SaveFile, intFeet);
        ReadLn(SaveFile, intExtra);
        ReadLn(SaveFile, intRingLeft);
        ReadLn(SaveFile, intRingRight);
        ReadLn(SaveFile, intPoison);
        ReadLn(SaveFile, intConfusion);
        ReadLn(SaveFile, intInvis);
        ReadLn(SaveFile, intBlind);
        ReadLn(SaveFile, intSleep);
        ReadLn(SaveFile, intWall);
        ReadLn(SaveFile, longPrayers);
        ReadLn(SaveFile, intPara);
        ReadLn(SaveFile, intCalm);
        ReadLn(SaveFile, longContCleared);
        ReadLn(SaveFile, intTotalVitari);
        ReadLn(SaveFile, intNextVitari);
        ReadLn(SaveFile, intNeedVitari);
        ReadLn(SaveFile, DownX);
        ReadLn(SaveFile, DownY);
        ReadLn(SaveFile, UpX);
        ReadLn(SaveFile, UpY);
        ReadLn(SaveFile, HiveX);
        ReadLn(SaveFile, HiveY);
        ReadLn(SaveFile, DiffLevel);
        ReadLn(SaveFile, LastSong);
        ReadLn(SaveFile, intLimit);
        ReadLn(SaveFile, n);
        if n=0 then
            blCursed:=false
        else
            blCursed:=true;
        ReadLn(SaveFile, n);
        if n=0 then
            blBlessed:=false
        else
            blBlessed:=true;
        ReadLn(SaveFile, n);
        if n=0 then
            blEvil:=false
        else
            blEvil:=true;
    end;

    // load killed monsters
    for i:=1 to 200 do
        ReadLn(SaveFile, ThePlayer.longKilled[i]);

    // load level visits
    for i:=1 to WinLevel do
        ReadLn(SaveFile, ThePlayer.longLevelVisits[i]);

    // load inventory
    for i:=1 to 16 do
        with inventory[i] do
        begin
            ReadLn(SaveFile, intType);
            ReadLn(SaveFile, longNumber);
        end;

    // load songbook
    for i:=1 to 12 do
    begin
        with spellbook[i] do
        begin
            ReadLn(SaveFile, intType);
            ReadLn(SaveFile, intKnown);
            ReadLn(SaveFile, intRefresh);
        end;
        ReadLn(SaveFile, strQuickKey[i]);
    end;


    // load memory of killed unique monsters
    for i:=1 to 200 do
    begin
        ReadLn(SaveFile, n);
        if n=0 then
            ThePlayer.blUnKilled[i]:=false
        else
            ThePlayer.blUnKilled[i]:=true;
    end;

    // load memory of identified items
    for i:=1 to ItemCount do
    begin
        ReadLn(SaveFile, n);
        if n=0 then
            Thing[i].blIdentified := false
        else
        begin
            Thing[i].blIdentified := true;
            Thing[i].strName := Thing[i].strRealName;
        end;
    end;


    Close(SaveFile);

    ClearScreenSDL;

    if UseSDL=true then
        BlitImage_Title;

//    GetKeyInput ('Welcome back, ' + ThePlayer.strName + '.', false);

end;


procedure SetKeyConfig;
    var
        dummy: string;
        ch: char;
begin

    if UseSDL=true then
    begin
        TempAnsi := PathData+'graphics/decobg.jpg';
        LoadImage_Title(PathData+'graphics/decobg.jpg');
    end;

    repeat
        ClearScreenSDL;

        if UseSDL=true then
            BlitImage_Title
        else
            StatusDeco;

        TransTextXY(1, 1, 'KEYBINDINGS');

        TransTextXY(2, 3, 'Movement keys (N,S,E,W,NE,NW,SE,SW): '+KeyNorth+KeySouth+KeyEast+KeyWest+KeyNorthEast+KeyNorthWest+KeySouthEast+KeySouthWest);
        TransTextXY(2, 4, '[' + KeyEnter + '] (General action)');

        TransTextXY(2, 5, '  open chest                        (in front of a treasure chest)');
        TransTextXY(2, 6, '  sacrifice money                   (in front of an altar)');
        TransTextXY(2, 7, '  enter staircase                   (in front of a staircase)');
        TransTextXY(2, 8, '  drink from well                   (in front of a well)');
        TransTextXY(2, 9, '  examine crypt                     (in front of a crypt)');

        TransTextXY(2,11, '['+KeyTake+'] pick up item                    ['+KeyInventory+'] show inventory');
        TransTextXY(2,12, '['+KeyChant+'] show songbook (spells)          ['+KeyChantLast+'] chant last spell again');
        TransTextXY(2,13, '['+KeyTrade+'] talk to NPC or trader           ['+KeyShoot+'] fire long-range weapon');
        TransTextXY(2,14, '['+KeyTunnel+'] dig                             ['+KeyPray+'] pray to your god');
        TransTextXY(2,15, '['+KeyStatus+'] show status screen              ['+KeyQuestlog+'] show questlog');
        TransTextXY(2,16, '['+KeyShortRest+'] rest one turn                   ['+KeyRest+'] rest certain number of turns');
        TransTextXY(2,17, '['+KeyLook+'] identify tile                   ['+KeyQuit+'] show game menu');
        TransTextXY(2,18, '['+KeySearchSteal+'] search/steal                    ['+KeySetQuickKeys+'] show and reset quick keys');
        TransTextXY(2,19, '['+KeyCloseDoor+'] close door                      ['+KeyBigMap+'] show big minimap (SDL only)');

        dummy := GetKeyInput('Press a key to change it. Press [Enter] to accept, [ESC] to cancel.', false);
        ch:=dummy[1];
        if (dummy='ESC') or (dummy='ENTER') then
            ch:=chr(254);

        if ch=KeyNorth then
        begin
            BottomBar;
            dummy := GetKeyInput('Movement in northern direction (current: '+KeyNorth+')', false);
            KeyNorth := dummy[1];
        end;

        if ch=KeySouth then
        begin
            BottomBar;
            dummy := GetKeyInput('Movement in southern direction (current: '+KeySouth+')', false);
            KeySouth := dummy[1];
        end;

        if ch=KeySouth then
        begin
            BottomBar;
            dummy := GetKeyInput('Movement in eastern direction (current: '+KeyEast+')', false);
            KeyEast := dummy[1];
        end;

        if ch=KeyWest then
        begin
            BottomBar;
            dummy := GetKeyInput('Movement in western direction (current: '+KeyWest+')', false);
            KeyWest := dummy[1];
        end;

        if ch=KeyNorthEast then
        begin
            BottomBar;
            dummy := GetKeyInput('Movement in north-eastern direction (current: '+KeyNorthEast+')', false);
            KeyNorthEast := dummy[1];
        end;

        if ch=KeySouthEast then
        begin
            BottomBar;
            dummy := GetKeyInput('Movement in south-eastern direction (current: '+KeySouthEast+')', false);
            KeySouthEast := dummy[1];
        end;

        if ch=KeyNorthWest then
        begin
            BottomBar;
            dummy := GetKeyInput('Movement in north-western direction (current: '+KeyNorthWest+')', false);
            KeyNorthWest := dummy[1];
        end;

        if ch=KeySouthWest then
        begin
            BottomBar;
            dummy := GetKeyInput('Movement in south-western direction (current: '+KeySouthWest+')', false);
            KeySouthWest := dummy[1];
        end;

        if ch=KeyEnter then
        begin
            BottomBar;
            dummy := GetKeyInput('General action / use (current: '+KeyEnter+')', false);
            KeyEnter := dummy[1];
        end;

        if ch=KeyTake then
        begin
            BottomBar;
            dummy := GetKeyInput('Pickup item (current: '+KeyTake+')', false);
            KeyTake := dummy[1];
        end;

        if ch=KeySearchSteal then
        begin
            BottomBar;
            dummy := GetKeyInput('Search / steal (current: '+KeySearchSteal+')', false);
            KeySearchSteal := dummy[1];
        end;

        if ch=KeyTrade then
        begin
            BottomBar;
            dummy := GetKeyInput('Talk and trade (current: '+KeyTrade+')', false);
            KeyTrade := dummy[1];
        end;

        if ch=KeyCloseDoor then
        begin
            BottomBar;
            dummy := GetKeyInput('Close door (current: '+KeyCloseDoor+')', false);
            KeyCloseDoor := dummy[1];
        end;

        if ch=KeyTunnel then
        begin
            BottomBar;
            dummy := GetKeyInput('Dig through stone (current: '+KeyTunnel+')', false);
            KeyTunnel := dummy[1];
        end;

        if ch=KeyPray then
        begin
            BottomBar;
            dummy := GetKeyInput('Pray to your god (current: '+KeyPray+')', false);
            KeyPray := dummy[1];
        end;

        if ch=KeyShoot then
        begin
            BottomBar;
            dummy := GetKeyInput('Shoot with equipped fire-arm (current: '+KeyShoot+')', false);
            KeyShoot := dummy[1];
        end;

        if ch=KeyChant then
        begin
            BottomBar;
            dummy := GetKeyInput('Show songbook with magical songs (current: '+KeyChant+')', false);
            KeyChant := dummy[1];
        end;

        if ch=KeyChantLast then
        begin
            BottomBar;
            dummy := GetKeyInput('Chant last magical song again (current: '+KeyChantLast+')', false);
            KeyChantLast := dummy[1];
        end;

        if ch=KeyShortRest then
        begin
            BottomBar;
            dummy := GetKeyInput('Rest for one turn (current: '+KeyShortRest+')', false);
            KeyShortRest := dummy[1];
        end;

        if ch=KeyRest then
        begin
            BottomBar;
            dummy := GetKeyInput('Rest (current: '+KeyRest+')', false);
            KeyRest := dummy[1];
        end;

        if ch=KeyStatus then
        begin
            BottomBar;
            dummy := GetKeyInput('Show status screen (current: '+KeyStatus+')', false);
            KeyStatus := dummy[1];
        end;

        if ch=KeyQuestlog then
        begin
            BottomBar;
            dummy := GetKeyInput('Show questlog (current: '+KeyQuestlog+')', false);
            KeyQuestlog := dummy[1];
        end;

        if ch=KeyInventory then
        begin
            BottomBar;
            dummy := GetKeyInput('Show inventory (current: '+KeyInventory+')', false);
            KeyInventory := dummy[1];
        end;

        if ch=KeyHelp then
        begin
            BottomBar;
            dummy := GetKeyInput('Show help screen (current: '+KeyHelp+')', false);
            KeyHelp := dummy[1];
        end;

        if ch=KeyLook then
        begin
            BottomBar;
            dummy := GetKeyInput('Identify tile (current: '+KeyLook+')', false);
            KeyLook := dummy[1];
        end;

        if ch=KeyQuit then
        begin
            BottomBar;
            dummy := GetKeyInput('Show game menu (current: '+KeyQuit+')', false);
            KeyQuit := dummy[1];
        end;

        if ch=KeySetQuickKeys then
        begin
            BottomBar;
            dummy := GetKeyInput('Set quick keys (current: '+KeySetQuickKeys+')', false);
            KeySetQuickKeys := dummy[1];
        end;

        if ch=KeyBigMap then
        begin
            BottomBar;
            dummy := GetKeyInput('Show big minimap (current: '+KeyBigMap+')', false);
            KeyBigMap := dummy[1];
        end;
    
    until (dummy='ESC') or (dummy='ENTER');

    //writeln(dummy);

    if dummy='ENTER' then
    begin
        ClearScreenSDL;
        SaveConfig;
//        GetKeyInput('Keybindings saved.', true);
    end;

    if dummy='ESC' then
    begin
        ClearScreenSDL;
        InitKeys(false);
//        GetKeyInput('Keybindings restored from existing config file.', true);
    end;
end;

procedure SetOptions;
    var
        dummy: string;
        ch: char;
        Fullscreenflag: longword;
begin

    if UseSDL=true then
    begin
        TempAnsi := PathData+'graphics/invbg.jpg';
        LoadImage_Title(PathData+'graphics/invbg.jpg');
    end;

    ch:='-';

    repeat
        ClearScreenSDL;

        if UseSDL=true then
            BlitImage_Title
        else
        begin
            DialogWin;
            StatusDeco;
        end;

        TransTextXY(1, 1, 'GENERAL OPTIONS');
        TransTextXY(8, 5, 'a  Use graphical output? ' + BoolString(UseSDL, 1) + '. (requires restart)');
        TransTextXY(8, 6, 'b  Use small tiles? ' + BoolString(UseSmallTiles, 1) + '.');
        TransTextXY(8, 7, 'c  Use fullscreen in graphical mode? ' + BoolString(UseFullscreen, 1) + '.');
        TransTextXY(8, 8, 'd  1024x768 in graphical mode? ' + BoolString(UseHiRes, 1) + '.');
        TransTextXY(8, 9, 'e  Optimize interface in 1024x768 for netbooks? ' + BoolString(Netbook, 1) + '.');
        TransTextXY(8,10, 'f  Show blood in graphical mode? ' + BoolString(ShowBlood, 1)+'.');

        TransTextXY(8,12, 'g  Play music? ' + BoolString(blUseExternPlayer, 1)+'.');
        TransTextXY(8,13, 'h  Play sound effects? ' + BoolString(blUseExternPlayerTwo, 1)+'.');
        TransTextXY(8,14, 'i  Current media player: ' + strExternPlayer);

        TransTextXY(8,16, 'j  Create big, strong and mighty enemies? ' + BoolString(GrowingMonsters, 1)+'.');
        TransTextXY(8,17, 'k  Move after victory? ' + BoolString(AutoMoveToTile, 1)+'.');

        dummy := GetKeyInput('Select an option or press [Enter] to save settings, [ESC] to cancel.', false);
        ch:=dummy[1];

        case ch of
            'a':
            begin
                GetKeyInput('LambdaRogue will now save and close. Restart it afterwards.', true);
                SaveGame(ThePlayer.strName, DungeonLevel);
                StopGraphics;
                StopMusic;
                FreeMusic;
                UseSDL:=BoolToggle(UseSDL);
                SaveConfig;
                if UseSDL=true then
                    writeln('LambdaRogue will run in graphical mode when restarted.')
                else
                    writeln('LambdaRogue will run in console mode when restarted.');
                halt;
            end;
            'b':
            begin
                UseSmallTiles:=BoolToggle(UseSmallTiles);

                if UseSmallTiles=true then
                begin
                    tilesetASCII := LR_IMG_LOAD(PathData+'graphics/tileset-2-small.png');
                    if UseHiRes=false then
                    begin
                        intCenterX := 40 div 2;
                        intCenterY := 10 div 2;
                    end
                    else
                    begin
                        intCenterX := 51 div 2;
                        intCenterY := (19 div 2)-1;
                    end;
                end
                else
                begin
                    tilesetASCII := LR_IMG_LOAD(PathData+'graphics/tileset-2-big.png');
                    if UseHiRes=false then
                    begin
                        intCenterX := 20 div 2;
                        intCenterY := 4 div 2;
                    end
                    else
                    begin
                        intCenterX := 25 div 2;
                        intCenterY := (9 div 2)-1;
                    end;
                end;
                ThePlayer.intBX := intCenterX;
                ThePlayer.intBY := intCenterY;
                SaveConfig;
            end;
            'c':
            begin
                UseFullscreen:=BoolToggle(UseFullscreen);
                SaveConfig;

                SDL_FREESURFACE(screen);
                if UseFullScreen=true then
                    FullscreenFlag := SDL_Fullscreen
                else
                    FullscreenFlag := 0;
                SDL_QUIT;

                delay(50);

                SDL_INIT(SDL_INIT_VIDEO);
                SDL_ENABLEUNICODE(1);
                SDL_ENABLEKEYREPEAT(SDL_DEFAULT_REPEAT_DELAY, SDL_DEFAULT_REPEAT_INTERVAL);

                SDL_WM_SETCAPTION('LambdaRogue '+strVersion, 'LambdaRogue '+strVersion);
                //SDL_WM_SETICON(SDL_LOADBMP(PathData+'graphics/icon.bmp'),0);  //SDL-bug on maemo5
                if UseHiRes=false then
                    screen := SDL_SETVIDEOMODE(800,480,24,SDL_HWSURFACE + FullscreenFlag)
                else
                    screen := SDL_SETVIDEOMODE(1024,768,24,SDL_HWSURFACE + FullscreenFlag);

            end;
            'd':
            begin
                UseHiRes:=BoolToggle(UseHiRes);
                SaveConfig;

                SDL_FREESURFACE(screen);
                if UseFullScreen=true then
                    FullscreenFlag := SDL_Fullscreen
                else
                    FullscreenFlag := 0;
                SDL_QUIT;

                delay(50);

                SDL_INIT(SDL_INIT_VIDEO);
                SDL_ENABLEUNICODE(1);
                SDL_ENABLEKEYREPEAT(SDL_DEFAULT_REPEAT_DELAY, SDL_DEFAULT_REPEAT_INTERVAL);

                SDL_WM_SETCAPTION('LambdaRogue '+strVersion, 'LambdaRogue '+strVersion);
                //SDL_WM_SETICON(SDL_LOADBMP(PathData+'graphics/icon.bmp'),0);  //SDL-bug on maemo5

                if UseHiRes=false then
                begin
                    screen := SDL_SETVIDEOMODE(800,480,24,SDL_HWSURFACE + FullscreenFlag);
                    HiResOffsetX:=0;
                    HiResOffsetY:=0;
                    HiResTextOffsetX:=0;
                    HiResTextOffsetY:=0;
                    ChantBarX:=63;
                    ChantBarY:=68;
                    LoadImage_Back(PathData+'graphics/bg-800.png');
                end
                else
                begin
                    screen := SDL_SETVIDEOMODE(1024,768,24,SDL_HWSURFACE + FullscreenFlag);

                    HiResOffsetX:=110;
                    HiResTextOffsetX:=-12;
                    ChantBarX:=219;
                    ChantBarY:=9;
                    LoadImage_Back(PathData+'graphics/bg-1024.png');

                    if Netbook=true then
                    begin
                        HiResOffsetY:=0;
                        HiResTextOffsetY:=0;
                    end
                    else
                    begin
                        HiResOffsetY:=80;
                        HiResTextOffsetY:=3;
                    end;
                end;

                if UseSmallTiles=true then
                begin
                    if UseHiRes=false then
                    begin
                        intCenterX := 40 div 2;
                        intCenterY := 10 div 2;
                    end
                    else
                    begin
                        intCenterX := 51 div 2;
                        intCenterY := (19 div 2)-1;
                    end
                end
                else
                begin
                    if UseHiRes=false then
                    begin
                        intCenterX := 20 div 2;
                        intCenterY := 4 div 2;
                    end
                    else
                    begin
                        intCenterX := 25 div 2;
                        intCenterY := (9 div 2)-1;
                    end;
                end;
                ThePlayer.intBX := intCenterX;
                ThePlayer.intBY := intCenterY;
            end;
            'e':
            begin
                Netbook:=BoolToggle(Netbook);
                if Netbook=true then
                begin
                    HiResOffsetY:=0;
                    HiResTextOffsetY:=0;
                end
                else
                begin
                    HiResOffsetY:=80;
                    HiResTextOffsetY:=3;
                end;
            end;
            'f':
                ShowBlood:=BoolToggle(ShowBlood);
            'g':
                blUseExternPlayer:=BoolToggle(blUseExternPlayer);
            'h':
                blUseExternPlayerTwo:=BoolToggle(blUseExternPlayerTwo);
            'i':
                strExternPlayer:=GetTextInput('Enter executable file: ', 50);

            'j':
                GrowingMonsters:=BoolToggle(GrowingMonsters);
            'k':
                AutoMoveToTile:=BoolToggle(AutoMoveToTile);
        end;

    until (dummy='ESC') or (dummy='ENTER');

    if dummy='ENTER' then
    begin
        ClearScreenSDL;
        SaveConfig;
        //GetKeyInput('Settings saved.', true);
    end;

    if dummy='ESC' then
    begin
        ClearScreenSDL;
        InitKeys(true);
        //GetKeyInput('Settings restored from existing config file.', true);
    end;

    if blUseExternPlayer=false then
        StopMusic;

end;


// returns true, if the player selected to quit the game
function GameMenu: boolean;
    var
        ch: char;
        dummy: string;
begin
    GameMenu:=false;

    repeat
        ClearScreenSDL;

        if UseSDL=true then
            LoadImage_Title(PathData+'graphics/menubg.jpg');

        repeat
            if UseSDL=true then
            begin
                BlitImage_Title
            end
            else
            begin
                TextXY(31, 10, '1  General Options');
                TextXY(31, 11, '2  Keybindings');
                TextXY(31, 12, '3  Save and Quit');
            end;

            dummy:=GetKeyInput('Please select an option. Press [ESC] to resume game.', false);
        until (dummy='1') or (dummy='2') or (dummy='3') or (dummy='ESC');
        ch:=dummy[1];

        case ch of
            '1':
                SetOptions;
            '2':
                SetKeyConfig;
            '3':
                GameMenu:=true;
        end;

    until (dummy='ESC') or (GameMenu=true);

    ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
end;


procedure ShowPersonalLib;
begin

end;


// shows a bigger minimap
procedure BigMap;
var
    i, j, k, x, y: Integer;
    TempAnsi: Ansistring;
    dummy: string;
begin

    TempAnsi := PathData+'graphics/plotbg.jpg';
    LoadImage_Title(PathData+'graphics/plotbg.jpg');

    dummy:='-';

    repeat

        BlitImage_Title;

        if UseHiRes=true then
            x:=37
        else
            x:=20;

        for i:=1 to DngMaxWidth-1 do
        begin
            inc(x);

            if (UseHiRes=true) and (Netbook=false) then
                y:=5
            else if (UseHiRes=true) and (Netbook=true) then
                y:=-2
            else if (UseHiRes=false) then
                y:=-2;

            for j:=19 to DngMaxHeight-1 do
            begin
                inc(y);
                if DngLvl[i,j].blKnown=true then
                begin
                    if (DngLvl[i,j].intFloorType=1) or (DngLvl[i,j].intFloorType=24) then
                        SmallCharXY(x, y, '#', true);

                    if (DngLvl[i,j].intFloorType=6) then
                        SmallCharXY(x, y, '^', true);

                    if (DngLvl[i,j].intFloorType=2) or (DngLvl[i,j].intFloorType=31) or (DngLvl[i,j].intFloorType=60) or (DngLvl[i,j].intFloorType=61) then
                        SmallCharXY(x, y, '.', true);
                    if (DngLvl[i,j].intFloorType=10) or (DngLvl[i,j].intFloorType=13) or (DngLvl[i,j].intFloorType=17) then
                        SmallCharXY(x, y, 'T', true);
                    if (DngLvl[i,j].intFloorType=12) or (DngLvl[i,j].intFloorType=21) or (DngLvl[i,j].intFloorType=22) or (DngLvl[i,j].intFloorType=23) then
                        SmallCharXY(x, y, ',', true);
                    if (DngLvl[i,j].intFloorType=5) or (DngLvl[i,j].intFloorType=38) or (DngLvl[i,j].intFloorType=39) or (DngLvl[i,j].intFloorType=20) then
                        SmallCharXY(x, y, '=', true);
                    if DngLvl[i,j].intFloorType=18 then
                        SmallCharXY(x, y, '~', true);
                    if DngLvl[i,j].intFloorType=15 then
                        SmallCharXY(x, y, '|', false);
                    if DngLvl[i,j].intFloorType=28 then
                        SmallCharXY(x, y, 'U', false);
                    if (DngLvl[i,j].intFloorType=9) or (DngLvl[i,j].intFloorType=63) then  // runter
                        SmallCharXY(x, y, '>', false);
                    if (DngLvl[i,j].intFloorType=8) or (DngLvl[i,j].intFloorType=62) then  // hoch
                        SmallCharXY(x, y, '<', false);
                    if (DngLvl[i,j].intFloorType=25) or (DngLvl[i,j].intFloorType=3) then
                        SmallCharXY(x, y, '+', true);

                end;
            end;
        end;

        // Player

        if UseHiRes=true then
            x:=37
        else
            x:=20;

        for i:=1 to DngMaxWidth-1 do
        begin
            inc(x);

            if (UseHiRes=true) and (Netbook=false) then
                y:=5
            else if (UseHiRes=true) and (Netbook=true) then
                y:=-2
            else if (UseHiRes=false) then
                y:=-2;

            for j:=19 to DngMaxHeight-1 do
            begin
                inc(y);

                // Discovered NPCs
                for k:=1 to 9 do
                    if (i=NPC[k].intX) and (j=NPC[k].intY) then
                        if not ((i=ThePlayer.intX) and (j=ThePlayer.intY)) then
                            SmallCharXY(x, y, IntToStr(k)[1], false);

                // Player
                if (i=ThePlayer.intX) and (j=ThePlayer.intY) then
                    SmallCharXY(x, y, '@', false);
            end;
        end;

        dummy := GetKeyInput('', false);

    until (dummy='ESC');

end;

procedure ShowStatusMessagelog;
var

    perPage: integer;  //How many rows per page 
    totalOfPages: integer;
    intViewPage: integer; //Currently viewed page
    i, j, y: integer;
    dummy, colon: String;
    ch: Char;

LengthOfLog: integer;
begin

    

    LengthOfLog := ThePlayer.intNumberOfLogs;
    perPage := 16;

    //Count how many pages are needed (div is integer division, so check if it was rounded down)
    totalOfPages := LengthOfLog DIV perPage;
    if totalOfPages*perPage < LengthOfLog then
       totalOfPages := totalOfPages +1;

    // Then, display the array pages
    ch:='-';
    dummy:='-';
    intViewPage:=1;

    if UseSDL=true then
    begin
        TempAnsi := PathData+'graphics/txtbg.jpg';
        LoadImage_Title(PathData+'graphics/txtbg.jpg');
    end
    else
        ClearScreenSDL;

    repeat
        PlaySFX('page-turn.mp3'); //Play every time page is flipped

        if intViewPage<1 then
            intViewPage:=1;

        if intViewPage>totalOfPages then
            intViewPage:=totalOfPages;

        repeat
            if UseSDL=true then
                BlitImage_Title
            else
            begin
                ClearScreenSDL;
                StatusDeco;
            end;

        TransTextXY(1, 1, 'MESSAGELOG (Page '+IntToStr(intViewPage)+'/'+IntToStr(totalOfPages)+')');

        for i:=(intViewPage)*perPage downto (intViewPage-1)*perPage+1 do
            if i <= LengthOfLog then
            begin
                TransTextXY(1, 3+ ((i-1) MOD perPage), ThePlayer.strMessageLog[i]);
                //Writeln ('i= '+IntToStr(i)+' . row = '+IntToStr(2+ (i-1 MOD perPage)));
            end;

            dummy := GetKeyInput('[+] or [PgDn] Next Page    [-] or [PgUp] Previous Page    [ESC] Resume game', false);
        until (dummy='+') or (dummy='s') or (dummy='-') or (dummy='f') or (dummy='ESC');
        ch := dummy[1];

        case ch of
            's':
                inc(intViewPage);
            '+':
                inc(intViewPage);
            'f':
                dec(intViewPage);
            '-':
                dec(intViewPage);
        end;

    until dummy='ESC';

end;

procedure ShowStatusQuestlog;
    var
        i, j, y, intViewPage: integer;
        dummy, colon: String;
        ch: Char;
        strComplete: array[1..64] of String;    // this array contains the complete list and is made
                                            // made visible in steps of 16
begin

    PlaySFX('page-turn.mp3');

    for i:=1 to 64 do
        strComplete[i]:='';

    // First, fill the array
    y:=1;
    for j:=1 to WinLevel do
        for i:=1 to 9 do
            if (ThePlayer.intQuestState[j,i]=1) or (ThePlayer.intQuestState[j,i]=2) then
            begin
                if j<10 then // this for aligning numbers correctly.
                    colon:='  '
                else
                    colon:=' ';

                if ThePlayer.intQuestState[j,i]=2 then
                    colon := colon + ' :)  '
                else
                    colon := colon + '     ';

                strComplete[y]:=IntToStr(j)+'/'+IntToStr(i)+ colon + Quest[j,i].strDescri;
                inc(y);
            end;


    // Then, display the array pages
    ch:='-';
    dummy:='-';
    intViewPage:=1;

    if UseSDL=true then
    begin
        TempAnsi := PathData+'graphics/txtbg.jpg';
        LoadImage_Title(PathData+'graphics/txtbg.jpg');
    end
    else
        ClearScreenSDL;

    repeat

        if intViewPage<1 then
            intViewPage:=1;

        if intViewPage>3 then
            intViewPage:=3;

        repeat
            if UseSDL=true then
                BlitImage_Title
            else
            begin
                ClearScreenSDL;
                StatusDeco;
            end;

            TransTextXY(1, 1, 'QUESTLOG (Page '+IntToStr(intViewPage)+'/3)');

            if intViewPage=1 then
                for i:=1 to 16 do
                    TransTextXY(1, 4+i, strComplete[i]);

            if intViewPage=2 then
                for i:=1 to 16 do
                    TransTextXY(1, 4+i, strComplete[16+i]);

            if intViewPage=3 then
                for i:=1 to 16 do
                    TransTextXY(1, 4+i, strComplete[32+i]);

            dummy := GetKeyInput('[+] or [PgDn] Next Page    [-] or [PgUp] Previous Page    [ESC] cancel', false);
        until (dummy='+') or (dummy='s') or (dummy='-') or (dummy='f') or (dummy='ESC');
        ch := dummy[1];

        case ch of
            's':
                inc(intViewPage);
            '+':
                inc(intViewPage);
            'f':
                dec(intViewPage);
            '-':
                dec(intViewPage);
        end;

    until dummy='ESC';

end;


// detailed information on equip and bonuses
procedure EquipmentInfo;
    var
        strName, strWP, strAP, strGP, strSP, dummy: string;
        intTotalWP, intTotalAP, intTotalGP, intTotalSP, i: integer;
begin
    if UseSDL=false then
        ClearScreenSDL;

    repeat

        if UseSDL=true then
            BlitImage_Title
        else
            BottomBar;

        TransTextXY(1,1, 'EQUIPMENT SUMMARY');


        for i:=4 to 18 do
        begin
            TransTextXY(14, i, '|');
            TransTextXY(48, i, '|');
            TransTextXY(53, i, '|');
            TransTextXY(58, i, '|');
            TransTextXY(63, i, '|');
        end;

        TransTextXY(1, 7, 'Weapon');
        TransTextXY(1, 8, 'Shield/Extra');

        TransTextXY(1,10, 'Armour');
        TransTextXY(1,11, 'Hat');
        TransTextXY(1,12, 'Shoes');

        TransTextXY(1,14, 'Ring (left)');
        TransTextXY(1,15, 'Ring (right)');

        TransTextXY(1,18, 'Total');

        TransTextXY(16,4, 'Name');
        TransTextXY(50,4, 'WP');
        TransTextXY(55,4, 'AP');
        TransTextXY(60,4, 'GP');
        TransTextXY(65,4, 'SP');

        if UseSDL=false then
        begin
            TransTextXY(1, 5, '-------------+---------------------------------+----+----+----+-------------');
            TransTextXY(1,17, '-------------+---------------------------------+----+----+----+-------------');
        end
        else
        begin
            TransTextXY(1, 5, '----------------------------------------------------------------------------');
            TransTextXY(1,17, '----------------------------------------------------------------------------');
        end;


        intTotalWP:=0;
        intTotalAP:=0;
        intTotalGP:=0;
        intTotalSP:=0;

        // Weapon
        if ThePlayer.intWeapon>0 then
        begin
            strName:=Thing[ThePlayer.intWeapon].chLetter+' '+Thing[ThePlayer.intWeapon].strName;
            strWP:=IntToStr(Thing[ThePlayer.intWeapon].intWP);
            strAP:=IntToStr(Thing[ThePlayer.intWeapon].intAP);
            strGP:=IntToStr(Thing[ThePlayer.intWeapon].intGP);
            strSP:=IntToStr(Thing[ThePlayer.intWeapon].intSP);
            inc(intTotalWP,Thing[ThePlayer.intWeapon].intWP);
            inc(intTotalAP,Thing[ThePlayer.intWeapon].intAP);
            inc(intTotalGP,Thing[ThePlayer.intWeapon].intGP);
            inc(intTotalSP,Thing[ThePlayer.intWeapon].intSP);
        end
        else
        begin
            strName:='  -';
            strWP:='-';
            strAP:='-';
            strGP:='-';
            strSP:='-';
        end;
        TransTextXY(16, 7, strName);
        TransTextXY(50, 7, strWP);
        TransTextXY(55, 7, strAP);
        TransTextXY(60, 7, strGP);
        TransTextXY(65, 7, strSP);


        // Shield/Extra
        if ThePlayer.intExtra>0 then
        begin
            strName:=Thing[ThePlayer.intExtra].chLetter+' '+Thing[ThePlayer.intExtra].strName;
            strWP:=IntToStr(Thing[ThePlayer.intExtra].intWP);
            strAP:=IntToStr(Thing[ThePlayer.intExtra].intAP);
            strGP:=IntToStr(Thing[ThePlayer.intExtra].intGP);
            strSP:=IntToStr(Thing[ThePlayer.intExtra].intSP);
            inc(intTotalWP,Thing[ThePlayer.intExtra].intWP);
            inc(intTotalAP,Thing[ThePlayer.intExtra].intAP);
            inc(intTotalGP,Thing[ThePlayer.intExtra].intGP);
            inc(intTotalSP,Thing[ThePlayer.intExtra].intSP);
        end
        else
        begin
            strName:='  -';
            strWP:='-';
            strAP:='-';
            strGP:='-';
            strSP:='-';
        end;
        TransTextXY(16, 8, strName);
        TransTextXY(50, 8, strWP);
        TransTextXY(55, 8, strAP);
        TransTextXY(60, 8, strGP);
        TransTextXY(65, 8, strSP);


        // Armour
        if ThePlayer.intArmour>0 then
        begin
            strName:=Thing[ThePlayer.intArmour].chLetter+' '+Thing[ThePlayer.intArmour].strName;
            strWP:=IntToStr(Thing[ThePlayer.intArmour].intWP);
            strAP:=IntToStr(Thing[ThePlayer.intArmour].intAP);
            strGP:=IntToStr(Thing[ThePlayer.intArmour].intGP);
            strSP:=IntToStr(Thing[ThePlayer.intArmour].intSP);
            inc(intTotalWP,Thing[ThePlayer.intArmour].intWP);
            inc(intTotalAP,Thing[ThePlayer.intArmour].intAP);
            inc(intTotalGP,Thing[ThePlayer.intArmour].intGP);
            inc(intTotalSP,Thing[ThePlayer.intArmour].intSP);
        end
        else
        begin
            strName:='  -';
            strWP:='-';
            strAP:='-';
            strGP:='-';
            strSP:='-';
        end;
        TransTextXY(16,10, strName);
        TransTextXY(50,10, strWP);
        TransTextXY(55,10, strAP);
        TransTextXY(60,10, strGP);
        TransTextXY(65,10, strSP);


        // Hat
        if ThePlayer.intHat>0 then
        begin
            strName:=Thing[ThePlayer.intHat].chLetter+' '+Thing[ThePlayer.intHat].strName;
            strWP:=IntToStr(Thing[ThePlayer.intHat].intWP);
            strAP:=IntToStr(Thing[ThePlayer.intHat].intAP);
            strGP:=IntToStr(Thing[ThePlayer.intHat].intGP);
            strSP:=IntToStr(Thing[ThePlayer.intHat].intSP);
            inc(intTotalWP,Thing[ThePlayer.intHat].intWP);
            inc(intTotalAP,Thing[ThePlayer.intHat].intAP);
            inc(intTotalGP,Thing[ThePlayer.intHat].intGP);
            inc(intTotalSP,Thing[ThePlayer.intHat].intSP);
        end
        else
        begin
            strName:='  -';
            strWP:='-';
            strAP:='-';
            strGP:='-';
            strSP:='-';
        end;
        TransTextXY(16,11, strName);
        TransTextXY(50,11, strWP);
        TransTextXY(55,11, strAP);
        TransTextXY(60,11, strGP);
        TransTextXY(65,11, strSP);


        // Shoes
        if ThePlayer.intFeet>0 then
        begin
            strName:=Thing[ThePlayer.intFeet].chLetter+' '+Thing[ThePlayer.intFeet].strName;
            strWP:=IntToStr(Thing[ThePlayer.intFeet].intWP);
            strAP:=IntToStr(Thing[ThePlayer.intFeet].intAP);
            strGP:=IntToStr(Thing[ThePlayer.intFeet].intGP);
            strSP:=IntToStr(Thing[ThePlayer.intFeet].intSP);
            inc(intTotalWP,Thing[ThePlayer.intFeet].intWP);
            inc(intTotalAP,Thing[ThePlayer.intFeet].intAP);
            inc(intTotalGP,Thing[ThePlayer.intFeet].intGP);
            inc(intTotalSP,Thing[ThePlayer.intFeet].intSP);
        end
        else
        begin
            strName:='  -';
            strWP:='-';
            strAP:='-';
            strGP:='-';
            strSP:='-';
        end;
        TransTextXY(16,12, strName);
        TransTextXY(50,12, strWP);
        TransTextXY(55,12, strAP);
        TransTextXY(60,12, strGP);
        TransTextXY(65,12, strSP);


        // Ring (left)
        if ThePlayer.intRingLeft>0 then
        begin
            strName:=Thing[ThePlayer.intRingLeft].chLetter+' '+Thing[ThePlayer.intRingLeft].strName;
            strWP:=IntToStr(Thing[ThePlayer.intRingLeft].intWP);
            strAP:=IntToStr(Thing[ThePlayer.intRingLeft].intAP);
            strGP:=IntToStr(Thing[ThePlayer.intRingLeft].intGP);
            strSP:=IntToStr(Thing[ThePlayer.intRingLeft].intSP);
            inc(intTotalWP,Thing[ThePlayer.intRingLeft].intWP);
            inc(intTotalAP,Thing[ThePlayer.intRingLeft].intAP);
            inc(intTotalGP,Thing[ThePlayer.intRingLeft].intGP);
            inc(intTotalSP,Thing[ThePlayer.intRingLeft].intSP);
        end
        else
        begin
            strName:='  -';
            strWP:='-';
            strAP:='-';
            strGP:='-';
            strSP:='-';
        end;
        TransTextXY(16,14, strName);
        TransTextXY(50,14, strWP);
        TransTextXY(55,14, strAP);
        TransTextXY(60,14, strGP);
        TransTextXY(65,14, strSP);


        // Ring (right)
        if ThePlayer.intRingRight>0 then
        begin
            strName:=Thing[ThePlayer.intRingRight].chLetter+' '+Thing[ThePlayer.intRingRight].strName;
            strWP:=IntToStr(Thing[ThePlayer.intRingRight].intWP);
            strAP:=IntToStr(Thing[ThePlayer.intRingRight].intAP);
            strGP:=IntToStr(Thing[ThePlayer.intRingRight].intGP);
            strSP:=IntToStr(Thing[ThePlayer.intRingRight].intSP);
            inc(intTotalWP,Thing[ThePlayer.intRingRight].intWP);
            inc(intTotalAP,Thing[ThePlayer.intRingRight].intAP);
            inc(intTotalGP,Thing[ThePlayer.intRingRight].intGP);
            inc(intTotalSP,Thing[ThePlayer.intRingRight].intSP);
        end
        else
        begin
            strName:='  -';
            strWP:='-';
            strAP:='-';
            strGP:='-';
            strSP:='-';
        end;
        TransTextXY(16,15, strName);
        TransTextXY(50,15, strWP);
        TransTextXY(55,15, strAP);
        TransTextXY(60,15, strGP);
        TransTextXY(65,15, strSP);


        // totals
        TransTextXY(50,18, IntToStr(intTotalWP));
        TransTextXY(55,18, IntToStr(intTotalAP));
        TransTextXY(60,18, IntToStr(intTotalGP));
        TransTextXY(65,18, IntToStr(intTotalSP));

        dummy := GetKeyInput('[ESC] Resume game', false);
    until (dummy='ESC');
end;


// Collected diploas; view diplomas and their effects
procedure DiplomaInfo;
var
   dummy: string;
   Rankpin : array[1..30] of string;
begin
    if UseSDL=false then
        ClearScreenSDL;

    if UseSDL=true then
    begin
        Rankpin[1]:=chr(229)+'  ';
        Rankpin[2]:=chr(229)+chr(229)+' ';
        Rankpin[3]:=chr(231)+'  ';
        Rankpin[4]:=chr(231)+chr(231)+' ';
        Rankpin[5]:=chr(229)+chr(231)+chr(230);

        Rankpin[6]:=chr(232)+'  ';
        Rankpin[7]:=chr(232)+chr(232)+' ';
        Rankpin[8]:=chr(234)+'  ';
        Rankpin[9]:=chr(234)+chr(234)+' ';
        Rankpin[10]:=chr(229)+chr(233)+chr(230);

        Rankpin[11]:=chr(235)+chr(231)+chr(235);  // Assassin
        Rankpin[12]:=chr(229)+chr(235)+chr(230); // Chief of Agents
        Rankpin[13]:=chr(235)+'  ';  // Master Thief
        Rankpin[14]:=chr(235)+chr(235)+' '; // Chief of Thiefs

        Rankpin[15]:=chr(236)+'  ';
        Rankpin[16]:=chr(236)+chr(236)+' ';
        Rankpin[17]:=chr(236)+chr(231)+chr(236);
        Rankpin[18]:=chr(229)+chr(236)+chr(230);
    end
    else
    begin
        Rankpin[1] := '<  ';
        Rankpin[2] := '<< ';
        Rankpin[3] := '^  ';
        Rankpin[4] := '^^ ';
        Rankpin[5] := '<^>';

        Rankpin[6] := '*  ';
        Rankpin[7] := '** ';
        Rankpin[8] := '|  ';
        Rankpin[9] := '|| ';
        Rankpin[10]:= '<|>';

        Rankpin[11]:='/^/';  // Assassin
        Rankpin[12]:='</>'; // Chief of Agents
        Rankpin[13]:='/  ';  // Master Thief
        Rankpin[14]:='// '; // Chief of Thiefs

        Rankpin[15]:='!  ';  // Ranger
        Rankpin[16]:='!! ';  // hunter
        Rankpin[17]:='!^!';  // marksman
        Rankpin[18]:='<!>';  // sergeant
    end;



    repeat

        if UseSDL=true then
            BlitImage_Title
        else
            BottomBar;

        TransTextXY(3, 4, 'Rank                             Insignia');
        TransTextXY(3, 5, '-------------------------------------------------------------------------');

        // profession-dependant dipls
        case ThePlayer.intProf of
             2:   // enchanter
             begin
                 TransTextXY(1, 1,'DIPLOMAS ISSUED BY THE COUNCIL OF ENCHANTERS AND MAGES');
                 TransTextXY(3, 6,'Career: Enchanter');

                 TransTextXY(3, 7,'|');

                 if ThePlayer.intDipl[6] =1 then // Mage
                    TransTextXY(3, 8,'+--Mage                          '+Rankpin[6])
                 else
                    TransTextXY(3, 8,'+--(Mage)');

                    TransTextXY(3, 9,'|  |');

                 if ThePlayer.intDipl[7] =1 then // Battlemage
                    TransTextXY(3,10,'|  +--Battlemage                 '+Rankpin[7])
                 else
                    TransTextXY(3,10,'|  +--(Battlemage)');

                    TransTextXY(3,11,'|');

                 if ThePlayer.intDipl[8] =1 then // Monk
                    TransTextXY(3,12,'+--Believer                      '+Rankpin[8])
                 else
                    TransTextXY(3,12,'+--(Believer)');

                    TransTextXY(3,13,'   |');

                 if ThePlayer.intDipl[9] =1 then // Monk
                    TransTextXY(3,14,'   +--Monk                       '+Rankpin[9])
                 else
                    TransTextXY(3,14,'   +--(Monk)');

                    TransTextXY(3,15,'      |');

                 if ThePlayer.intDipl[10]=1 then // Holy Warrior
                    TransTextXY(3,16,'      +--Holy Warrior            '+Rankpin[10])
                 else
                    TransTextXY(3,16,'      +--(Holy Warrior)');
             end;

             3:   // thief
             begin
                 TransTextXY(1, 1,'DIPLOMAS ISSUED BY THE GUILD OF THIEVES AND AGENTS');
                 TransTextXY(3, 6,'Career: Thief');

                 TransTextXY(3, 7,'|');

                 if ThePlayer.intDipl[11]=1 then // Assassin
                    TransTextXY(3, 8,'+--Assassin                      '+Rankpin[11])
                 else
                    TransTextXY(3, 8,'+--(Assassin)');

                    TransTextXY(3, 9,'|  |');

                 if ThePlayer.intDipl[12]=1 then // Chief of Agents
                    TransTextXY(3,10,'|  +--Agent                      '+Rankpin[12])
                 else
                    TransTextXY(3,10,'|  +--(Agent)');


                    TransTextXY(3,11,'|');

                 if ThePlayer.intDipl[13]=1 then // Master Thief
                    TransTextXY(3,12,'+--Master Thief                  '+Rankpin[13])
                 else
                    TransTextXY(3,12,'+--(Master Thief)');

                    TransTextXY(3,13,'   |');

                 if ThePlayer.intDipl[14] =1 then // Guild Leader
                    TransTextXY(3,14,'   +--Guild Leader               '+Rankpin[14])
                 else
                    TransTextXY(3,14,'   +--(Guild Leader)');
             end;

             4:   // archer
             begin
                 TransTextXY(1, 1,'DIPLOMAS ISSUED BY THE ORDER OF ARCHERS AND RANGERS');
                 TransTextXY(3, 6,'Career: Archer');

                 TransTextXY(3, 7,'|');

                 if ThePlayer.intDipl[15]=1 then // Hunter
                    TransTextXY(3, 8,'+--Hunter                        '+Rankpin[15])
                 else
                    TransTextXY(3, 8,'+--(Hunter)');

                    TransTextXY(3, 9,'|  |');

                 if ThePlayer.intDipl[16]=1 then // Ranger
                    TransTextXY(3,10,'|  +--Ranger                     '+Rankpin[16])
                 else
                    TransTextXY(3,10,'|  +--(Ranger)');

                    TransTextXY(3,11,'|');

                 if ThePlayer.intDipl[17]=1 then // Marksman
                    TransTextXY(3,12,'+--Marksman                      '+Rankpin[17])
                 else
                    TransTextXY(3,12,'+--(Marksman)');

                    TransTextXY(3,13,'   |');

                 if ThePlayer.intDipl[18]=1 then // Sergeant
                    TransTextXY(3,14,'   +--Sergeant                   '+Rankpin[18])
                 else
                    TransTextXY(3,14,'   +--(Sergeant)');
             end;


             5:   // soldier
             begin
                 TransTextXY(1, 1,'DIPLOMAS ISSUED BY THE ORDER OF SOLDIERS AND KNIGHTS');
                 TransTextXY(3, 6,'Career: Soldier');

                 TransTextXY(3, 7,'|');

                 if ThePlayer.intDipl[1]=1 then // Centurio
                    TransTextXY(3, 8,'+--Centurio                      '+Rankpin[1])
                 else
                    TransTextXY(3, 8,'+--(Centurio)');

                    TransTextXY(3, 9,'   |');

                 if ThePlayer.intDipl[2]=1 then // Hastatus
                    TransTextXY(3,10,'   +--Hastatus                   '+Rankpin[2])
                 else
                    TransTextXY(3,10,'   +--(Hastatus)');

                    TransTextXY(3,11,'      |');

                 if ThePlayer.intDipl[3]=1 then // Princeps
                    TransTextXY(3,12,'      +--Princeps                '+Rankpin[3])
                 else
                    TransTextXY(3,12,'      +--(Princeps)');

                    TransTextXY(3,13,'         |');

                 if ThePlayer.intDipl[4]=1 then // Pilus
                    TransTextXY(3,14,'         +--Pilus                '+Rankpin[4])
                 else
                    TransTextXY(3,14,'         +--(Pilus)');

                    TransTextXY(3,15,'            |');

                 if ThePlayer.intDipl[5]=1 then // Primus Pilus
                    TransTextXY(3,16,'            +--Primus Pilus      '+Rankpin[5])
                 else
                    TransTextXY(3,16,'            +--(Primus Pilus)');

             end;
        end;

        TransTextXY(3, 19, 'Diplomas shown in brackets & without insignia have not yet been obtained.');

        dummy := GetKeyInput('[ESC] Resume game', false);
    until (dummy='ESC');
end;

// Tidy status screen
procedure TidyPlayerStatus;
    var
        strSex, strProf, strDiff, dummy: string;
        ToNextLevel: longint;
        ch: char;
begin
    if UseSDL=true then
    begin
        if ThePlayer.intSex=1 then
            TempAnsi := PathData+'graphics/'+IntToStr(ThePlayer.intProf)+'-m.png'
        else
            TempAnsi := PathData+'graphics/'+IntToStr(ThePlayer.intProf)+'-f.png';
        LoadImage_Title(TempAnsi);
    end
    else
        ClearScreenSDL;


    repeat

        if UseSDL=true then
            BlitImage_Title
        else
            BottomBar;

        case ThePlayer.intSex of
            1:
                strSex:='male';
            2:
                strSex:='female';
        end;

        case ThePlayer.intProf of
            1:
                strProf:='constructor';
            2:
                strProf:='enchanter';
            3:
                strProf:='thief';
            4:
                strProf:='archer';
            5:
                strProf:='soldier';
        end;

        case DiffLevel of
            1:
                strDiff:='Bronze';
            2:
                strDiff:='Silver';
            3:
                strDiff:='Gold';
        end;

        if ThePlayer.blEvil=true then
            TransTextXY(1,1, uppercase(ThePlayer.strName + ', DEVOTED TO DEATH'))
        else
            TransTextXY(1,1, uppercase(ThePlayer.strName + ', ' + strSex + ' ' + strProf));


        ToNextLevel := (ThePlayer.intLvl  * CONST_LVL_MULTI) * (ThePlayer.intLvl * CONST_LVL_MULTI) * ThePlayer.intLvl;

        TransTextXY(1, 4, 'HP  : ' + IntToStr(ThePlayer.intHP)+'/'+IntToStr(ThePlayer.intMaxHP));
        TransTextXY(1, 5, 'PP  : ' + IntToStr(ThePlayer.intPP)+'/'+IntToStr(ThePlayer.intMaxPP));
        TransTextXY(1, 7, 'STR : ' + IntToStr(ThePlayer.intStrength)+'%/100%');
        TransTextXY(1, 8, 'DVR : ' + IntToStr(ThePlayer.intLimit)+'%/100%');

        if ThePlayer.intLvl<CONST_MAXCLV then
            TransTextXY(1,10, 'EXP : ' + IntToStr(ThePlayer.longEXP)+' (Next: '+IntToStr(ToNextLevel)+')')
        else
            TransTextXY(1,10, 'EXP : ' + IntToStr(ThePlayer.longEXP));

        TransTextXY(1,11, 'CLV : ' + IntToStr(ThePlayer.intLvl)+' ('+strDiff+')');

        if DungeonLevel<20 then
            TransTextXY(1, 13, 'DLV : ' + IntToStr(DungeonLevel));
        if (DungeonLevel=20) or (DungeonLevel=21) then
            TransTextXY(1, 13, 'DLV : ?');
        if DungeonLevel>21 then
            TransTextXY(1, 13, 'DLV : ' + IntToStr(DungeonLevel-21));

        // abilities
        TransTextXY(30, 4, 'Fight    : ' + IntToStr(ThePlayer.intFight));
        TransTextXY(30, 5, 'View     : ' + IntToStr(ThePlayer.intView));
        TransTextXY(30, 6, 'Chant    : ' + IntToStr(ThePlayer.intChant));
        TransTextXY(30, 7, 'Move     : ' + IntToStr(ThePlayer.intMove));
        TransTextXY(30, 8, 'Steal    : ' + IntToStr(ThePlayer.intBurgle));

        // skills
        TransTextXY(30,10, 'Sword    : ' + IntToStr(ThePlayer.intSword));
        TransTextXY(30,11, 'Axe      : ' + IntToStr(ThePlayer.intAxe));
        TransTextXY(30,12, 'Lance    : ' + IntToStr(ThePlayer.intWhip));
        TransTextXY(30,13, 'Fire-arm : ' + IntToStr(ThePlayer.intGun));
        TransTextXY(30,14, 'Tool     : ' + IntToStr(ThePlayer.intTool));

        // talents
        TransTextXY(53, 4, 'Humility : ' + IntToStr(ThePlayer.intHumility));
        TransTextXY(53, 5, 'Trade    : ' + IntToStr(ThePlayer.intTrade));

        TransTextXY(53, 7, 'Melee        : '+IntToStr(CollectTP));
        TransTextXY(53, 8, 'Long-range   : '+IntToStr(CollectGunTP));
        TransTextXY(53, 9, 'Defense      : '+IntToStr(CollectDP));

        TransTextXY(53, 11, 'Skill Points : '+IntToStr(ThePlayer.longSkillPoints));

        TransTextXY(1, 17, ThePlayer.strName + ' is active since ' + IntToStr(longTotalTurns) + ' turns and has achieved a score of '+IntToStr(ThePlayer.longScore)+'.');
        TransTextXY(1, 18, ThePlayer.strName + ' has prayed to '+ThePlayer.strReli+' '+IntToStr(ThePlayer.longPrayers)+' time(s).');


        dummy:=GetKeyInput('[q]uests  [d]iplomas  [m]essages  [e]quipment summary    [ESC] Resume game', false);
    until (dummy='q') or (dummy='m') or (dummy='e') or (dummy='d') or (dummy='ESC');

    if dummy<>'ESC' then
    begin
        ch:=dummy[1];

        case ch of
            'q':
                ShowStatusQuestlog;
            'm':
                ShowStatusMessagelog;
            'e':
                EquipmentInfo;
            'd':
                DiplomaInfo;
        end;
    end
end;


// clears all values for new players
procedure ClearPlayer;
begin
    DungeonLevel:=1;
    ThePlayer.intLvl := 1;

    ThePlayer.blDead := false;

    ThePlayer.intMaxHP:=55;
    ThePlayer.intMaxPP:=10;
    ThePlayer.intStrength := 100;

    ThePlayer.intTotalVitari := 0;
    ThePlayer.intNextVitari := 0;
    ThePlayer.intNeedVitari := 0;

    ThePlayer.intFight:=3+trunc(random(4));
    ThePlayer.intMove:=1+trunc(random(3));;
    ThePlayer.intChant:=1+trunc(random(3));;
    ThePlayer.intBurgle:=1+trunc(random(3));;
    ThePlayer.intView:=1+trunc(random(4));;
    ThePlayer.intHumility:=1+trunc(random(3));;

    ThePlayer.intSword:=3+trunc(random(3));;
    ThePlayer.intAxe:=3+trunc(random(3));;
    ThePlayer.intWhip:=3+trunc(random(3));;
    ThePlayer.intGun:=3+trunc(random(3));;
    ThePlayer.intTool:=1+trunc(random(3));;
    ThePlayer.intTrade:=1+trunc(random(3));;
    
    ThePlayer.longFood := 1500;
    ThePlayer.longGold := 150 + trunc(random(150));

    Storage.longWood := 0;
    Storage.longMetal := 0;
    Storage.longStone := 0;
    Storage.longLeather := 0;
    Storage.longPlastics := 0;
    Storage.longPaper := 0;

    ThePlayer.longContCleared := 0;
    ThePlayer.longScore := 0;

    // empty inventory and spellbook
    for i:=1 to 16 do
    begin
        inventory[i].intType:=0;
        inventory[i].longNumber:=0;
        spellbook[i].intType:=0;
        spellbook[i].blKnown:=false;
        spellbook[i].intKnown:=0;
        spellbook[i].intRefresh:=0;
    end;

    // clear diplomas and hive status
    for i:=1 to 30 do
    begin
        ThePlayer.intDipl[i]:=0;
        intNoHivesAnymore[i]:=0;
    end;
end;


// final player inits
procedure FinalizePlayer (blGiveWeapon: boolean);
    var
        i: Integer;
begin
    ThePlayer.intHP := ThePlayer.intMaxHP;
    ThePlayer.intPP := ThePlayer.intMaxPP;

    // set effects to zero
    ThePlayer.intPoison := 0;
    ThePlayer.intConfusion := 0;
    ThePlayer.intBlind:= 0;
    ThePlayer.intSleep := 0;
    ThePlayer.intWall := 0;
    ThePlayer.intPara:= 0;
    ThePlayer.intCalm := 0;
    ThePlayer.intFreeze := 0;
    ThePlayer.intInvis := 0;
    ThePlayer.blCursed:=false;
    ThePlayer.blBlessed:=false;
    ThePlayer.blEvil:=false;
    ThePlayer.intLastSong := 0;

    for i:=1 to 100 do
        ThePlayer.intTempResist[i]:=0;

    for i:=1 to 200 do
        ThePlayer.blUnKilled[i]:=false;

    // no quests at startup
    for i:=1 to WinLevel do
        for j:=1 to 9 do
            ThePlayer.intQuestState[i,j]:=0;

    // no story files
    for i:=1 to 80 do
        ThePlayer.blStory[i]:=false;

    // all visits to 0
    for i:=1 to WinLevel do
        ThePlayer.longLevelVisits[i]:=0;

    // initialize time and day
    intDay:=trunc(random(16))+1;
    intDayTime:=trunc(random(240)+1);
    longYear:=1715;

    longTotalTurns:=0;
    ThePlayer.longLifeIns:=0;

    ThePlayer.intLimit := 0;

    // empty messagelog
    SetLength(ThePlayer.strMessageLog, 0);
    SetLength(ThePlayer.strMessageLog, 1000);
    ThePlayer.intNumberOfLogs := 0;

    ShowTransMessage('Log started.', false); //Hack. All log handling starts at index 1.

    // give start weapon (depending on highest weapon skill)
    if blGiveWeapon=true then
    begin
    ThePlayer.intWeapon:=-1;

        // - sword
        if (ThePlayer.intSword>ThePlayer.intAxe) and (ThePlayer.intSword>ThePlayer.intWhip) and (ThePlayer.intSword>ThePlayer.intGun) then
        begin
            Inventory[3].intType:=ReturnItemByName('Dagger');
            Inventory[3].longNumber:=1;
            ThePlayer.intWeapon:=ReturnItemByName('Dagger');
        end;

        // - axe
        if (ThePlayer.intAxe>ThePlayer.intSword) and (ThePlayer.intAxe>ThePlayer.intWhip) and (ThePlayer.intAxe>ThePlayer.intGun) then
        begin
            Inventory[3].intType:=ReturnItemByName('Axe');
            Inventory[3].longNumber:=1;
            ThePlayer.intWeapon:=ReturnItemByName('Axe');
        end;

        // - lance
        if (ThePlayer.intWhip>ThePlayer.intAxe) and (ThePlayer.intWhip>ThePlayer.intSword) and (ThePlayer.intWhip>ThePlayer.intGun) then
        begin
            Inventory[3].intType:=ReturnItemByName('Lance');
            Inventory[3].longNumber:=1;
            ThePlayer.intWeapon:=ReturnItemByName('Lance');
        end;

        // - bow
        if (ThePlayer.intGun>ThePlayer.intSword) and (ThePlayer.intGun>ThePlayer.intWhip) and (ThePlayer.intGun>ThePlayer.intAxe) then
        begin
            Inventory[3].intType:=ReturnItemByName('Bow');
            Inventory[3].longNumber:=1;
            ThePlayer.intWeapon:=ReturnItemByName('Bow');
            Inventory[4].intType:=ReturnItemByName('Package of Arrows');
            Inventory[4].longNumber:=200;
        end;

        // no weapon set?
        if ThePlayer.intWeapon=-1 then
        begin
            Inventory[3].intType:=ReturnItemByName('Dagger');
            Inventory[3].longNumber:=1;
            ThePlayer.intWeapon:=ReturnItemByName('Dagger');
        end;

    end;


    intAchieved:=0;
    StoreAchievement('Begins a new journey.');

    //inventory[16].intType:=ReturnItemByName('Crystal of "Revenge"');
    //inventory[16].longNumber:=1;
end;


// create: Difficulty
procedure CreateDifficulty;
    var
        dummy: String;
begin
    // Difficulty
    ClearScreenSDL;

    repeat
        if UseSDL=true then
            BlitImage_Title
        else
        begin
            DialogWin;
            StatusDeco;
        end;

        TransTextXY(1,1, 'STARTING A NEW JOURNEY');
        TransTextXY(8, 7, 'Which difficulty level do you desire?');
        TransTextXY(8, 9, 'Key   Level');
        TransTextXY(8,11, ' 1    Bronze       (easier)');
        TransTextXY(8,12, ' 2    Silver       (standard)');
        TransTextXY(8,13, ' 3    Gold         (harder)');

        dummy:=GetKeyInput('Press the according key to select a difficulty level.', false);
    until (dummy='1') or (dummy='2') or (dummy='3');

    DiffLevel:=StrToInt(dummy);
end;


procedure CreateBiography (blShowBio: boolean);
    var
        i, nn, intVitaBonus: Integer;
        strVitaRelationship, strVitaFather, strVitaMother1, strVitaMother2, strVitaEyes, strVitaHair1, strVitaHair2: string;
begin
    // 0. biography
    strVitaRelationship:='';
    i := trunc(random(10));
    case i of
        0:
            strVitaRelationship:='loved';
        1:
            strVitaRelationship:='grown';
        2:
            strVitaRelationship:='hated';
        3:
            strVitaRelationship:='pampered';
        4:
            strVitaRelationship:='watched carefully';
        5:
            strVitaRelationship:='denied';
        6:
            strVitaRelationship:='admired';
        7:
            strVitaRelationship:='seen sceptically';
        8:
            strVitaRelationship:='forced to work';
        9:
            strVitaRelationship:='abandoned';
    end;

    // if child was beloved, pampered or admired, then double bonus
    intVitaBonus:=0;
    if (i=0) or (i=6) then
        intVitaBonus:=3;
    if (i=2) or (i=5) or (i=9) then
        intVitaBonus:=-2;

    nn:=i;

    strVitaFather:='';
    i := 1+trunc(random(4));
    case i of
        //0:
        //begin
            //strVitaFather:='a constructor';
            //inc(ThePlayer.intTool,intVitaBonus); // tool
        //end;
        1:
        begin
            strVitaFather:='an enchanter';
            inc(ThePlayer.intChant,intVitaBonus); // chant
        end;
        2:
        begin
            strVitaFather:='a thief';
            inc(ThePlayer.intBurgle,intVitaBonus); // burgle
        end;
        3:
        begin
            strVitaFather:='an archer';
            inc(ThePlayer.intGun,intVitaBonus); // fire-arm
        end;
        4:
        begin
            strVitaFather:='a soldier';
            inc(ThePlayer.intFight,intVitaBonus); // fight
        end;
    end;

    strVitaMother1:='';
    i := trunc(random(11));
    case i of
        0:
            strVitaMother1:='beautiful';
        1:
            strVitaMother1:='young';
        2:
            strVitaMother1:='old';
        3:
            strVitaMother1:='lovely';
        4:
            strVitaMother1:='sad';
        5:
            strVitaMother1:='small';
        6:
            strVitaMother1:='impressive';
        7:
            strVitaMother1:='creative';
        8:
            strVitaMother1:='loving';
        9:
            strVitaMother1:='caring';
        10:
            strVitaMother1:='depressive';
    end;

    strVitaMother2:='';
    i := trunc(random(5));
    case i of
        0:
            strVitaMother2:='wife';
        1:
            strVitaMother2:='friend';
        2:
            strVitaMother2:='lover';
        3:
            strVitaMother2:='concubine';
        4:
            strVitaMother2:='cousin';
    end;

    strVitaEyes:='';
    i := trunc(random(7));
    case i of
        0:
            strVitaEyes:='blue';
        1:
            strVitaEyes:='blue-gray';
        2:
            strVitaEyes:='gray';
        3:
            strVitaEyes:='black';
        4:
            strVitaEyes:='brown';
        5:
            strVitaEyes:='green';
        6:
            strVitaEyes:='silver';
    end;

    strVitaHair1:='';
    i := trunc(random(4));
    case i of
        0:
            strVitaHair1:='short';
        1:
            strVitaHair1:='long';
        2:
            strVitaHair1:='thin';
        3:
            strVitaHair1:='dense';
    end;

    strVitaHair2:='';
    i := trunc(random(6));
    case i of
        0:
            strVitaHair2:='brown';
        1:
            strVitaHair2:='blonde';
        2:
            strVitaHair2:='black';
        3:
            strVitaHair2:='red';
        4:
            strVitaHair2:='white';
        5:
            strVitaHair2:='light brown';
    end;

    ThePlayer.strVita:=ThePlayer.strName+' was the child of '+strVitaFather+' and his '+strVitaMother1+' '+strVitaMother2+'.';
    ThePlayer.strDescription:=ThePlayer.strName+chr(39)+'s eyes were '+strVitaEyes+' and the '+strVitaHair1+' hair was '+strVitaHair2+'.';


    if blShowBio=true then
    begin
        ClearScreenSDL;

        if UseSDL=true then
            BlitImage_Title
        else
        begin
            DialogWin;
            StatusDeco;
        end;

        TransTextXY(1,1, 'BIOGRAPHY OF '+uppercase(ThePlayer.strName));
        TransTextXY(8,9, ThePlayer.strName+', your father was '+strVitaFather+'.');
        TransTextXY(8,10,'With his '+strVitaMother1+' '+strVitaMother2+', he gave you life.');
        TransTextXY(8,11,'You were '+strVitaRelationship+' by your parents.');

        if (nn=0) or (nn=6) then
        begin
            TransTextXY(8, 12, 'Watching your father at work gave you some deeper');
            TransTextXY(8, 13, 'knowledge about being '+strVitaFather+'.');
        end;

        if (nn=2) or (nn=5) or (nn=9) then
        begin
            TransTextXY(8, 12, 'Because of the bad relationship to your father in');
            TransTextXY(8, 13, 'your youth, you decided to refuse all his knowledge.');
        end;

        if (nn=1) or (nn=3) or (nn=4) or (nn=7) or (nn=8) then
        begin
            TransTextXY(8, 12, 'Although you loved your father, his profession as');
            TransTextXY(8, 13, strVitaFather+' made no strong impression to you.');
        end;

        TransTextXY(8,15, 'Your eyes are '+strVitaEyes+' and your '+strVitaHair1+' hair is '+strVitaHair2+'.');

        GetKeyInput('[Press any key to continue]', false);
    end;
end;

// quick-create player
procedure CreatePlayerFast;
    var
        i: Integer;
        dummy: string;
begin

    ClearPlayer;

    // CreateDifficulty;
    DiffLevel := 2;
    CreateBiography(false);

    // selection
    ClearScreenSDL;

    DialogWin;
    StatusDeco;

    if UseSDL=true then
    begin
        LoadImage_Title(PathData+'graphics/invbg.jpg');
    end;

    dummy:='-';
    repeat

        if UseSDL=true then
            BlitImage_Title
        else
        begin
            DialogWin;
            BottomBar;
        end;

        TransTextXY(1,1, 'SELECT A PREDEFINED EDUCATION');
        TransTextXY(8, 7, 'How do you want to start?');
        TransTextXY(8, 9, 'Key   Start as');
        TransTextXY(8,11, ' 1    male soldier, believer in Ares           (easy)');
        TransTextXY(8,12, ' 2    female archer, believer in Aphrodite     (moderate)');
        TransTextXY(8,13, ' 3    male enchanter, believer in Hermes       (very easy)');
        TransTextXY(8,14, ' 4    female enchanter, believer in Apoll      (challenging)');
        TransTextXY(8,16, 'Note that the beginning of the game is much easier with one of');
        TransTextXY(8,17, 'these presets than it would be by starting from scratch, be-');
        TransTextXY(8,18, 'cause you will start with better equipment and higher skills.');

        dummy:=GetKeyInput('Press the according key to select a preset.', false);
    until (dummy='1') or (dummy='2') or (dummy='3') or (dummy='4');

    i:=StrToInt(dummy);

    // male soldier
    if i=1 then
    begin
        ThePlayer.intReli:=5;
        ThePlayer.strReli:='Ares';

        ThePlayer.intSex:=1;
        inc(ThePlayer.intMaxHP, 3+trunc(random(4)));

        ThePlayer.intProf := 5;
        inc(ThePlayer.intMove, 3);

        ThePlayer.intFight := 7;
        ThePlayer.intSword := 8;
        ThePlayer.intWhip := 5;

        inventory[1].intType:=ReturnItemByName('Sword');
        inventory[1].longNumber:=1;
        ThePlayer.intWeapon:=ReturnItemByName('Sword');

        inventory[2].intType:=ReturnItemByName('Aspirin');
        inventory[2].longNumber:=20;
        strQuickKey[1]:='Aspirin';

        inventory[3].intType:=ReturnItemByName('Meat');
        inventory[3].longNumber:=1;

        inventory[4].intType:=ReturnItemByName('Leather Suit');
        inventory[4].longNumber:=1;
        ThePlayer.intArmour:=ReturnItemByName('Leather Suit');

        inventory[5].intType:=ReturnItemByName('Wooden Shield');
        inventory[5].longNumber:=1;
        ThePlayer.intExtra:=ReturnItemByName('Wooden Shield');

        inventory[6].intType:=ReturnItemByName('Walking Shoes');
        inventory[6].longNumber:=1;
        ThePlayer.intFeet:=ReturnItemByName('Walking Shoes');
    end;

    // female archer
    if i=2 then
    begin
        ThePlayer.intReli:=1;
        ThePlayer.strReli:='Aphrodite';
        inc(ThePlayer.intMove,2);

        ThePlayer.intSex:=2;
        inc(ThePlayer.intMove);

        ThePlayer.intProf := 4;
        inc(ThePlayer.intView, 4);
        inc(ThePlayer.longGold, 50);

        ThePlayer.intGun := 11;
        inventory[1].intType:=ReturnItemByName('Longbow');
        inventory[1].longNumber:=1;
        ThePlayer.intWeapon:=ReturnItemByName('Longbow');

        inventory[2].intType:=ReturnItemByName('Package of Long Arrows');
        inventory[2].longNumber:=300;
        inventory[3].intType:=ReturnItemByName('Aspirin');
        inventory[3].longNumber:=20;
        strQuickKey[1]:='Aspirin';
        inventory[4].intType:=ReturnItemByName('Meat');
        inventory[4].longNumber:=1;
        inventory[5].intType:=ReturnItemByName('Leather Suit');
        inventory[5].longNumber:=1;
        ThePlayer.intArmour:=ReturnItemByName('Leather Suit');

        inventory[6].intType:=ReturnItemByName('Walking Shoes');
        inventory[6].longNumber:=1;
        ThePlayer.intFeet:=ReturnItemByName('Walking Shoes');
    end;

    // male enchanter
    if i=3 then
    begin
        ThePlayer.intReli:=2;
        ThePlayer.strReli:='Hermes';
        inc(ThePlayer.intWhip,2);

        ThePlayer.intSex:=1;
        inc(ThePlayer.intMaxHP, 3+trunc(random(4)));

        ThePlayer.intProf := 2;
        inc(ThePlayer.intMaxPP, 30);
        inc(ThePlayer.intView, 4);

        ThePlayer.intFight := 7;
        ThePlayer.intChant := 10;
        ThePlayer.intSword := 8;

        inventory[1].intType:=ReturnItemByName('Sword');
        inventory[1].longNumber:=1;
        ThePlayer.intWeapon:=ReturnItemByName('Sword');

        inventory[2].intType:=ReturnItemByName('Cola');
        inventory[2].longNumber:=10;
        Thing[ReturnItemByName('Cola')].blIdentified := true;
        Thing[ReturnItemByName('Cola')].strName := Thing[ReturnItemByName('Cola')].strRealName;
        strQuickKey[12]:='Cola';

        spellbook[1].intType:=ReturnSpellByName('Flame');
        spellbook[1].intKnown:=1;
        spellbook[1].intRefresh:=spell[spellbook[1].intType].intRefresh;
        strQuickKey[1]:='Flame';

        spellbook[2].intType:=ReturnSpellByName('Cold');
        spellbook[2].intKnown:=1;
        spellbook[2].intRefresh:=spell[spellbook[2].intType].intRefresh;
        strQuickKey[2]:='Cold';

        inventory[3].intType:=ReturnItemByName('Meat');
        inventory[3].longNumber:=1;

        inventory[4].intType:=ReturnItemByName('Leather Suit');
        inventory[4].longNumber:=1;
        ThePlayer.intArmour:=ReturnItemByName('Leather Suit');

        inventory[5].intType:=ReturnItemByName('Walking Shoes');
        inventory[5].longNumber:=1;
        ThePlayer.intFeet:=ReturnItemByName('Walking Shoes');
    end;

    // female enchanter
    if i=4 then
    begin
        ThePlayer.intReli:=3;
        ThePlayer.strReli:='Apoll';
        inc(ThePlayer.intBurgle,2);

        ThePlayer.intSex:=2;
        inc(ThePlayer.intMove);

        ThePlayer.intProf := 2;
        inc(ThePlayer.intMaxPP, 30);
        inc(ThePlayer.intView, 4);

        ThePlayer.intChant:=8;
        ThePlayer.intHumility:=10;
        ThePlayer.intSword:=6;

        inventory[1].intType:=ReturnItemByName('Rusty Dagger');
        inventory[1].longNumber:=1;
        ThePlayer.intWeapon:=ReturnItemByName('Rusty Dagger');

        inventory[2].intType:=ReturnItemByName('Cola');
        inventory[2].longNumber:=10;
        Thing[ReturnItemByName('Cola')].blIdentified := true;
        Thing[ReturnItemByName('Cola')].strName := Thing[ReturnItemByName('Cola')].strRealName;
        strQuickKey[12]:='Cola';

        spellbook[1].intType:=ReturnSpellByName('Heal');
        spellbook[1].intKnown:=1;
        spellbook[1].intRefresh:=spell[spellbook[1].intType].intRefresh;
        strQuickKey[1]:='Heal';

        spellbook[2].intType:=ReturnSpellByName('Water');
        spellbook[2].intKnown:=1;
        spellbook[2].intRefresh:=spell[spellbook[2].intType].intRefresh;
        strQuickKey[2]:='Water';

        inventory[3].intType:=ReturnItemByName('Cape');
        inventory[3].longNumber:=1;
        ThePlayer.intArmour:=ReturnItemByName('Cape');

        inventory[4].intType:=ReturnItemByName('Walking Shoes');
        inventory[4].longNumber:=1;
        ThePlayer.intFeet:=ReturnItemByName('Walking Shoes');
    end;

    FinalizePlayer (false);
end;


// create new player from scratch
procedure CreatePlayer;
    var
        dummy: string;
        menu, RandomItem: integer;
begin

    ClearScreenSDL;

    if UseSDL=true then
    begin
        LoadImage_Title(PathData+'graphics/invbg.jpg');
    end;

    repeat
        if UseSDL=true then
            BlitImage_Title
        else
        begin
            DialogWin;
            StatusDeco;
        end;

        TransTextXY(1,1,  'CHARACTER GENERATION');
        TransTextXY(8,7,  'You are about to create an individual character. This allows you');
        TransTextXY(8,8,  'to determine the sex, profession and religion of your character.');
        TransTextXY(8,9,  'You will also select the difficulty of the game, and you have to');
        TransTextXY(8,10, 'distribute initial skill points.');
        TransTextXY(8,12, 'Beware that you are totally free in combining these factors, but');
        TransTextXY(8,13, 'not every combination will lead to an easy or balanced game.');
        TransTextXY(8,15, 'However, this can be seen as an additional challenge for more');
        TransTextXY(8,16, 'experienced players, so you may continue.');
        TransTextXY(8,18, 'Do you want to proceed?');

        dummy:=GetKeyInput('Press [y] to continue or [n] to select a quickstart preset.', false);
    until (dummy='y') or (dummy='Y') or (dummy='n') or (dummy='N');

    if (dummy='n') or (dummy='N') then
        CreatePlayerFast
    else
    begin
        ClearPlayer;

        repeat
            RandomItem := ReturnRandomItem;
        until (thing[RandomItem].intCharLvl=1) and (thing[RandomItem].blWield=false) and (NPos('Credits', thing[RandomItem].strName, 1)=0) and (NPos('Package', thing[RandomItem].strName, 1)=0);
        inventory[1].intType:=RandomItem;
        inventory[1].longNumber:=1;

        inventory[2].intType:=ReturnItemByName('Aspirin');
        inventory[2].longNumber:=20;
        strQuickKey[1]:='Aspirin';

        CreateDifficulty;
        CreateBiography(true);


        // 1. Sex; determines initial HP
        ClearScreenSDL;

        repeat

            if UseSDL=true then
                BlitImage_Title
            else
            begin
                DialogWin;
                StatusDeco;
            end;

            TransTextXY(1,1, 'CHARACTER GENERATION');
            TransTextXY(8, 7, 'Tell me, ' + ThePlayer.strName + ', are you male or female?');
            TransTextXY(8, 9, 'Key   Sex                    Bonus/Malus');
            TransTextXY(8,11, ' 1    Male                   HP +(3 to 6)');
            TransTextXY(8,12, ' 2    Female                 Move +1');

            dummy:=GetKeyInput('Press the according key to determine your sex.', false);
        until (dummy='1') or (dummy='2');

        menu:=StrToInt(dummy);
        case menu of
            1:
                inc(ThePlayer.intMaxHP, 3+trunc(random(4)));
            2:
                inc(ThePlayer.intMove);
        end;

        ThePlayer.intSex:=menu;


        // 2. Profession; determines initial skills
        ClearScreenSDL;

        repeat

            if UseSDL=true then
                BlitImage_Title
            else
            begin
                DialogWin;
                StatusDeco;
            end;

            TransTextXY(1,1, 'CHARACTER GENERATION');
            TransTextXY(8, 7, 'What is your profession?');

            TransTextXY(8, 9, 'Key   Profession     Bonus/Malus');
    //        TextXY(8,11, ' 1    Constructor    Tool +6 / clears contamination');
            TransTextXY(8,11, ' 1    Enchanter      PP +30 / View +3 / Chant +6');  // internal: 2
            TransTextXY(8,12, ' 2    Thief          Steal +6');                     // internal: 3
            TransTextXY(8,13, ' 3    Archer         Fire-arm +6 / View +3');        // internal: 4
            TransTextXY(8,14, ' 4    Soldier        Fight +5 / Move +3');           // internal: 5

            dummy:=GetKeyInput('Press the according key to select your profession.', false);

        until (dummy='1') or (dummy='2') or (dummy='3') or (dummy='4'); // or (dummy='5');


        menu:=StrToInt(dummy);
        case menu of
            //1:
            //begin
                //inc(ThePlayer.intTool, 6);
                //Inventory[3].intType:=ReturnItemByName('Dagger');
                //Inventory[3].longNumber:=1;
                //ThePlayer.intWeapon:=ReturnItemByName('Dagger');
                //Inventory[4].intType:=ReturnItemByName('Spade');
                //Inventory[4].longNumber:=1;
            //end;
            1:
            begin
                inc(ThePlayer.intChant, 6);
                inc(ThePlayer.intMaxPP, 30);
                inc(ThePlayer.intView, 3);
                Inventory[5].intType:=ReturnItemByName('Cola');
                Inventory[5].longNumber:=6;
                Thing[ReturnItemByName('Cola')].blIdentified := true;
                Thing[ReturnItemByName('Cola')].strName := Thing[ReturnItemByName('Cola')].strRealName;

                spellbook[1].intType:=ReturnSpellByName('Heal');
                spellbook[1].intKnown:=1;
                spellbook[1].intRefresh:=spell[spellbook[1].intType].intRefresh;
                strQuickKey[1]:='Heal';

                spellbook[2].intType:=ReturnSpellByName('Water');
                spellbook[2].intKnown:=1;
                spellbook[2].intRefresh:=spell[spellbook[2].intType].intRefresh;
                strQuickKey[7]:='Water';

                strQuickKey[2]:='Cola';
            end;
            2:
            begin
                inc(ThePlayer.intBurgle, 6);
                Inventory[5].intType:=ReturnItemByName('Dungeon Key');
                Inventory[5].longNumber:=4;
            end;
            3:
            begin
                inc(ThePlayer.intGun, 6);
                inc(ThePlayer.intView, 3);
                inc(ThePlayer.longGold, 100);
            end;
            4:
            begin
                inc(ThePlayer.intMove, 3);
                inc(ThePlayer.intFight, 5);
                Inventory[5].intType:=ReturnItemByName('Meat');
                Inventory[5].longNumber:=1;
            end;
        end;

        ThePlayer.intProf:=menu+1;


        // 3. Religion
        ClearScreenSDL;

        repeat

            if UseSDL=true then
                BlitImage_Title
            else
            begin
                DialogWin;
                StatusDeco;
            end;

            TransTextXY(1,1, 'CHARACTER GENERATION');
            TransTextXY(8, 7, 'Tell me, ' + ThePlayer.strName + ', in which god do you believe?');

            TransTextXY(8, 9, 'Key   Deity          Bonus/Malus');
            TransTextXY(8,11, ' 1    Aphrodite      Move +2');
            TransTextXY(8,12, ' 2    Hermes         Lance +2');
            TransTextXY(8,13, ' 3    Apoll          Steal +2');
            TransTextXY(8,14, ' 4    Dionysos       needs less food');
            TransTextXY(8,15, ' 5    Ares           Sword +3 / Axe +3');

            dummy:=GetKeyInput('Press the according key to select your belief.', false);
        until (dummy='1') or (dummy='2') or (dummy='3') or (dummy='4') or (dummy='5');



        menu:=StrToInt(dummy);
        case menu of
            1:
                inc(ThePlayer.intMove, 2);
            2:
                inc(ThePlayer.intWhip, 2);
            3:
                inc(ThePlayer.intBurgle, 2);
            4:
                inc(ThePlayer.longFood, 1500);
            5:
            begin
                inc(ThePlayer.intSword, 3);
                inc(ThePlayer.intAxe, 3);
            end;
        end;

        ThePlayer.strReli := PlayClass[menu].strName;
        ThePlayer.intReli:=StrToInt(dummy);


        // 4. Distribute skill points
        ClearScreenSDL;
        TextXY(2, 2, 'Finally, you have the possibility to train some of your skills.');
        case DiffLevel of
            1:
                TrainSkill(6);
            2:
                TrainSkill(5);
            3:
                TrainSkill(4);
        end;

        FinalizePlayer (true);
    end;
end;

// teleports an enemy
procedure Teleport(n: integer);
    var
        CurX, CurY, rx, ry, i, Distance: Integer;
begin
    CurX:=Monster[n].intX;
    CurY:=Monster[n].intY;
    Distance:=Monster[n].intTeleport;
    i:=0;
    repeat
        inc(i);
        rx := trunc(random(DngMaxWidth-12)) + 1;
        ry := trunc(random(DngMaxHeight-12)) + 1;
    until (i>1000) or ((DngLvl[rx,ry].intIntegrity = 0) and (abs(rx-CurX)<Distance) and (abs(ry-CurY)<Distance));
    Monster[n].intX := rx;
    Monster[n].intY := ry;
end;


// splits an enemy
procedure SplitMonster(n: integer; summon: boolean);
    var
        FreeMonsterID, SumX, SumY, CurX, CurY: integer;
begin
    // if we forgot to name a monster to be summ. in data files, exit
    if (Monster[n].strSplitInto='') or (Monster[n].strSplitInto=' ') then
        exit;

    // if "summon" = false, then remove
    if summon=false then
    begin
        FreeMonsterID:=GetFirstFreeMonsterID;
        if FreeMonsterID>-1 then
        begin
            CreateMonster(FreeMonsterID, Monster[n].strSplitInto, 0);
            Monster[FreeMonsterID].intX:=Monster[n].intX;
            Monster[FreeMonsterID].intY:=Monster[n].intY;
        end;

        FreeMonsterID:=GetFirstFreeMonsterID;
        if DngLvl[Monster[n].intX+1,Monster[n].intY].intIntegrity=0 then
            if FreeMonsterID>-1 then
            begin
                CreateMonster(FreeMonsterID, Monster[n].strSplitInto, 0);
                Monster[FreeMonsterID].intX:=Monster[n].intX+1;
                Monster[FreeMonsterID].intY:=Monster[n].intY;
            end;

        FreeMonsterID:=GetFirstFreeMonsterID;
        if DngLvl[Monster[n].intX-1,Monster[n].intY].intIntegrity=0 then
            if FreeMonsterID>-1 then
            begin
                CreateMonster(FreeMonsterID, Monster[n].strSplitInto, 0);
                Monster[FreeMonsterID].intX:=Monster[n].intX-1;
                Monster[FreeMonsterID].intY:=Monster[n].intY;
            end;

        FreeMonsterID:=GetFirstFreeMonsterID;
        if DngLvl[Monster[n].intX+1,Monster[n].intY-1].intIntegrity=0 then
            if FreeMonsterID>-1 then
            begin
                CreateMonster(FreeMonsterID, Monster[n].strSplitInto, 0);
                Monster[FreeMonsterID].intX:=Monster[n].intX+1;
                Monster[FreeMonsterID].intY:=Monster[n].intY-1;
            end;

        FreeMonsterID:=GetFirstFreeMonsterID;
        if DngLvl[Monster[n].intX+1,Monster[n].intY+1].intIntegrity=0 then
            if FreeMonsterID>-1 then
            begin
                CreateMonster(FreeMonsterID, Monster[n].strSplitInto, 0);
                Monster[FreeMonsterID].intX:=Monster[n].intX+1;
                Monster[FreeMonsterID].intY:=Monster[n].intY+1;
            end;

        FreeMonsterID:=GetFirstFreeMonsterID;
        if DngLvl[Monster[n].intX-1,Monster[n].intY-1].intIntegrity=0 then
            if FreeMonsterID>-1 then
            begin
                CreateMonster(FreeMonsterID, Monster[n].strSplitInto, 0);
                Monster[FreeMonsterID].intX:=Monster[n].intX-1;
                Monster[FreeMonsterID].intY:=Monster[n].intY-1;
            end;

        FreeMonsterID:=GetFirstFreeMonsterID;
        if DngLvl[Monster[n].intX-1,Monster[n].intY-1].intIntegrity=0 then
            if FreeMonsterID>-1 then
            begin
                CreateMonster(FreeMonsterID, Monster[n].strSplitInto, 0);
                Monster[FreeMonsterID].intX:=Monster[n].intX-1;
                Monster[FreeMonsterID].intY:=Monster[n].intY+1;
            end;

        FreeMonsterID:=GetFirstFreeMonsterID;
        if DngLvl[Monster[n].intX,Monster[n].intY+1].intIntegrity=0 then
            if FreeMonsterID>-1 then
            begin
                CreateMonster(FreeMonsterID, Monster[n].strSplitInto, 0);
                Monster[FreeMonsterID].intX:=Monster[n].intX;
                Monster[FreeMonsterID].intY:=Monster[n].intY+1;
            end;

        FreeMonsterID:=GetFirstFreeMonsterID;
        if DngLvl[Monster[n].intX,Monster[n].intY-1].intIntegrity=0 then
            if FreeMonsterID>-1 then
            begin
                CreateMonster(FreeMonsterID, Monster[n].strSplitInto, 0);
                Monster[FreeMonsterID].intX:=Monster[n].intX;
                Monster[FreeMonsterID].intY:=Monster[n].intY-1;
            end;

        // draw blood...
        CreateBlood(Monster[n].intX,Monster[n].intY);
        Monster[n].intX:=DngMaxWidth+10;
        Monster[n].intY:=DngMaxHeight+10;
    end
    else
    begin
        FreeMonsterID:=GetFirstFreeMonsterID;
        if FreeMonsterID>-1 then
        begin
            CurX:=Monster[n].intX;
            CurY:=Monster[n].intY;

            SumX:=CurX;
            SumY:=CurY;

            if CheckForMonster(CurX+1,CurY)=false then
            begin
                SumX:=CurX+1;
                SumY:=CurY;
            end;

            if CheckForMonster(CurX-1,CurY)=false then
            begin
                SumX:=CurX-1;
                SumY:=CurY;
            end;

            if CheckForMonster(CurX+1,CurY+1)=false then
            begin
                SumX:=CurX+1;
                SumY:=CurY+1;
            end;

            if CheckForMonster(CurX+1,CurY-1)=false then
            begin
                SumX:=CurX+1;
                SumY:=CurY-1;
            end;

            if CheckForMonster(CurX-1,CurY+1)=false then
            begin
                SumX:=CurX-1;
                SumY:=CurY+1;
            end;

            if CheckForMonster(CurX-1,CurY-1)=false then
            begin
                SumX:=CurX-1;
                SumY:=CurY-1;
            end;

            if CheckForMonster(CurX,CurY+1)=false then
            begin
                SumX:=CurX;
                SumY:=CurY+1;
            end;

            if CheckForMonster(CurX,CurY-1)=false then
            begin
                SumX:=CurX;
                SumY:=CurY-1;
            end;

            if ((SumX<>CurX) or (SumY<>CurY)) then
                if not((SumX=ThePlayer.intX) and (SumY=ThePlayer.intY)) then
                begin
                    CreateMonster(FreeMonsterID, Monster[n].strSplitInto, 0);
                    Monster[FreeMonsterID].intX:=SumX;
                    Monster[FreeMonsterID].intY:=SumY;
                end;
        end;
    end;
end;


// gods perform support attacks when player's limit bar is 100%
function DivineAttack(r: integer): integer;
var
    intTP: integer;
    strMsg: string;
begin
    intTP := 0;

    if ThePlayer.intHumility>0 then
    begin
        PlaySFX('swoosh.mp3');

        case ThePlayer.intReli of
            1:
            begin
                // Aphrodite's Passion
                strMsg:='Aphrodite'+chr(39)+'s Passion heals you and';
                ThePlayer.intHP:=ThePlayer.intMaxHP;
                ThePlayer.intPP:=ThePlayer.intMaxPP;
                intTP:=ThePlayer.intHumility;
            end;

            2:
            begin
                // Hermes fast attack
                strMsg:='Hermes attacks '+IntToStr(ThePlayer.intHumility)+' times and';
                intTP:=CollectTP*ThePlayer.intHumility;
            end;

            3:
            begin
                // Apoll's Goldrush
                strMsg:='Apoll'+chr(39)+'s Goldrush ';
                intTP:=ThePlayer.intBurgle * ThePlayer.intHumility;
                inc(ThePlayer.longGold, intTP);
            end;

            4:
            begin
                // Happy Meal
                strMsg:='Dionysos serves a happy meal and';
                inc(ThePlayer.longFood, 100*(ThePlayer.intHumility*ThePlayer.intHumility));
                intTP:=ThePlayer.intHumility;
            end;

            5:
            begin
                // Warhammer of Ares
                strMsg:='The Warhammer of Ares';
                intTP:=ThePlayer.intStrength;
            end;
        end;

        ThePlayer.intLimit := 0;

        ShowTransMessage(strMsg+' inflicts '+IntToStr(intTP)+' damage.', false);

    end
    else
        intTP:=0;

    DivineAttack := intTP;
end;


// process normal (melee) fights with monsters
function FightMonster(x: integer; y: integer): boolean;
    var
        i, s, k: integer;
        intTP, intDP, intMDP, intMTP, AttackType, SecAt: integer;
        strMsg1, strMsg2: string;
        blReallyAttack: boolean;
begin
    FightMonster := true;

    intTP:=CollectTP;
    intDP:=CollectDP;
    intMTP := 0;

    for i:=1 to 510 do
    begin

        // process every monster that is on the given x/y-position
        if (Monster[i].intX = x) and (Monster[i].intY = y) then
        begin

            blReallyAttack:=true;
            KeyRepeatIst:=KeyRepeatSoll;

            // if the attack shall really be processed, continue ...
            if blReallyAttack=true then
            begin
                Monster[i].blHuman := false;    // even men and peaceful monsters will now be angry
                Monster[i].blAttacked := true;  // set attacked flag
                ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
    
                // attack hits the enemy
                if (HitOrMiss(i)=false) or (random(400)>200) then
                begin
                    intMDP := Monster[i].intAP;
                    intTP := intTP - intMDP;

                    // if melee weapon has magic effect, process it
                    if ThePlayer.intWeapon>0 then
                    begin
                        if (random(500)>300) and (Thing[ThePlayer.intWeapon].intGP=0) then
                        begin
                            if MaximizedWeaponSkill=true then
                            begin
                                // weapon with fire effect
                                if thing[ThePlayer.intWeapon].intEffect=38 then
                                begin
                                    BlendMagic(15);
                                    if Monster[i].blFire = false then
                                        inc(intTP, thing[ThePlayer.intWeapon].intRange)
                                    else
                                        intTP:=0;
                                end;

                                // weapon with ice effect
                                if thing[ThePlayer.intWeapon].intEffect=39 then
                                begin
                                    BlendMagic(16);
                                    if Monster[i].blIce=false then
                                        inc(intTP, thing[ThePlayer.intWeapon].intRange)
                                    else
                                        intTP:=0;

                                    // freeze air
                                    DngLvl[Monster[i].intX,Monster[i].intY].intAirType:=2;
                                    DngLvl[Monster[i].intX,Monster[i].intY].intAirRange:=5+trunc(random(10));
                                end;

                                // weapon with water effect
                                if thing[ThePlayer.intWeapon].intEffect=40 then
                                begin
                                    BlendMagic(17);
                                    if (Monster[i].blWaterC = false) and (Monster[i].blWater = false) then
                                        inc(intTP, thing[ThePlayer.intWeapon].intRange)
                                    else
                                        intTP:=0;
                                end;
                            end;

                        end;
                    end;

                    if (intTP < 1) and ((ThePlayer.intLimit<100) or ((ThePlayer.intLimit=100) and (ThePlayer.intHumility<1))) then
                    begin
                        intTP := 0;
                        if Monster[i].intInvis>0 then
                            strMsg1:='You hit something, but do no significant damage.'
                        else
                            strMsg1:='You hit the ' + Monster[i].strName + ', but do no significant damage.';
                    end
                    else
                    begin // attack hits the enemy

                        //writeln('TP: '+IntToStr(intTP));

                        if Monster[i].intInvis>0 then
                            strMsg1:='You attack something'
                        else
                            strMsg1:='You attack the ' + Monster[i].strName;

                        // a really good hit?
                        if random(400)>CONST_CHANCE_FOR_GOOD_HIT then
                            if MaximizedWeaponSkill = true then
                            begin
                                intTP:=CollectTP;
                                if Monster[i].intInvis>0 then
                                    strMsg1:='You do critical damage'
                                else
                                    strMsg1:='The ' + Monster[i].strName + ' suffers critical damage';
                            end;

                        // divine special attack?
                        if (ThePlayer.intLimit=100) and (ThePlayer.intHumility>0) then
                            intTP:=DivineAttack(ThePlayer.intReli);

                    end;
                end
                else
                begin // attack does not hit the enemy
                    Monster[i].blAttacked := true;  // set attacked flag
                    ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
                    intTP := 0;
                    if Monster[i].intInvis>0 then
                        strMsg1:='You try to hit something, but you fail.'
                    else
                        strMsg1:='You try to hit the ' + Monster[i].strName + ', but you fail.';
                end;
    
                // if the player isn't invisible and if the player isn't frozen, process monster's counter-attack
                if (IsInvisible=false) and (ThePlayer.intFreeze=0) then
                begin
                    // randomly select monster's attack type
                    AttackType := trunc(random(500));
                    if AttackType > 400 then
                        AttackType:=1   // secondary action (special or wait)
                    else
                        AttackType:=0;  // primary action (attack)

                    // if monster's PP <= 0 then select standard attack
                    if Monster[i].intPP<=0 then
                        AttackType:=0;

                    // if the monster is in frozen air, select type 1 (which makes monster not to attack)
                    if DngLvl[Monster[i].intX,Monster[i].intY].intAirType=2 then
                        AttackType := 1;

//                     writeln (IntToStr(DngLvl[Monster[i].intX,Monster[i].intY].intAirType));
    
                    case AttackType of
                        0:
                        begin
                            // standard attack
                            if HitOrMiss(i)=true then
                            begin
                                intMTP := Monster[i].intWP - intDP;
                                if intMTP < 1 then
                                    intMTP := 0;
                                if intMTP > 0 then
                                begin
                                    if Monster[i].intInvis>0 then
                                        strMsg2:='Something ' + Monster[i].strVerb + ' [-' + IntToStr(intMTP) + '].'
                                    else
                                        strMsg2:='The ' + Monster[i].strName + ' ' + Monster[i].strVerb + ' [-' + IntToStr(intMTP) + '].';
                                end
                                else
                                begin
                                    if Monster[i].intInvis>0 then
                                        strMsg2:='Something ' + Monster[i].strVerb + ', but does no damage.'
                                    else
                                        strMsg2:='The ' + Monster[i].strName + ' ' + Monster[i].strVerb + ', but does no damage.';
                                end;

                                if (ThePlayer.intWall > 0) then
                                begin
                                    intMTP:=0;
                                    strMsg2:='Your barrier blocks the attack.';
                                end;

                                // if damage suffered increase divine range
                                if intMTP>0 then
                                begin
                                    if ThePlayer.intLimit<100 then
                                    begin
                                        // only if humility talent is greater than 0
                                        if ThePlayer.intHumility>0 then
                                        begin
                                            // basic increase depending on humility talent
                                            if ThePlayer.intHumility<=3 then
                                                inc(ThePlayer.intLimit);
                                            if (ThePlayer.intHumility>3) and (ThePlayer.intHumility<=6) then
                                                inc(ThePlayer.intLimit,2);
                                            if (ThePlayer.intHumility>6) and (ThePlayer.intHumility<=9) then
                                                inc(ThePlayer.intLimit,3);
                                            if (ThePlayer.intHumility>9) then
                                                inc(ThePlayer.intLimit,4);

                                            // items with influence on divine range generation
                                            inc(ThePlayer.intLimit, ReturnStatusIncRate(49));

                                            // ensure that humility is never more than 100%
                                            if ThePlayer.intLimit>100 then
                                                ThePlayer.intLimit:=100;
                                        end;
                                    end;
                                end;
                            end
                            else
                            begin
                                if Monster[i].intInvis>0 then
                                    strMsg2:='Something tries to hit, but misses.'
                                else
                                    strMsg2:='The ' + Monster[i].strName + ' misses.';
                                intMTP:=0;
                            end;
                        end;
        
                        1:
                        begin
                            // Select special attack
                            SecAt:=0;
                            s:=0;
                            repeat
                                if (Monster[i].blPoison=true) and (random(1000)>500) then
                                    SecAt:=1;
                                if (Monster[i].blFire=true) and (random(1000)>500) then
                                    SecAt:=2;
                                if (Monster[i].blIce=true) and (random(1000)>500) then
                                    SecAt:=3;
                                if (Monster[i].blConfusion=true) and (random(1000)>500) then
                                    SecAt:=4;
                                if (Monster[i].blBlindness=true) and (random(1000)>500) then
                                    SecAt:=5;
                                if (Monster[i].blInvis=true) and (random(1000)>500) then
                                    SecAt:=6;
                                if (Monster[i].blPara=true) and (random(1000)>500) then
                                    SecAt:=7;
                                if (Monster[i].blCalm=true) and (random(1000)>500) then
                                    SecAt:=8;
                                if (Monster[i].blCurse=true) and (random(1000)>500) then
                                    SecAt:=9;
                                if (Monster[i].blWaterC=true) and (random(1000)>500) then
                                    SecAt:=10;
                                if (Monster[i].blDestWeapon=true) and (random(1000)>500) and (ThePlayer.intWeapon>0) then
                                    SecAt:=11;
                                if (Monster[i].blDestArmour=true) and (random(1000)>500) and (ThePlayer.intArmour>0) then
                                    SecAt:=12;
                                if (Monster[i].blHitsHard=true) and (random(1000)>500) then
                                    SecAt:=13;
                                if (Monster[i].blTeleport=true) and (random(1000)>500) then
                                    SecAt:=14;
                                if (Monster[i].blSplit=true) and (random(1000)>500) then
                                    SecAt:=15;
                                if (Monster[i].blSummon=true) and (random(1000)>500) then
                                    SecAt:=16;
                                if (Monster[i].blSelfHeal=true) and (random(1000)>500) then
                                    SecAt:=17;
                                if (Monster[i].blDrainSTR=true) and (random(1000)>500) then
                                    SecAt:=18;
                                if (Monster[i].blDrainPP=true) and (random(1000)>500) then
                                    SecAt:=19;
                                inc(s);
                            until (s=1000) or (SecAt>0);

                            // is monster frozen?
                            if DngLvl[Monster[i].intX,Monster[i].intY].intAirType=2 then
                                SecAt:=99;
        
                            case SecAt of
                                0:
                                begin
                                    // for this monster, there's no special available, so just block attack
                                    if Monster[i].intInvis=0 then
                                        strMsg2:='The ' + Monster[i].strName + ' blocks your attack.';
                                    intTP:=0;
                                end;

                                1:
                                begin  // poison
                                    dec(Monster[i].intPP);
                                    strMsg2:='The ' + Monster[i].strName + ' poisons you.';
                                    intMTP:=0;
                                    if CheckEffect(10)=true then
                                    begin
                                        strMsg2:=strMsg2+' You resist the poison.';
                                    end
                                    else
                                    begin
                                        PlaySFX('magic-bad.mp3');
                                        BlendMagic(4);
                                        ThePlayer.intPoison:=Monster[i].intLvl+trunc(random(6));
                                        if ThePlayer.intPoison > ThePlayer.intHP-1 then
                                            ThePlayer.intPoison:=1;
                                    end;
                                end;

                                2:
                                begin  // fireball
                                    dec(Monster[i].intPP);
                                    strMsg2:='The '+Monster[i].strName+' chants a fire song';
                                    if CheckEffect(8)=true then
                                    begin
                                        strMsg2:=strMsg2+', but it does no harm.';
                                    end
                                    else
                                    begin
                                        strMsg2:=strMsg2+'. The flames hurt you.';
                                        PlaySFX('magic-combat.mp3');
                                        BlendMagic(1);
                                        ShowMonsterSpell(1);
                                        intMTP := 7 * (trunc(random(DungeonLevel))+1);
                                    end;
                                end;

                                3:
                                begin  // ice
                                    dec(Monster[i].intPP);
                                    strMsg2:='The '+Monster[i].strName+' chants an ice song';
                                    if CheckEffect(9)=true then
                                    begin
                                        strMsg2:=strMsg2+', but you resist.';
                                    end
                                    else
                                    begin
                                        strMsg2:=strMsg2+'. Your skin freezes and your attack is stopped.';
                                        PlaySFX('magic-combat.mp3');
                                        BlendMagic(2);
                                        ShowMonsterSpell(2);
                                        intMTP := 6 * (trunc(random(DungeonLevel))+1);
                                        intTP:=0;
                                    end;
                                end;

                                4:
                                begin  // confusion
                                    dec(Monster[i].intPP);
                                    strMsg2:='You are confused';
                                    intMTP:=0;
                                    if CheckEffect(11)=true then
                                    begin
                                        strMsg2:=strMsg2+', but finally you manage to concentrate.';
                                    end
                                    else
                                    begin
                                        PlaySFX('magic-bad.mp3');
                                        BlendMagic(10);
                                        strMsg2:=strMsg2+' and feel helpless.';
                                        ThePlayer.intConfusion:=Monster[i].intLvl+trunc(random(6));
                                    end;
                                end;

                                5:
                                begin  // blindness
                                    dec(Monster[i].intPP);
                                    strMsg2:='You are blind.';
                                    intMTP:=0;
                                    if CheckEffect(17)=true then
                                    begin
                                        strMsg2:=strMsg2+' You can see again.';
                                    end
                                    else
                                    begin
                                        PlaySFX('magic-bad.mp3');
                                        BlendMagic(11);
                                        strMsg2:=strMsg2+' Everything is so dark ...';
                                        ThePlayer.intBlind:=Monster[i].intLvl+trunc(random(6));
                                        ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
                                    end;
                                end;
                                6:
                                begin // monster gets invisible
                                    dec(Monster[i].intPP);
                                    PlaySFX('magic-time.mp3');
                                    strMsg2:='The '+Monster[i].strName+' becomes invisible!';
                                    Monster[i].intInvis:=DungeonLevel+trunc(random(3));
                                    intMTP:=0;
                                end;
                                7:
                                begin  // paralization
                                    dec(Monster[i].intPP);
                                    strMsg2:='The '+Monster[i].strName+' tries to paralyze you';
                                    intMTP:=0;
                                    if CheckEffect(26)=true then
                                    begin
                                        strMsg2:=strMsg2+', but you break the chains.';
                                    end
                                    else
                                    begin
                                        PlaySFX('magic-bad.mp3');
                                        BlendMagic(12);
                                        strMsg2:=strMsg2+'. You feel helpless.';
                                        ThePlayer.intPara:=Monster[i].intLvl+trunc(random(6));
                                    end;
                                end;

                                8:
                                begin  // calm (player now can't use spells)
                                    dec(Monster[i].intPP);
                                    strMsg2:='You can'+chr(39)+'t chant.';
                                    intMTP:=0;
                                    if (ThePlayer.blBlessed=true) or (CheckEffect(28)=true) then
                                    begin
                                        strMsg2:=strMsg2+' You resist the effects of Calm.';
                                    end
                                    else
                                    begin
                                        PlaySFX('magic-bad.mp3');
                                        strMsg2:=strMsg2+' You feel quiet.';
                                        ThePlayer.intCalm:=Monster[i].intLvl+trunc(random(6));
                                    end;
                                end;

                                9:
                                begin  // curse (the most terrible spell in this game ...)
                                    dec(Monster[i].intPP);
                                    if ThePlayer.blEvil=false then
                                    begin
                                        strMsg2:='An evil shadow penetrates your mind';
                                        intMTP:=0;
                                        if (CheckEffect(31)=true) or (ThePlayer.intHumility>=15) then
                                        begin
                                            strMsg2:=strMsg2+', but you resist it.';
                                        end
                                        else
                                        begin
                                            PlaySFX('magic-bad.mp3');
                                            BlendMagic(8);
                                            strMsg2:=strMsg2+'. You feel very sad.';
                                            ThePlayer.blCursed:=true;
                                            ThePlayer.blBlessed:=false;
                                        end;
                                    end;
                                end;

                                10:
                                begin  // water
                                    dec(Monster[i].intPP);
                                    strMsg2:='The '+Monster[i].strName+' summons the powers of water.';
                                    if CheckEffect(35)=true then
                                    begin
                                        strMsg2:=strMsg2+' You resist.';
                                    end
                                    else
                                    begin
                                        strMsg2:=strMsg2+' Hard waves hit you.';
                                        PlaySFX('magic-combat.mp3');
                                        BlendMagic(3);
                                        ShowMonsterSpell(3);
                                        intMTP := 5 * (trunc(random(DungeonLevel))+1);
                                    end;
                                end;

                                11:
                                begin   // lose weapon
                                    dec(Monster[i].intPP);
                                    strMsg2:='Your weapon vibrates';
                                    if ThePlayer.intWeapon>0 then
                                    begin
                                        PlaySFX('magic-bad.mp3');
                                        intMTP:=0;
                                        if Thing[ThePlayer.intWeapon].intEffect=36 then
                                            strMsg2:=strMsg2+', but nothing happens.'
                                        else
                                        begin
                                            strMsg2:=strMsg2+' and you lose it.';
                                            ThePlayer.intWeapon:=0;
                                        end;
                                    end;
                                end;

                                12:
                                begin  // lose armour
                                    dec(Monster[i].intPP);
                                    strMsg2:='Your armour vibrates';
                                    if ThePlayer.intArmour>0 then
                                    begin
                                        PlaySFX('magic-bad.mp3');
                                        intMTP:=0;
                                        if Thing[ThePlayer.intArmour].intEffect=36 then
                                            strMsg2:=strMsg2+', but nothing happens.'
                                        else
                                        begin
                                            strMsg2:=strMsg2+' and you lose it.';
                                            ThePlayer.intArmour:=0;
                                        end;
                                    end;
                                end;
                                13:
                                begin // hits hard
                                    strMsg2:='You suffer a mighty hit. [HP reduced to 1!]';
                                    intMTP:=ThePlayer.intHP-1;
                                end;
                                14:
                                begin // teleport
                                    dec(Monster[i].intPP);
                                    PlaySFX('magic-time.mp3');
                                    strMsg2:='The '+Monster[i].strName+' disappears.';
                                    Teleport(i);
                                end;
                                15:
                                begin // split
                                    dec(Monster[i].intPP);
                                    strMsg2:='The '+Monster[i].strName+' bears children.';
                                    SplitMonster(i, false);
                                end;
                                16:
                                begin // summon
                                    dec(Monster[i].intPP);
                                    PlaySFX('magic-time.mp3');
                                    strMsg2:='The '+Monster[i].strName+' summons a monster.';
                                    SplitMonster(i, true);
                                end;
                                17:
                                begin // heal (1/4 of max. HP)
                                    dec(Monster[i].intPP);
                                    strMsg2:='The '+Monster[i].strName+' glows slightly. ['+Monster[i].strName+chr(39)+'s HP increased by '+IntToStr(Monster[i].intMaxHP div 4)+']';
                                    inc(Monster[i].intHP, Monster[i].intMaxHP div 4);
                                    if Monster[i].intHP > Monster[i].intMaxHP then
                                        Monster[i].intHP := Monster[i].intMaxHP;
                                end;
                                18:
                                begin // decrease player strength
                                    dec(Monster[i].intPP);
                                    if CheckEffect(52)=false then
                                    begin
                                        PlaySFX('magic-bad.mp3');
                                        strMsg2:='You feel weaker.';
                                        intMTP := trunc(random(Monster[i].intDrainSTR)+1);
                                        dec(ThePlayer.intStrength, intMTP);
                                        if ThePlayer.intStrength < 0 then
                                            ThePlayer.intStrength := 0;
                                        inc(Monster[i].intHP, intMTP);
                                        if Monster[i].intHP > Monster[i].intMaxHP then
                                            Monster[i].intHP := Monster[i].intMaxHP;
                                        strMsg2 := strMsg2 + ' ['+Monster[i].strName+chr(39)+'s HP increased by '+IntToStr(intMTP)+']';
                                    end
                                    else
                                        strMsg2:='For a moment, you feel weak, but you pull yourself together.';
                                    intMTP:=0;
                                end;
                                19:
                                begin // drain player PP (and add it to monster PP)
                                    dec(Monster[i].intPP);
                                    if CheckEffect(51)=false then
                                    begin
                                        intMTP := trunc(random(Monster[i].intDrainPP)+1);
                                        strMsg2:='Your head seems to burst. ['+Monster[i].strName+chr(39)+' PP increased by '+IntToStr(intMTP)+']';
                                        inc(Monster[i].intPP, intMTP);
                                    end
                                    else
                                        strMsg2:='You feel dizzy for a second.';
                                end;

                                99:
                                begin // monster is frozen and can't do anything
                                    strMsg2:='The enemy is frozen.';
                                    intMTP:=0;
                                end;
                            end;
                            ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
                        end;
                    end;

                    // decrease player HP
                    //Writeln('Hit by '+ IntToStr(intMTP));

                    ShowDamage(intMTP);

                    ThePlayer.intHP := ThePlayer.intHP - intMTP;
                    if IsPlayerDead=true then // check for player's death
                    begin
                        if Monster[i].intInvis>0 then
                            GameOver('Killed by ... something')
                        else
                        begin
                            strMsg2:=lowercase(LeftStr(Monster[i].strName, 1));
                            if (strMsg2='a') or (strMsg2='e') or (strMsg2='u') or (strMsg2='o') or (strMsg2='i') then
                                GameOver('Killed by an '+Monster[i].strName)
                            else
                                GameOver('Killed by a '+Monster[i].strName);
                        end;
                    end;
    
                end
                else // player is invisble or time is frozen
                if IsInvisible=true then
                    strMsg2:='The enemy can' + chr(39) + 't see you.'
                else
                    strMsg2:='Time is frozen.';
    
                // decrease monster's HP
                Monster[i].intHP := Monster[i].intHP - intTP;
                if Monster[i].intHP < 0 then
                    Monster[i].intHP := 0;

                if Monster[i].intInvis=0 then
                    strMsg1:=strMsg1+' [HP/PP: ' + IntToStr(Monster[i].intHP) + '/'+IntToStr(Monster[i].intPP)+'].'
                else
                    strMsg1:=strMsg1+' [HP/PP: ?/?].';
    
                //TextXY(1, 26, '                                                                            ');
//                TransTextXY(1, 26, strMsg1);

                // ShowStatus;
                ShowTransMessage(strMsg1, false);
                ShowTransMessage(strMsg2, false);

                // currently don't really know why this is here. has something to do with the
                // config option to enter a formerly occupied tile directly after monster's death
                if AutoMoveToTile=true then
                    if Monster[i].intHP > 0 then
                        FightMonster := false
                    else
                        IsMonsterDead(i)
                else
                begin
                    FightMonster := false;
                    IsMonsterDead(i);
                end;

                // do effects
                EffectTicker;

            end;
        end;
    end;

end;


// show aura spells (these are spells which affect the air around the player)
procedure AuraSpell (n, range: integer);
    var
        i, j: integer;
begin
    case n of
        1:
            BlendMagic(1);        // fire
        2:
            BlendMagic(2);        // ice
        3:
            BlendMagic(3);        // water
        4:
            BlendMagic(14);        // purify
        5:
            BlendMagic(19);      // lichtbringer
        6:
            BlendMagic(20);      // spider web
    end;

    SetVisible;
    for i:=1 to DngMaxWidth do
        for j:=1 to DngMaxHeight do
            if (DngLvl[i,j].blLOS=true) and (DngLvl[i,j].intIntegrity=0) then
            begin
                // create "normal" aura
                if n<>5 then
                begin
                    // only on non-dark tiles
                    if DngLvl[i,j].intAirType <> 5 then
                    begin
                        DngLvl[i,j].intAirType := n;
                        DngLvl[i,j].intAirRange := range;
                    end
                end
                else
                begin   // create "Lichtbringer" aura
                    // but ONLY dark tiles
                    if DngLvl[i,j].intAirType = 5 then
                    begin
                        DngLvl[i,j].intAirType := 0;
                        DngLvl[i,j].intAirRange := range;
                    end;
                end;
            end;
end;


function CloneItem(): String;
    var
        i, n: Integer;
        equi: String;
begin

    CloneItem:='After some thinking, you decide against duplication of an item';

    ClearScreenSDL;

    if UseSDL=true then
    begin
        LoadImage_Title(PathData+'graphics/invbg.jpg');
    end;

    repeat

        if UseSDL=true then
            BlitImage_Title
        else
        begin
            StatusDeco;
            DialogWin;
        end;

        TransTextXY(1, 1, 'WHICH ITEM SHOULD BE DUPLICATED?');

        for i:=1 to 16 do
        begin
            equi := '';
            if inventory[i].intType > 0 then
            begin

                if (thing[inventory[i].intType].blWield) and (ThePlayer.intWeapon=inventory[i].intType) then
                    equi:='[wielded]';
                if (thing[inventory[i].intType].blWear) and (ThePlayer.intArmour=inventory[i].intType) then
                    equi:='[worn]';
                if (thing[inventory[i].intType].blHat) and (ThePlayer.intHat=inventory[i].intType) then
                    equi:='[worn]';
                if (thing[inventory[i].intType].blShoes) and (ThePlayer.intFeet=inventory[i].intType) then
                    equi:='[worn]';
                if (thing[inventory[i].intType].blExtra) and (ThePlayer.intExtra=inventory[i].intType) then
                    equi:='[wielded]';
                if (thing[inventory[i].intType].blRing) and (ThePlayer.intRingLeft=inventory[i].intType) then
                    equi:='[worn] ';
                if (thing[inventory[i].intType].blRing) and (ThePlayer.intRingRight=inventory[i].intType) then
                    equi:=equi + '[worn]';

                TransTextXY(6, 4+i, IntToStr(i));
                TransTextXY(10, 4+i, thing[inventory[i].intType].chLetter + ' ' + thing[inventory[i].intType].strName);
                TransTextXY(43, 4+i, 'x' + IntToStr(inventory[i].longNumber) + ' '+equi);
            end;
        end;

        n := GetNumericalInput('Selection?', 2);

    until (n>-1) and (n<17) and (Inventory[n].intType>0);

    if n>0 then
        if Inventory[n].longNumber>0 then
            if Thing[Inventory[n].intType].strName='Potion of Duplication' then
            begin
                CloneItem:='You use the potion to duplicate itself, but it does not work.';
            end
            else
            begin
                inc(Inventory[n].longNumber);
                CloneItem:='You duplicate the '+Thing[Inventory[n].intType].strName;
            end;
end;


function IdentifyItem(): String;
    var
        i, n: Integer;
        equi: String;
begin

    IdentifyItem:='You identify nothing';

    if UseSDL=true then
    begin
        LoadImage_Title(PathData+'graphics/invbg.jpg');
        BlitImage_Title;
    end
    else
    begin
        ClearScreenSDL;
        DialogWin;
        StatusDeco;
    end;

    TransTextXY(1, 1, 'WHICH ITEM SHOULD BE IDENTIFIED?');

    for i:=1 to 16 do
    begin
        equi := '';
        if inventory[i].intType > 0 then
        begin
            if (thing[inventory[i].intType].blWield) and (ThePlayer.intWeapon=inventory[i].intType) then
                equi:='[wielded]';
            if (thing[inventory[i].intType].blWear) and (ThePlayer.intArmour=inventory[i].intType) then
                equi:='[worn]';
            if (thing[inventory[i].intType].blHat) and (ThePlayer.intHat=inventory[i].intType) then
                equi:='[worn]';
            if (thing[inventory[i].intType].blShoes) and (ThePlayer.intFeet=inventory[i].intType) then
                equi:='[worn]';
            if (thing[inventory[i].intType].blExtra) and (ThePlayer.intExtra=inventory[i].intType) then
                equi:='[wielded]';
            if (thing[inventory[i].intType].blRing) and (ThePlayer.intRingLeft=inventory[i].intType) then
                equi:='[worn] ';
            if (thing[inventory[i].intType].blRing) and (ThePlayer.intRingRight=inventory[i].intType) then
                equi:=equi + '[worn]';

            TransTextXY(6, 4+i, IntToStr(i));
            TransTextXY(10, 4+i, thing[inventory[i].intType].chLetter + ' ' + thing[inventory[i].intType].strName);
            TransTextXY(43, 4+i, 'x' + IntToStr(inventory[i].longNumber) + ' '+equi);
        end;
    end;

    repeat
        n := GetNumericalInput('Selection?', 2);
    until (n=0) or ((n>0) and (n<17));

    if n>0 then
        if Inventory[n].longNumber>0 then
            if Thing[Inventory[n].intType].blIdentified = true then
            begin
                IdentifyItem:='Identifying this already known item reveals nothing new';
            end
            else
            begin
                Thing[Inventory[n].intType].blIdentified := true;
                Thing[Inventory[n].intType].strName := Thing[Inventory[n].intType].strRealName;
                IdentifyItem:='You identify the item as '+Thing[Inventory[n].intType].strName;

                // Northdoom?
                if Thing[Inventory[n].intType].strName='Northdoom' then
                begin
                    PlaySFX('page-turn.mp3');
                    ShowText('interlude');
                    ShowPlot(22);
                end;
            end;
end;


// effects of items and chants selected in inventory or in spellbook
procedure DoEffect(intEffectType: integer; intEffectRange: integer; strEffectText: string);
    var
        rx, ry, n: integer;
begin

    // random effect?
    if intEffectType=-1 then
    begin
        n:=trunc(random(34));
        case n of
            0:
                intEffectType:=2;
            1:
                intEffectType:=3;
            2:
                intEffectType:=4;
            3:
                intEffectType:=5;
            4:
                intEffectType:=8;
            5:
                intEffectType:=9;
            6:
                intEffectType:=12;
            7:
                intEffectType:=15;
            8:
                intEffectType:=16;
            9:
                intEffectType:=18;
            10:
                intEffectType:=21;
            11:
                intEffectType:=22;
            12:
                intEffectType:=23;
            13:
                intEffectType:=24;
            14:
                intEffectType:=27;
            15:
                intEffectType:=29;
            16:
                intEffectType:=30;
            17:
                intEffectType:=32;
            18:
                intEffectType:=33;
            19:
                intEffectType:=34;
            20:
                intEffectType:=38;
            21:
                intEffectType:=39;
            22:
                intEffectType:=40;
            23:
                intEffectType:=10;
            24:
                intEffectType:=13;
            25:
                intEffectType:=14;
            26:
                intEffectType:=17;
            27:
                intEffectType:=35;
            28:
                intEffectType:=31;
            29:
                intEffectType:=49;
            30:
                intEffectType:=28;
            31:
                intEffectType:=37;
            32:
                intEffectType:=26;
            33:
                intEffectType:=50;
        end;

        // if not a well, it must be a random potion or spell
        if strEffectText<>'You drink from the well, hoping that it is water ...' then
            strEffectText := 'You are curious what will happen';

        GetKeyInput(strEffectText,true);
        strEffectText:='-';
    end;


    // MaxMagic item equipped?
    if CheckEffect(37)=true then
        intEffectRange := intEffectRange * 2;


    // TODO: include temporary resistances (basically all remaining effect no. ...)

    case intEffectType of

        // regain food
        1:
        begin
            PlaySFX('magic-holy.mp3');
            inc(ThePlayer.longFood, intEffectRange);
            if ThePlayer.intReli=4 then
                inc(ThePlayer.longFood, intEffectRange);
            if strEffectText='-' then
                strEffectText:='Your stomach has been filled with food';
            ShowStatus;
        end;

        // reduce poison
        2:
        begin
            PlaySFX('magic-holy.mp3');
            dec(ThePlayer.intPoison, intEffectRange);
            if ThePlayer.intPoison < 0 then
            begin
                ThePlayer.intPoison := 0;
                if strEffectText='-' then
                    strEffectText:='It was an antidote';
            end;
        end;

        // regain PP
        3:
        begin
            PlaySFX('magic-holy.mp3');
            BlendMagic(7);
            inc(ThePlayer.intPP, intEffectRange);
            if ThePlayer.intPP > ThePlayer.intMaxPP then
                ThePlayer.intPP := ThePlayer.intMaxPP;
            if strEffectText='-' then
                strEffectText:='The drink regenerated your psychic power';
        end;

        // regain HP
        4:
        begin
            PlaySFX('magic-holy.mp3');
            BlendMagic(6);
            inc(ThePlayer.intHP, intEffectRange);
            if ThePlayer.intHP > ThePlayer.intMaxHP then
                ThePlayer.intHP := ThePlayer.intMaxHP;
            if strEffectText='-' then
                strEffectText:='The drink regenerated your health';
        end;

        // become invisible
        5:
        begin
            PlaySFX('magic-time.mp3');
            ThePlayer.intInvis := intEffectRange;
            if strEffectText='-' then
                strEffectText:='Suddenly, you became invisible';
        end;

        // ExtraGold
        7:
        begin
            PlaySFX('magic-time.mp3');
            inc(ThePlayer.intTempResist[7], intEffectRange);
            if strEffectText='-' then
                strEffectText:='You feel that you will be a little richer soon';
            ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
        end;


        // fire aura
        8:
        begin
            PlaySFX('magic-combat.mp3');
            if strEffectText='-' then
                strEffectText:='You create an Fire Aura';
            ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
            AuraSpell(1, intEffectRange);
        end;

        // ice aura
        9:
        begin
            PlaySFX('magic-combat.mp3');
            if strEffectText='-' then
                strEffectText:='You create an Ice Aura';
            ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
            AuraSpell(2, intEffectRange);
        end;


        // poison resistance
        10:
        begin
            PlaySFX('resistance.mp3');
            inc(ThePlayer.intTempResist[10], intEffectRange);
            if strEffectText='-' then
                strEffectText:='You feel resistant against poison';
            ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
        end;

            
        // heal confusion
        12:
        begin
            PlaySFX('magic-holy.mp3');
            if ThePlayer.intConfusion > 0 then
            begin
                dec(ThePlayer.intConfusion, intEffectRange);
                if ThePlayer.intConfusion < 0 then
                    ThePlayer.intConfusion := 0;
                if strEffectText='-' then
                    strEffectText:='The drink made you less confuse';
            end;
        end;

        // fire resistance
        13:
        begin
            PlaySFX('resistance.mp3');
            inc(ThePlayer.intTempResist[13], intEffectRange);
            if strEffectText='-' then
                strEffectText:='You feel resistant against fire';
            ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
        end;

        // ice resistance
        14:
        begin
            PlaySFX('resistance.mp3');
            inc(ThePlayer.intTempResist[14], intEffectRange);
            if strEffectText='-' then
                strEffectText:='You feel resistant against ice';
            ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
        end;

        // magic wall
        15:
        begin
            PlaySFX('magic-time.mp3');
            BlendMagic(9);
            ThePlayer.intWall := intEffectRange;
            if strEffectText='-' then
                strEffectText:='A magic barrier has been created around you';
        end;

        // strength drug
        16:
        begin
            if ThePlayer.intTotalVitari<2 then
                inc(ThePlayer.intStrength, intEffectRange);
            if ThePlayer.intTotalVitari>=2 then
                inc(ThePlayer.intStrength, intEffectRange div 2);
            if ThePlayer.intTotalVitari>=4 then
                inc(ThePlayer.intStrength, intEffectRange div 4);
            if ThePlayer.intTotalVitari>=6 then
                inc(ThePlayer.intStrength, intEffectRange div 6);
            if ThePlayer.intTotalVitari>=8 then
                inc(ThePlayer.intStrength, intEffectRange div 8);
            if ThePlayer.intTotalVitari>=10 then
                inc(ThePlayer.intStrength, intEffectRange div 10);
            inc(ThePlayer.intTotalVitari);
            if strEffectText='-' then
                strEffectText:='You feel stronger';

            case ThePlayer.intTotalVitari of
                1:
                    ThePlayer.intNextVitari := 500;
                2:
                    ThePlayer.intNextVitari := 400;
                3:
                    ThePlayer.intNextVitari := 350;
                4:
                    ThePlayer.intNextVitari := 200;
                5:
                    ThePlayer.intNextVitari := 80;
                6:
                    ThePlayer.intNextVitari := 50;
                7:
                    ThePlayer.intNextVitari := 20;
            end;
            if ThePlayer.intTotalVitari > 7 then
                ThePlayer.intNextVitari := 10;
            ThePlayer.intNeedVitari := 0;
        end;

        // blindness resistance
        17:
        begin
            PlaySFX('resistance.mp3');
            inc(ThePlayer.intTempResist[17], intEffectRange);
            if strEffectText='-' then
                strEffectText:='You feel resistant against blindness';
            ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
        end;

        // heal blindness
        18:
        begin
            PlaySFX('magic-holy.mp3');
            if ThePlayer.intBlind > 0 then
            begin
                dec(ThePlayer.intBlind, intEffectRange);
                if ThePlayer.intBlind < 0 then
                    ThePlayer.intBlind := 0;
                if strEffectText='-' then
                    strEffectText:='Your blindness is cured';
            end;
        end;

        // force
        19:
        begin
            ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
            if DoEffectOnMonster(intEffectRange, 19)=0 then
                strEffectText:='The force attack missed; no enemy was hurt';
            BlendMagic(13);
            if strEffectText='-' then
                strEffectText:='You throw concentrated psychic power to the enemy';
        end;

        // heal aura
        20:
        begin
            PlaySFX('magic-holy.mp3');
            if strEffectText='-' then
                strEffectText:='You create a healing Aura';
            ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
            AuraSpell(4, intEffectRange);
        end;


        // teleport
        21:
        begin
            PlaySFX('magic-time.mp3');
            if strEffectText='-' then
                strEffectText:='You have been teleported to another region';
            repeat
                rx := trunc(random(DngMaxWidth-12)) + 1;
                ry := trunc(random(DngMaxHeight-12)) + 1;
            until DngLvl[rx,ry].intIntegrity = 0;
            ThePlayer.intX := rx;
            ThePlayer.intY := ry;
        end;


        // water aura
        22:
        begin
            PlaySFX('magic-combat.mp3');
            if strEffectText='-' then
                strEffectText:='You create a Water Aura';
            ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
            AuraSpell(3, intEffectRange);
        end;


        // lose HP
        23:
        begin
            PlaySFX('magic-bad.mp3');
            if strEffectText='-' then
                strEffectText:='The drink was unhealthy';
            dec(ThePlayer.intHP, intEffectRange);
            if IsPlayerDead=true then
                GameOver('Poisoned to death.');
        end;


        // lose PP
        24:
        begin
            PlaySFX('magic-bad.mp3');
            if strEffectText='-' then
                strEffectText:='The drink drained your psychic power';
            dec(ThePlayer.intPP, intEffectRange);
            if ThePlayer.intPP < 0 then
                ThePlayer.intPP:=0;
        end;

        // resist paralye
        26:
        begin
            PlaySFX('resistance.mp3');
            inc(ThePlayer.intTempResist[26], intEffectRange);
            if strEffectText='-' then
                strEffectText:='You feel resistant against paralization';
            ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
        end;

        // heal paralyze
        27:
        begin
            PlaySFX('magic-holy.mp3');
            if ThePlayer.intPara > 0 then
            begin
                dec(ThePlayer.intPara, intEffectRange);
                if ThePlayer.intPara < 0 then
                    ThePlayer.intPara := 0;
                if strEffectText='-' then
                    strEffectText:='You can move again';
            end;
        end;

        // calm resistance
        28:
        begin
            PlaySFX('resistance.mp3');
            inc(ThePlayer.intTempResist[28], intEffectRange);
            if strEffectText='-' then
                strEffectText:='You feel resistant against calm';
            ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
        end;


        // heal calm
        29:
        begin
            PlaySFX('magic-holy.mp3');
            if ThePlayer.intCalm > 0 then
            begin
                dec(ThePlayer.intCalm, intEffectRange);
                if ThePlayer.intCalm < 0 then
                    ThePlayer.intCalm := 0;
                if strEffectText='-' then
                    strEffectText:='You can chant again';
            end;
        end;


        // freeze time
        30:
        begin
            PlaySFX('magic-time.mp3');
            ThePlayer.intFreeze := intEffectRange;
            if strEffectText='-' then
                strEffectText:='Suddenly, time seems to be frozen';
        end;

        // curse resistance
        31:
        begin
            PlaySFX('resistance.mp3');
            inc(ThePlayer.intTempResist[31], intEffectRange);
            if strEffectText='-' then
                strEffectText:='You feel resistant against curses';
            ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
        end;


        // uncurse
        32:
        begin
            PlaySFX('magic-holy.mp3');
            if ThePlayer.blCursed = true then
            begin
                ThePlayer.blCursed := false;
                if strEffectText='-' then
                    strEffectText:='You are not cursed anymore';
            end;
        end;


        // bless
        33:
        begin
            PlaySFX('magic-holy.mp3');
            if ThePlayer.blEvil=false then
            begin
                BlendMagic(5);
                ThePlayer.blBlessed := true;
                ThePlayer.blCursed := false;
                if ThePlayer.intTotalVitari > 0 then
                begin
                    ThePlayer.intTotalVitari := 0;
                    ThePlayer.intNextVitari := 0;
                    ThePlayer.intNeedVitari := 0;
                    strEffectText:=ThePlayer.strReli+' blessed you and healed your from your drug addiction';
                end
                else
                if strEffectText='-' then
                    strEffectText:='You feel blessed by '+ThePlayer.strReli;
            end;
        end;


        // curse
        34:
        begin
            PlaySFX('magic-bad.mp3');
            if ThePlayer.blEvil=false then
            begin
                if (CheckEffect(31)=false) and (ThePlayer.intHumility<10) then
                begin
                    BlendMagic(8);
                    ThePlayer.blBlessed := false;
                    ThePlayer.blCursed := true;
                    if strEffectText='-' then
                        strEffectText:='A dark shadow threatens your mind';
                end
                else
                    strEffectText:='Due to your high humility, you resist a dark shadow';
             end;
        end;

        // water resistance
        35:
        begin
            PlaySFX('resistance.mp3');
            inc(ThePlayer.intTempResist[35], intEffectRange);
            if strEffectText='-' then
                strEffectText:='You feel resistant against water';
            ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
        end;

        // MaxMagic
        37:
        begin
            PlaySFX('magic-time.mp3');
            inc(ThePlayer.intTempResist[37], intEffectRange);
            if strEffectText='-' then
                strEffectText:='Your magic is stronger';
            ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
        end;


        // fire
        38:
        begin
            ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
            if DoEffectOnMonster(intEffectRange, 1)=0 then
                strEffectText:='Your fire attack missed; no enemy was hurt';
            BlendMagic(1);
            if strEffectText='-' then
                strEffectText:='You throw fire out of your arms';
        end;


        // ice
        39:
        begin
            ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
            if DoEffectOnMonster(intEffectRange, 2)=0 then
                strEffectText:='Your ice attack missed; no enemy was hurt';
            BlendMagic(2);
            if strEffectText='-' then
                strEffectText:='You throw ice out of your arms';
        end;


        // water
        40:
        begin
            ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
            if DoEffectOnMonster(intEffectRange, 3)=0 then
                strEffectText:='Your water attack missed; no enemy was hurt';
            BlendMagic(3);
            if strEffectText='-' then
                strEffectText:='You let water flow, out of your arms';
        end;

        // clone item
        42:
        begin
            strEffectText:=CloneItem;
            PlaySFX('magic-holy.mp3');
            ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
        end;

        // confusion
        44:
        begin
            PlaySFX('magic-bad.mp3');
            ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
            BlendMagic(10);
            inc(ThePlayer.intConfusion, intEffectRange);
            if strEffectText='-' then
                strEffectText:='You feel confused';
        end;

        // poison
        45:
        begin
            PlaySFX('magic-bad.mp3');
            ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
            BlendMagic(4);
            inc(ThePlayer.intPoison, intEffectRange);
            if strEffectText='-' then
                strEffectText:='You feel very sick';
        end;

        // meteor
        46:
        begin
            if ThePlayer.intQuestState[1,1]=2 then  // ensure that meteor only works after the Book of Stars has been found
                if DungeonLevel=1 then
                begin

                    StopMusic;
                    Meteor;
                    StoreAchievement('Used "Meteor" potion. Created a way to the surface of Earth.');

                    if ThePlayer.blEvil=false then
                    begin
                        // alternate good way to win
                        ShowPlot(5);
                        PlaySFX('page-turn.mp3');
                        ShowText('chapter-3-good');
                        ShowPlot(7);
                    end
                    else
                    begin
                        // evil way to win
                        ShowPlot(20);
                        PlaySFX('page-turn.mp3');
                        ShowText('chapter-3-evil');
                        ShowPlot(21);
                    end;
                    WinGame(false);
                end;
        end;

        // identify item
        47:
        begin
            strEffectText:=IdentifyItem;
            PlaySFX('magic-holy.mp3');
           ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
        end;

        // lichtbringer
        48:
        begin
            PlaySFX('magic-time.mp3');
            if strEffectText='-' then
                strEffectText:='You enlighten the darkness';
            ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
            AuraSpell(5, intEffectRange);
        end;

        // divine rage bonus
        49:
        begin
            PlaySFX('magic-time.mp3');
            inc(ThePlayer.intTempResist[49], intEffectRange);
            if strEffectText='-' then
                strEffectText:='You feel your god watch over you';
            ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
        end;

        // wipe
        50:
        begin
            PlaySFX('magic-time.mp3');
            KillAllMonsters;
            ThePlayer.intHP:=ThePlayer.intHP div 2;
            if strEffectText='-' then
                strEffectText:='You feel pain and suddenly you are all alone';
            ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
        end;
                
    end;

    if strEffectText='-' then
        strEffectText:='Nothing seems to happen';
    ShowTransMessage(strEffectText + '.', false);
end;


procedure TakeItem (x: integer; y: integer);
    var
        i, t: integer;
        blRemItem: boolean;
        strMsg: string;
begin
    blRemItem := false;
    t := DngLvl[x,y].intItem;

    if Thing[t].chLetter=chr(167) then
    begin
        inc(ThePlayer.longGold,Thing[t].intPrice);
        DngLvl[x,y].intItem := 0;
        ShowTransMessage('You pick up the item: ' + Thing[t].strName + '.', false);
    end
    else
    begin
        // check if player already has one item of this type
        for i:=1 to 16 do
            if inventory[i].intType = t then
            begin
                inc(inventory[i].longNumber, Thing[Inventory[i].intType].intAmount);
                blRemItem:=true;
            end;
    
        // if item not taken, check if free space available
        if blRemItem=false then
            for i:=1 to 16 do
                if (inventory[i].intType = 0) and (blRemItem=false) then
                begin
                    inventory[i].intType := t;
                    inventory[i].longNumber := Thing[Inventory[i].intType].intAmount;;
                    blRemItem:=true;
                end;
    
        // if item taken, remove item from map
        if blRemItem then
        begin
            DngLvl[x,y].intItem := 0;

            strMsg:=lowercase(LeftStr(Thing[t].strName, 1));
            if (strMsg='a') or (strMsg='e') or (strMsg='u') or (strMsg='o') or (strMsg='i') then
                ShowTransMessage('You pick up an ' + Thing[t].strName + '.', false)
            else
                ShowTransMessage('You pick up a ' + Thing[t].strName + '.', false);
        end
        else
            ShowTransMessage('You want to pick up the ' + Thing[t].strName + ', but your inventory is full.', false);
    end;

//      ClearScreenSDL;
    ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
    if UseSDL=true then
        SDL_UPDATERECT(screen,0,0,0,0)
    else
        UpdateScreen(true);
end;


procedure SpellInfo(id: integer; n: integer);
var
    dummy: string;
    IconRect, IconRectS: SDL_RECT;
begin

    IconRect.x:=38*(Spell[id].intEffect-1);
    IconRect.y:=415;
    IconRect.w:=38;
    IconRect.h:=38;

    IconRectS.x:=735+HiResOffsetX;
    IconRectS.y:=12+HiResOffsetY;
    IconRectS.w:=38;
    IconRectS.h:=38;

    if UseSDL=true then
    begin
        LoadImage_Title(PathData+'graphics/invbg.jpg');
    end;

    dummy:='-';
    repeat

        if UseSDL=true then
        begin
            BlitImage_Title;
            SDL_BLITSURFACE(extratiles, @IconRect, screen, @IconRectS);
        end
        else
        begin
            DialogWin;
            BottomBar;
        end;

        if Spell[id].strName<>'-' then
        begin

            TransTextXY(1,1, '"' + uppercase(Spell[id].strName) + '"');

            if Spell[id].strDescri<>'-' then
                TransTextXY(8,7, Spell[id].strDescri+'.');

            TransTextXY(8,10, 'Minimum level: ' + IntToStr(Spell[id].intCharLvl));

            TransTextXY(8,12, 'Current spell level: ' + IntToStr(SpellBook[n].intKnown));

            if CheckEffect(37)=true then
                TransTextXY(8,13, 'Current efficiency : ' + IntToStr(2*(Spell[id].intRange + (Spellbook[n].intKnown * Spellbook[n].intKnown))) + ' (maximized)')
            else
                TransTextXY(8,13, 'Current efficiency : ' + IntToStr(Spell[id].intRange + (Spellbook[n].intKnown * Spellbook[n].intKnown)));

            TransTextXY(8,14, 'Current PP cost    : ' + IntToStr(Spell[id].intPP + (2*SpellBook[n].intKnown)));
            TransTextXY(8,16, 'Wait for refresh   : ' + IntToStr(Spell[id].intRefresh) + ' turns');


            dummy:=GetKeyInput('[ESC] Resume game', false);
        end;
    until dummy='ESC';

end;


procedure Hospital;
    var
        blHasInsurance, blRemItem: Boolean;
        intPrice, i, t: Integer;
        longInsurancePrice: Longint;
        ch, dummy: string;
        strHospitalName: string;
begin
    strHospitalName := 'the hospital';
    case DungeonLevel of
        1:
            strHospitalName:='the Healing Lodge';
        5:
            strHospitalName:='at PHARRao Doctors';
        10:
            strHospitalName:='the hospital';
        15:
            strHospitalName:='this Health Care Center';
    end;

    DecoIcon.x:=520;
    DecoIcon.y:=80;
    DecoIcon.w:=60;
    DecoIcon.h:=80;

    DecoIconS.x:=717+HiResOffsetX;
    DecoIconS.y:=400+HiResOffsetY;
    DecoIconS.w:=72;
    DecoIconS.h:=72;

    ClearScreenSDL;

    if UseSDL=true then
    begin
        LoadImage_Title(PathData+'graphics/invbg.jpg');
    end;

    repeat

        if UseSDL=true then
        begin
            BlitImage_Title;
            SDL_BLITSURFACE(extratiles, @DecoIcon, screen, @DecoIconS);
        end
        else
        begin
            DialogWin;
            StatusDeco;
        end;


        TransTextXY(1, 1, 'WELCOME TO '+uppercase(strHospitalName)+'.');
        TransTextXY(62, 1, 'Credits: ' + IntToStr(ThePlayer.longGold));

        if ThePlayer.intHP < ThePlayer.intMaxHP then
        begin
            intPrice := ThePlayer.intMaxHP - ThePlayer.intHP;
            TransTextXY(8, 7, 'Treatment without health insurance will cost '+IntToStr(intPrice)+' Credits.');
        end
        else
            TransTextXY(8, 8, 'You need no healing at the moment.');

        blHasInsurance:=false;
        if (PlayerHasItem('Insurance Card')>0) or (PlayerHasItem('V.I.P. Card')>0) then
            blHasInsurance:=true;

        longInsurancePrice := (WinLevel-DungeonLevel)*CONST_INSURANCE_DLV + (ThePlayer.longGold div CONST_INSURANCE_BASE);
        if blHasInsurance=false then
            TransTextXY(8, 9, 'A lifetime health insurance card is available for '+IntToStr(longInsurancePrice)+' Credits.')
        else
            TransTextXY(8, 9, 'You have a health insurance card.');

        TransTextXY(8, 10, 'We also offer a single life insurance for '+IntToStr(250*(ThePlayer.longLifeIns+1))+' Credits.');
        if ThePlayer.intTotalVitari > 0 then
            TransTextXY(8,11, 'A withdrawal treatment is available for '+IntToStr(130*ThePlayer.intTotalVitari)+' Credits.');

        dummy := GetKeyInput('[h]eal  [i]nsurance cd.  [w]ithdrawal t.   [l]ife insur.   [ESC] to leave', false);

    until (dummy='h') or (dummy='i') or (dummy='l') or (dummy='w') or (dummy='ESC');
    ch := dummy;

    if ch='i' then
    begin
        if blHasInsurance=false then
        begin
            if ThePlayer.longGold > longInsurancePrice-1 then
            begin
                // get ID of insurance card
                t:=ReturnItemByName('Insurance Card');

                // check if inventory has space for card
                blRemItem:=false;
                for i:=1 to 16 do
                    if (inventory[i].intType = 0) and (blRemItem=false) then
                    begin
                        inventory[i].intType := t;
                        inc(inventory[i].longNumber);
                        blRemItem:=true;
                        break;
                    end;
    
                if blRemItem=true then
                begin
                    dec(ThePlayer.longGold, longInsurancePrice);
                    ShowTransMessage('You have bought a health insurance card. Now healing will be for free.', false);
                end
                else
                    GetKeyInput('You do not have enough space in inventory to store an insurance card.', true);

            end
            else
                GetKeyInput('Unfortunately you can'+chr(39)+'t afford a health insurance now.', true);
        end
        else
            GetKeyInput('You already have a health insurance card.', true);
    end;

    if ch='h' then
        if ThePlayer.intHP < ThePlayer.intMaxHP then
        begin
            if blHasInsurance=false then
            begin
                if ThePlayer.longGold > intPrice-1 then
                begin
                    ThePlayer.intHP:=ThePlayer.intMaxHP;
                    dec(ThePlayer.longGold, intPrice);
                    ShowTransMessage('You have been healed for '+IntToStr(intPrice)+' Credits.', false);
                end
                else
                    GetKeyInput('We are sorry, but you can'+chr(39)+'t afford a treatment now.', true);
            end
            else
            begin
                ThePlayer.intHP:=ThePlayer.intMaxHP;
                ShowTransMessage('You have been healed for free in the hospital.', false);
            end;
        end
        else
            GetKeyInput('You are healthy and need no treatment.', true);

    if ch='l' then
        if ThePlayer.longGold > 250*(ThePlayer.longLifeIns+1) then
        begin
            ShowTransMessage('You bought a life insurance for '+IntToStr(250*(ThePlayer.longLifeIns+1))+' Credits.', false);
            dec(ThePlayer.longGold, 250*(ThePlayer.longLifeIns+1));
            inc(ThePlayer.longLifeIns, 1);
            SaveGame(ThePlayer.strName, DungeonLevel);
        end
        else
            GetKeyInput('You can'+chr(39)+'t afford a life insurance.', true);

    if ch='w' then
        if ThePlayer.intTotalVitari>0 then
        begin
            if ThePlayer.longGold > 130*ThePlayer.intTotalVitari then
            begin
                ShowTransMessage('You made a withdrawal treatment for '+IntToStr(130*ThePlayer.intTotalVitari)+' Credits.', false);
                dec(ThePlayer.longGold, 130*ThePlayer.intTotalVitari);
                ThePlayer.intTotalVitari:=0;
                ThePlayer.intNextVitari:=0;
                ThePlayer.intNeedVitari:=0;
            end
            else
                GetKeyInput('You can'+chr(39)+'t afford a withdrawal treatment. Pray to your god instead.', true);
        end
        else
            GetKeyInput('You don'+chr(39)+'t need a withdrawal treatment.', true);
end;


procedure Restaurant;
    var
        i: Integer;
        ch: string;
        strRestaurantName: string;
        strMenues: array[1..5] of string;
        intMenues: array[1..5] of integer;
        kx, ky, knoepfe: longint;
begin

    intMenues[1]:=400;    // small
    intMenues[2]:=800;    // normal
    intMenues[3]:=1200;    // bigger
    intMenues[4]:=1600;    // very big
    intMenues[5]:=2000;    // SuperSizeMe

    case DungeonLevel of
        1:
            strRestaurantName:='Clive'+chr(39)+'s Pub';
        5:
            strRestaurantName:='the "Chez Luc"';
        10:
            strRestaurantName:='Akpir Catering';
        15:
            strRestaurantName:='the "Czerwone Jabluszko"';
    end;

    if strRestaurantName='Clive'+chr(39)+'s Pub' then
    begin
        strMenues[1]:='Chocolate Cake with Custard';
        strMenues[2]:='Baked Beans on Toast';
        strMenues[3]:='Sunday Roast with Pudding';
        strMenues[4]:='Scrambled Egg and Bacon';
        strMenues[5]:='Fish and Chips';
    end
    else
    if strRestaurantName='the "Chez Luc"' then
    begin
        strMenues[1]:='Quiche Lorraine';
        strMenues[2]:='Tarte Tatin';
        strMenues[3]:='Canard a l'+chr(39)+'orange';
        strMenues[4]:='Ratatouille';
        strMenues[5]:='Bouillabaisse';
    end
    else
    if strRestaurantName='Akpir Catering' then
    begin
        strMenues[1]:='Soljanka';
        strMenues[2]:='Nudeln mit Kaesesauce';
        strMenues[3]:='Haehnchenpfanne';
        strMenues[4]:='Ueberbackener Fisch';
        strMenues[5]:='Bratkartoffeln mit Ei';
    end
    else
    if strRestaurantName='the "Czerwone Jabluszko"' then
    begin
        strMenues[1]:='Barszcz';
        strMenues[2]:='Pierogi';
        strMenues[3]:='Kielbasa';
        strMenues[4]:='Bigos';
        strMenues[5]:='Schabowy';
    end;

    DecoIcon.x:=360;
    DecoIcon.y:=80;
    DecoIcon.w:=80;
    DecoIcon.h:=80;


    DecoIconS.x:=711+HiResOffsetX;
    DecoIconS.y:=400+HiResOffsetY;
    DecoIconS.w:=72;
    DecoIconS.h:=72;

    ClearScreenSDL;

    if UseSDL=true then
    begin
        LoadImage_Title(PathData+'graphics/invbg.jpg');
    end;

    repeat

        if UseSDL=true then
        begin
            BlitImage_Title;
            SDL_BLITSURFACE(extratiles, @DecoIcon, screen, @DecoIconS);
        end
        else
        begin
            DialogWin;
            StatusDeco;
        end;

        TransTextXY(1, 1, 'WELCOME TO '+uppercase(strRestaurantName)+'.');
        TransTextXY(62, 1, 'Credits: ' + IntToStr(ThePlayer.longGold));

        TransTextXY(8, 7, 'What would you like to eat today?');

        TransTextXY(8, 9, 'Key   Name of dish');

        for i:=1 to 5 do
        begin
            TransTextXY(8, 10+i, ' '+IntToStr(i)+'    '+strMenues[i]);
            TransTextXY(52, 10+i, '$'+IntToStr((8*intMenues[i]) div 100));
        end;

        ch := GetKeyInput('Press [1] to [5] to select a dish. Press [ESC] to leave.', false);
    until (ch='1') or (ch='2') or (ch='3') or (ch='4') or (ch='5') or (ch='ESC');


    if ch<>'ESC' then
        if ThePlayer.longGold > ((8*intMenues[StrToInt(ch)]) div 100)-1 then
        begin
            inc(ThePlayer.longFood, intMenues[StrToInt(ch)]);
            dec(ThePlayer.longGold, (8*intMenues[StrToInt(ch)]) div 100);
            ShowTransMessage('You eat the '+strMenues[StrToInt(ch)]+'.', false);
        end
        else
            GetKeyInput('You need more Credits to order the selected dish.', true);

end;


// Items can be disassemled
procedure DisassembleItem(n: integer);
    var
        strItemName: string;
        chItemChar: char;
        intItemPrice: integer;
        intPlusMetal, intPlusWood, intPlusStone, intPlusLeather, intPlusPaper, intPlusPlastics: integer;
begin
    strItemName := Thing[n].strName;
    chItemChar := Thing[n].chLetter;
    intItemPrice := Thing[n].intPrice;

    intPlusMetal:=0;
    intPlusWood:=0;
    intPlusStone:=0;
    intPlusLeather:=0;
    intPlusPaper:=0;
    intPlusPlastics:=0;

    // items containing metal
    if (chItemChar=chr(156)) or (chItemChar=chr(159)) or (chItemChar=chr(158)) or (chItemChar=chr(157)) or (chItemChar=chr(162)) or (chItemChar=chr(166)) or (chItemChar=chr(165)) or (chItemChar=chr(172)) or (chItemChar=chr(191)) or (chItemChar=chr(160)) then
        intPlusMetal := 2 + (intItemPrice div 80);

    // items containing wood
    if (chItemChar=chr(156)) or (chItemChar=chr(159)) or (chItemChar=chr(158)) or (chItemChar=chr(160)) or (chItemChar=chr(162)) then
        intPlusWood := 2 + (intItemPrice div 80);

    // items containing stone
    if (chItemChar=chr(171)) or (chItemChar=chr(169)) then
        if chItemChar=':' then
            intPlusStone := 15
        else
            intPlusStone := 8;

    // items containing leather
    if (chItemChar=chr(163)) or (chItemChar=chr(157)) or (chItemChar=chr(164)) or (chItemChar=chr(165)) then
        intPlusLeather := 2 + (intItemPrice div 80);

    // items containing paper
    if (chItemChar=chr(193)) then
        intPlusPaper := 2;

    // items containing plastics
    if (chItemChar=chr(191)) or (strItemName='V.I.P. Card') or (strItemName='Insurance Card') or (chItemChar=chr(161)) then
        intPlusPlastics :=1 + (intItemPrice div 80);

    inc(Storage.longWood, intPlusWood);
    inc(Storage.longMetal, intPlusMetal);
    inc(Storage.longStone, intPlusStone);
    inc(Storage.longLeather, intPlusLeather);
    inc(Storage.longPlastics, intPlusPlastics);
    inc(Storage.longPaper, intPlusPaper);
end;


procedure ResourceWorkshop;
    var
        dummy: string;
        ch: char;
        n, longValue: longint;
        MouseItem: Integer;
begin
    DecoIcon.x:=580;
    DecoIcon.y:=80;
    DecoIcon.w:=75;
    DecoIcon.h:=80;

    DecoIconS.x:=711+HiResOffsetX;
    DecoIconS.y:=400+HiResOffsetY;
    DecoIconS.w:=72;
    DecoIconS.h:=72;

    ClearScreenSDL;

    if UseSDL=true then
    begin
        LoadImage_Title(PathData+'graphics/invbg.jpg');
    end;

    if UseSDL=true then
    begin
        BlitImage_Title;
        SDL_BLITSURFACE(extratiles, @DecoIcon, screen, @DecoIconS);
    end
    else
    begin
        DialogWin;
        StatusDeco;
    end;

    TransTextXY(1, 1, 'WELCOME TO THIS RESOURCE WORKSHOP.');

    TransTextXY(8, 5, '1  Wood     : ' + IntToStr(Storage.longWood));
    TransTextXY(8, 6, '2  Metal    : ' + IntToStr(Storage.longMetal));
    TransTextXY(8, 7, '3  Stone    : ' + IntToStr(Storage.longStone));
    TransTextXY(8, 8, '4  Leather  : ' + IntToStr(Storage.longLeather));
    TransTextXY(8, 9, '5  Plastics : ' + IntToStr(Storage.longPlastics));
    TransTextXY(8,10, '6  Paper    : ' + IntToStr(Storage.longPaper));

    longValue:=0;
    inc(longValue, Storage.longWood*CONST_GOLDWOOD);
    inc(longValue, Storage.longMetal*CONST_GOLDMETAL);
    inc(longValue, Storage.longStone*CONST_GOLDSTONE);
    inc(longValue, Storage.longLeather*CONST_GOLDLEATHER);
    inc(longValue, Storage.longPlastics*CONST_GOLDPLASTICS);
    inc(longValue, Storage.longPaper*CONST_GOLDPAPER);

    TransTextXY(8,16, 'Your resources are worth '+IntToStr(longValue)+' Credits.');

    dummy := GetKeyInput('Press [s] to sell a resource, another key to cancel.', false);
    ch := dummy[1];


    if ch='s' then
    begin
        BottomBar;
        repeat
            dummy := GetKeyInput('Select a resource type or press [ESC] to leave the workshop.', false);
        until (dummy='1') or (dummy='2') or (dummy='3') or (dummy='4') or (dummy='5') or (dummy='6') or (dummy='ESC');
        ch := dummy[1];

        if ch<>'E' then
            case ch of
                '1':
                begin
                    if Storage.longWood>0 then
                    begin
                       n := GetNumericalInput('How many of your '+IntToStr(Storage.longWood)+' units of wood do you want to sell?', 8);
                        if (n>0) and (n<= Storage.longWood) then
                        begin
                            dec(Storage.longWood, n);
                            inc(ThePlayer.longGold, CONST_GOLDWOOD*n);
                            ShowTransMessage('You have sold '+IntToStr(n)+' units of wood for '+IntToStr(CONST_GOLDWOOD*n)+' Credits.', false);
                            n:=0;
                        end;

                        if (n>0) and (n>Storage.longWood) then
                        begin
                            GetKeyInput('You do not have that much wood.', true);
                            n:=0;
                        end;

                        if (n<0) then
                        begin
                            GetKeyInput('You can'+chr(39)+'t sell negative amounts of wood.', true);
                            n:=0;
                        end;
                    end
                    else
                        GetKeyInput('You don'+chr(39)+'t have any wood to sell.', true);
                end;

                '2':
                begin
                    if Storage.longMetal>0 then
                    begin
                       n := GetNumericalInput('How many of your '+IntToStr(Storage.longMetal)+' units of metal do you want to sell?', 8);
                        if (n>0) and (n<= Storage.longMetal) then
                        begin
                            dec(Storage.longMetal, n);
                            inc(ThePlayer.longGold, CONST_GOLDMETAL*n);
                            ShowTransMessage('You have sold '+IntToStr(n)+' units of metal for '+IntToStr(CONST_GOLDMETAL*n)+' Credits.', false);
                            n:=0;
                        end;

                        if (n>0) and (n>Storage.longMetal) then
                        begin
                            GetKeyInput('You do not have that much metal.', true);
                            n:=0;
                        end;

                        if (n<0) then
                        begin
                            GetKeyInput('You can'+chr(39)+'t sell negative amounts of metal.', true);
                            n:=0;
                        end;
                    end
                    else
                        GetKeyInput('You don'+chr(39)+'t have any metal to sell.', true);
                end;

                '3':
                begin
                    if Storage.longStone>0 then
                    begin
                        n := GetNumericalInput('How many of your '+IntToStr(Storage.longStone)+' units of stone do you want to sell?', 8);
                        if (n>0) and (n<= Storage.longStone) then
                        begin
                            dec(Storage.longStone, n);
                            inc(ThePlayer.longGold, CONST_GOLDSTONE*n);
                            ShowTransMessage('You have sold '+IntToStr(n)+' units of stone for '+IntToStr(CONST_GOLDSTONE*n)+' Credits.', false);
                            n:=0;
                        end;

                        if (n>0) and (n>Storage.longStone) then
                        begin
                            GetKeyInput('You do not have that much stone.', true);
                            n:=0;
                        end;

                        if (n<0) then
                        begin
                            GetKeyInput('You can'+chr(39)+'t sell negative amounts of stone.', true);
                            n:=0;
                        end;
                    end
                    else
                        GetKeyInput('You don'+chr(39)+'t have any stone to sell.', true);
                end;

                '4':
                begin
                    if Storage.longLeather>0 then
                    begin
                        n := GetNumericalInput('How many of your '+IntToStr(Storage.longLeather)+' units of leather do you want to sell?', 8);
                        if (n>0) and (n<= Storage.longLeather) then
                        begin
                            dec(Storage.longLeather, n);
                            inc(ThePlayer.longGold, CONST_GOLDLEATHER*n);
                            ShowTransMessage('You have sold '+IntToStr(n)+' units of leather for '+IntToStr(CONST_GOLDLEATHER*n)+' Credits.', false);
                            n:=0;
                        end;

                        if (n>0) and (n>Storage.longLeather) then
                        begin
                            GetKeyInput('You do not have that much leather.', true);
                            n:=0;
                        end;

                        if (n<0) then
                        begin
                            GetKeyInput('You can'+chr(39)+'t sell negative amounts of leather.', true);
                            n:=0;
                        end;
                    end
                    else
                        GetKeyInput('You don'+chr(39)+'t have any leather to sell.', true);
                end;


                '5':
                begin
                    if Storage.longPlastics>0 then
                    begin
                        n := GetNumericalInput('How many of your '+IntToStr(Storage.longPlastics)+' units of plastics do you want to sell?', 8);
                        if (n>0) and (n<= Storage.longPlastics) then
                        begin
                            dec(Storage.longPlastics, n);
                            inc(ThePlayer.longGold, CONST_GOLDPLASTICS*n);
                            ShowTransMessage('You have sold '+IntToStr(n)+' units of plastics for '+IntToStr(CONST_GOLDPLASTICS*n)+' Credits.', false);
                            n:=0;
                        end;

                        if (n>0) and (n>Storage.longPlastics) then
                        begin
                            GetKeyInput('You do not have that much plastics.', true);
                            n:=0;
                        end;

                        if (n<0) then
                        begin
                            GetKeyInput('You can'+chr(39)+'t sell negative amounts of plastics.', true);
                            n:=0;
                        end;
                    end
                    else
                        GetKeyInput('You don'+chr(39)+'t have any plastics to sell.', true);
                end;


                '6':
                begin
                    if Storage.longPaper>0 then
                    begin
                        n := GetNumericalInput('How many of your '+IntToStr(Storage.longPaper)+' units of paper do you want to sell?', 8);
                        if (n>0) and (n<= Storage.longPaper) then
                        begin
                            dec(Storage.longPaper, n);
                            inc(ThePlayer.longGold, CONST_GOLDPAPER*n);
                            ShowTransMessage('You have sold '+IntToStr(n)+' units of paper for '+IntToStr(CONST_GOLDPAPER*n)+' Credits.', false);
                            n:=0;
                        end;

                        if (n>0) and (n>Storage.longPaper) then
                        begin
                            GetKeyInput('You do not have that much paper.', true);
                            n:=0;
                        end;

                        if (n<0) then
                        begin
                            GetKeyInput('You can'+chr(39)+'t sell negative amounts of paper.', true);
                            n:=0;
                        end;
                    end
                    else
                        GetKeyInput('You don'+chr(39)+'t have any paper to sell.', true);
                end;

            end;
    end;

    ClearScreenSDL;
end;


procedure Academy;
    var
        intPrice, MouseItem: Integer;
        ch, dummy: string;
begin
    DecoIcon.x:=160;
    DecoIcon.y:=80;
    DecoIcon.w:=60;
    DecoIcon.h:=80;

    DecoIconS.x:=711+HiResOffsetX;
    DecoIconS.y:=400+HiResOffsetY;
    DecoIconS.w:=72;
    DecoIconS.h:=72;

    ClearScreenSDL;

    if UseSDL=true then
    begin
        LoadImage_Title(PathData+'graphics/invbg.jpg');
    end;

    repeat

        if UseSDL=true then
        begin
            BlitImage_Title;
            SDL_BLITSURFACE(extratiles, @DecoIcon, screen, @DecoIconS);
        end
        else
        begin
            DialogWin;
            StatusDeco;
        end;

        TransTextXY(1, 1, 'WELCOME TO THE PUBLIC ACADEMY. YOU HAVE ' + IntToStr(ThePlayer.longSkillPoints) + ' SKILL POINTS.');

        intPrice := (ThePlayer.intLvl*345)+(ThePlayer.longEXP div 3);
        if PlayerHasItem('V.I.P. Card')>0 then
             dec(intPrice, abs(ThePlayer.intTrade)*2);
        TransTextXY(8, 5, 'A preparation course to achieve one skill point is for '+IntToStr(intPrice)+' Cr.');
        TransTextXY(8, 6, 'Improving one skill in a regular course costs one skill point.');

        dummy := GetKeyInput('[p]reparation course   [r]egular course   [ESC] leave academy', false);

    until (dummy='p') or (dummy='r') or (dummy='ESC');

    ch := dummy;

    if ch<>'ESC' then
    begin

        if ch='p' then
            if ThePlayer.longGold > intPrice-1 then
            begin
                inc(ThePlayer.longSkillPoints);
                dec(ThePlayer.longGold, intPrice);
                GetKeyInput('You have absolved a preparation course and gained one skill point.', true);
            end
            else
                GetKeyInput('You don'+chr(39)+'t have enough money for the course.', true);

        if ch='r' then
            if ThePlayer.longSkillPoints > 0 then
            begin
                ClearScreenSDL;
                TrainSkill(ThePlayer.longSkillPoints);
                ThePlayer.longSkillPoints:=0;
            end
            else
                GetKeyInput('You don'+chr(39)+'t meet the requirements to absolve a regular course.', true);
    end;

    ClearScreenSDL;

end;



procedure ShowShop(intID: integer);
    var
        MouseItem, i, t: integer;
        n, p: longint;
        ch, dummy, equi: string;
        blRemItem: boolean;
begin
    DecoIcon.w:=80;
    DecoIcon.y:=80;
    DecoIcon.h:=80;

    case intID of
        1:
            DecoIcon.x:=360;
        2:
            DecoIcon.x:=280;
        3:
            begin
                DecoIcon.x:=220;
                DecoIcon.w:=60;
            end;
        4:
            DecoIcon.x:=0;
    end;

    DecoIconS.x:=711+HiResOffsetX;
    DecoIconS.y:=400+HiResOffsetY;
    DecoIconS.w:=72;
    DecoIconS.h:=72;

    ClearScreenSDL;

    if UseSDL=true then
    begin
        LoadImage_Title(PathData+'graphics/invbg.jpg');
    end;

    repeat

        if UseSDL=true then
        begin
            BlitImage_Title;
            SDL_BLITSURFACE(extratiles, @DecoIcon, screen, @DecoIconS);
        end
        else
        begin
            DialogWin;
            StatusDeco;
        end;


        TransTextXY(1, 1, 'WELCOME TO ' + uppercase(Shops[MyShop[intID].intType]) + '.');
        TransTextXY(62, 1, 'Credits: ' + IntToStr(ThePlayer.longGold));

        for i:=1 to 16 do
        begin
            if MyShop[intID].Inventory[i].intType > 0 then
            begin
                p := CollectBuyPrice(MyShop[intID].Inventory[i].intType);

                TransTextXY(6, 4+i, IntToStr(i));
                TransTextXY(10, 4+i, thing[MyShop[intID].Inventory[i].intType].chLetter + ' ' + thing[MyShop[intID].Inventory[i].intType].strRealName);
                TransTextXY(40, 4+i, '$' + IntToStr(p));

                equi:='';

                // mark items with + - =
                if ThePlayer.intArmour > 0 then
                begin
                    if thing[MyShop[intID].Inventory[i].intType].intAP > 0 then
                        if thing[MyShop[intID].Inventory[i].intType].blWear = true then
                        begin
                            if thing[MyShop[intID].Inventory[i].intType].intAP > thing[ThePlayer.intArmour].intAP then
                                equi:=' '+chr(214);
                            if thing[MyShop[intID].Inventory[i].intType].intAP < thing[ThePlayer.intArmour].intAP then
                                equi:=' '+chr(215);
                            if thing[MyShop[intID].Inventory[i].intType].intAP = thing[ThePlayer.intArmour].intAP then
                                equi:=' '+chr(216);
                        end;

                    if thing[MyShop[intID].Inventory[i].intType].blIdentified = false then
                        if thing[MyShop[intID].Inventory[i].intType].blWear = true then
                            equi:=' '+chr(217);
                end;

                if ThePlayer.intHat > 0 then
                begin
                    if thing[MyShop[intID].Inventory[i].intType].intAP > 0 then
                        if thing[MyShop[intID].Inventory[i].intType].blHat = true then
                        begin
                            if thing[MyShop[intID].Inventory[i].intType].intAP > thing[ThePlayer.intHat].intAP then
                                equi:=' '+chr(214);
                            if thing[MyShop[intID].Inventory[i].intType].intAP < thing[ThePlayer.intHat].intAP then
                                equi:=' '+chr(215);
                            if thing[MyShop[intID].Inventory[i].intType].intAP = thing[ThePlayer.intHat].intAP then
                                equi:=' '+chr(216);
                        end;

                    if thing[MyShop[intID].Inventory[i].intType].blIdentified = false then
                        if thing[MyShop[intID].Inventory[i].intType].blHat = true then
                            equi:=' '+chr(217);
                end;

                if ThePlayer.intFeet > 0 then
                begin
                    if thing[MyShop[intID].Inventory[i].intType].intAP > 0 then
                        if thing[MyShop[intID].Inventory[i].intType].blShoes = true then
                        begin
                            if thing[MyShop[intID].Inventory[i].intType].intAP > thing[ThePlayer.intFeet].intAP then
                                equi:=' '+chr(214);
                            if thing[MyShop[intID].Inventory[i].intType].intAP < thing[ThePlayer.intFeet].intAP then
                                equi:=' '+chr(215);
                            if thing[MyShop[intID].Inventory[i].intType].intAP = thing[ThePlayer.intFeet].intAP then
                                equi:=' '+chr(216);
                        end;

                    if thing[MyShop[intID].Inventory[i].intType].blIdentified = false then
                        if thing[MyShop[intID].Inventory[i].intType].blShoes = true then
                            equi:=' '+chr(217);
                end;

                if ThePlayer.intExtra > 0 then
                begin
                    if thing[MyShop[intID].Inventory[i].intType].intAP > 0 then
                        if thing[MyShop[intID].Inventory[i].intType].blExtra = true then
                        begin
                            if thing[MyShop[intID].Inventory[i].intType].intAP > thing[ThePlayer.intExtra].intAP then
                                equi:=' '+chr(214);
                            if thing[MyShop[intID].Inventory[i].intType].intAP < thing[ThePlayer.intExtra].intAP then
                                equi:=' '+chr(215);
                            if thing[MyShop[intID].Inventory[i].intType].intAP = thing[ThePlayer.intExtra].intAP then
                                equi:=' '+chr(216);
                        end;

                    if thing[MyShop[intID].Inventory[i].intType].blIdentified = false then
                        if thing[MyShop[intID].Inventory[i].intType].blExtra = true then
                            equi:=' '+chr(217);
                end;


                if ThePlayer.intWeapon > 0 then
                begin
                    if thing[MyShop[intID].Inventory[i].intType].intWP > 0 then
                        if thing[ThePlayer.intWeapon].intWP > 0 then
                            if thing[MyShop[intID].Inventory[i].intType].blWield = true then
                            begin
                                if thing[MyShop[intID].Inventory[i].intType].intWP > thing[ThePlayer.intWeapon].intWP then
                                    equi:=' '+chr(214);
                                if thing[MyShop[intID].Inventory[i].intType].intWP < thing[ThePlayer.intWeapon].intWP then
                                    equi:=' '+chr(215);
                                if thing[MyShop[intID].Inventory[i].intType].intWP = thing[ThePlayer.intWeapon].intWP then
                                    equi:=' '+chr(216);
                            end;

                    if thing[MyShop[intID].Inventory[i].intType].intGP > 0 then
                        if thing[ThePlayer.intWeapon].intGP > 0 then
                            if thing[MyShop[intID].Inventory[i].intType].blWield = true then
                            begin
                                if thing[MyShop[intID].Inventory[i].intType].intGP > thing[ThePlayer.intWeapon].intGP then
                                    equi:=' '+chr(214);
                                if thing[MyShop[intID].Inventory[i].intType].intGP < thing[ThePlayer.intWeapon].intGP then
                                    equi:=' '+chr(215);
                                if thing[MyShop[intID].Inventory[i].intType].intGP = thing[ThePlayer.intWeapon].intGP then
                                    equi:=' '+chr(216);
                            end;

                    if thing[MyShop[intID].Inventory[i].intType].blIdentified = false then
                        if thing[MyShop[intID].Inventory[i].intType].blWield = true then
                            equi:=' '+chr(217);
                end;

                // mark items with a higher clvl
                if ThePlayer.intLvl<Thing[MyShop[intID].Inventory[i].intType].intCharLvl then
                    equi:=' '+chr(218);

                TransTextXY(45, 4+i, equi);

                // mark items with a higher skill
                if (Thing[MyShop[intID].Inventory[i].intType].chLetter=chr(156)) then
                   if ThePlayer.intSword < Thing[MyShop[intID].Inventory[i].intType].intWP then
                       TransTextXY(52, 4+i, 'Skill!');
                if (Thing[MyShop[intID].Inventory[i].intType].chLetter=chr(159)) then
                   if ThePlayer.intAxe < Thing[MyShop[intID].Inventory[i].intType].intWP then
                       TransTextXY(52, 4+i, 'Skill!');
                if (Thing[MyShop[intID].Inventory[i].intType].chLetter=chr(164)) then
                   if ThePlayer.intWhip < Thing[MyShop[intID].Inventory[i].intType].intWP then
                       TransTextXY(52, 4+i, 'Skill!');
                if (Thing[MyShop[intID].Inventory[i].intType].chLetter=chr(158)) then
                   if ThePlayer.intGun < Thing[MyShop[intID].Inventory[i].intType].intGP then
                       TransTextXY(52, 4+i, 'Skill!');

                // mark useless items
                if (ThePlayer.intProf<>Thing[MyShop[intID].Inventory[i].intType].intProf) and (Thing[MyShop[intID].Inventory[i].intType].intProf>0) then
                begin
                    case Thing[MyShop[intID].Inventory[i].intType].intProf of
                        1:
                            TransTextXY(59, 4+i, 'Constructor!');  // obsolete since LR 1.4
                        2:
                            TransTextXY(59, 4+i, 'Enchanter!');
                        3:
                            TransTextXY(59, 4+i, 'Thief!');
                        4:
                            TransTextXY(59, 4+i, 'Archer!');
                        5:
                            TransTextXY(59, 4+i, 'Soldier!');
                    end;
                end;
            end;
        end;

        n := -1;


        dummy := GetKeyInput('[g]et an item  [l]ook at an item   Press [ESC] to leave '+ Shops[MyShop[intID].intType]+'.', false);
    until (dummy='g') or (dummy='l') or (dummy='ESC');
    ch := dummy;

    if MouseItem>0 then
        n:=MouseItem;

    if ch='l' then
    begin
        if (UseSDL=false) or (UseMouseToMove=false) then
            repeat
                n := GetNumericalInput('Enter the number of the item you wish to see [ENTER to cancel]:', 2);
            until (n=0) or ((n>0) and (n<17) and (MyShop[intID].Inventory[n].intType>0));

        if n>0 then
            ItemInfo(MyShop[intID].Inventory[n].intType);
    end;

    if ch='g' then
    begin
        if (UseSDL=false) or (UseMouseToMove=false) then
            repeat
                n := GetNumericalInput('Enter the number of the item you wish to buy [ENTER to cancel]:', 2);
            until (n=0) or ((n>0) and (n<17) and (MyShop[intID].Inventory[n].intType>0));

        if n>0 then
        begin

            t := MyShop[intID].Inventory[n].intType;
            p := thing[MyShop[intID].Inventory[n].intType].intPrice;
            if ThePlayer.intTrade>3 then
                dec(p,(1*p) div 100);
            if ThePlayer.intTrade>6 then
                dec(p,(3*p) div 100);
            if ThePlayer.intTrade>9 then
                dec(p,(6*p) div 100);
            if ThePlayer.intTrade>11 then
                dec(p,(8*p) div 100);
            if PlayerHasItem('V.I.P. Card')>0 then
                p:=p div 2;
            if p<1 then
                p:=1;

            blRemItem:=false;

            // has player enough money?
            if ThePlayer.longGold >= p then
            begin
                // check if player already has one item of this type
                for i:=1 to 16 do
                    if inventory[i].intType = t then
                    begin
                        inc(inventory[i].longNumber, Thing[Inventory[i].intType].intAmount);
                        blRemItem:=true;
                        break;
                    end;
    
                // if item not bought, check if free space available
                if blRemItem=false then
                    for i:=1 to 16 do
                        if (inventory[i].intType = 0) and (blRemItem=false) then
                        begin
                            inventory[i].intType := t;
                            inventory[i].longNumber := Thing[Inventory[i].intType].intAmount;
                            blRemItem:=true;
                            break;
                        end;
    
                // if item taken, identify item type and remove item from store
                if blRemItem then
                begin
                    PlaySFX('coins.mp3');
                    Thing[MyShop[intID].Inventory[n].intType].blIdentified := true;
                    Thing[MyShop[intID].Inventory[n].intType].strName := Thing[MyShop[intID].Inventory[n].intType].strRealName;

                    MyShop[intID].Inventory[n].intType := 0;

                    dec(ThePlayer.longGold, p);
                    ShowTransMessage('You buy the ' + Thing[t].strName + '.', false);
                end
                else
                    GetKeyInput('You want to buy the ' + Thing[t].strName + ', but your inventory is full.', true);
            end
            else
                GetKeyInput('You need more Credits to buy this.', true);

        end;
    end;

    ClearScreenSDL;
end;


// bind F-key to Item
procedure BindQuickKey_Item (n: integer);
var
   id: integer;
begin
    id:=-1;
    repeat
        id := GetNumericalInput('Which item do you want to bind to quickkey F'+IntToStr(n)+'?', 2);
    until (id=0) or ((id>0) and (id<17) and (Inventory[id].intType>0));

    if id>0 then
       if (Thing[inventory[id].intType].blEat=true) or (Thing[inventory[id].intType].blDrink=true) then
       begin
           if Thing[inventory[id].intType].blIdentified=true then
           begin
              strQuickKey[n]:=Thing[inventory[id].intType].strRealName;
           end
           else
              GetKeyInput('You can only bind identified items to quickkeys.', true);
       end
       else
           GetKeyInput('You can only bind comestibles and potions to quickkeys.', true);
end;


// bind F-key to Spell
procedure BindQuickKey_Spell (n: integer);
var
   id: integer;
begin
    id:=-1;
    repeat
        id := GetNumericalInput('Which chant do you want to bind to quickkey F'+IntToStr(n)+'?', 2);
    until (id=0) or ((id>0) and (id<13) and (Spellbook[id].intType>0));

    if id>0 then
        strQuickKey[n]:=Spell[spellbook[id].intType].strName;
end;



// show binding of F-keys to spells or items
procedure SetQuickKeys;
var
   dummy, ch : string;
begin
     // display a list of quick keys
    if UseSDL=true then
        LoadImage_Title(PathData+'graphics/invbg.jpg')
    else
            ClearScreenSDL;

    repeat
        if UseSDL=true then
        begin
            BlitImage_Title;
        end
        else
        begin
            DialogWin;
            StatusDeco;
        end;

        TransTextXY(1, 1, 'QUICKKEY BINDINGS');

        for i:=1 to 12 do
        begin
            TransTextXY(6, 3+i, 'F'+IntToStr(i));
            if strQuickKey[i]<>'-' then
                TransTextXY(10, 3+i, strQuickKey[i]);
        end;

        TransTextXY(6, 17, 'To bind an item or chant to a quickkey, go to inventory or songbook');
        TransTextXY(6, 18, 'and press one of the function keys (F1 to F12).');

        dummy := GetKeyInput('[F1] to [F12] delete binding   [ESC] resume game', false);
    until (dummy='ESC') or (dummy='F1') or (dummy='F2') or (dummy='F3') or (dummy='F4') or (dummy='F5') or (dummy='F6') or (dummy='F7') or (dummy='F8') or (dummy='F9') or (dummy='F10') or (dummy='F11') or (dummy='F12');

    ch:=dummy;

    if ch='F1' then
       strQuickKey[1]:='-';

    if ch='F2' then
       strQuickKey[2]:='-';

    if ch='F3' then
       strQuickKey[3]:='-';

    if ch='F4' then
       strQuickKey[4]:='-';

    if ch='F5' then
       strQuickKey[5]:='-';

    if ch='F6' then
       strQuickKey[6]:='-';

    if ch='F7' then
       strQuickKey[7]:='-';

    if ch='F8' then
       strQuickKey[8]:='-';

    if ch='F9' then
       strQuickKey[9]:='-';

    if ch='F10' then
       strQuickKey[10]:='-';

    if ch='F11' then
       strQuickKey[11]:='-';

    if ch='F12' then
       strQuickKey[12]:='-';

end;


// Inventory
procedure ShowInventory;
    var
        i, MouseItem: integer;
        n: longint;
        ch, equi, dummy: string;
        blRingOff, blKnowsSpell: boolean;
begin

    DecoIcon.x:=80;
    DecoIcon.y:=80;
    DecoIcon.w:=80;
    DecoIcon.h:=80;

    DecoIconS.x:=711+HiResOffsetX;
    DecoIconS.y:=400+HiResOffsetY;
    DecoIconS.w:=72;
    DecoIconS.h:=72;

    if UseSDL=true then
    begin
        LoadImage_Title(PathData+'graphics/invbg.jpg');
    end
    else
            ClearScreenSDL;

    repeat
        if UseSDL=true then
        begin
            BlitImage_Title;
            SDL_BLITSURFACE(extratiles, @DecoIcon, screen, @DecoIconS);
        end
        else
        begin
            DialogWin;
            StatusDeco;
        end;

        if UseSDL=true then
            SmallTextXY(36, 5, 'Press [F1] to [F12] to bind potions or food to the quickbar.', true);

        TransTextXY(1, 1, 'INVENTORY OF ' + uppercase(ThePlayer.strName));
        TransTextXY(62, 1, 'Credits: ' + IntToStr(ThePlayer.longGold));

        for i:=1 to 16 do
        begin
            equi := '';
            if inventory[i].intType > 0 then
            begin

                if ThePlayer.intArmour > 0 then
                begin
                    if thing[Inventory[i].intType].intAP > 0 then
                        if thing[Inventory[i].intType].blWear = true then
                        begin
                            if thing[Inventory[i].intType].intAP > thing[ThePlayer.intArmour].intAP then
                                equi:=chr(214);
                            if thing[Inventory[i].intType].intAP < thing[ThePlayer.intArmour].intAP then
                                equi:=chr(215);
                            if thing[Inventory[i].intType].intAP = thing[ThePlayer.intArmour].intAP then
                                equi:=chr(216);
                        end;

                    if thing[Inventory[i].intType].blIdentified = false then
                        if thing[Inventory[i].intType].blWear = true then
                            equi:=chr(217);
                end;

                if ThePlayer.intHat > 0 then
                begin
                    if thing[Inventory[i].intType].intAP > 0 then
                        if thing[Inventory[i].intType].blHat = true then
                        begin
                            if thing[Inventory[i].intType].intAP > thing[ThePlayer.intHat].intAP then
                                equi:=chr(214);
                            if thing[Inventory[i].intType].intAP < thing[ThePlayer.intHat].intAP then
                                equi:=chr(215);
                            if thing[Inventory[i].intType].intAP = thing[ThePlayer.intHat].intAP then
                                equi:=chr(216);
                        end;

                    if thing[Inventory[i].intType].blIdentified = false then
                        if thing[Inventory[i].intType].blHat = true then
                            equi:=chr(217);
                end;

                if ThePlayer.intFeet > 0 then
                begin
                    if thing[Inventory[i].intType].intAP > 0 then
                        if thing[Inventory[i].intType].blShoes = true then
                        begin
                            if thing[Inventory[i].intType].intAP > thing[ThePlayer.intFeet].intAP then
                                equi:=chr(214);
                            if thing[Inventory[i].intType].intAP < thing[ThePlayer.intFeet].intAP then
                                equi:=chr(215);
                            if thing[Inventory[i].intType].intAP = thing[ThePlayer.intFeet].intAP then
                                equi:=chr(216);
                        end;

                    if thing[Inventory[i].intType].blIdentified = false then
                        if thing[Inventory[i].intType].blShoes = true then
                            equi:=chr(217);
                end;

                if ThePlayer.intExtra > 0 then
                begin
                    if thing[Inventory[i].intType].intAP > 0 then
                        if thing[Inventory[i].intType].blExtra = true then
                        begin
                            if thing[Inventory[i].intType].intAP > thing[ThePlayer.intExtra].intAP then
                                equi:=chr(214);
                            if thing[Inventory[i].intType].intAP < thing[ThePlayer.intExtra].intAP then
                                equi:=chr(215);
                            if thing[Inventory[i].intType].intAP = thing[ThePlayer.intExtra].intAP then
                                equi:=chr(216);
                        end;

                    if thing[Inventory[i].intType].blIdentified = false then
                        if thing[Inventory[i].intType].blExtra = true then
                            equi:=chr(217);
                end;

                if ThePlayer.intWeapon > 0 then
                begin
                    if thing[Inventory[i].intType].intWP > 0 then
                        if thing[ThePlayer.intWeapon].intWP > 0 then
                            if thing[Inventory[i].intType].blWield = true then
                            begin
                                if thing[Inventory[i].intType].intWP > thing[ThePlayer.intWeapon].intWP then
                                    equi:=chr(214);
                                if thing[Inventory[i].intType].intWP < thing[ThePlayer.intWeapon].intWP then
                                    equi:=chr(215);
                                if thing[Inventory[i].intType].intWP = thing[ThePlayer.intWeapon].intWP then
                                    equi:=chr(216);
                            end;

                    if thing[Inventory[i].intType].intGP > 0 then
                        if thing[ThePlayer.intWeapon].intGP > 0 then
                            if thing[Inventory[i].intType].blWield = true then
                            begin
                                if thing[Inventory[i].intType].intGP > thing[ThePlayer.intWeapon].intGP then
                                    equi:=chr(214);
                                if thing[Inventory[i].intType].intGP < thing[ThePlayer.intWeapon].intGP then
                                    equi:=chr(215);
                                if thing[Inventory[i].intType].intGP = thing[ThePlayer.intWeapon].intGP then
                                    equi:=chr(216);
                            end;

                    if thing[Inventory[i].intType].blIdentified = false then
                        if thing[Inventory[i].intType].blWield = true then
                            equi:=chr(217);
                end;

                // mark items with a higher clvl
                if ThePlayer.intLvl<Thing[Inventory[i].intType].intCharLvl then
                    equi:=chr(218);

                if (thing[inventory[i].intType].blWield) and (ThePlayer.intWeapon=inventory[i].intType) then
                    equi:='[wielded]';
                if (thing[inventory[i].intType].blWear) and (ThePlayer.intArmour=inventory[i].intType) then
                    equi:='[worn]';
                if (thing[inventory[i].intType].blHat) and (ThePlayer.intHat=inventory[i].intType) then
                    equi:='[worn]';
                if (thing[inventory[i].intType].blShoes) and (ThePlayer.intFeet=inventory[i].intType) then
                    equi:='[worn]';
                if (thing[inventory[i].intType].blExtra) and (ThePlayer.intExtra=inventory[i].intType) then
                    equi:='[wielded]';
                if (thing[inventory[i].intType].blRing) and (ThePlayer.intRingLeft=inventory[i].intType) then
                    equi:='[worn] ';
                if (thing[inventory[i].intType].blRing) and (ThePlayer.intRingRight=inventory[i].intType) then
                    equi:=equi + '[worn]';

                TransTextXY(6, 4+i, IntToStr(i));
                TransTextXY(10, 4+i, thing[inventory[i].intType].chLetter + ' ' + thing[inventory[i].intType].strName);
                TransTextXY(43, 4+i, 'x' + IntToStr(inventory[i].longNumber));
                TransTextXY(47, 4+i, equi);
            end;
        end;

        n := -1;

        dummy := GetKeyInput('[e]quip  [r]emove  [d]rop  [l]ook  [s]tudy  [c]onsume  [ESC] cancel', false);
    until (dummy='c') or (dummy='r') or (dummy='d') or (dummy='e') or (dummy='l') or (dummy='ESC') or (dummy='s') or (dummy='F1') or (dummy='F2') or (dummy='F3') or (dummy='F4') or (dummy='F5') or (dummy='F6') or (dummy='F7') or (dummy='F8') or (dummy='F9') or (dummy='F10') or (dummy='F11') or (dummy='F12');
    ch := dummy;


    if MouseItem>0 then
        n:=MouseItem;

    // define quick keys
    if ch='F1' then
       BindQuickKey_Item(1);

    if ch='F2' then
       BindQuickKey_Item(2);

    if ch='F3' then
       BindQuickKey_Item(3);

    if ch='F4' then
       BindQuickKey_Item(4);

    if ch='F5' then
       BindQuickKey_Item(5);

    if ch='F6' then
       BindQuickKey_Item(6);

    if ch='F7' then
       BindQuickKey_Item(7);

    if ch='F8' then
       BindQuickKey_Item(8);

    if ch='F9' then
       BindQuickKey_Item(9);

    if ch='F10' then
       BindQuickKey_Item(10);

    if ch='F11' then
       BindQuickKey_Item(11);

    if ch='F12' then
       BindQuickKey_Item(12);

    // equip item
    if ch='e' then
    begin
        if (UseSDL=false) or (UseMouseToMove=false) then
            repeat
                n := GetNumericalInput('Which item do you want to equip?', 2);
            until (n=0) or ((n>0) and (n<17) and (Inventory[n].intType>0));

        if n>0 then
        begin

            // check for identification
            if Thing[inventory[n].intType].blIdentified = false then
            begin
                GetKeyInput('You can only equip identified items.', true);
                n:=0;
            end
            else
            begin
                // check sex
                if (thing[inventory[n].intType].intSex=0) or (ThePlayer.intSex = thing[inventory[n].intType].intSex) then
                begin
                    // check character level
                    if ThePlayer.intLvl > thing[inventory[n].intType].intCharLvl-1 then
                    begin
                        // check character profession
                        if (Thing[inventory[n].intType].intProf=0) or (ThePlayer.intProf=Thing[inventory[n].intType].intProf) then
                        begin
                            // check if item is equipable
                            if (thing[inventory[n].intType].blWear=false) and (thing[inventory[n].intType].blWield=false) and (thing[inventory[n].intType].blHat=false) and (thing[inventory[n].intType].blRing=false) and (thing[inventory[n].intType].blShoes=false) and (thing[inventory[n].intType].blExtra=false) then
                            begin
                                GetKeyInput ('You can'+chr(39)+'t equip this item.', true);
                            end
                            else
                            begin
    
                                // ** armour, clothes **
                                if thing[inventory[n].intType].blWear then
                                begin
                                    ShowTransMessage ('You wear the ' + thing[inventory[n].intType].strName + '.', false);
                                    ThePlayer.intArmour := inventory[n].intType;
                                end;
    
                                // ** weapon **
                                if thing[inventory[n].intType].blWield then
                                begin
                                    // two hand weapons?
                                    if Thing[inventory[n].intType].blTwoHands=true then
                                    begin
                                        if ThePlayer.intExtra > 0 then
                                        begin
                                            ShowTransMessage('You remove the '+Thing[ThePlayer.intExtra].strName+' first.', false);
                                            ThePlayer.intExtra:=0;
                                        end;
                                    end;
    
                                    ShowTransMessage ('You wield the ' + thing[inventory[n].intType].strName + '.', false);
                                    ThePlayer.intWeapon := inventory[n].intType;
    
                                    If Thing[inventory[n].intType].strName='Northdoom' then
                                    begin
                                        ThePlayer.blEvil:=true;
                                        ThePlayer.intHumility:=-6;
                                        ThePlayer.longPrayers:=0;
                                        GetKeyInput('Suddenly it becomes death-cold ...', true);
                                        ShowPlot(14);
                                        ShowPlot(18);
                                        ShowTransMessage('You have entered a dark path with no way back.', false);
                                    end;
    
                                end;
    
                                // ** hat **
                                if thing[inventory[n].intType].blHat then
                                begin
                                    ShowTransMessage ('You put the ' + thing[inventory[n].intType].strName + ' on your head.', false);
                                    ThePlayer.intHat := inventory[n].intType;
                                end;
    
                                if thing[inventory[n].intType].blRing then
                                begin
                                    if ThePlayer.intRingLeft = 0 then
                                    begin
                                        ShowTransMessage ('You put the ' + thing[inventory[n].intType].strName + ' on your left finger.', false);
                                        ThePlayer.intRingLeft := inventory[n].intType;
                                    end
                                    else
                                    if ThePlayer.intRingRight = 0 then
                                    begin
                                        ShowTransMessage ('You put the ' + thing[inventory[n].intType].strName + ' on your right finger.', false);
                                        ThePlayer.intRingRight := inventory[n].intType;
                                    end;
                                end;
    
                                // ** shield **
                                if thing[inventory[n].intType].blExtra then
                                begin
    
                                    // two hand weapon wielded?
                                    if ThePlayer.intWeapon>0 then
                                    begin
                                        if Thing[ThePlayer.intWeapon].blTwoHands=true then
                                        begin
                                            ShowTransMessage('You unwield the ' + Thing[ThePlayer.intWeapon].strName + ' first.', false);
                                            ThePlayer.intWeapon:=0;
                                        end;
                                    end;
    
                                    ShowTransMessage('You take the ' + thing[inventory[n].intType].strName + ' in the right hand.', false);
                                    ThePlayer.intExtra := inventory[n].intType;
                                end;
    
                                // ** shoes **
                                if thing[inventory[n].intType].blShoes then
                                begin
                                    ShowTransMessage('You put the ' + thing[inventory[n].intType].strName + ' on your feet.', false);
                                    ThePlayer.intFeet := inventory[n].intType;
                                end;
                            end;
                        end
                        else
                            GetKeyInput ('To equip that item, you have to work as '+PlayProf[Thing[inventory[n].intType].intProf]+'.', true);
                    end
                    else
                        GetKeyInput ('You need at least level ' + IntToStr(thing[inventory[n].intType].intCharLvl) + ' to use this item.', true);
                end
                else
                begin
                    if thing[inventory[n].intType].intSex = 1 then
                        GetKeyInput ('Only men are able to equip this item.', true)
                    else
                        GetKeyInput ('Only women may equip this item.', true);
                end;
            end;
        end;
    end;

    // remove equipped item
    if ch='r' then
    begin
        if (UseSDL=false) or (UseMouseToMove=false) then
            repeat
                n := GetNumericalInput('Which currently equipped item do you want to remove?', 2);
            until (n=0) or ((n>0) and (n<17) and (Inventory[n].intType>0));

        if n>0 then
        begin
            if ThePlayer.blCursed=false then
            begin
                if (Thing[inventory[n].intType].blWear) and (ThePlayer.intArmour = inventory[n].intType) then
                begin
                    ShowTransMessage ('You remove the ' + Thing[inventory[n].intType].strName + '.', false);
                    ThePlayer.intArmour := 0;
                end;
                if (Thing[inventory[n].intType].blWield) and (ThePlayer.intWeapon = inventory[n].intType) then
                begin
                    ShowTransMessage ('You remove the ' + Thing[inventory[n].intType].strName + '.', false);
                    ThePlayer.intWeapon := 0;
                end;
                if (Thing[inventory[n].intType].blHat) and (ThePlayer.intHat = inventory[n].intType) then
                begin
                    ShowTransMessage ('You take the ' + Thing[inventory[n].intType].strName + ' off your head.', false);
                    ThePlayer.intHat := 0;
                end;
                if (Thing[inventory[n].intType].blShoes) and (ThePlayer.intFeet = inventory[n].intType) then
                begin
                    ShowTransMessage ('You take the ' + Thing[inventory[n].intType].strName + ' off your feet.', false);
                    ThePlayer.intFeet := 0;
                end;
                if (Thing[inventory[n].intType].blExtra) and (ThePlayer.intExtra = inventory[n].intType) then
                begin
                    ShowTransMessage ('You remove the ' + Thing[inventory[n].intType].strName + '.', false);
                    ThePlayer.intExtra := 0;
                end;
                blRingOff:=false;
                if (Thing[inventory[n].intType].blRing) and (ThePlayer.intRingLeft = inventory[n].intType) then
                begin
                    ShowTransMessage ('You remove the left ' + Thing[inventory[n].intType].strName + '.', false);
                    ThePlayer.intRingLeft := 0;
                    blRingOff:=true;
                end;
                if (blRingOff=false) and (Thing[inventory[n].intType].blRing) and (ThePlayer.intRingRight = inventory[n].intType) then
                begin
                    ShowTransMessage ('You remove the right ' + Thing[inventory[n].intType].strName + '.', false);
                    ThePlayer.intRingRight := 0;
                end;
            end
            else
                GetKeyInput('Something prevents you from removing the item.', true);
        end;
    end;

    // consume item
    if ch='c' then
    begin
        if (UseSDL=false) or (UseMouseToMove=false) then
            repeat
                n := GetNumericalInput('Which item do you want to consume?', 2);
            until (n=0) or ((n>0) and (n<17) and (Inventory[n].intType>0));

        if n>0 then
            if (thing[inventory[n].intType].blEat = true) or (thing[inventory[n].intType].blDrink = true) then
            begin
                if (ThePlayer.intWeapon = inventory[n].intType) or (ThePlayer.intArmour = inventory[n].intType) or (ThePlayer.intHat = inventory[n].intType) or (ThePlayer.intFeet = inventory[n].intType) or (ThePlayer.intExtra = inventory[n].intType) then
                    GetKeyInput('You must remove the ' + thing[inventory[n].intType].strName + ' before eating it.', true)
                else
                begin
                    // identify item
                    Thing[inventory[n].intType].blIdentified := true;
                    Thing[inventory[n].intType].strName := Thing[inventory[n].intType].strRealName;

                    if thing[inventory[n].intType].blEat = true then
                        ShowTransMessage('You eat the ' + thing[inventory[n].intType].strName + '.', false)
                    else
                        ShowTransMessage('You drink the ' + thing[inventory[n].intType].strName + '.', false);
                    DoEffect(thing[inventory[n].intType].intEffect, thing[inventory[n].intType].intRange, thing[inventory[n].intType].strEfText);

                    dec(inventory[n].longNumber);
                    if inventory[n].longNumber = 0 then
                        inventory[n].intType := 0;
                end;
            end
            else
                GetKeyInput('You cannot eat or drink this.', true);
    end;

    // simply drop item to ground or into well or on altar
    if (ch='d') and (DngLvl[ThePlayer.intX,ThePlayer.intY].intBuilding=0) then
    begin
        if (UseSDL=false) or (UseMouseToMove=false) then
            repeat
                n := GetNumericalInput('Which item do you want to drop?', 2);
            until (n=0) or ((n>0) and (n<17) and (Inventory[n].intType>0));

        if n>0 then
        begin
            // remove equipped items before dropping
            if Inventory[n].longNumber=1 then
            begin
                if ThePlayer.intWeapon=Inventory[n].intType then
                    ThePlayer.intWeapon:=0;
                if ThePlayer.intArmour=Inventory[n].intType then
                    ThePlayer.intArmour:=0;
                if ThePlayer.intHat=Inventory[n].intType then
                    ThePlayer.intHat:=0;
                if ThePlayer.intFeet=Inventory[n].intType then
                    ThePlayer.intFeet:=0;
                if ThePlayer.intExtra=Inventory[n].intType then
                    ThePlayer.intExtra:=0;
                if ThePlayer.intRingLeft=Inventory[n].intType then
                    ThePlayer.intRingLeft:=0;
                if ThePlayer.intRingRight=Inventory[n].intType then
                    ThePlayer.intRingRight:=0;
            end;

            // sacrifice item to god
            if DngLvl[ThePlayer.intX,ThePlayer.intY].intFloorType=15 then
            begin
                ShowTransMessage('You'+chr(39)+'ve sacrified the item to '+ThePlayer.strReli+'.', false);

                inc(ThePlayer.longPrayers, Thing[inventory[n].intType].intPrice div 4);

                // if item "Scroll of Hope", drop "Pandora's Ring"
                if Thing[inventory[n].intType].strName='Scroll of Hope' then
                    DngLvl[ThePlayer.intX,ThePlayer.intY].intItem:=ReturnItemByName('Pandora'+chr(39)+'s Ring');

                // if item "Rune of Muin" (DLV 12), heal HP and PP
                if Thing[inventory[n].intType].strName='Rune of Muin' then
                begin
                    ThePlayer.intHP := ThePlayer.intMaxHP;
                    ThePlayer.intPP := ThePlayer.intMaxPP;
                    GetKeyInput('Suddenly, you are fully healed.', true);
                end;

                // if item "Rune of hUath" (DLV 3), drop "Cola"
                if Thing[inventory[n].intType].strName='Rune of hUath' then
                    DngLvl[ThePlayer.intX,ThePlayer.intY].intItem:=ReturnItemByName('Cola');

                // if item "Rune of Beithe" (DLV 1), drop "Bottle of Water"
                if Thing[inventory[n].intType].strName='Rune of Beithe' then
                    DngLvl[ThePlayer.intX,ThePlayer.intY].intItem:=ReturnItemByName('Bottle of Water');

                // if item "Rune of Ailm" (DLV 17), get a skill point
                if Thing[inventory[n].intType].strName='Rune of Ailm' then
                begin
                    inc(ThePlayer.longSkillPoints);
                    ShowTransMessage('You feel wiser.', false);
                end;

                // if "Pandora's Ring", drop "100 Credits"
                if Thing[inventory[n].intType].strName='Pandora'+chr(39)+'s Ring' then
                    DngLvl[ThePlayer.intX,ThePlayer.intY].intItem:=ReturnItemByName('100 Credits');


                // if "Crystal of Revenge", create portal through time
                if DungeonLevel=1 then
                    if Thing[inventory[n].intType].strName='Crystal of "Revenge"' then
                    begin
                        PlaySFX('page-turn.mp3');
                        ShowText('noldarur');
                        ShowPlot(25);
                        DngLvl[ThePlayer.intX,ThePlayer.intY-1].intFloorType:=59;
                    end;

                // if "Crystal of Inferno", drop "Fire Blade of Thagor"
                if Thing[inventory[n].intType].strName='Crystal of "Inferno"' then
                    DngLvl[ThePlayer.intX,ThePlayer.intY].intItem:=ReturnItemByName('Fire Blade of Thagor');

                // if "Beer", drop "Book of Joy"
                if Thing[inventory[n].intType].strName='Beer' then
                    DngLvl[ThePlayer.intX,ThePlayer.intY].intItem:=ReturnItemByName('Book of Joy');

                dec(inventory[n].longNumber,Thing[inventory[n].intType].intAmount);
                if inventory[n].longNumber < 1 then
                begin
                    inventory[n].intType := 0;
                    inventory[n].longNumber := 0;
                end;
            end;

            // drop item into well
            if DngLvl[ThePlayer.intX,ThePlayer.intY].intFloorType=28 then
            begin
                ShowTransMessage('You'+chr(39)+'ve dropped the item into the well.', false);

                // if Unique Item "Tears of War" dropped, drop "Book of Tears"
                if Thing[inventory[n].intType].strName='Tears of War' then
                    DngLvl[ThePlayer.intX,ThePlayer.intY].intItem:=ReturnItemByName('Book of Tears');

                // if "Ring of Wisdom" dropped, drop "Pandora's Ale"
                if Thing[inventory[n].intType].strName='Ring of Wisdom' then
                    DngLvl[ThePlayer.intX,ThePlayer.intY].intItem:=ReturnItemByName('Pandora'+chr(39)+'s Ale');

                // if "Rosegarden's seductive perfume" dropped, drop "Leviathan's blood"
                if Thing[inventory[n].intType].strName='Rosegarden'+chr(39)+'s seductive perfume' then
                    DngLvl[ThePlayer.intX,ThePlayer.intY].intItem:=ReturnItemByName('Leviathan'+chr(39)+'s Blood');

                // if "Bottle of Water" dropped, drop "Antidot"
                if Thing[inventory[n].intType].strName='Bottle of Water' then
                    DngLvl[ThePlayer.intX,ThePlayer.intY].intItem:=ReturnItemByName('Antidot');

                // if "Antidot" dropped, drop "Cola"
                if Thing[inventory[n].intType].strName='Antidot' then
                    DngLvl[ThePlayer.intX,ThePlayer.intY].intItem:=ReturnItemByName('Cola');

                // if "Identificator" dropped, increase invisible counter by 5
                if Thing[inventory[n].intType].strName='Identificator' then
                    inc(ThePlayer.intInvis, 5);

                // if "Cola" dropped, drop "Amphetamine"
                if Thing[inventory[n].intType].strName='Cola' then
                    DngLvl[ThePlayer.intX,ThePlayer.intY].intItem:=ReturnItemByName('Amphetamine');

                // if "Amphetamine" dropped, drop "Phiol of Agility"
                if Thing[inventory[n].intType].strName='Amphetamine' then
                    DngLvl[ThePlayer.intX,ThePlayer.intY].intItem:=ReturnItemByName('Phiol of Agility');

                // if "Dragon Poison" dropped, drop "Penicillin"
                if Thing[inventory[n].intType].strName='Dragon Poison' then
                    DngLvl[ThePlayer.intX,ThePlayer.intY].intItem:=ReturnItemByName('Penicillin');

                // if "Beer" dropped, drop "Dragon Poison"
                if Thing[inventory[n].intType].strName='Beer' then
                    DngLvl[ThePlayer.intX,ThePlayer.intY].intItem:=ReturnItemByName('Dragon Poison');

                // if "Dust" dropped, drop "Coffee"
                if Thing[inventory[n].intType].strName='Dust' then
                    DngLvl[ThePlayer.intX,ThePlayer.intY].intItem:=ReturnItemByName('Coffee');

                // if "Coffee" dropped, drop "Vitari"
                if Thing[inventory[n].intType].strName='Coffee' then
                    DngLvl[ThePlayer.intX,ThePlayer.intY].intItem:=ReturnItemByName('Vitari');

                // if "Vitari" dropped, drop "Dust"
                if Thing[inventory[n].intType].strName='Vitari' then
                    DngLvl[ThePlayer.intX,ThePlayer.intY].intItem:=ReturnItemByName('Dust');

                // if "Phiol of Agility" dropped, drop "Bottle of Water"
                if Thing[inventory[n].intType].strName='Phiol of Agility' then
                    DngLvl[ThePlayer.intX,ThePlayer.intY].intItem:=ReturnItemByName('Bottle of Water');

                // if "Drink of Profanation" dropped, drop "Junoblood"
                if Thing[inventory[n].intType].strName='Drink of Profanation' then
                    DngLvl[ThePlayer.intX,ThePlayer.intY].intItem:=ReturnItemByName('Junoblood');

                dec(inventory[n].longNumber,Thing[inventory[n].intType].intAmount);
                if inventory[n].longNumber < 1 then
                begin
                    inventory[n].intType := 0;
                    inventory[n].longNumber := 0;
                end;
            end;

            if (DngLvl[ThePlayer.intX,ThePlayer.intY].intFloorType<>15) and (DngLvl[ThePlayer.intX,ThePlayer.intY].intFloorType<>28) then
            begin
                if DngLvl[ThePlayer.intX, ThePlayer.intY].intItem=0 then
                begin
                    // drop packed items...
                    if Thing[inventory[n].intType].intAmount>1 then
                        // ...only if they are a complete package
                        if inventory[n].longNumber >= Thing[inventory[n].intType].intAmount then
                        begin
                            DngLvl[ThePlayer.intX,ThePlayer.intY].intItem := inventory[n].intType;
                            dec(inventory[n].longNumber,Thing[inventory[n].intType].intAmount);

                            if inventory[n].longNumber < 1 then
                            begin
                                inventory[n].intType := 0;
                                inventory[n].longNumber := 0;
                            end;
                        end
                        else
                            GetKeyInput('You can only drop complete packages.', true)
                    else
                    begin

                        // Northdoom dropped on lava?
                        if DngLvl[ThePlayer.intX,ThePlayer.intY].intFloorType=20 then
                        begin
                            if Thing[inventory[n].intType].strName='Northdoom' then
                            begin
                                ShowPlot(19);
                                inc(ThePlayer.longExp, 1250);
                                inc(ThePlayer.longScore, 500);
                            end;
                        end
                        // normal drop
                        else
                            DngLvl[ThePlayer.intX,ThePlayer.intY].intItem := inventory[n].intType;


                        // barricade
                        if thing[inventory[n].intType].blBarricade = true then
                        begin
                            DngLvl[ThePlayer.intX,ThePlayer.intY].intIntegrity:=100;
                            DngLvl[ThePlayer.intX,ThePlayer.intY].intItem:=0;
                            DngLvl[ThePlayer.intX,ThePlayer.intY].intFloorType:=6;
                        end;
                        // monster trap
                        if thing[inventory[n].intType].blTrap = true then
                        begin
                            DngLvl[ThePlayer.intX,ThePlayer.intY].intIntegrity:=0;
                            DngLvl[ThePlayer.intX,ThePlayer.intY].intItem:=0;
                            DngLvl[ThePlayer.intX,ThePlayer.intY].intFloorType:=16;
                        end;

                        dec(inventory[n].longNumber,Thing[inventory[n].intType].intAmount);

                        if inventory[n].longNumber < 1 then
                        begin
                            inventory[n].intType := 0;
                            inventory[n].longNumber := 0;
                        end;
                    end;
                end
                else
                    GetKeyInput('There is no space on the floor.', true);
            end;
        end;
    end;

    // drop item on trader --> sell it
    if (ch='d') and (DngLvl[ThePlayer.intX,ThePlayer.intY].intBuilding>0) then
    begin
        if (UseSDL=false) or (UseMouseToMove=false) then
            repeat
                n := GetNumericalInput('Enter the number of the item to sell [ENTER to cancel]:', 2);
            until (n=0) or ((n>0) and (n<17) and (Inventory[n].intType>0));

        if n>0 then
        begin
            // remove equipped items before dropping
            if Inventory[n].longNumber=1 then
            begin
                if ThePlayer.intWeapon=Inventory[n].intType then
                    ThePlayer.intWeapon:=0;
                if ThePlayer.intArmour=Inventory[n].intType then
                    ThePlayer.intArmour:=0;
                if ThePlayer.intHat=Inventory[n].intType then
                    ThePlayer.intHat:=0;
                if ThePlayer.intFeet=Inventory[n].intType then
                    ThePlayer.intFeet:=0;
                if ThePlayer.intExtra=Inventory[n].intType then
                    ThePlayer.intExtra:=0;
                if ThePlayer.intRingLeft=Inventory[n].intType then
                    ThePlayer.intRingLeft:=0;
                if ThePlayer.intRingRight=Inventory[n].intType then
                    ThePlayer.intRingRight:=0;
            end;

            if (DngLvl[ThePlayer.intX,ThePlayer.intY].intBuilding > 0) then
            begin
                if DngLvl[ThePlayer.intX,ThePlayer.intY].intBuilding<>8 then
                begin
                    // sell

                    inc(ThePlayer.longGold, CollectSellPrice(Inventory[n].intType));
                    dec(inventory[n].longNumber,Thing[inventory[n].intType].intAmount);

                    ShowTransMessage('You'+chr(39)+'ve sold the '+Thing[inventory[n].intType].strName+' for '+IntToStr(CollectSellPrice(Inventory[n].intType))+' Credits.', false);

                    if inventory[n].longNumber < 1 then
                    begin
                        inventory[n].intType := 0;
                        inventory[n].longNumber := 0;
                    end;
                end
                else
                begin
                    // disassemble

                    dec(inventory[n].longNumber,Thing[inventory[n].intType].intAmount);

                    DisassembleItem(inventory[n].intType);

                    ShowTransMessage('You'+chr(39)+'ve disassembled the '+Thing[inventory[n].intType].strName+' in the factory.', false);

                    if inventory[n].longNumber < 1 then
                    begin
                        inventory[n].intType := 0;
                        inventory[n].longNumber := 0;
                    end;
                end;
            end
            else
                GetKeyInput('You need to go to a trader if you want to sell items.', true);
        end;
    end;

    // look at item
    if ch='l' then
    begin
        if (UseSDL=false) or (UseMouseToMove=false) then
            repeat
                n := GetNumericalInput('Which item do you want to examine?', 2);
            until (n=0) or ((n>0) and (n<17) and (Inventory[n].intType>0));

        if n>0 then
        begin
            // check for identification
            if Thing[inventory[n].intType].blIdentified = false then
            begin
                GetKeyInput('You know nothing about this unidentified item.', true);
                n:=0;
            end
            else
                ItemInfo (inventory[n].intType);
        end;
    end;

    // study item -> learn chant or read text
    if ch='s' then
    begin
        if (UseSDL=false) or (UseMouseToMove=false) then
            repeat
                n := GetNumericalInput('Which item do you want to study?', 2);
//             Writeln('Selection: '+IntToStr(n));
            until (n=0) or ((n>0) and (n<17) and (Inventory[n].intType>0));

        if n>0 then
        begin

            // check for identification
            if Thing[inventory[n].intType].blIdentified = false then
            begin
                GetKeyInput('You can only study identified items.', true);
                n:=0;
            end
            else
            begin
                if inventory[n].longNumber=0 then
                begin
                    GetKeyInput('You did not select an item.',true);
                end
                else
                begin
                    // not readable, not learnable
                    if (Thing[inventory[n].intType].strTextfile='-') and (Thing[inventory[n].intType].chLetter <> chr(193)) and (Thing[inventory[n].intType].chLetter <> chr(161)) then
                        GetKeyInput('This item can'+chr(39)+'t be studied.', true);

                    // paper / book
                    if (Thing[inventory[n].intType].strTextfile<>'-') or (Thing[inventory[n].intType].chLetter = chr(193)) then
                    begin
                        PlaySFX('page-turn.mp3');
                        ShowText(Thing[inventory[n].intType].strTextfile);
                    end;

                    // magic crystal
                    if Thing[inventory[n].intType].chLetter = chr(161) then
                    begin

                        if (Thing[inventory[n].intType].intProf=0) or (ThePlayer.intProf=Thing[inventory[n].intType].intProf) then
                        begin
                            if ThePlayer.intLvl > Thing[inventory[n].intType].intCharLvl - 1 then
                            begin
                                blKnowsSpell:=false;
                                // check if spellbook has still space
                                if spellbook[12].intType=0 then
                                begin
                                    // check if player already knows this spell
                                    for i:=1 to 12 do
                                        if spellbook[i].intType = Thing[inventory[n].intType].intSpellID then
                                        begin
                                            blKnowsSpell:=true;
                                            inc(spellbook[i].intKnown);
                                            ShowTransMessage ('You perform better in chanting "' + Thing[inventory[n].intType].strLearnChant + '".', false);
                                            break;
                                        end;

                                    // if spell not known, check if free space available
                                    if blKnowsSpell=false then
                                        for i:=1 to 12 do
                                            if (spellbook[i].intType = 0) then
                                            begin
                                                spellbook[i].intKnown := 1;
                                                spellbook[i].intType := Thing[inventory[n].intType].intSpellID;
                                                spellbook[i].intRefresh:=Spell[spellbook[i].intType].intRefresh;
                                                ShowTransMessage ('You have learned the song "' + Thing[inventory[n].intType].strLearnChant + '".', false);

                                                // enchanter becomes mage if 6th spell is learned
                                                if ThePlayer.intProf=2 then // enchanter
                                                    if ThePlayer.intDipl[6]=0 then // not mage yet
                                                        if i=6 then
                                                        begin
                                                            ThePlayer.intDipl[6]:=1;
                                                            ShowTransMessage('You have been promoted to the rank "Mage".', true);
                                                        end;

                                                break;
                                            end;

        //                             writeln('Spell obtained / practiced.');

                                    // remove used crystal from inventory
                                    dec(inventory[n].longNumber);
                                    if inventory[n].longNumber = 0 then
                                        inventory[n].intType := 0;

        //                             writeln('Crystal removed.');

                                end
                                else
                                    GetKeyInput ('Unfortunately your songbook is full.', true);

                            end
                            else
                                GetKeyInput ('You need at least level ' + IntToStr(thing[inventory[n].intType].intCharLvl) + ' to learn this song.', true);
                        end
                        else
                            GetKeyInput ('This crystal cannot be studied by someone with your profession.', true);
                    end;

                end;

            end;

        end;

    end;

    //ClearScreenSDL;
    ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
end;


// chant song number n from songbook
procedure ChantSong (n: integer);
var
    t, e, c: integer;
begin

    t:=spellbook[n].intType;

    if t>0 then
    begin
        if (ThePlayer.blCursed=true) or (ThePlayer.intCalm > 0) or ((DngLvl[ThePlayer.intX,ThePlayer.intY].intFloorType = 11) and (ThePlayer.blBlessed=false)) then
            ShowTransMessage ('Your magic skills are blocked; you cannot chant songs right now.', false)
        else
        begin
            if ThePlayer.intPP >= Spell[t].intPP + spellbook[n].intKnown then
            begin
                if Spellbook[n].intRefresh=Spell[t].intRefresh then
                begin

                    // song efficiency
                    if CheckEffect(37)=true then
                        e := 2 * (Spell[t].intRange + (Spellbook[n].intKnown * Spellbook[n].intKnown))   // maximized magic
                    else
                        e := Spell[t].intRange + (Spellbook[n].intKnown * Spellbook[n].intKnown);

                    // song costs
                    c := Spell[t].intPP + (2*spellbook[n].intKnown);
                    dec(ThePlayer.intPP, c);  // always reduce the PP, even if spell fails!

                    if (random(ThePlayer.intChant*1500) > CONST_SPELLRATE) or (ThePlayer.blBlessed=true) then
                    begin
                        LastSong:=n;
                        ShowTransMessage('You chant the song "'+Spell[t].strName+'".', false);
                        DoEffect(Spell[t].intEffect, e, Spell[t].strEfText);
                        spellbook[n].intRefresh:=0;
                    end
                    else
                    begin
                        PlaySFX('magic-fail.mp3');
                        ShowTransMessage ('You failed to chant the song correctly.', false);
                    end;
                end
                else
                    ShowTransMessage ('This song has not yet recovered.', false);
            end
            else
                ShowTransMessage ('You need more PP to chant this song.', false);

        end;
    end;
    ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
end;


// This procedure takes the number of the pressed F-key and then decides whether to drink/eat an item or to call ChantSong
procedure QuickKeys (n: Integer);
var
   i, id, invID:integer;
   blIsSpell: boolean;
begin
    blIsSpell:=false;
    id := -1;
    invID := -1;

    // okay ... we got the number of the function key, now scan all items and spells
    // if there is a matching name to the name in the function key array
    for i:=1 to ChantCount do
        if Spell[i].strName = strQuickKey[n] then
        begin
             id:=i;
             blIsSpell:=true;
        end;

    if blIsSpell=false then
       for i:=1 to ItemCount do
           if Thing[i].strRealName = strQuickKey[n] then
              id:=i;


    // Spell or Item found?
    if id>-1 then
        if blIsSpell=true then
        begin
             // get ID of Spell in Songbook
             for i:=1 to 12 do
                 if SpellBook[i].intType = id then
                    invID:=i;
             ChantSong(invID);  // execute Spell
             ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);

             if UseSDL=true then
                 SDL_UPDATERECT(screen,0,0,0,0)
             else
                 UpdateScreen(true);
        end
        else
        begin
             // get ID of Item in Inventory
             for i:=1 to 16 do
                 if Inventory[i].intType = id then
                    invID:=i;

             // only use item if we have a real item invID
             if invID>-1 then
                 if inventory[invID].longNumber>0 then // invID only guarantees that there is an item referenced in invslot, but this ID can also be less than 1
                 begin
                     // although only identified items can be bound to quick keys, make sure to identify item ...
                     Thing[inventory[invID].intType].blIdentified := true;
                     Thing[inventory[invID].intType].strName := Thing[inventory[invID].intType].strRealName;

                     // execute item effect
                     DoEffect(thing[inventory[invID].intType].intEffect, thing[inventory[invID].intType].intRange, thing[inventory[invID].intType].strEfText);

                     // decrease amount of item in inventory
                     dec(inventory[invID].longNumber);
                     if inventory[invID].longNumber = 0 then
                         inventory[invID].intType := 0;

                     ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
                     if UseSDL=true then
                         SDL_UPDATERECT(screen,0,0,0,0)
                     else
                         UpdateScreen(true);
                 end;
        end;
end;



// This is the spellbook
procedure ShowSpellbook;
    var
        i, MouseItem: integer;
        n: longint;
        ch, dummy: string;
begin

    PlaySFX('page-turn.mp3');

    DecoIcon.x:=0;
    DecoIcon.y:=80;
    DecoIcon.w:=80;
    DecoIcon.h:=80;

    DecoIconS.x:=711+HiResOffsetX;
    DecoIconS.y:=400+HiResOffsetY;
    DecoIconS.w:=72;
    DecoIconS.h:=72;

    if UseSDL=true then
    begin
        LoadImage_Title(PathData+'graphics/invbg.jpg');
    end
    else
        ClearScreenSDL;


    repeat

        if UseSDL=true then
        begin
            BlitImage_Title;
            SDL_BLITSURFACE(extratiles, @DecoIcon, screen, @DecoIconS);
        end
        else
        begin
            DialogWin;
            StatusDeco;
        end;

        if UseSDL=true then
            SmallTextXY(36, 5, 'Press [F1] to [F12] to bind magical songs to the quickbar.', true);

        TransTextXY(1, 1, 'SONGBOOK OF ' + uppercase(ThePlayer.strName));
        TransTextXY(67, 1, 'PP ' + IntToStr(ThePlayer.intPP)+'/'+IntToStr(ThePlayer.intMaxPP));

        for i:=1 to 12 do
            if spellbook[i].intKnown > 0 then
            begin
                TransTextXY(6, 4+i, IntToStr(i));
                TransTextXY(10, 4+i, '"'+Spell[spellbook[i].intType].strName + '", level '+IntToStr(spellbook[i].intKnown));
                TransTextXY(35, 4+i, 'PP: -' + IntToStr(Spell[spellbook[i].intType].intPP + (2*spellbook[i].intKnown)));

                if spellbook[i].intRefresh < Spell[spellbook[i].intType].intRefresh then
                    TransTextXY(45, 4+i, ' ' + IntToStr(Spell[spellbook[i].intType].intRefresh - spellbook[i].intRefresh) + ' turn(s) for refresh');
            end;

        n := -1;

       dummy := GetKeyInput('[c]hant   [i]nfo   [ESC] to cancel', false);

    until (dummy='c') or (dummy='i') or (dummy='ESC') or (dummy='F1') or (dummy='F2') or (dummy='F3') or (dummy='F4') or (dummy='F5') or (dummy='F6') or (dummy='F7') or (dummy='F8') or (dummy='F9') or (dummy='F10') or (dummy='F11') or (dummy='F12');

    ch := dummy;

    // define quick keys
    if ch='F1' then
       BindQuickKey_Spell(1);

    if ch='F2' then
       BindQuickKey_Spell(2);

    if ch='F3' then
       BindQuickKey_Spell(3);

    if ch='F4' then
       BindQuickKey_Spell(4);

    if ch='F5' then
       BindQuickKey_Spell(5);

    if ch='F6' then
       BindQuickKey_Spell(6);

    if ch='F7' then
       BindQuickKey_Spell(7);

    if ch='F8' then
       BindQuickKey_Spell(8);

    if ch='F9' then
       BindQuickKey_Spell(9);

    if ch='F10' then
       BindQuickKey_Spell(10);

    if ch='F11' then
       BindQuickKey_Spell(11);

    if ch='F12' then
       BindQuickKey_Spell(12);

    if ch='c' then
    begin

        if (ThePlayer.blCursed=true) or (ThePlayer.intCalm > 0) or ((DngLvl[ThePlayer.intX,ThePlayer.intY].intFloorType = 11) and (ThePlayer.blBlessed=false)) then
            ShowTransMessage ('Your magic skills are blocked; you cannot chant songs right now.', false)
        else
        begin
            if (UseSDL=false) or (UseMouseToMove=false) then
                repeat
                    n := GetNumericalInput('Enter the number of the magical song to chant [ENTER to cancel]:', 2);
                until (n=0) or ((n>0) and (n<13) and (Spellbook[n].intKnown>0));
    
            if n>0 then
                ChantSong(n)
            else
                GetKeyInput ('You need at least level ' + IntToStr(Spell[spellbook[n].intType].intCharLvl) + ' to chant this song.', true);
        end;
    end;

    if ch='i' then
    begin
        if (UseSDL=false) or (UseMouseToMove=false) then
            repeat
                n := GetNumericalInput('Select a magical song:', 2);
            until (n=0) or ((n>0) and (n<13) and (Spellbook[n].intKnown>0));

        if n>0 then
            SpellInfo(spellbook[n].intType, n);
    end;

    ClearScreenSDL;
end;


// chants the last song
procedure ChantLastSong;
begin
    if LastSong>0 then
        ChantSong(LastSong)
    else
        ShowSpellbook;
end;


procedure Pray;
    var
        intGift: integer;
begin

    if ThePlayer.blEvil=false then
    begin

        ShowTransMessage('You pray to your god.', false);
        inc(ThePlayer.longPrayers);

        intGift := 0;


        if ThePlayer.blCursed=true then
            if random(500)>450 then
            begin
                ThePlayer.blCursed:=false;
                ShowTransMessage('Your god has removed the curse from your soul.', false);
            end;

        if (ThePlayer.intTotalVitari>0) and (ThePlayer.intNeedVitari >= ThePlayer.intNextVitari) then
            if random(500)>480 then
            begin
                ThePlayer.intTotalVitari := 0;
                ThePlayer.intNextVitari := 0;
                ThePlayer.intNeedVitari := 0;
                ShowTransMessage('Your god has healed you from your drug addiction.', false);
            end;

        if (DngLvl[ThePlayer.intX,ThePlayer.intY].intAirType=6) then
            if random(500)>480 then
                DoEffect(21, 0, 'Your god lifts you up ...');


        // first, influence skills that are affiliated to the player's religion (class)
        // improve fight-skill?
        if PlayClass[ThePlayer.intReli].afSkill='Fight' then
        begin
            if ThePlayer.longPrayers > (550 - (ThePlayer.intHumility*ThePlayer.intHumility)) * ThePlayer.intFight then
            begin
                if random(500)>250 then
                begin
                    inc(ThePlayer.intFight);
                    ShowTransMessage('Your fighting skill improves.', false);
                end;
            end;
        end;

        // improve view-skill?
        if PlayClass[ThePlayer.intReli].afSkill='View' then
        begin
            if ThePlayer.longPrayers > (550 - (ThePlayer.intHumility*ThePlayer.intHumility)) * ThePlayer.intView then
            begin
                if random(500)>250 then
                begin
                    inc(ThePlayer.intView);
                    ShowTransMessage('Your viewing skill improves.', false);
                end;
            end;
        end;

        // improve move-skill?
        if PlayClass[ThePlayer.intReli].afSkill='Move' then
        begin
            if ThePlayer.longPrayers > (550 - (ThePlayer.intHumility*ThePlayer.intHumility)) * ThePlayer.intMove then
            begin
                if random(500)>250 then
                begin
                    inc(ThePlayer.intMove);
                    ShowTransMessage('Your moving skill improves.', false);
                end;
            end;
        end;

        // improve chant-skill?
        if PlayClass[ThePlayer.intReli].afSkill='Chant' then
        begin
            if ThePlayer.longPrayers > (550 - (ThePlayer.intHumility*ThePlayer.intHumility)) * ThePlayer.intChant then
            begin
                if random(500)>250 then
                begin
                    inc(ThePlayer.intChant);
                    ShowTransMessage('Your chanting skill improves.', false);
                end;
            end;
        end;

        // improve burgle-skill?
        if PlayClass[ThePlayer.intReli].afSkill='Burgle' then
        begin
            if ThePlayer.longPrayers > (550 - (ThePlayer.intHumility*ThePlayer.intHumility)) * ThePlayer.intBurgle then
            begin
                if random(500)>250 then
                begin
                    inc(ThePlayer.intBurgle);
                    ShowTransMessage('Your steal skill improves.', false);
                end;
            end;
        end;


        // now, influence status values that are affiliated to the player's religion
        // heal the player?
        if PlayClass[ThePlayer.intReli].afStat='HP' then
        begin
            if ThePlayer.longPrayers > (550 - (ThePlayer.intHumility*ThePlayer.intHumility)) * ThePlayer.intLvl then
            begin
                if random(500)>200 then
                begin
                    if ThePlayer.intHP < 6 then
                    begin
                        inc(ThePlayer.intHP, 15);
                        if ThePlayer.intHP > ThePlayer.intMaxHP then
                            ThePlayer.intHP := ThePlayer.intMaxHP;
                        ShowTransMessage('Suddenly you feel better.', false);
                    end;
                end;
            end;
        end;

        // give PP?
        if PlayClass[ThePlayer.intReli].afStat='PP' then
        begin
            if ThePlayer.longPrayers > (550 - (ThePlayer.intHumility*ThePlayer.intHumility)) * ThePlayer.intLvl then
            begin
                if random(500)>200 then
                begin
                    if ThePlayer.intPP < 3 then
                    begin
                        inc(ThePlayer.intPP, 8);
                        if ThePlayer.intPP > ThePlayer.intMaxPP then
                            ThePlayer.intHP := ThePlayer.intMaxHP;
                        ShowTransMessage('Your mind seems clearer now.', false);
                    end;
                end;
            end;
        end;

        // decrease hunger?
        if PlayClass[ThePlayer.intReli].afStat='Hunger' then
        begin
            if ThePlayer.longPrayers > (550 - (ThePlayer.intHumility*ThePlayer.intHumility)) * ThePlayer.intLvl then
            begin
                if random(500)>200 then
                begin
                    if ThePlayer.longFood < 200 then
                    begin
                        inc(ThePlayer.longFood, 450);
                        ShowTransMessage('Your stomach is filled with holy food.', false);
                    end;
                end;
            end;
        end;

        // give EXP?
        if PlayClass[ThePlayer.intReli].afStat='EXP' then
        begin
            if ThePlayer.longPrayers > (550 - (ThePlayer.intHumility*ThePlayer.intHumility)) * ThePlayer.intLvl then
            begin
                if random(500)>200 then
                begin
                    intGift := trunc(random(10)+10);
                    inc(ThePlayer.longExp, intGift);
                    ShowTransMessage('Your feel more experienced.', false);
                end;
            end;
        end;

        // give Gold?
        if PlayClass[ThePlayer.intReli].afStat='Gold' then
        begin
            if ThePlayer.longPrayers > (550 - (ThePlayer.intHumility*ThePlayer.intHumility)) * ThePlayer.intLvl then
            begin
                if random(500)>200 then
                begin
                    intGift := trunc(random(300)+15);
                    inc(ThePlayer.longGold, intGift);
                    ShowTransMessage('You find some mysterious Credits.', false);
                end;
            end;
        end;
    end
    else
        ShowTransMessage(ThePlayer.strReli+' is not interested in your prayers.', false);

end;


procedure DrinkFromWell;
begin
    DngLvl[ThePlayer.intX,ThePlayer.intY].intFloorType:=29;
    if DungeonLevel>1 then
    begin
        DoEffect(-1,10,'You drink from the well, hoping that it is water ...')
    end
    else
        DoEffect(4,10,'You drink from the fresh water.');
end;


procedure ExamineCrypt;
    var
        msg1: string;
begin

    if random(500)>250 then
        CreateBonesMonster;

    msg1:='The crypt is empty';

    if DngLvl[ThePlayer.intX,ThePlayer.intY].intItem=0 then
    begin
        if random(CONST_CHESTISEMPTY) > 240 then
        begin
            DngLvl[ThePlayer.intX,ThePlayer.intY].intItem := ReturnRareItem;
            msg1:='You find something in the crypt';
        end;
    end;

    ShowTransMessage(msg1+'.', false);

    // make crypt visited
    DngLvl[ThePlayer.intX,ThePlayer.intY].intFloorType:=33;
end;


procedure Sacrifice;
    var
        cr, percent: integer;
        mes1: string;
begin

    if ThePlayer.blEvil=false then
    begin
    if UseSDL=false then
            for i:=22 to 25 do
                TextXY(0, i, '                                                                                ');

        repeat
            cr := GetNumericalInput('How many of your '+IntToStr(ThePlayer.longGold)+' credits do you want to sacrifice?', 5);
        until (cr>-1) and (cr<ThePlayer.longGold+1);

        if ThePlayer.longGold > 0 then
        begin

            percent := (100*cr) div ThePlayer.longGold;

            if percent < 5 then
            begin
                mes1:=ThePlayer.strReli+' says: "DO YOU WANT TO FOOL ME??"';
                if ThePlayer.intHumility < 3 then
                begin
                    BlendMagic(8);
                    ThePlayer.blCursed:=true;
                    ThePlayer.blBlessed:=false;
                    mes1:=mes1+' Sadness creeps in your mind.';
                end;
            end;

            if percent > 5 then
                mes1:='Although very small, your offer is taken';
            if percent > 10 then
                mes1:='Your offer is taken';
            if percent > 30 then
                mes1:='Your offer is appreciated';
            if percent > 50 then
                mes1:='Your remarkable offer is very appreciated';
            if percent > 80 then
                mes1:='Your god is thankful for your great offer';
            if percent > 95 then
                mes1:='Your god is enthusiastic about your worship';

            // enchanter becomes believer if 1000 (or more) Cr offered
            if ThePlayer.intProf=2 then // enchanter
                if ThePlayer.intDipl[8]=0 then // not believer yet
                    if cr>=1000 then
                    begin
                        ThePlayer.intDipl[8]:=1;
                        ShowTransMessage('You have been promoted to the rank "Believer".', true);
                    end;

            dec(ThePlayer.longGold, cr);
            inc(ThePlayer.longPrayers, ThePlayer.intHumility * (percent div 3));

            GetKeyInput(mes1 + '.', true);

            if (ThePlayer.longPrayers>CONST_PRAYERSNEEDEDFORBLESS) and (ThePlayer.blCursed=false) then
            begin
                BlendMagic(5);
                ThePlayer.blBlessed:=true;
                if ThePlayer.intTotalVitari > 0 then
                begin
                    ThePlayer.intTotalVitari := 0;
                    ThePlayer.intNextVitari := 0;
                    ThePlayer.intNeedVitari := 0;
                    ShowTransMessage(ThePlayer.strReli+' blessed you and healed your from your drug addiction.', false);
                end
                else
                    ShowTransMessage('You feel blessed by the spirit of '+ThePlayer.strReli+'.', false);
            end;
        end;
    end
    else
        ShowTransMessage(ThePlayer.strReli+' is not interested in offers from you.', false);
end;


// open treasure chests
procedure LootChest;
    var
        i, j, n, t: integer;
        msg1: string;
begin
    DngLvl[ThePlayer.intX,ThePlayer.intY].intFloorType:=14;
    msg1:='It is empty';

    n:=240;

    if DngLvl[ThePlayer.intX,ThePlayer.intY].intItem=0 then
    begin
        if random(CONST_CHESTISEMPTY) > n then
        begin
            i:=0;
            repeat
                inc(i);
                j:=trunc(1+random(ItemCount));
                if (Thing[j].blUnique=false) and (Thing[j].blRare=false) and (Thing[j].blShopOnly=false) and (DungeonLevel>=Thing[j].intMinLvl) then
                begin
                    DngLvl[ThePlayer.intX,ThePlayer.intY].intItem:=j;
                    msg1:='You find something';
                end;
            until (i=2000) or (msg1='You find something');
        end;
    end;

    PlaySFX('open-chest.mp3');
    ShowTransMessage('You open a treasure chest. ' + msg1 + '.', false);

    if random(500)>440 then
    begin
        if (ThePlayer.intBurgle>15) or (CheckEffect(41)=true) then
        begin
            // Trap prevented
            ShowTransMessage('You discovered a trap and disarmed it.', false);
        end
        else
        begin
            ShowTrapEffect;
            t:=trunc(1+random(6));
            ShowDamage(t);
            dec(ThePlayer.intHP, t);
            ShowTransMessage('You were hurt by a trap!', false);
            if IsPlayerDead=true then
                GameOver('Killed by a trap');
        end;
    end;
    ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
end;

// returns true, if the given NPC on the given level is TargetID of any quest
function IsTargetNPC (lvl, npcID: integer): boolean;
    var
        i: Integer;
begin
    IsTargetNPC:=false;

    // check every quest in the level
    for i:=1 to 9 do
        if (Quest[lvl,i].intTargetID=npcID) and (ThePlayer.intQuestState[lvl,i]=1) then
        begin
            IsTargetNPC:=true;
            ThePlayer.intQuestState[lvl,i]:=3;
        end;
end;

// talk with an NPC (new)
procedure TalkNPC (lvl, npcID: integer);
    var
        i, itemID: integer;
        strAccQuest, strAnswer: string;
begin
    itemID:=0;

    // Check if player is evil; if yes, they won't talk to him
    if ThePlayer.blEvil=false then
    begin
    // Check if this is a NPC chosen as target in another quest
        if IsTargetNPC(lvl,npcID)=true then
        begin
            GetKeyInput('You did the delivery [return to your client to get your loan].', true);
            exit;
        end;


        // NPC is undefined and not target NPC - show random statement, then exit
        if IsTargetNPC(lvl,npcID)=false then
            if Quest[lvl,npcID].strNPCName='-' then
            begin
                i:=1+trunc(random(MaxStatements));
                GetKeyInput('"'+Statement[i]+'"', true);
                exit;
            end;


        // Select NPC behavior depending on quest state
        // ThePlayer.intQuestState[lvl,npcID]:  0=still unvisited; 1=offered; 2=solved; 3=solved (but has to be confirmed)

        // First visit ever; offer quest
        if ThePlayer.intQuestState[lvl,npcID]=0 then
        begin

            // first, we check all conditions the quest might depend on

            // check if player's religion is okay for quest's religion condition
            if (Quest[lvl,npcID].intClass=0) or (Quest[lvl,npcID].intClass=ThePlayer.intReli) then
            begin
                // check if player's level is okay for quest's level condition
                if ThePlayer.intLvl>=Quest[lvl,npcID].intCharLvl then
                begin
                    // check if the plot-condition for the quest is met
                    if (Quest[lvl,npcID].intNeedPlot=0) or ((Quest[lvl,npcID].intNeedPlot>0) and (ThePlayer.blStory[Quest[lvl,npcID].intNeedPlot]=true)) then
                    begin
                        // check if the quest-condition for the quest is met
                        if (Quest[lvl,npcID].intNeedQuest=0) or ((Quest[lvl,npcID].intNeedQuest>0) and (ThePlayer.intQuestState[lvl,Quest[lvl,npcID].intNeedQuest]=2)) then
                        begin
                            // check if player's profession is okay for quest's profession condition
                            if (Quest[lvl,npcID].intNeedProf=0) or (Quest[lvl,npcID].intNeedProf=ThePlayer.intProf) then
                            begin
                                // check if player has needed diploma
                                if (Quest[lvl,npcID].intNeedDipl=0) or (ThePlayer.intDipl[Quest[lvl,npcID].intNeedDipl]=1) then
                                begin
                                // show plot text, if strIntroPlot is set
                                    if Quest[lvl,npcID].strIntroPlot<>'-' then
                                        ShowPlot(StrToInt(Quest[lvl,npcID].strIntroPlot));

                                    // show intro book or scroll, if strIntroText is set
                                    if Quest[lvl,npcID].strIntroBook<>'-' then
                                        ShowText(Quest[lvl,npcID].strIntroBook);

                                    if UseSDL=true then
                                    begin
                                        LoadImage_Title(PathData+'graphics/txtbg.jpg');
                                        BlitImage_Title;
                                    end
                                    else
                                    begin
                                        ClearScreenSDL;
                                        StatusDeco;
                                    end;

                                    TransTextXY(1, 1, 'YOU TALK TO ' + uppercase(Quest[lvl,npcID].strNPCname));

                                    TransTextXY(1, 3, Quest[lvl,npcID].strText1);
                                    TransTextXY(1, 4, Quest[lvl,npcID].strText2);
                                    TransTextXY(1, 5, Quest[lvl,npcID].strText3);
                                    TransTextXY(1, 6, Quest[lvl,npcID].strText4);
                                    TransTextXY(1, 7, Quest[lvl,npcID].strText5);

                                    // Is the "quest" only an info text?
                                    if Quest[lvl,npcID].intType=6 then
                                    begin
                                        GetKeyInput('Press any key to continue.', false);
                                        ThePlayer.intQuestState[lvl,npcID]:=3;
                                    end
                                    else
                                    begin
                                        TransTextXY(1, 10, 'Quest: '+Quest[lvl,npcID].strDescri);

                                        // It is indeed a real quest. Offer the player to accept it.
                                        strAccQuest := GetKeyInput('Press ['+KeyEnter+'] to accept this quest, another key to cancel.', false);
                                        if strAccQuest=KeyEnter then
                                        begin
                                            ThePlayer.intQuestState[lvl,npcID]:=1;
                                            ShowTransMessage('Quest: '+Quest[lvl,npcID].strDescri, false);
                                            StoreAchievement('Accepted a quest by '+Quest[lvl,npcID].strNPCname+'.');
                                            inc(ThePlayer.longScore, 50);
                                            CreateUniqueMonster(lvl);
                                        end;
                                    end;
                                end
                                else
                                    GetKeyInput('You have to be '+strDiplomaList[Quest[lvl,npcID].intNeedDipl]+' to work for me.', true);
                            end
                            else
                                GetKeyInput('Sorry, but you have the wrong profession for the job I offer.', true);
                        end
                        else
                            GetKeyInput('I am busy. Perhaps we can talk later.', true);
                    end
                    else
                        GetKeyInput('At the moment, I have no work for you. Try again later.', true);
                end
                else
                    GetKeyInput('I think you need a higher level for the job I offer.', true);
            end
            else
                GetKeyInput('I have no work for anybody of your religious order.', true);

            if Quest[lvl,npcID].intType<>6 then
                exit;
        end;


        // Not the first visit; a quest is already given
        if (ThePlayer.intQuestState[lvl,npcID]=1) or (ThePlayer.intQuestState[lvl,npcID]=3) then
        begin

            // first, we check all quests which need return to the client for being solved
    //         writeln('Type: '+IntToStr(Quest[lvl,npcID].intType));

            // 1: get item quest?
            if Quest[lvl,npcID].intType=1 then
                for i:=1 to 16 do
                    if inventory[i].intType>0 then
                        if Thing[inventory[i].intType].strName = Quest[lvl,npcID].strGetItem then
                        begin
                            ThePlayer.intQuestState[lvl,npcID]:=2;
                            GetKeyInput('You give the '+Thing[inventory[i].intType].strName+' to '+Quest[lvl,npcID].strNPCname+'.', true);
                            dec(inventory[i].longNumber,Thing[inventory[i].intType].intAmount);
                            if inventory[i].longNumber<1 then
                                inventory[i].intType:=0;
                        end;

            // 2: Delivery quest?
            if Quest[lvl,npcID].intType=2 then
                if ThePlayer.intQuestState[lvl,npcID]=3 then
                begin
                    ThePlayer.intQuestState[lvl,npcID]:=2;
                    GetKeyInput('You return to your client and tell him about the delivery.', true);
                end;

            // 4: hunt quest?
            if Quest[lvl,npcID].intType=4 then
    //         begin
    //             Writeln(IntToStr(ThePlayer.intQuestHunt[lvl,npcID])+' from '+IntToStr(Quest[lvl,npcID].intHuntAmount));
                if ThePlayer.intQuestHunt[lvl,npcID]>=Quest[lvl,npcID].intHuntAmount then
                begin
                    ThePlayer.intQuestState[lvl,npcID]:=2;
                    GetKeyInput('You return to ' + Quest[lvl,npcID].strNPCname +' and report your success.', true);
                end;
    //         end;

            // 6: just information
            if Quest[lvl,npcID].intType=6 then
                ThePlayer.intQuestState[lvl,npcID]:=2;

            // 5: collect itemset quest?
            if Quest[lvl,npcID].intType=5 then
                if CheckNeededItems(Quest[lvl,npcID].strGetItem,true)=true then
                begin
                    ThePlayer.intQuestState[lvl,npcID]:=2;
                    GetKeyInput('You give all needed items to ' + Quest[lvl,npcID].strNPCname + '.', true);
                end;

            // 7: get gold quest?
            if Quest[lvl,npcID].intType=7 then
                if ThePlayer.longGold>=Quest[lvl,npcID].intGetGold then
                begin
                    ThePlayer.intQuestState[lvl,npcID]:=2;
                    dec(ThePlayer.longGold,Quest[lvl,npcID].intGetGold);
                    GetKeyInput('You pay the '+ IntToStr(Quest[lvl,npcID].intGetGold) +' to '+Quest[lvl,npcID].strNPCname+'.', true);
                end;

            // 8: correct answer
            if Quest[lvl,npcID].intType=8 then
            begin
                strAnswer:=trim(uppercase(GetTextInput('Tell me the correct answer: ', 20)));
                if strAnswer=trim(uppercase(Quest[lvl,npcID].strTargetName)) then
                begin
                    ThePlayer.intQuestState[lvl,npcID]:=2;
                    GetKeyInput('You are right, '+ThePlayer.strName+'; this was the correct answer.', true);
                end
                else
                    GetKeyInput('This was not the correct answer.', true);
            end;


            // 9: collect resources quest?
            if Quest[lvl,npcID].intType=9 then
            begin
                //GetKeyInput(Quest[lvl,npcID].strGetResources, true);
                if CheckNeededResources(Quest[lvl,npcID].strGetResources) then
                begin
                    ThePlayer.intQuestState[lvl,npcID]:=2;
                    ReduceResources(Quest[lvl,npcID].strGetResources);
                    GetKeyInput('You give the resources to ' + Quest[lvl,npcID].strNPCname + '.', true);
                end;
            end;

            // 10: destroy hive?
            if Quest[lvl,npcID].intType=10 then
                if intNoHivesAnymore[Quest[lvl,npcID].intTargetHive]=1 then
                begin
                    ThePlayer.intQuestState[lvl,npcID]:=2;
                    GetKeyInput('You return to ' + Quest[lvl,npcID].strNPCname +' and report your success.', true);
                end;


            // if the quest is solved, give reward
            if (ThePlayer.intQuestState[lvl,npcID]=2) or (ThePlayer.intQuestState[lvl,npcID]=3) then
            begin

                if Quest[lvl,npcID].strTextSolved <> '-' then
                    GetKeyInput(Quest[lvl,npcID].strTextSolved, true);

                StoreAchievement('Solved '+Quest[lvl,npcID].strNPCname+chr(39)+'s quest.');

                // give reward to player
                case Quest[lvl,npcID].intRewardType of
                    1:
                    begin
                        ShowTransMessage('You gain ' + IntToStr(Quest[lvl,npcID].longRewardAmount) + ' Credits.', false);
                        inc(ThePlayer.longGold, Quest[lvl,npcID].longRewardAmount);
                    end;
                    2:
                    begin
                        ShowTransMessage('You gain ' + IntToStr(Quest[lvl,npcID].longRewardAmount) + ' EXP.', false);
                        inc(ThePlayer.longEXP, Quest[lvl,npcID].longRewardAmount);
                        LevelUp;
                    end;
                    3:
                    begin
                        ShowTransMessage(Quest[lvl,npcID].strNPCname + ' drops an item.', false);
                        DngLvl[ThePlayer.intX,ThePlayer.intY].intItem := ReturnItemByName(Quest[lvl,npcID].strRewardItem);
                    end;
                    4:
                    begin
                        ShowPlot(Quest[lvl,npcID].longRewardAmount);
                    end;
                    5:
                    begin
                        ThePlayer.blBlessed:=true;
                        ShowTransMessage('You feel blessed by the spirit of '+ThePlayer.strReli+'.', false);
                    end;
                    6:
                    begin
                        WinGame(true);
                    end;
                    7:
                    begin
                        ShowTransMessage('You can train ' + IntToStr(Quest[lvl,npcID].longRewardAmount) + ' skills now.', false);
                        TrainSkill(Quest[lvl,npcID].longRewardAmount);
                    end;
                    8:
                    begin
                        LevelUp;
                    end;
                    9:
                    begin
                        ThePlayer.intHP := ThePlayer.intMaxHP;
                        ThePlayer.intPP := ThePlayer.intMaxPP;
                        ShowTransMessage('You are healed completely.', false);
                    end;
                    10:
                    begin
                        ShowTransMessage('You leave '+Quest[lvl,npcID].strNPCname+' without any reward.', false);
                    end;
                    11:
                    begin
                        DngLvl[ThePlayer.intX,ThePlayer.intY].intFloorType:=59;
                        ShowTransMessage('Suddenly a magical portal appears in front of you.', false);
                    end;
                    12:
                    begin
                        ThePlayer.intDipl[Quest[lvl,npcID].longRewardAmount]:=1;
                        ShowTransMessage('Congratulations, '+ThePlayer.strName+'! You have been promoted to '+strDiplomaList[Quest[lvl,npcID].longRewardAmount]+'.', false);
                        StoreAchievement('Promoted to '+strDiplomaList[Quest[lvl,npcID].longRewardAmount]+'.');
                    end;
                end;

                // show extro plot text, if strExtroPlot is set
                if Quest[lvl,npcID].strExtroPlot<>'-' then
                    ShowPlot(StrToInt(Quest[lvl,npcID].strExtroPlot));

                // show extro book or scroll, if strExtroText is set
                if Quest[lvl,npcID].strExtroBook<>'-' then
                    ShowText(Quest[lvl,npcID].strExtroBook);


                // extra EXP for class 1
                if (ThePlayer.intReli=1) and (ThePlayer.intHumility>0) then
                begin
                    ShowTransMessage(ThePlayer.strReli + ' gives you ' + IntToStr(ThePlayer.intLvl*2) + ' additional EXP.', false);
                    inc(ThePlayer.longEXP, ThePlayer.intLvl*2);
                    LevelUp;
                end;

                // score
                inc(ThePlayer.longScore, 100);

            end
            else
                GetKeyInput(Quest[lvl,npcID].strTextProgress, true);

            exit;
        end;

        // Is this a NPC whom we already helped?
        if ThePlayer.intQuestState[lvl,npcID]=2 then
        begin
            GetKeyInput('I am glad to know you, '+ThePlayer.strName+'.', true);
            exit;
        end;
    end
    else
        GetKeyInput(Quest[lvl,npcID].strNPCname+' shivers because of the aura of cold death that is surrounding you.', true);
end;


// help
procedure HelpScreenKeys;
begin
    ClearScreenSDL;

    if UseSDL=true then
    begin
        LoadImage_Title(PathData+'graphics/txtbg.jpg');
    end;

    if UseSDL=true then
        BlitImage_Title
    else
        StatusDeco;

    TransTextXY(2, 1, 'KEYS USED TO PERFORM ACTIONS');
    //TransTextXY(2, 3, 'Default movement: arrow keys or NumPad (switch Numlock ON!)');
    TransTextXY(2, 2, 'Movement keys from config file (N,S,E,W,NE,NW,SE,SW): '+KeyNorth+KeySouth+KeyEast+KeyWest+KeyNorthEast+KeyNorthWest+KeySouthEast+KeySouthWest);

    TransTextXY(2, 4, '[ENTER] or [' + KeyEnter + ']');


    TransTextXY(2, 5, '  open chest                        (in front of a treasure chest)');
    TransTextXY(2, 6, '  sacrifice money                   (in front of an altar)');
    TransTextXY(2, 7, '  enter staircase                   (in front of a staircase)');
    TransTextXY(2, 8, '  drink from well                   (in front of a well)');
    TransTextXY(2, 9, '  examine crypt                     (in front of a crypt)');

    TransTextXY(2,11, '['+KeyTake+'] pick up item                    ['+KeyInventory+'] show inventory');
    TransTextXY(2,12, '['+KeyChant+'] show songbook (spells)          ['+KeyChantLast+'] chant last spell again');
    TransTextXY(2,13, '['+KeyTrade+'] talk to NPC or trader           ['+KeyShoot+'] fire long-range weapon');
    TransTextXY(2,14, '['+KeyTunnel+'] dig                             ['+KeyPray+'] pray to your god');
    TransTextXY(2,15, '['+KeyStatus+'] show status screen              ['+KeyQuestlog+'] show questlog');
    TransTextXY(2,16, '['+KeyShortRest+'] rest one turn                   ['+KeyRest+'] rest certain number of turns');
    TransTextXY(2,17, '['+KeyLook+'] identify tile                   ['+KeyQuit+'] show game menu; also: [ESC]');
    TransTextXY(2,18, '['+KeySearchSteal+'] search/steal                    ['+KeySetQuickKeys+'] show and reset quick keys');
    TransTextXY(2,19, '['+KeyCloseDoor+'] close door                      ['+KeyBigMap+'] show big minimap (SDL only)');

    GetKeyInput('Press any key to return to help menu.', false);
end;

procedure HelpScreenChars;
begin
    ClearScreenSDL;

    if UseSDL=true then
    begin
        LoadImage_Title(PathData+'graphics/sdlhelp.jpg');
    end;

    if UseSDL=true then
        BlitImage_Title
    else
        StatusDeco;

    TransTextXY(2, 1, 'SYMBOLS ON THE SCREEN');

    if UseSDL=false then
    begin
        TextXY(2, 4, 'White letters from a to z and A to Z are animals, monsters or other enemies.');
        TextXY(2, 5, 'White numbers from 0 to 9 are NPCs, colored numbers from 1 to 7 are traders.');
        TextXY(2, 6, 'You, ' + ThePlayer.strName + ', are shown as white @ . A grey @ is a ghost.');
        TextXY(2, 8, chr(134) + ' small tree             '+ chr(137) + ' big tree               '+ chr(136) + ' sand');
        TextXY(2, 9, chr(148) + ' grass                  '+ chr(149) + ' hill                   '+ chr(178) + ' mountain');
        TextXY(2,10, chr(183) + ' lava                   '+ chr(135) + ' caveworm excrements    '+ chr(177) + ' water');
        TextXY(2,11, chr(150) + ' floor                  '+ chr(175) + ' staircase up           '+ chr(176) + ' staircase down');
        TextXY(2,12, chr(151) + ' closed door            '+ chr(185) + ' locked door            '+ chr(152) + ' open door');
        TextXY(2,13, chr(180) + ' wall                   '+ chr(198) + ' monster hive           '+ chr(188) + ' crypt');
        TextXY(2,14, chr(153) + ' closed treasure chest  '+ chr(154) + ' open treasure chest    '+ chr(170) + ' altar');
        TextXY(2,15, chr(189) + ' well                   '+ chr(196) + ' empty well             '+ chr(171) + ' rock');
        TextXY(2,16, chr(156) + ' sword, knife           '+ chr(159) + ' axe                    '+ chr(164) + ' lance');
        TextXY(2,17, chr(158) + ' bow, crossbow          '+ chr(162) + ' spade, pickaxe         '+ chr(160) + ' ammunition');
        TextXY(2,18, chr(163) + ' clothes                '+ chr(157) + ' armour                 '+ chr(165) + ' helmet, cap');
        TextXY(2,19, chr(166) + ' ring                   '+ chr(172) + ' candle, lantern        '+ chr(169) + ' rune');
        TextXY(2,20, chr(168) + ' food                   '+ chr(155) + ' potion, drink          '+ chr(191) + ' key, picklock');
        TextXY(2,21, chr(193) + ' paper, page            '+ chr(161) + ' crystal                '+ chr(167) + ' credits');
    end;

    GetKeyInput('Press any key to return to help menu.', false);
end;


procedure HelpScreenYou;
    var
        i, l: integer;
begin
    ClearScreenSDL;

    if UseSDL=true then
    begin
        LoadImage_Title(PathData+'graphics/decobg.jpg');
    end;

    if UseSDL=true then
        BlitImage_Title
    else
        StatusDeco;

    TransTextXY(2, 1, 'SOME HINTS BASED ON YOUR CURRENT STATS');

    l:=4;

    // HP
    if ThePlayer.intHP <= (ThePlayer.intMaxHP div 4) then
    begin
        TransTextXY(2, l, 'Use a healing item (e.g. Aspirin) or chant a healing spell immediately!');
        inc(l);
    end;
    if (ThePlayer.intHP > (ThePlayer.intMaxHP div 4)) and (ThePlayer.intHP <= (ThePlayer.intMaxHP div 2)) then
    begin
        TransTextXY(2, l, 'Watch your health (HP)! It is rather low!');
        inc(l);
    end;

    // PP
    if ThePlayer.intMaxPP>=15 then
    begin
        if ThePlayer.intPP <= (ThePlayer.intMaxPP div 4) then
        begin
            TransTextXY(2, l, 'You should regenerate your psychic points (PP), e.g. by drinking a Cola.');
            inc(l);
        end;
        if (ThePlayer.intPP > (ThePlayer.intMaxPP div 4)) and (ThePlayer.intPP <= (ThePlayer.intMaxPP div 2)) then
        begin
            TransTextXY(2, l, 'Watch your psychic points (PP). They are rather low.');
            inc(l);
        end;
    end;

    // STR
    if ThePlayer.intStrength<100 then
    begin
        TransTextXY(2, l, 'Your strength is lower than normal. You should take a rest to regenerate.');
        inc(l);
    end;

    // TP / GunTP?
    if (ThePlayer.intWeapon>0) then
    begin
        if Thing[ThePlayer.intWeapon].intGP=0 then
        begin
            if CollectTP < 6 then
            begin
                TransTextXY(2, l, 'You are not suited for melee combat. Try to avoid enemies.');
                inc(l);
            end;
        end
        else
        begin
            if CollectGunTP < 6 then
            begin
                TransTextXY(2, l, 'If you would use bows or crossbows, it would be a waste of ammu.');
                inc(l);
            end;
        end;

        // Sword wielded with low Sword-skill?
        if (Thing[ThePlayer.intWeapon].chLetter=chr(156)) and (ThePlayer.intSword<Thing[ThePlayer.intWeapon].intWP) then
        begin
            TransTextXY(2, l, 'Train your Sword-skill to take full advantage of your '+Thing[ThePlayer.intWeapon].strName+'.');
            inc(l);
        end;

        // Axe wielded with low Axe-skill?
        if (Thing[ThePlayer.intWeapon].chLetter=chr(159)) and (ThePlayer.intAxe<Thing[ThePlayer.intWeapon].intWP) then
        begin
            TransTextXY(2, l, 'Train your Axe-skill to take full advantage of your '+Thing[ThePlayer.intWeapon].strName+'.');
            inc(l);
        end;

        // Lance wielded with low Lance-skill?
        if (Thing[ThePlayer.intWeapon].chLetter=chr(164)) and (ThePlayer.intWhip<Thing[ThePlayer.intWeapon].intWP) then
        begin
            TransTextXY(2, l, 'Train your Lance-skill to take full advantage of your '+Thing[ThePlayer.intWeapon].strName+'.');
            inc(l);
        end;

        // Gun wielded with low Gun-skill?
        if (Thing[ThePlayer.intWeapon].intGP>0) and (ThePlayer.intGun<Thing[ThePlayer.intWeapon].intGP) then
        begin
            TransTextXY(2, l, 'Train your Firearm-skill to take full advantage of your '+Thing[ThePlayer.intWeapon].strName+'.');
            inc(l);
        end;

        // weapon with effect wielded without maximized skill?
        if (Thing[ThePlayer.intWeapon].intEffect=38) or (Thing[ThePlayer.intWeapon].intEffect=39) or (Thing[ThePlayer.intWeapon].intEffect=40) then
            if MaximizedWeaponSkill=false then
            begin
                TransTextXY(2, l, 'Your weapon skill is too low to use the magical bonus of your weapon.');
                inc(l);
            end;

    end;


    // Move-Skill
    if ThePlayer.intMove<5 then
    begin
        TransTextXY(2, l, 'Train your Move-skill to become better in evading enemy attacks.');
        inc(l);
    end;


    // View-Skill
    if ThePlayer.intView<5 then
    begin
        TransTextXY(2, l, 'If you plan to use bows or magic, you should train your View-skill.');
        inc(l);
    end;


    // Trade-Skill
    if ThePlayer.intTrade<5 then
    begin
        TransTextXY(2, l, 'You could save lots of money if your Trade-skill was trained better.');
        inc(l);
    end;

    // Ahna?
    if ThePlayer.intQuestState[1,1]=0 then
    begin
        TransTextXY(2, l, 'You did not talk to Ahna (1) yet. You should do it to begin your quest.');
        inc(l);
    end;

    // Book of Stars
    if PlayerHasItem('Book of Stars')>-1 then
    begin
        TransTextXY(2, l, 'Deliver the Book of Stars to Ahna to solve your quest.');
        inc(l);
    end;

    // Eris' Key
    if PlayerHasItem('Eris'+chr(39)+' Key')>-1 then
    begin
        TransTextXY(2, l, 'Do not throw away or sell Eris'+chr(39)+' Key. You will need it.');
        inc(l);
    end;

    // Useless picklock?
    if (PlayerHasItem('Picklock')>-1) and (ThePlayer.intProf<>3) then
    begin
        TransTextXY(2, l, 'As only thiefs can use picklocks, you should sell the one you own.');
        inc(l);
    end;

    // Addicted?
    if ThePlayer.intTotalVitari>0 then
    begin
        TransTextXY(2, l, 'You are drug addictive. Going to a hospital or praying will help you.');
        inc(l);
    end;

    // Poisoned?
    if (ThePlayer.intPoison>0) and (PlayerHasItem('Antidot')>-1) then
    begin
        TransTextXY(2, l, 'You are poisoned. Consume the antidot that is in your inventory.');
        inc(l);
    end;


    // soldier killed caveworms?
    if (ThePlayer.intReli=5) then
        for i:=1 to MonsterTemplates do
            if (MonTe[i].strName='caveworm') or (MonTe[i].strName='worm mother') then
                if ThePlayer.longKilled[i]>0 then
                begin
                    TransTextXY(2, l, 'You killed '+IntToStr(ThePlayer.longKilled[i])+' caveworms, but did not receive experience for that shame.');
                    inc(l);
                end;

    // skill points to distribute?
    if ThePlayer.longSkillPoints>0 then
    begin
        TransTextXY(2, l, 'You can train some skills. Go to an academy to do that.');
        inc(l);
    end;

    // cursed
    if ThePlayer.blCursed=true then
    begin
        TransTextXY(2, l, 'You are cursed. You may pray to your god to remove the curse.');
        inc(l);
    end;

    // unknown potion in inventory?
    if PlayerHasItem('unknown potion')>-1 then
    begin
        TransTextXY(2, l, 'To see the effects of unknown potions, identify or consume them.');
        inc(l);
    end;

    // unknown weapons?
    if (PlayerHasItem('unknown sword')>-1) or (PlayerHasItem('unknown axe')>-1) or (PlayerHasItem('unknown lance')>-1) or (PlayerHasItem('unknown firearm')>-1) then
    begin
        TransTextXY(2, l, 'Identify the unknown weapons in your inventory!');
        inc(l);
    end;

    // unknown armour / clothes?
    if (PlayerHasItem('unknown armour')>-1) or (PlayerHasItem('unknown helmet')>-1) or (PlayerHasItem('unknown clothes')>-1) or (PlayerHasItem('unknown ring')>-1) then
    begin
        TransTextXY(2, l, 'Before wearing any unknown item, you have to identify it.');
        inc(l);
    end;

    // never prayed?
    if ThePlayer.longPrayers = 0 then
    begin
        TransTextXY(2, l, 'You never prayed to your god. ' + ThePlayer.strReli + ' might get angry about this.');
        inc(l);
    end;

    // trapped in a spider's web?
    if DngLvl[ThePlayer.intX,ThePlayer.intY].intAirType=6 then
    begin
        TransTextXY(2, l, 'Spider webs disappear after a while.');
        inc(l);
    end;

    GetKeyInput('Press any key to return to help menu.', false);
end;



procedure HelpScreen;
    var
        dummy: String;
        ch: Char;
begin
    ch:='-';

    repeat
        ClearScreenSDL;

        if UseSDL=true then
        begin
            LoadImage_Title(PathData+'graphics/decobg.jpg');
            BlitImage_Title;
        end
        else
            StatusDeco;

        TransTextXY(2, 1, 'HELP');

        TransTextXY(4, 3, 'a   Purpose of the game - what is it all about?');
        TransTextXY(4, 4, 'b   How to contact the developer and get free stuff');
        TransTextXY(4, 5, 'c   Credits and "thank you"');

        TransTextXY(4, 7, 'd   Information on symbols shown on the screen');
        TransTextXY(4, 8, 'e   Some notes on basic status values');
        TransTextXY(4, 9, 'f   About skills, abilities and talents');
        TransTextXY(4,10, 'g   Status values influenced by items');

        TransTextXY(4,11, 'h   Keys used to perform movement and actions in the game');
        TransTextXY(4,12, 'i   Configuring the quickbar');

        TransTextXY(4,14, 'j   Fighting monsters in melee- and long-range battle');
        TransTextXY(4,15, 'k   The basics of using magic (i.e. chanting spells)');
        TransTextXY(4,16, 'l   Magic in combat situations');

        TransTextXY(4,17, 'm   How quests work and how quests are solved');
        TransTextXY(4,18, 'n   Some general hints');
        TransTextXY(4,19, 'o   A context-sensitive help page, based on your current stats');


        dummy := GetKeyInput('Press the according key to make a selection. Press [ESC] to exit.', false);
        ch := dummy[1];

        case ch of
            'a':
                OutputPlot('6');
            'b':
                OutputPlot('help_contact');
            'c':
                OutputPlot('help_credits');
            'd':
                HelpScreenChars;
            'e':
                OutputPlot('help_status');
            'f':
                OutputPlot('help_skills');
            'g':
                OutputPlot('help_statusitems');
            'h':
                HelpScreenKeys;
            'i':
                OutputPlot('help_quickbar');
            'j':
                OutputPlot('help_fighting');
            'k':
                OutputPlot('help_basicmagic');
            'l':
                OutputPlot('help_combatmagic');
            'm':
                OutputPlot('help_quests');
            'n':
                OutputPlot('help_winning');
            'o':
                HelpScreenYou;
        end;

    until dummy='ESC';
end;


// returns true if the unique monster of the given level is still alive
function UniqueAlive(lvl: integer): boolean;
    var
        a, i: integer;
begin
    a:=0;
    for i:=1 to MonsterTemplates do
        if (MonTe[i].intLvl = lvl) and (MonTe[i].blUnique = true) then
            a:=i;

    if a>0 then
        if ThePlayer.blUnKilled[a]=false then
            UniqueAlive:=true
        else
            UniqueAlive:=false;
end;


procedure LevelFeeling;
    var
        EnterVerb, feeling, levelname: string;
begin
    feeling:='-';
    levelname:='another boring level';

    if DungeonLevel=1 then
        if UniqueAlive(1)=true then
            feeling:='The spirit of this place seems soiled.'
        else
            feeling:='You feel safe.';

    if DungeonLevel=3 then
        if UniqueAlive(3)=true then
            feeling:='You hear somebody reciting poems.'
        else
            feeling:='You see many empty bookshelves here.';

    if (DungeonLevel=5) and (UniqueAlive(5)=true) then
        feeling:='You hear the metallic sound of a blacksmith'+chr(39)+'s hammer.';

    if (DungeonLevel=9) and (UniqueAlive(9)=true) then
        feeling:='You hear a voice of beauty and sadness.';

    if (DungeonLevel=15) and (UniqueAlive(15)=true) then
        feeling:='You hear a deep groaning of agony.';

    if DungeonLevel=19 then
        if UniqueAlive(19)=true then
            feeling:='You sense death.'
        else
            feeling:='Ornaments everywhere, showing legendary battles.';

    if (DungeonLevel=20) and (UniqueAlive(20)=true) then
        feeling:='You hear an old woman crying blue murder.';

    if DungeonLevel=21 then
        feeling:='It is stormy and cold.';

    if DungeonLevel=22 then
        feeling:='You admire the wideness of the wilderness.';

    if DungeonLevel=23 then
        feeling:='You feel observed.';

    if DungeonLevel=24 then
        feeling:='It smells like death.';

    if DungeonLevel=25 then
        feeling:='You are curious.';


    if (DungeonLevel=23) or (DungeonLevel=24) then
        if (ThePlayer.blUnKilled[ReturnMonTeByName('Undying King')]=true) or (ThePlayer.blUnKilled[ReturnMonTeByName('Eris')]=true) then
            feeling:='You feel very safe.';



    if (DungeonLevel>2) and (ThePlayer.intLvl>DungeonLevel) then
        if feeling='-' then
            feeling:='You feel confident.';

    if feeling='-' then
        if (DungeonLevel>2) and (ThePlayer.intLvl<DungeonLevel) then
        begin
            if DungeonLevel-ThePlayer.intLvl>1 then
                feeling:='You feel brave.';
            if DungeonLevel-ThePlayer.intLvl>3 then
                feeling:='You feel nervous.';
            if DungeonLevel-ThePlayer.intLvl>5 then
                feeling:='You are afraid.';
            if DungeonLevel-ThePlayer.intLvl>10 then
                feeling:='You are scared to death.';
        end;

    case DungeonLevel of
        1:
            levelname:='the Temple of Enoa';
        2:
            levelname:='the Catacombs of Enoa';
        3:
            levelname:='the Sewers of Enoa';
        4:
            levelname:='the Old Mine';
        5:
            levelname:='the Lost Outpost';
        6:
            levelname:='the Dungeon, level 1';
        7:
            levelname:='the Dungeon, level 2';
        8:
            levelname:='the Caves, level 1';
        9:
            levelname:='the Caves, level 2';
        10:
            levelname:='the Caves, level 3';
        11:
            levelname:='the Caves, level 4';
        12:
            levelname:='the Old Maze, level 1';
        13:
            levelname:='the Old Maze, level 2';
        14:
            levelname:='the Ash Caves, level 1';
        15:
            levelname:='the Ash Caves, level 2';
        16:
            levelname:='the New Maze, level 1'; // TODO: better name
        17:
            levelname:='the New Maze, level 2'; // TODO: better name
        18:
            levelname:='Krice'+chr(39)+'s hideout';
        19:
            levelname:='the Dark Temple';
        20:
            levelname:='the Halls of Eris';
        21:
            levelname:='the Ice Ruins of Noldarur';
        22:
            levelname:='the Outskirts of Enoa';
        23:
            levelname:='the Forgotten Realm, outer part';
        24:
            levelname:='the Forgotten Realm, inner part';
        25:
            levelname:='the Forgotten Realm, King'+chr(39)+'s estate';
    end;

    if ThePlayer.longLevelVisits[DungeonLevel]<2 then
    begin
        EnterVerb:='Entering';
        StoreAchievement('Enters '+levelname+'.');
    end
    else
        EnterVerb:='Returning to';

    if feeling<>'-' then
        ShowTransMessage(EnterVerb+' '+levelname+'. '+feeling, false)
    else
        ShowTransMessage(EnterVerb+' '+levelname+'. ', false);
end;


procedure Difficulty;
begin
//     writeln ('Difficulty: '+IntToStr(DiffLevel));

    // bronze
    if DiffLevel=1 then
    begin
        CONST_MONSTERRECDIST := 4;
        CONST_MONSTERFIRERATE := 300;
        CONST_PERCENTMONSTERS := 1;
        CONST_MIN_ITEMS := 15;
        CONST_SPAWN := 490;
    end;

    // silver
    if DiffLevel=2 then
    begin
        CONST_MONSTERRECDIST := 6;
        CONST_MONSTERFIRERATE := 250;
        CONST_PERCENTMONSTERS := 2;
        CONST_MIN_ITEMS := 12;
        CONST_SPAWN := 470;
    end;

    // gold
    if DiffLevel=3 then
    begin
        CONST_MONSTERRECDIST := 8;
        CONST_MONSTERFIRERATE := 200;
        CONST_PERCENTMONSTERS := 5;
        CONST_MIN_ITEMS := 10;
        CONST_SPAWN := 400;
    end;
end;

function CheckExistencyOfDirectory(path: String; autocreate: boolean): boolean;
begin
    if DirectoryExists(path) then
    begin
       CheckExistencyOfDirectory := true;
    end
    else
    begin
        CheckExistencyOfDirectory := false;
        if autocreate then
           CreateDir(path);
    end;
end;

function CheckExistencyOfFile(path: String): boolean;
begin
    if FileExists(path) then
    begin
        Writeln('Loaded: data file '+path);
        CheckExistencyOfFile := true;
    end

    else
    begin
        Writeln('Error: data file '+path+' is missing.');
        CheckExistencyOfFile := false;
    end;
end;




// MAIN LOOP

{$IFDEF WINDOWS}{$R fprl.rc}{$ENDIF}
Var home : String;
    working_directory : String;
begin


    randomize;

    home := GetUserDir ();  //User's home directory (with leading /)
    PathUserWritable := home+'.lambdarogue/';

    //Check first for installed version, if not, then use data from current directory
    PathData := '/opt/lambdarogue/data/';
    if CheckExistencyOfDirectory(PathData,false)=false then
    begin
        GetDir (0,working_directory); //Current directory (without leading /)
        PathData := working_directory+'/data/';
    end;

    // check for writable directories and make them if needed
    CheckExistencyOfDirectory(PathUserWritable,true);
    CheckExistencyOfDirectory(PathUserWritable+'random_levels',true);
    CheckExistencyOfDirectory(PathUserWritable+'saves',true);

    //Check configure file and create default if missing
    if CheckExistencyOfFile(PathUserWritable+'lambdarogue.cfg')=false then
    begin
        Writeln('..first start, create configuration file '+PathUserWritable+'lambdarogue.cfg"');
        CreateDefaultConfig;
    end;

    // check for important files
    if CheckExistencyOfFile(PathData+'data/monsters.txt')=false or
       CheckExistencyOfFile(PathData+'data/items.txt')=false or
       CheckExistencyOfFile(PathData+'data/quests.txt')=false or
       CheckExistencyOfFile(PathData+'data/statements.txt')=false or
       CheckExistencyOfFile(PathData+'data/chants.txt')=false or
       CheckExistencyOfFile(PathData+'data/classes.txt')=false
    then halt;


    InitMusic;
    InitSFX;

    InitKeys(true);

//     SaveConfig;
//     halt;

    InitGraphics(true);

    ThePlayer.intTileset:=1;
    LastSong:=0;
    blQuit := false;

    // empty quick keys
    for i:=1 to 12 do
        strQuickKey[i]:='-';

    // diplomas
    for i:=1 to 30 do
        strDiplomaList[i]:='unassigned diploma';

    intAchieved := 0;
    for i:=1 to 500 do
        Achievements[i]:='-';


    strDiplomaList[1]:='Centurio';
    strDiplomaList[2]:='Hastatus';
    strDiplomaList[3]:='Princeps';
    strDiplomaList[4]:='Pilus';
    strDiplomaList[5]:='Primus Pilus';
    strDiplomaList[6]:='Mage';
    strDiplomaList[7]:='Battlemage';
    strDiplomaList[8]:='Believer';
    strDiplomaList[9]:='Monk';
    strDiplomaList[10]:='Holy Warrior';

    strDiplomaList[11]:='Assassin';
    strDiplomaList[12]:='Agent';
    strDiplomaList[13]:='Master Thief';
    strDiplomaList[14]:='Guild Leader';
    strDiplomaList[15]:='Hunter';
    strDiplomaList[16]:='Ranger';
    strDiplomaList[17]:='Marksman';
    strDiplomaList[18]:='Sergeant';

    MonsterInit;
    BonesInit;
    ClassInit;
    ChantInit;
    ItemInit;
    QuestInit;
    InitStatements;

    // create random maps for situations in which we don't have a predef map
    for i:=1 to WinLevel do
        CreatePuzzleLandscape(i);

    blStartGame:=false;
    PlayMusic('title.mp3');
    repeat
        TitleSelection := TitleScreen();
        ClearScreenSDL;

        if UseSDL=true then
        begin
            LoadImage_Title(PathData+'graphics/plotbg.jpg');
            BlitImage_Title;
        end;

        if TitleSelection<3 then
        begin
            ThePlayer.strName := GetTextInput('How would you like to be called? Enter a name:', 12);
            if length(ThePlayer.strName)=0 then
                ThePlayer.strName:='Anonymous';
            if ThePlayer.strName='ESC' then
            begin
                GetKeyInput ('Do not choose name "ESC".', true);
                if UseSDL=true then
                begin
                    LoadImage_Title(PathData+'graphics/title.jpg');
                end;
            end
            else if fileexists('saves/' + ThePlayer.strName + '.lambdarogue') then
            begin
                GetKeyInput ('There already exists a saved character with that name.', true);
                if UseSDL=true then
                begin
                    LoadImage_Title(PathData+'graphics/title.jpg');
                end;
            end
            else
                blStartGame:=true;
        end;

        // Load Game
        if TitleSelection=3 then
        begin
            THePlayer.strName:=ShowSavegames;
            if ThePlayer.strName<>'ESC' then
            begin
                if fileexists(PathUserWritable+'saves/' + ThePlayer.strName + '.lambdarogue') then
                begin
                    InitDungeon;
                    LoadGame(ThePlayer.strName);
                    Difficulty;
                    DeleteFile(PathUserWritable+'saves/' + ThePlayer.strName + '.lambdarogue');
                    FillDungeon(DungeonLevel);
                    blStartGame:=true;
                end
                else
                begin
                    GetKeyInput('Error: savegame not found.', true);
                    if UseSDL=true then
                    begin
                        LoadImage_Title(PathData+'graphics/title.jpg');
                    end;
                end;
            end
            else
            begin
                if UseSDL=true then
                begin
                    LoadImage_Title(PathData+'graphics/title.jpg');
                end;
            end;
        end;

        // Quit
        if TitleSelection=4 then
        begin
            StopMusic;
            FreeMusic;
            StopSFX;
            FreeSFX;
            StopGraphics;
            halt;
        end;
    until blStartGame = true;

    case TitleSelection of
        1:
            CreatePlayer;
        2:
            CreatePlayerFast;
    end;


    // initialization of new characters
    if TitleSelection<3 then
    begin
        Difficulty;
        DungeonLevel := 1;
        CreateDungeon(DungeonLevel);

        // intro texts
        // ShowPlot(6);

        PlaySFX('page-turn.mp3');
        ShowText('chapter-0');

        ShowPlot(1);

        // initial placement of player
        PlacePlayer(-1,0);
        NextTurn();
    end;

    if UseSDL=false then
    begin
        intCenterX := 80 div 2;
        intCenterY := 25 div 2;
    end
    else
    begin
        if UseSmallTiles=true then
        begin
            if UseHiRes=false then
            begin
                intCenterX := 40 div 2;
                intCenterY := 10 div 2;
            end
            else
            begin
                intCenterX := 51 div 2;
                intCenterY := (19 div 2)-1;
            end
        end
        else
        begin
            if UseHiRes=false then
            begin
                intCenterX := 20 div 2;
                intCenterY := 4 div 2;
            end
            else
            begin
                intCenterX := 25 div 2;
                intCenterY := (9 div 2)-1;
            end;
        end;
    end;

    ThePlayer.intBX := intCenterX;
    ThePlayer.intBY := intCenterY;

    ClearScreenSDL;

    PlayMusic(IntToStr(trunc(random(8)+1))+'.mp3');

    if ThePlayer.longLevelVisits[1]=0 then
        ThePlayer.longLevelVisits[1]:=1;

    blTodayStolenTrader := false;
    blTodayStolenNPC := false;

    for i:=1 to 8 do
        blShopStolen[i] := false;

    if ThePlayer.blEvil=false then
    begin
        if ThePlayer.strReli = 'Ares' then
            ShowTransMessage(ThePlayer.strReli +' salutes you, ' + ThePlayer.strName + '!', false);
        if ThePlayer.strReli = 'Aphrodite' then
            ShowTransMessage(ThePlayer.strReli +' sends her love to you, ' + ThePlayer.strName + '!', false);
        if ThePlayer.strReli = 'Dionysos' then
            ShowTransMessage(ThePlayer.strReli +' raises his glass, ' + ThePlayer.strName + '!', false);
        if ThePlayer.strReli = 'Hermes' then
            ShowTransMessage(ThePlayer.strReli +' greets you, ' + ThePlayer.strName + '!', false);
        if ThePlayer.strReli = 'Apoll' then
            ShowTransMessage(ThePlayer.strReli +' welcomes you, ' + ThePlayer.strName + '!', false);
    end
    else
        ShowTransMessage('Nobody welcomes you, ' + ThePlayer.strName + '!', false);

    ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
    if UseSDL=true then
        SDL_UPDATERECT(screen,0,0,0,0)
    else
        UpdateScreen(true);


    repeat
        chKey := ' ';
        KeyRepeatSoll:=1;
        KeyRepeatIst:=0;

        repeat
//             Writeln(IntToStr(ThePlayer.intX)+'/'+IntToStr(ThePlayer.intY));

            k:=0;
            FunKey:='-';
            KX:=0;
            KY:=0;

            if ThePlayer.blEvil=true then
            begin
                DngLvl[ThePlayer.intX,ThePlayer.intY].intAirType:=2;
                DngLvl[ThePlayer.intX,ThePlayer.intY].intAirRange:=6;
            end;

            // wait for pressed key
            if UseSDL=true then
            begin

                if SDL_WaitEvent( @MyEvent ) > 0 then
                begin
                    KX:=MyEvent.motion.x;
                    KY:=MyEvent.motion.y;

                    //if (KX-42>=ChantBarX) and (KY>=ChantBarY) and (KX-42<=ChantBarX+100) and (KY<=ChantBarY+40) then
                    //begin
                        //SmallTextXY(5, 5, 'Chantbar', true);
                        //SDL_UPDATERECT(screen,0,0,0,0);
                    //end;

                    Knoepfe := SDL_GetMouseState(KX, KY);
                    if (Knoepfe and SDL_BUTTON(SDL_BUTTON_LEFT))<>0 then
                    begin
                        //k:=TransformClickInKey(KX, KY);   // if we ever want to re-use this function, we have to rewrite it for different resolutions
                        if UseHiRes=true then
                        begin
                            if (KX>=1001) and (KY>=83) and (KX<=1013) and (KY<=95) then
                                k:=ord(KeyBigMap);
                        end
                        else
                        begin
                            if (KX>=783) and (KY>=83) and (KX<=795) and (KY<=95) then
                                k:=ord(KeyBigMap);
                        end;
                    end;

                    if (Knoepfe and SDL_BUTTON(SDL_BUTTON_RIGHT))<>0 then
                    begin
                        ChantBarX := KX-42;
                        ChantBarY := KY;
                        ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
                        ShowStatus;
                    end;

                    if (k=0) and (MyEvent.type_ = SDL_KEYDOWN) then
                        k:=cookKey(MyEvent.key.keysym.unicode, MyEvent.key.keysym.sym);
                end;
            end
            else
                k:=ConsoleInput();

            case k of

                SDLK_ESCAPE:
                    k := ord(KeyQuit);
                13:
                    k := ord(KeyEnter);
                SDLK_UP:
                    k := ord(KeyNorth);
                SDLK_KP8:
                    k := ord(KeyNorth);
                SDLK_DOWN:
                    k := ord(KeySouth);
                SDLK_KP2:
                    k := ord(KeySouth);
                SDLK_LEFT:
                    k := ord(KeyWest);
                SDLK_KP4:
                    k := ord(KeyWest);
                SDLK_RIGHT:
                    k := ord(KeyEast);
                SDLK_KP6:
                    k := ord(KeyEast);
                SDLK_KP9:
                    k := ord(KeyNorthEast);
                SDLK_KP7:
                    k := ord(KeyNorthWest);
                SDLK_KP3:
                    k := ord(KeySouthEast);
                SDLK_KP1:
                    k := ord(KeySouthWest);
                ord('1'):
                    k := ord(KeySouthWest);
                ord('2'):
                    k := ord(KeySouth);
                ord('3'):
                    k := ord(KeySouthEast);
                ord('4'):
                    k := ord(KeyWest);
                ord('6'):
                    k := ord(KeyEast);
                ord('7'):
                    k := ord(KeyNorthWest);
                ord('8'):
                    k := ord(KeyNorth);
                ord('9'):
                    k := ord(KeyNorthEast);
                SDLK_F1:
                    FunKey := 'F1';
                SDLK_F2:
                    FunKey := 'F2';
                SDLK_F3:
                    FunKey := 'F3';
                SDLK_F4:
                    FunKey := 'F4';
                SDLK_F5:
                    FunKey := 'F5';
                SDLK_F6:
                    FunKey := 'F6';
                SDLK_F7:
                    FunKey := 'F7';
                SDLK_F8:
                    FunKey := 'F8';
                SDLK_F9:
                    FunKey := 'F9';
                SDLK_F10:
                    FunKey := 'F10';
                SDLK_F11:
                    FunKey := 'F11';
                SDLK_F12:
                    FunKey := 'F12';
            end;
            
        until ((k>31) and (k<127)) or (FunKey<>'-');

//         writeln(chr(k));


        // evaluate pressed key (except the F1 - F12 keys)
        if k=ord(KeyNorth) then
            chKey:=KeyNorth
        else
        if k=ord(KeySouth) then
            chKey:=KeySouth
        else
        if k=ord(KeyEast) then
            chKey:=KeyEast
        else
        if k=ord(KeyWest) then
            chKey:=KeyWest
        else
        if k=ord(KeyNorthEast) then
            chKey:=KeyNorthEast
        else
        if k=ord(KeyNorthWest) then
            chKey:=KeyNorthWest
        else
        if k=ord(KeySouthEast) then
            chKey:=KeySouthEast
        else
        if k=ord(KeySouthWest) then
            chKey:=KeySouthWest;

        if k=ord(KeyQuit) then
            chKey:=KeyQuit;


        // cheat
        //if k=ord('B') then
            //chKey:='B';

        if k=ord(KeyTunnel) then
            chKey:=KeyTunnel
        else
        if k=ord(KeySetQuickKeys) then
            chKey:=KeySetQuickKeys
        else
        if k=ord(KeyEnter) then
            chKey:=KeyEnter
        else
        if k=ord(KeyInventory) then
            chKey:=KeyInventory
        else
        if k=ord(KeyTake) then
            chKey:=KeyTake
        else
        if k=ord(KeyHelp) then
            chKey:=KeyHelp
        else
        if k=ord(KeyCloseDoor) then
            chKey:=KeyCloseDoor
        else
        if k=ord(KeyQuestlog) then
            chKey:=KeyQuestlog;

        if k=ord(KeyPray) then
            chKey:=KeyPray
        else
        if k=ord(KeyLook) then
            chKey:=KeyLook
        else
        if k=ord(KeyStatus) then
            chKey:=KeyStatus
        else
        if k=ord(KeyTrade) then
            chKey:=KeyTrade
        else
        if k=ord(KeyShoot) then
            chKey:=KeyShoot
        else
        if k=ord(KeyChant) then
            chKey:=KeyChant;

        if k=ord(KeyRest) then
            chKey:=KeyRest
        else
        if k=ord(KeyShortRest) then
            chKey:=KeyShortRest
        else
        if k=ord(KeyChantLast) then
            chKey:=KeyChantLast
        else
        if k=ord(KeySearchSteal) then
            chKey:=KeySearchSteal
        else
        if k=ord(KeyBigMap) then
            chKey:=KeyBigMap;


        // confusion?
        if ThePlayer.intConfusion > 0 then
            if chKey=KeyNorth then
                chKey:=KeySouth
            else
            if chKey=KeySouth then
                chKey:=KeyNorth
            else
            if chKey=KeyEast then
                chKey:=KeyWest
            else
            if chKey=KeyWest then
                chKey:=KeyEast
            else
            if chKey=KeyNorthEast then
                chKey:=KeySouthWest
            else
            if chKey=KeyNorthWest then
                chKey:=KeySouthEast
            else
            if chKey=KeySouthEast then
                chKey:=KeyNorthWest
            else
            if chKey=KeySouthWest then
                chKey:=KeyNorthEast;


        // cheat
        //if chKey='B' then
            //CheatCodes;


        // function keys
        if FunKey='F1' then
            QuickKeys(1);
        if FunKey='F2' then
            QuickKeys(2);
        if FunKey='F3' then
            QuickKeys(3);
        if FunKey='F4' then
            QuickKeys(4);
        if FunKey='F5' then
            QuickKeys(5);
        if FunKey='F6' then
            QuickKeys(6);
        if FunKey='F7' then
            QuickKeys(7);
        if FunKey='F8' then
            QuickKeys(8);
        if FunKey='F9' then
            QuickKeys(9);
        if FunKey='F10' then
            QuickKeys(10);
        if FunKey='F11' then
            QuickKeys(11);
        if FunKey='F12' then
            QuickKeys(12);


        if chKey=KeyTake then
        begin
            if DngLvl[ThePlayer.intX, ThePlayer.intY].intItem > 0 then
                TakeItem(ThePlayer.intX, ThePlayer.intY)
            else
            begin
                // small mushrooms
                if DngLvl[ThePlayer.intX, ThePlayer.intY].intFloorType = 54 then
                begin
                    DngLvl[ThePlayer.intX, ThePlayer.intY].intFloorType := 12;
                    DngLvl[ThePlayer.intX, ThePlayer.intY].intItem := ReturnItemByName('Small mushroom');
                    ShowTransMessage('You harvest a small mushroom.', false);
                    ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
                    if UseSDL=true then
                        SDL_UPDATERECT(screen,0,0,0,0)
                    else
                        UpdateScreen(true);
                end;

                // big mushrooms
                if DngLvl[ThePlayer.intX, ThePlayer.intY].intFloorType = 53 then
                begin
                    DngLvl[ThePlayer.intX, ThePlayer.intY].intFloorType := 12;
                    DngLvl[ThePlayer.intX, ThePlayer.intY].intItem := ReturnItemByName('Big mushroom');
                    ShowTransMessage('You harvest a big mushroom.', false);
                    ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
                    if UseSDL=true then
                        SDL_UPDATERECT(screen,0,0,0,0)
                    else
                        UpdateScreen(true);
                end;

                // wooden garbage
                if DngLvl[ThePlayer.intX, ThePlayer.intY].intFloorType = 55 then
                begin
                    DngLvl[ThePlayer.intX, ThePlayer.intY].intFloorType := 2;
                    inc(Storage.longWood,2+trunc(random(3)));
                    ShowTransMessage('You put some wooden garbage in your resource storage.', false);
                    ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
                    if UseSDL=true then
                        SDL_UPDATERECT(screen,0,0,0,0)
                    else
                        UpdateScreen(true);
                end;

                // old machine parts
                if DngLvl[ThePlayer.intX, ThePlayer.intY].intFloorType = 58 then
                begin
                    DngLvl[ThePlayer.intX, ThePlayer.intY].intFloorType := 2;
                    inc(Storage.longMetal,3+trunc(random(3)));
                    ShowTransMessage('You transfer metal from the floor to your resource storage.', false);
                    ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
                    if UseSDL=true then
                        SDL_UPDATERECT(screen,0,0,0,0)
                    else
                        UpdateScreen(true);
                end;
            end;
            blQuit := NextTurn();
        end;

        if chKey=KeyHelp then
        begin
            HelpScreen;
            blQuit := NextTurn();
        end;

        if chKey=KeyBigMap then
        begin
            if UseSDL=true then
            begin
                BigMap;
                blQuit:=NextTurn();
            end;
        end;

        if chKey=KeySetQuickKeys then
        begin
            SetQuickKeys;
            blQuit := NextTurn();
        end;

        if chKey=KeyStatus then
        begin
            TidyPlayerStatus;
            blQuit := NextTurn();
        end;

        if chKey=KeyQuestlog then
        begin
            ShowStatusQuestlog;
            blQuit := NextTurn();
        end;


        // movement keys
        vx:=0;
        vy:=0;
        if (chKey=KeyNorth) and (ThePlayer.intY>1) then
        begin
            vx:=0;
            vy:=-1;
        end;
        if (chKey=KeySouth) and (ThePlayer.intY<DngMaxHeight) then
        begin
            vx:=0;
            vy:=1;
        end;
        if (chKey=KeyEast) and (ThePlayer.intX<DngMaxWidth) then
        begin
            vx:=1;
            vy:=0;
        end;
        if (chKey=KeyWest) and (ThePlayer.intX>1) then
        begin
            vx:=-1;
            vy:=0;
        end;
        if (chKey=KeyNorthEast) and (ThePlayer.intY>1) and (ThePlayer.intX<DngMaxWidth) then
        begin
            vx:=1;
            vy:=-1;
        end;
        if (chKey=KeyNorthWest) and (ThePlayer.intY>1) and (ThePlayer.intX>1) then
        begin
            vx:=-1;
            vy:=-1;
        end;
        if (chKey=KeySouthEast) and (ThePlayer.intY<DngMaxHeight) and (ThePlayer.intX<DngMaxWidth) then
        begin
            vx:=1;
            vy:=1;
        end;
        if (chKey=KeySouthWest)  and (ThePlayer.intY<DngMaxHeight) and (ThePlayer.intX>1) then
        begin
            vx:=-1;
            vy:=1;
        end;

        // move
        if (ThePlayer.intPara=0) and (DngLvl[ThePlayer.intX,ThePlayer.intY].intAirType<>6) then
        begin
            if (chKey=KeyNorth) or (chKey=KeySouth) or (chKey=KeyEast) or (chKey=KeyWest) or (chKey=KeyNorthEast) or (chKey=KeySouthEast) or (chKey=KeyNorthWest) or (chKey=KeySouthWest) then
            begin
                repeat
                    // check for and open door
                    OpenDoor(chKey);

                    // if free tile, move into that direction
                    if DngLvl[ThePlayer.intX+vx, ThePlayer.intY+vy].intIntegrity=0 then
                    begin
                        if FightMonster(ThePlayer.intX+vx, ThePlayer.intY+vy) then
                        begin
                            if ThePlayer.intPara=0 then
                            begin
                                inc (ThePlayer.intX,vx);
                                inc (ThePlayer.intY,vy);
                                blQuit := NextTurn();
                            end;
                        end
                        else
                            blQuit:=NextTurn();
                    end;

                    // kick barrel
                    if DngLvl[ThePlayer.intX+vx, ThePlayer.intY+vy].intFloorType=52 then
                    begin
                        ShowTransMessage('You kick the barrel. [Integrity: '+IntToStr(DngLvl[ThePlayer.intX+vx, ThePlayer.intY+vy].intIntegrity)+']', false);
                        if ThePlayer.intWeapon>0 then
                            Dec(DngLvl[ThePlayer.intX+vx, ThePlayer.intY+vy].intIntegrity, Thing[ThePlayer.intWeapon].intWP)
                        else
                            Dec(DngLvl[ThePlayer.intX+vx, ThePlayer.intY+vy].intIntegrity, 1);
                        if DngLvl[ThePlayer.intX+vx, ThePlayer.intY+vy].intIntegrity<1 then
                        begin
                            DngLvl[ThePlayer.intX+vx, ThePlayer.intY+vy].intFloorType:=55;
                            BarrelDestroyed(ThePlayer.intX+vx, ThePlayer.intY+vy);
                        end;
                        blQuit := NextTurn();
                    end;

                    // kick gas cylinder
                    if DngLvl[ThePlayer.intX+vx, ThePlayer.intY+vy].intFloorType=56 then
                    begin
                        ShowTransMessage('You kick the gas cylinder. [Integrity: '+IntToStr(DngLvl[ThePlayer.intX+vx, ThePlayer.intY+vy].intIntegrity)+']', false);
                        if ThePlayer.intWeapon>0 then
                            Dec(DngLvl[ThePlayer.intX+vx, ThePlayer.intY+vy].intIntegrity, Thing[ThePlayer.intWeapon].intWP)
                        else
                            Dec(DngLvl[ThePlayer.intX+vx, ThePlayer.intY+vy].intIntegrity, 1);
                        if DngLvl[ThePlayer.intX+vx, ThePlayer.intY+vy].intIntegrity<1 then
                        begin
                            DngLvl[ThePlayer.intX+vx, ThePlayer.intY+vy].intFloorType:=55;
                            CylinderDestroyed(ThePlayer.intX+vx, ThePlayer.intY+vy);
                            ThePlayer.intHP := ThePlayer.intHP div 2;
                        end;
                        blQuit := NextTurn();
                    end;

                    // attack monster's hive
                    if DngLvl[ThePlayer.intX+vx, ThePlayer.intY+vy].intFloorType=34 then
                    begin
                        KeyRepeatIst:=KeyRepeatSoll;
                        ShowTransMessage('You attack the monster'+chr(39)+'s hive. [Integrity: '+IntToStr(DngLvl[ThePlayer.intX+vx, ThePlayer.intY+vy].intIntegrity)+']', false);
                        if ThePlayer.intWeapon>0 then
                            Dec(DngLvl[ThePlayer.intX+vx, ThePlayer.intY+vy].intIntegrity, Thing[ThePlayer.intWeapon].intWP)
                        else
                            Dec(DngLvl[ThePlayer.intX+vx, ThePlayer.intY+vy].intIntegrity, 1);
                        if DngLvl[ThePlayer.intX+vx, ThePlayer.intY+vy].intIntegrity<1 then
                        begin
                            DngLvl[ThePlayer.intX+vx, ThePlayer.intY+vy].intFloorType:=2;
                            HiveDestroyed;
                        end;
                        blQuit := NextTurn();
                    end;

                    inc(KeyRepeatIst);

                until (KeyRepeatIst >= KeyRepeatSoll) or (blQuit=true);
            end;
        end
        else
        begin
            //ShowMessage('You can'+chr(39)+'t move.', false);
            blQuit := NextTurn();
        end;

        if chKey=KeyQuit then
        begin
            if GameMenu=true then
                blQuit:=true;
        end;

        if chKey=KeyTunnel then
        begin
            Dig;
            blQuit := NextTurn();
        end;

        if chKey=KeyRest then
        begin
            Rest;
            blQuit := NextTurn();
        end;

        if chKey=KeyShortRest then
            blQuit := NextTurn();

        if chKey=KeyShoot then
        begin
            Shoot;
            blQuit := NextTurn();
        end;


        if chKey=KeyChant then
        begin
            ShowSpellbook;
            blQuit:=NextTurn();
        end;

        if chKey=KeyChantLast then
        begin
            ChantLastSong;
            blQuit:=NextTurn();
        end;

        if chKey=KeyLook then
        begin
            IdentifyTile;
            blQuit:=NextTurn();
        end;

        if chKey=KeyPray then
        begin
            Pray;
            blQuit:=NextTurn();
        end;

        if chKey=KeyEnter then
        begin
            // treasure chest
            if DngLvl[ThePlayer.intX, ThePlayer.intY].intFloorType = 7 then
                LootChest
            else
            // next level
            if (DngLvl[ThePlayer.intX, ThePlayer.intY].intFloorType = 9) then //and (DungeonLevel < WinLevel) then
            begin
                inc(DungeonLevel);

                if fileexists(PathData+'levels/' + IntToStr(DungeonLevel) + '.txt')=false then
                    CreatePuzzleLandscape(DungeonLevel);

                if DungeonLevel=2 then
                    if ThePlayer.longLevelVisits[2]<1 then
                    begin
                        PlaySFX('page-turn.mp3');
                        ShowText('chapter-1');
                    end;

                if DungeonLevel=23 then
                    if ThePlayer.longLevelVisits[23]<1 then
                        if (ThePlayer.blUnKilled[ReturnMonTeByName('Undying King')]=false) and (ThePlayer.blUnKilled[ReturnMonTeByName('Eris')]=false) then
                        begin
                            PlaySFX('page-turn.mp3');
                            ShowText('interlude-c');
                            ShowPlot(28);
                        end;

                if DungeonLevel=25 then
                    if ThePlayer.longLevelVisits[25]<1 then
                        if (ThePlayer.blUnKilled[ReturnMonTeByName('Undying King')]=false) and (ThePlayer.blUnKilled[ReturnMonTeByName('Eris')]=false) then
                            ShowPlot(29);


                PlaySFX('steps.mp3');

                //if DungeonLevel <= WinLevel then
                //begin
                    CreateDungeon(DungeonLevel);
                    PlacePlayer(8, DungeonLevel-1);
                    inc(ThePlayer.longLevelVisits[DungeonLevel]);

                    // corpse of Lassa Kana found (for Northdoom Quest)
                    if DungeonLevel=13 then
                        if ThePlayer.intQuestState[5,4]=1 then
                            if ThePlayer.blStory[16]=false then
                            begin
                                ShowPlot(16);
                                DngLvl[ThePlayer.intX, ThePlayer.intY].intItem:=ReturnItemByName('Crystal of "Revenge"');
                            end;

                    NextTurn;
                    LevelFeeling;
                    ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
                //end;
            end
            else
            // prev. level
            if (DngLvl[ThePlayer.intX, ThePlayer.intY].intFloorType = 8) and (DungeonLevel > 1) then
            begin
                if fileexists(PathData+'levels/' + IntToStr(DungeonLevel) + '.txt')=false then
                    CreatePuzzleLandscape(DungeonLevel);

                dec(DungeonLevel);
                PlaySFX('steps.mp3');
                CreateDungeon(DungeonLevel);
                PlacePlayer(9, DungeonLevel+1);
                inc(ThePlayer.longLevelVisits[DungeonLevel]);
                NextTurn;
                LevelFeeling;
                ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
            end
            else
            // stairs in lvl 2 to wilderness (lvl 22)
            if DngLvl[ThePlayer.intX, ThePlayer.intY].intFloorType = 62 then
            begin
                PlaySFX('steps.mp3');
                DungeonLevel:=22;
                if fileexists(PathData+'levels/' + IntToStr(DungeonLevel) + '.txt')=false then
                    CreatePuzzleLandscape(DungeonLevel);
                CreateDungeon(DungeonLevel);
                PlacePlayer(-1, DungeonLevel+1);
                ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
                if ThePlayer.longLevelVisits[22]<1 then
                    ShowPlot(31);
                inc(ThePlayer.longLevelVisits[DungeonLevel]);
                NextTurn;
                LevelFeeling;
                ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
            end
            else
            // stairs in wilderness (lvl 22) to lvl 2
            if DngLvl[ThePlayer.intX, ThePlayer.intY].intFloorType = 63 then
            begin
                PlaySFX('steps.mp3');
                DungeonLevel:=2;
                CreateDungeon(DungeonLevel);
                PlacePlayer(9, DungeonLevel+1);
                inc(ThePlayer.longLevelVisits[DungeonLevel]);
                NextTurn;
                LevelFeeling;
                ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
            end
            else
            // portal through time to Noldarur
            if DngLvl[ThePlayer.intX, ThePlayer.intY].intFloorType = 59 then
            begin
                PlaySFX('swoosh.mp3');
                if DungeonLevel=1 then
                begin
                    DungeonLevel:=21;
                    if fileexists(PathData+'levels/' + IntToStr(DungeonLevel) + '.txt')=false then
                        CreatePuzzleLandscape(DungeonLevel);
                    CreateDungeon(DungeonLevel);
                    PlacePlayer(-1, DungeonLevel+1);
                end
                else
                begin
                    DungeonLevel:=1;
                    CreateDungeon(DungeonLevel);
                    PlacePlayer(1, 0);
                end;
                inc(ThePlayer.longLevelVisits[DungeonLevel]);
                NextTurn;
                LevelFeeling;
                ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
            end
            else
            // drink from well
            if DngLvl[ThePlayer.intX,ThePlayer.intY].intFloorType=28 then
            begin
                DrinkFromWell;
                blQuit := NextTurn();
            end
            else
            // sacrifice
            if DngLvl[ThePlayer.intX, ThePlayer.intY].intFloorType=15 then
            begin
                Sacrifice;
                blQuit := NextTurn();
            end
            else
            // examine crypt
            if DngLvl[ThePlayer.intX,ThePlayer.intY].intFloorType=26 then
            begin
                ExamineCrypt;
                blQuit := NextTurn();
            end
            else
            // examine crypt in DLV 1
            if DngLvl[ThePlayer.intX,ThePlayer.intY].intFloorType=33 then
            begin // we have to check both for "Eris' Key" and for "Unknown Key", cause the player might not have identified the key as Eris' key yet
                if (DungeonLevel=1) and ((PlayerHasItem('Eris'+chr(39)+' Key')=-1) and (PlayerHasItem('unknown key')=-1)) then
                    DngLvl[ThePlayer.intX,ThePlayer.intY].intItem := ReturnItemByName('Eris'+chr(39)+' Key');
                blQuit := NextTurn();
            end;

        end;

        // search for hidden door; steal money or items (thief only)
        if chKey=KeySearchSteal then
        begin
            SearchAndSteal;
            blQuit := NextTurn();
        end;

        if chKey=KeyTrade then
        begin
            // trader
            if (DngLvl[ThePlayer.intX, ThePlayer.intY].intBuilding > 0) and (DngLvl[ThePlayer.intX, ThePlayer.intY].intBuilding < 5) then
            begin
                if blShopStolen[DngLvl[ThePlayer.intX, ThePlayer.intY].intBuilding]=false then
                    ShowShop(DngLvl[ThePlayer.intX, ThePlayer.intY].intBuilding)
                else
                    ShowTransMessage('Due to your criminal record, you are temporarly banned from this trader.', false);
                blQuit := NextTurn();
            end
            else
            // hospital
            if DngLvl[ThePlayer.intX, ThePlayer.intY].intBuilding = 5 then
            begin
                Hospital;
                blQuit := NextTurn();
            end
            else
            // restaurant
            if DngLvl[ThePlayer.intX, ThePlayer.intY].intBuilding = 6 then
            begin
                Restaurant;
                blQuit := NextTurn();
            end
            else
            // academy
            if DngLvl[ThePlayer.intX, ThePlayer.intY].intBuilding = 7 then
            begin
                Academy;
                blQuit := NextTurn();
            end
            else
            // resource workshop
            if DngLvl[ThePlayer.intX, ThePlayer.intY].intBuilding = 8 then
            begin
                ResourceWorkshop;
                blQuit := NextTurn();
            end
            else
            // NPC
            if DngLvl[ThePlayer.intX, ThePlayer.intY].intBuilding < 0 then
            begin
                TalkNPC(DungeonLevel, abs(DngLvl[ThePlayer.intX, ThePlayer.intY].intBuilding));
                blQuit := NextTurn();
            end;
        end;

        if chKey=KeyCloseDoor then
        begin
            CloseDoor;
            blQuit:=NextTurn();
        end;

        if chKey=KeyInventory then
        begin
            ShowInventory;
            blQuit:=NextTurn();
        end;

        // trapdoor?

        if (chKey=KeyNorth) or (chKey=KeySouth) or (chKey=KeyEast) or (chKey=KeyWest) or (chKey=KeyNorthEast) or (chKey=KeyNorthWest) or (chKey=KeySouthEast) or (chKey=KeySouthWest) then
        begin
            if ((DungeonLevel=4) or (DungeonLevel=9) or (DungeonLevel=14)) and (random(9999)>CONST_BEATTHISFORTRAPDOOR) then
            begin
                if DngLvl[ThePlayer.intX, ThePlayer.intY].intFloorType=2 then
                begin
                    if (ThePlayer.intProf<>3) and (CheckEffect(41)=false) then
                    begin
                        GetKeyInput('Suddenly a hole opens in the floor and you fall down ...', true);
                        if fileexists(PathData+'levels/' + IntToStr(DungeonLevel) + '.txt')=false then
                            CreatePuzzleLandscape(DungeonLevel);
                        inc(DungeonLevel);
                        CreateDungeon(DungeonLevel);
                        PlacePlayer(-1, 0);
                        inc(ThePlayer.longLevelVisits[DungeonLevel]);
                        NextTurn;
                        ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
                        LevelFeeling;
                    end
                    else
                        ShowTransMessage('You avoid a trapdoor.', false);
                end;
            end;
        end;


        if IsPlayerDead=true then
            blQuit := true;

        If MusicPlaying=false then
            PlayMusic(IntToStr(trunc(random(8)+1))+'.mp3');

        if UseSDL=false then
            UpdateScreen(true);

    until blQuit = true;

    if ThePlayer.intHP > 0 then
        SaveGame(ThePlayer.strName, DungeonLevel);

    StopGraphics;
    StopMusic;
    FreeMusic;
    StopSFX;
    FreeSFX;

end.
