{
     Copyright (C) 2006-2008 by Mario Donick    
     mario.donick@gmail.com
                                                                            
     This program is free software; you can redistribute it and/or modify   
     it under the terms of the GNU General Public License as published by   
     the Free Software Foundation; either version 2 of the License, or      
     (at your option) any later version.                                    
                                                                            
     This program is distributed in the hope that it will be useful,        
     but WITHOUT ANY WARRANTY; without even the implied warranty of         
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
     GNU General Public License for more details.                           
                                                                            
     You should have received a copy of the GNU General Public License      
     along with this program; if not, write to the                          
     Free Software Foundation, Inc.,                                        
     59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              
}


// contains types, variables and procedures to initialize player and monsters
unit Player;


interface

uses
    SysUtils, RandomArea;

type CharaClass = record
        strName: string;        // name of class (e.g. "Ares")
        intFight: integer;        // main ability "fight"
        intMove: integer;        // main ability "move"
        intChant: integer;        // main ability "chant"
        intView: integer;        // main ability "view"
        intBurgle: integer;        // main ability "code"
        intTool: integer;        // main ability "tool"
        afSkill: string;        // the related skill
        afStat: string;            // the related status value
    end;


type Creature = record            // prototype to store information about player and monsters
        strName: string;

        // player and monster-relevant
        intHP: integer;            // current health
        intMaxHP: integer;        // max. health
        intPP: integer;            // current psychic power
        intMaxPP: integer;        // max. psychic power
        longExp: longint;        // player: experience. monster: how many exp. is a kill worth?
        intLvl: longint;        // player: character level. monster: dungeonlevel on which monster first appears
        longGold: longint;        // player: credits. monster: how many credits is a kill worth?
        intX, intY: integer;        // position in dungeon
        intStrength: integer;        // strength
        longScore: longint;        // total score
        intMove: integer;        // main ability "move"

        // only player-relevant
        blDead: boolean; // game over screen already shown
        blEvil: boolean;  // player is very bad; happens when Sword "Northdoom" equipped or humility < -5
        intDipl: array[1..30] of integer; // diplomas; 1: gained
        intQuestState: array[1..25,1..9] of integer;  // quest state (dlvl, npc)
        intQuestHunt: array[1..25,1..9] of integer; // amount of creatures to hunt for all level)
        longSkillPoints: longint;    // collected:skill points
        intSex: integer;        // male, female or neuter
        longFood: longint;        // food of player (0: death)
        intBX, intBY: integer;        // screen position of player (80x25 grid)
        strReli: string;        // name of player's religion
        intReli: integer;        // ID of player's religion
        intProf: integer;        // ID of player's profession
        strVita: string;        // short vita
        strDescription: string;        // appearance
        strDiploma: string;        // additional diplomas
        longLifeIns: longint;        // number of life insurances (= saves)

        intFight: integer;        // main ability "fight"
        intChant: integer;        // main ability "chant"
        intView: integer;        // main ability "view"
        intBurgle: integer;        // main ability "code"
        intSword: integer;        // sec. ability "sword"
        intAxe: integer;        // sec. ability "axe"
        intWhip: integer;        // sec. ability "whip"
        intGun: integer;        // sec. ability "gun"
        intTool: integer;        // sec. ability "tool"
        intHumility: integer;        // sec. ability "humility"
        intTrade: integer;        // sec. ability "trade"


        intWeapon: integer;        // item used as weapon
        intArmour: integer;        // item used as armour
        intHat: integer;        // item used as hat
        intRingLeft: integer;        // item used as ring on left hand
        intRingRight: integer;        // item used as ring on right hand
        intExtra: integer;         // shield or candle
        intFeet: integer;       // shoes etc.


        intPoison: integer;        // strongness of poison
        intConfusion: integer;        // strongness of Confusion
        intInvis: integer;        // strongness of Invisibility
        intBlind: integer;        // strongness of Blindness
        intSleep: integer;        // strongness of sleep
        intWall: integer;        // strongness of wall
        longPrayers: longint;        // how many times prayed to god
        intPara: integer;        // strongness of paralyze
        intCalm: integer;        // strongness of quietness
        intFreeze: integer;        // strongness of frozen time
        blCursed: boolean;        // cursed or not cursed?
        blBlessed: boolean;        // blessed or not?

        intTempResist: array [1..100] of integer;  // every element is one resistance; the number is equivalent to the effectnumbers used elsewhere;
                                                   // the value are the remaining turns (i think normal integer should be enough ...)

        intLimit: integer;

        intTotalVitari, intNextVitari, intNeedVitari: integer;   // addiction to Vitari (strength drung)

        blUnKilled: array [1..200] of boolean;    // array of all unique monsters killed by the player
        longKilled: array[1..200] of longint; // array that stores the total number of all killed

        blStory: array [1..80] of boolean; // storys already told story parts
        longLevelVisits: array [1..30] of longint; // the number of visits of a dungeon level
        longContCleared: longint;    // number of cleared decontaminated tiles

        intTileset: integer;        // tileset
        intLastSong: integer;        // the last song selected in quick songbook

        strMessageLog: array of string; // contains the last messages (dynamically growing)
        intNumberOfLogs: integer;

        // only monster relevant
        blAttacked: boolean;    // true: monster has been attacked at least once by the player
        intQuestS: integer;      // Quest that has to be solved before the monster appears
                            // (the Quest level taken for this is the same as the min. DLV of the monster)
        intQuestG: integer;      // Quest that has to be open before the monster appears
                            // (the Quest level taken for this is the same as the min. DLV of the monster)

        blSelfHeal: boolean;    // true: Monster can heal itself
        blWeaken: boolean;      // true: monster absorbs "str" from player
        blFire: boolean;        // true: monster can cast fire
        blIce: boolean;            // true: monster can cast ice
        blWaterC: boolean;        // true: monster can cast water
        blPoison: boolean;        // true: monster can poison the player
        blConfusion: boolean;        // true: monster can confuse the player
        blBlindness: boolean;        // true: monster can make player blind
        blPara: boolean;        // true: monster can paralyze player
        blInvis: boolean;        // true: monster can make itself invisible
        blHitsHard: boolean;        // true: monster has a real hard attack
        blCalm: boolean;        // true: monster can make player quit (unable to chant)
        blSummon: boolean;        // true: monster can summon one monster
        blSplit: boolean;        // true: monster can split itself into smaller creatures
        strSplitInto: string;        // name of monster that appears after splitting
        blTeleport: boolean;        // true: monster can teleport itself to a distant position
        intTeleport: integer;        // distance a monster teleports
        blDrainPP: boolean;          // true: monster can drain player's PP
        intDrainPP: integer;         // max. amount PP drained by the monster
        blDrainSTR: boolean;          // true: monster can drain player's STR
        intDrainSTR: integer;         // max. amount STR drained by the monster

        blUnique: boolean;        // true: unique monster (extra strong, only 1 per level)
        blLastEnemy: boolean;        // true: monster is last enemy; after it's dead the game is won
        strVerb: string;        // verb shown while attacking (e.g. "bites", "hits" etc.)
        strShootVerb: string;        // verb shown for long-range attacks
        chLetter: char;            // ASCII-character used to represent monster on map
        chBulletLetter: char;        // ASCII-character used to represent bullet on map
        intWP: integer;            // weapon points
        intAP: integer;            // armour points
        intGP: integer;            // gun points (set to 0 if monster does not use long-range combat)
        intTemplate: integer;        // used to store the number of used template
        blWater: boolean;        // monster can move on water
        blCurse: boolean;        // monster can curse player
        blDestWeapon: boolean;        // monster can destroy player's weapon
        blDestArmour: boolean;        // monster can destroy player's armour
        intType: integer;        // ID
        blHuman: boolean;        // true: monster normally does not attack; once the player attacks, it gets false
        intHitRate: integer;        // hit rate of shots (0: every shot hits, 400: nothing hits)
        intKillPlot: integer;    // plot that is shown after the monster has been killed the first time
    end;


var
    ThePlayer : Creature;            // The player
    Monster : array[1..550] of Creature;    // every monster
    MaxMonster: longint;            // maximum number of monsters
    MonsterCount: longint;            // current number of monsters

    MonsterTemplates: Integer;        // number of available monstertemplates (for monster creation)
    MonTe : array[1..200] of Creature;    // max. 200 monster templates

    PlayClass: array[1..5] of CharaClass;
    PlayProf: array[1..9] of String;

    Achievements: array[1..500] of String;  // achievements like level up, new dungeon level explored, rank gained etc.
    intAchieved: integer;  // achievement counter


procedure StoreAchievement(s: String);
function ReturnMonTeByName(s: String): Integer;
procedure MonsterInit;
procedure BonesInit;
procedure SaveBones;
procedure ClassInit;


implementation

procedure StoreAchievement(s: String);
begin
    if intAchieved<500 then
    begin
        inc(intAchieved);
        Achievements[intAchieved]:=IntToStr(intDayTime) + '-' + IntToStr(intDay) + '-' + IntToStr(longYear)+' (turn '+IntToStr(longTotalTurns)+'):'+chr(9)+chr(9)+s;
    end;
end;

// returns the ID of a monster template
function ReturnMonTeByName(s: String): Integer;
var
    i: Integer;
begin
    i:=0;
    for i:=1 to MonsterTemplates do
        if MonTe[i].strName = s then
            ReturnMonTeByName:=i;
end;

// This procedure reads "data/monsters.txt" and creates monstertemplates which are
// used by the CreateMonster-procedure in Dungeon.pp
procedure MonsterInit;
var
    MonsterFile: textfile;            // used to assign "data/monsters.txt"
    i: integer;                // counter
    strKey, strValue: string;        // used to read key-value-pairs out of MonsterFile
    cstr: char;                // used to convert parts of string to char
begin

    for i:=1 to 200 do
    begin
        MonTe[i].blHuman:=false;
        MonTe[i].blAttacked := false;
        MonTe[i].strShootVerb:='shoots at you';
        MonTe[i].chBulletLetter := chr(160);
        MonTe[i].intHitRate:=100;    // decrease to let more shoots succeed (max. is 400)
        MonTe[i].intGP:=0;
        MonTe[i].intMove:=0;
        MonTe[i].intQuestS:=0;
        MonTe[i].intQuestG:=0;
        MonTe[i].intKillPlot:=0;
        MonTe[i].intMaxPP:=4;
        MonTe[i].intPP:=MonTe[i].intMaxPP;
    end;

    // initialize key-value-pair
    strKey := '';
    strValue := '';

    // open monsters.txt
    Assign (MonsterFile, PathData+'data/monsters.txt');
    Reset (MonsterFile);

    // initialize counter
    i:=0;

    // as long as monsters.txt not completely read...
    while eof(MonsterFile)=false do
    begin
        // search for key
        repeat
            ReadLn(MonsterFile, strKey);
        until ((strKey<>'') and (strKey[1]<>'#')) or (eof(MonsterFile));

        // key found, now search for corresponding value
        ReadLn(MonsterFile, strValue);

        // create new monster
        if strKey='NewMonster:' then
            inc(i);

        // if we have at least 1 monster, evaluate keys and values
        if i>0 then
        begin
            if strKey='NewMonster:' then
            begin
                MonTe[i].strName := strValue;
                MonTe[i].intType := i;
            end;
    
            if (strKey='Unique') and (strValue='= True') then
                MonTe[i].blUnique := true;
            if (strKey='Human') and (strValue='= True') then
                MonTe[i].blHuman := true;
            if (strKey='LastEnemy') and (strValue='= True') then
                MonTe[i].blLastEnemy := true;

            // specials without parameter
            if (strKey='Poison') and (strValue='= True') then
                MonTe[i].blPoison := true;
            if (strKey='Confusion') and (strValue='= True') then
                MonTe[i].blConfusion := true;
            if (strKey='CastsFire') and (strValue='= True') then
                MonTe[i].blFire := true;
            if (strKey='CastsIce') and (strValue='= True') then
                MonTe[i].blIce := true;
            if (strKey='CastsWater') and (strValue='= True') then
                MonTe[i].blWaterC := true;
            if (strKey='LoseArmour') and (strValue='= True') then
                MonTe[i].blDestArmour := true;
            if (strKey='LoseWeapon') and (strValue='= True') then
                MonTe[i].blDestWeapon := true;
            if (strKey='Blindness') and (strValue='= True') then
                MonTe[i].blBlindness := true;
            if (strKey='Paralyze') and (strValue='= True') then
                MonTe[i].blPara := true;
            if (strKey='Calm') and (strValue='= True') then
                MonTe[i].blCalm := true;
            if (strKey='Curses') and (strValue='= True') then
                MonTe[i].blCurse := true;
            if (strKey='MightyHit') and (strValue='= True') then
                MonTe[i].blHitsHard := true;
            if (strKey='Invisible') and (strValue='= True') then
                MonTe[i].blInvis := true;
            if (strKey='MoveOnWater') and (strValue='= True') then
                MonTe[i].blWater := true;
            if (strKey='SelfHeal') and (strValue='= True') then
                MonTe[i].blSelfHeal := true;

            // specials with parameter
            if strKey='Teleport:' then
            begin
                MonTe[i].blTeleport:=true;
                MonTe[i].intTeleport:=StrToInt(strValue);
            end;

            if strKey='SplitInto:' then
            begin
                MonTe[i].blSplit:=true;
                MonTe[i].strSplitInto:=strValue;
            end;

            if strKey='Summon:' then
            begin
                MonTe[i].blSummon:=true;
                MonTe[i].strSplitInto:=strValue;
            end;

            if strKey='DrainPP:' then
            begin
                MonTe[i].blDrainPP:=true;
                MonTe[i].intDrainPP:=StrToInt(strValue);
            end;

            if strKey='DrainSTR:' then
            begin
                MonTe[i].blDrainSTR:=true;
                MonTe[i].intDrainSTR:=StrToInt(strValue);
            end;



            if strKey='Verb:' then
                MonTe[i].strVerb := strValue;
            if strKey='ShootVerb:' then
                MonTe[i].strShootVerb := strValue;
            if strKey='HitRate:' then
                MonTe[i].intHitRate := StrToInt(strValue);
        

            if strKey='Move:' then
                MonTe[i].intMove := StrToInt(strValue);

            if strKey='KillPlot:' then
                MonTe[i].intKillPlot := StrToInt(strValue);

            if strKey='Letter:' then
            begin
                cstr := strValue[1];
                MonTe[i].chLetter := cstr;
            end;

            if strKey='BulletLetter:' then
            begin
                cstr := strValue[1];
                MonTe[i].chBulletLetter := cstr;
            end;

            if strKey='OrdLetter:' then
                MonTe[i].chLetter := chr(StrToInt(strValue));
            if strKey='BulletOrdLetter:' then
                MonTe[i].chBulletLetter := chr(StrToInt(strValue));

            if strKey='WP:' then
                MonTe[i].intWP := StrToInt(strValue);
            if strKey='GP:' then
                MonTe[i].intGP := StrToInt(strValue);

            if strKey='AP:' then
                MonTe[i].intAP := StrToInt(strValue);

            if strKey='HP:' then
            begin
                MonTe[i].intMaxHP := StrToInt(strValue);
                MonTe[i].intHP := MonTe[i].intMaxHP;
            end;

            if strKey='PP:' then
            begin
                MonTe[i].intMaxPP := StrToInt(strValue);
                MonTe[i].intPP := MonTe[i].intMaxPP;
            end;

            if strKey='NeedsOpenQuest:' then
                MonTe[i].intQuestG := StrToInt(strValue);
            if strKey='NeedsSolvedQuest:' then
                MonTe[i].intQuestS := StrToInt(strValue);

            if strKey='EXP:' then
                MonTe[i].longExp := StrToInt(strValue);
            if strKey='Gold:' then
                MonTe[i].longGold := StrToInt(strValue);

            if strKey='DungeonLvl:' then
                MonTe[i].intLvl := StrToInt(strValue);
        end;
    end;

    // close monsters.txt
    Close (MonsterFile);

    // set total number of available MonsterTemplates
    MonsterTemplates:=i;


    // save a table with all monsters
	//Assign (MonsterFile, 'data/monster-overview.txt');
	//Rewrite (MonsterFile);
    //writeln(MonsterFile, 'Name,HP,PP,WP,GP,AP,DLV');
    //for i:=1 to MonsterTemplates do
        //writeln(MonsterFile, MonTe[i].strName + ',' + IntToStr(MonTe[i].intMaxHP) + ',' + IntToStr(MonTe[i].intMaxPP) + ',' + IntToStr(MonTe[i].intWP) + ',' + IntToStr(MonTe[i].intGP) + ',' + IntToStr(MonTe[i].intAP) + ',' + IntToStr(MonTe[i].intLvl));
    //Close (MonsterFile);
end;


procedure SaveBones;
var
    MonsterFile: textfile;
begin
    Assign (MonsterFile, PathUserWritable+'bones.txt');

    if fileexists(PathUserWritable+'bones.txt') then
        Append (MonsterFile)
    else
        Rewrite (MonsterFile);

    Writeln(MonsterFile,'NewMonster:');
    Writeln(MonsterFile,ThePlayer.strName);
    Writeln(MonsterFile,' ');

    Writeln(MonsterFile,'DungeonLvl:');
    Writeln(MonsterFile,IntToStr(DungeonLevel));
    Writeln(MonsterFile,' ');

    Writeln(MonsterFile,'HP:');
    Writeln(MonsterFile,IntToStr(ThePlayer.intMaxHP));
    Writeln(MonsterFile,' ');

    Writeln(MonsterFile,'PP:');
    Writeln(MonsterFile,IntToStr(ThePlayer.intMaxPP));
    Writeln(MonsterFile,' ');

    Writeln(MonsterFile,'WP:');
    Writeln(MonsterFile,IntToStr(ThePlayer.intFight));
    Writeln(MonsterFile,' ');

    Writeln(MonsterFile,'AP:');
    Writeln(MonsterFile,IntToStr(ThePlayer.intMove));
    Writeln(MonsterFile,' ');

    Writeln(MonsterFile,'EXP:');
    Writeln(MonsterFile,IntToStr(ThePlayer.intLvl));
    Writeln(MonsterFile,' ');

    Writeln(MonsterFile,'Gold:');
    Writeln(MonsterFile,IntToStr(ThePlayer.longGold));
    Writeln(MonsterFile,' ');

    Close(MonsterFile);
end;


procedure BonesInit;
var
    MonsterFile: textfile;            // used to assign "PathUserWritable+'bones.txt'"
    i: integer;                // counter
    strKey, strValue: string;        // used to read key-value-pairs out of MonsterFile
begin

    if fileexists(PathUserWritable+'bones.txt') then
    begin
        // initialize key-value-pair
        strKey := '';
        strValue := '';
    
        // open bones.txt
        Assign (MonsterFile, PathUserWritable+'bones.txt');
        Reset (MonsterFile);
    
        // initialize counter
        i:=MonsterTemplates;
    
        // as long as monsters.txt not completely read...
        while eof(MonsterFile)=false do
        begin
            // search for key
            repeat
                ReadLn(MonsterFile, strKey);
            until ((strKey<>'') and (strKey[1]<>'#')) or (eof(MonsterFile));
    
            // key found, now search for corresponding value
            ReadLn(MonsterFile, strValue);
    
            // create new monster
            if strKey='NewMonster:' then
                inc(i);
    

            if strKey='NewMonster:' then
            begin
                MonTe[i].strName := strValue;
                MonTe[i].intType := i;
                MonTe[i].chLetter := chr(143);
                MonTe[i].strVerb := 'attacks';
                MonTe[i].blUnique := false;
                MonTe[i].blLastEnemy := false;
                MonTe[i].blWater := false;
            end;


            if (strKey='Poison') and (strValue='= True') then
                MonTe[i].blPoison := true;
            if (strKey='Poison') and (strValue='= False') then
                MonTe[i].blPoison := false;

            if (strKey='Confusion') and (strValue='= True') then
                MonTe[i].blConfusion := true;
            if (strKey='Confusion') and (strValue='= False') then
                MonTe[i].blConfusion := false;

            if (strKey='CastsFire') and (strValue='= True') then
                MonTe[i].blFire := true;
            if (strKey='CastsFire') and (strValue='= False') then
                MonTe[i].blFire := false;

            if (strKey='CastsIce') and (strValue='= True') then
                MonTe[i].blIce := true;
            if (strKey='CastsIce') and (strValue='= False') then
                MonTe[i].blIce := false;

            if (strKey='CastsWater') and (strValue='= True') then
                MonTe[i].blWaterC := true;
            if (strKey='CastsWater') and (strValue='= False') then
                MonTe[i].blWaterC := false;

            if (strKey='DestroyArmour') and (strValue='= True') then
                MonTe[i].blDestArmour := true;
            if (strKey='DestroyArmour') and (strValue='= False') then
                MonTe[i].blDestArmour := false;

            if (strKey='DestroyWeapon') and (strValue='= True') then
                MonTe[i].blDestWeapon := true;
            if (strKey='DestroyWeapon') and (strValue='= False') then
                MonTe[i].blDestWeapon := false;

            if (strKey='Blindness') and (strValue='= True') then
                MonTe[i].blBlindness := true;
            if (strKey='Blindness') and (strValue='= False') then
                MonTe[i].blBlindness := false;

            if (strKey='Paralyze') and (strValue='= True') then
                MonTe[i].blPara := true;
            if (strKey='Paralyze') and (strValue='= False') then
                MonTe[i].blPara := false;

            if (strKey='Calm') and (strValue='= True') then
                MonTe[i].blCalm := true;
            if (strKey='Calm') and (strValue='= False') then
                MonTe[i].blCalm := false;

            if (strKey='Curses') and (strValue='= True') then
                MonTe[i].blCurse := true;
            if (strKey='Curses') and (strValue='= False') then
                MonTe[i].blCurse := false;

            if strKey='WP:' then
                MonTe[i].intWP := StrToInt(strValue) * 3;
            if strKey='AP:' then
                MonTe[i].intAP := StrToInt(strValue) * 3;
            if strKey='HP:' then
            begin
                MonTe[i].intMaxHP := StrToInt(strValue) * 2;
                MonTe[i].intHP := MonTe[i].intMaxHP;
            end;

            if strKey='Gold:' then
                MonTe[i].longGold := StrToInt(strValue);
            if strKey='EXP:' then
                MonTe[i].longEXP := StrToInt(strValue);

            if strKey='DungeonLvl:' then
                MonTe[i].intLvl := StrToInt(strValue);
        end;
    
        // close bones.txt
        Close (MonsterFile);
    
        // set total number of available MonsterTemplates
        MonsterTemplates:=i;
    end;
end;


// reads available classes from file
procedure ClassInit;
var
    ClassFile: textfile;            // used to assign "data/classes.txt"
    i: integer;                // counter
    strKey, strValue: string;        // used to read key-value-pairs out of ClassFile
begin

    // initialize key-value-pair
    strKey := '';
    strValue := '';

    // open monsters.txt
    Assign (ClassFile, PathData+'data/classes.txt');
    Reset (ClassFile);

    // initialize counter
    i:=0;

    // as long as monsters.txt not completely read...
    while eof(ClassFile)=false do
    begin
        // search for key
        repeat
            ReadLn(ClassFile, strKey);
        until ((strKey<>'') and (strKey[1]<>'#')) or (eof(ClassFile));

        // key found, now search for corresponding value
        ReadLn(ClassFile, strValue);

        // create new class
        if strKey='NewClass:' then
            inc(i);

        // if we have at least 1 class, evaluate keys and values
        if i>0 then
        begin
            if strKey='NewClass:' then
                PlayClass[i].strName := strValue;
    
            if strKey='Fight:' then
                PlayClass[i].intFight := StrToInt(strValue);
            if strKey='View:' then
                PlayClass[i].intView := StrToInt(strValue);
            if strKey='Move:' then
                PlayClass[i].intMove := StrToInt(strValue);
            if strKey='Chant:' then
                PlayClass[i].intChant := StrToInt(strValue);
            if strKey='Burgle:' then
                PlayClass[i].intBurgle := StrToInt(strValue);

            if (strKey='AffectedSkill') and (strValue='= Fight') then
                PlayClass[i].afSkill := 'Fight';
            if (strKey='AffectedSkill') and (strValue='= View') then
                PlayClass[i].afSkill := 'View';
            if (strKey='AffectedSkill') and (strValue='= Move') then
                PlayClass[i].afSkill := 'Move';
            if (strKey='AffectedSkill') and (strValue='= Chant') then
                PlayClass[i].afSkill := 'Chant';
            if (strKey='AffectedSkill') and (strValue='= Burgle') then
                PlayClass[i].afSkill := 'Burgle';

            if (strKey='AffectedStatus') and (strValue='= HP') then
                PlayClass[i].afStat := 'HP';
            if (strKey='AffectedStatus') and (strValue='= PP') then
                PlayClass[i].afStat := 'PP';
            if (strKey='AffectedStatus') and (strValue='= EXP') then
                PlayClass[i].afStat := 'EXP';
            if (strKey='AffectedStatus') and (strValue='= Gold') then
                PlayClass[i].afStat := 'Gold';
            if (strKey='AffectedStatus') and (strValue='= Hunger') then
                PlayClass[i].afStat := 'Hunger';
        end;
    end;

    // close classes.txt
    Close (ClassFile);

    PlayProf[1]:='Constructor';
    PlayProf[2]:='Enchanter';
    PlayProf[3]:='Thief';
    PlayProf[4]:='Archer';
    PlayProf[5]:='Soldier';
end;


end.
