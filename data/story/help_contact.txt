HOW TO CONTACT THE DEVELOPER AND GET FREE STUFF

If you have suggestions, wishes, or bug reports---please contact me:

website:  http://lambdarogue.net
e-mail :  mario.donick@live.de

If you want to show me that you like the game and motivate me to continue
development, please send me a postcard or even a handwritten letter to:

  Mario Donick
  Paulstr. 16
  D-18055 Rostock
  Germany

In return, you will receive the game in its newest version on CD in a DVD
case, the printed manual of the newest version of the game, a printed map
of NeoTerr and a printed History of NeoTerr (about 50 pages).

This stuff is totally FOR FREE except a tiny postcard. Think about it ;-)
