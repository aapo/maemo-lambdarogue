"Wait here. I'll be right back." Mario looks at you sceptically, while lea-
ving to the Godfather. You ask yourself why you came back with the book to
the family. Your plan was to show them the urgency of your search, so they
would let you go. You then wanted to return back to the Temple without any
hassle by the family. Well, now you're here again.

Ten minutes later, Mario returns. "Okay, listen. The Godfather likes the
book. He says it is expensive and he knows a guy who pays extremely well
for it. We'll change the plan. You'll stay here and we sell the book to
our client. This brings a great deal of money without any problems."

Frustration grows quickly on your face, recognized by Mario. "Yeah, c'mon,
I understand it, really." He smiles at you, surprisingly friendly. "I know,
you wanted to kid them, getting all their treasure, perhaps even make them
pay old debts, I don't know. But the price for this book will be shared
among the whole family! I tell you, this is more than this Temple could
ever have. Don't look so sad." Mario fetches a bottle of Vitari out of
his jacket and throws it to you. "Here! Relax a little bit!" You look at
the bottle and nod automatically.

It seems that you have to visit this mysterious client of the Godfather,
to get back the Book of Stars.
