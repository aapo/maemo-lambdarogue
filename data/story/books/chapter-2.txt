                       CHAPTER 2: Meteor

The Book of Stars gave us the answer: To set mankind free, the
powers of space have to be summoned. A giant meteor, glowing like
the lava deep down in mankind's caves, has to be called, to come
down on Earth and to break the chains...
