A woodland in full color is awesome

as a forest fire, in magnitude at least,

but a single tree is like a dancing

tongue of flame to warm the heart.

- H.B.
