                       EPILOGUE: A New Day

Meteor has destroyed the barriers between the inner world of men
and the surface of Earth. It is the first time in centuries that
people see the sky again, and they are overwhelmed by its beauty.

Ahna's search has come to an end, but mankind's redemption has just
begun...
