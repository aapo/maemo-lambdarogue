Dear Nina,

some weeks have passed since my last letter. I was waiting
for your answer, but no letter came. The postmen probably
have difficulties to travel through the caves; I must admit
that my current place of living is far away, dangerous, and
difficult to reach, so I'm not angry with them.

So, how are you? How's life back home? Is your mother still
in the hospital? I hope she's healthier than during my last
visit. She's such a friendly woman, I would be very sad if
she'd leave us.

Damn...I wanted to write more, but I have to cease now;
monsters have been sighted and we have to leave before they
reach us. Hopefully I can visit you soon, in a month or two.

Take care.

In love,
  Daniel