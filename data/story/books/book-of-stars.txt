In the name of the Gods, in memory of Hermes who came from the stars
and brought us to them a thousand years ago. This is the book to save
the future, may it be read by wise people, again and again, to sur-
vive in darkest times.

So spoke Tennyson, poet of men, living in times so far away that we
have nothing but these words left:

  Far I saw into the future, far as human eyes could see.
  Saw the visions of the world and all the wonders that
  would be.

If he had known about the truth of the future, he would not have spoken
of wonders. He would have spoken of fear. But fear is the emotion
that will hold us down forever if we do not take care.

So here I speak, Yron Dafarl from the land once known as Greeza, to
overcome fear, to fight doubts, to save the knowledge for the future,
to save the knowledge of the stars.

May we rise.
