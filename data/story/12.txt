Slowly you step into Silvio Capresi's office. The godfather is sitting be-
hind a heavy mahogony table, reading some documents about the profit of
his recent business activities. He does not look up as he says: "Come in,
come nearer!" For nearly a minute you stand there, not knowing if Silvio
is content with the way you did your recent job.

"Adrian Smith's weapons are really beautiful, aren't they?" Silvio sighs.
"It is a pity that I have to sell them to one of my customers, but it is
damn necessary!" What sounded like friendly small talk at first suddenly
changed to a hysteric shout. Yeah, this was Silvio, the original Silvio
Capresi, the two-sided coin, the bright and the dark face...ah, he
had many names.

"Do you remember Benito?" You think a second, then nod. Benito Leone was
the son of Silvio's second ex-wife. "He's grown up. Is a real man now."
You try to smile, but Silvio looks angrily at you. "He's a man and he's
a danger to my business! The old bitch has shown him all the tricks, now
he's trying to displace me!"

You nod again, this time demonstratively worried. The Godfather continues.