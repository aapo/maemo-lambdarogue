The heat of the lava makes you sweat and you really want to turn away from
the blaze, but you force yourself to stay. Carefully you loosen the sword
'Northdoom' from your back, anxious to not unsheathe it, to avoid any bad
curse that might occur by accidently wielding it.

You kneel down. With its sheath you slide 'Northdoom' into the lava. A few
seconds later the sheath is destroyed and you can see the blade glowing
brighter and brighter as it grows molten by a heat that is stronger than
the powers of ice once enclosed, but now set free with a sizzling noise.

You step back and wipe your forehead. "Ah, it is done!" whispers a voice
behind you. You turn around. A well-dressed man, watching sadly at your
face, but smiling. It is Ares again, who already had told you about your
options after your discovery of 'Northdoom'. Now you see him in person.

"The gods are proud of you. You have decided for the best way. Now go and
follow your actual quest, as your priestess has told you." You nod. Ares
laughs. "What you've done is a sign of wisdom, my friend!" he says.

Then Ares fades away, into the darkness, leaving you behind.
