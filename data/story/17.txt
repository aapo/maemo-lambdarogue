You tell Prof. Zimmerman about your findings. He listens silently and nods
from time to time. When you're finished he looks worried. "I really had no
idea that she was that far" he says. His voice is low, but hoarsely. "I'll
perform a quick examination of the crystal you gave me." He goes into an
adjoining room. Impatiently, you wait for his return. You feel nervous.

About 30 minutes later Zimmerman returns. "The case is more serious than
we all had thought" he says. "And we do not have much time. Listen!" The
professor looks insistently at you and you slowly nod.

"This crystal is not like the other crystals. Although it contains the es-
sence of a magical chant and thus can be studied, it also contains special
powers which can be unleashed by sacrificing the crystal at an altar. The
altar at the Temple of Enoa, to be precise." The professor stops.

"What would happen if we offered the crystal to the gods?" you ask. Zim-
merman breathes out slowly. "The old legends say that a magical portal
would be created, to a place where the Traitor hid the sword 'Northdoom',
until he could hand it over to the Drekh'Nar Overlord. You know that on
that day the fate of mankind took a turn for the worst." He pauses. Then:
"Now, with this crystal, we have the chance to get 'Northdoom' back!"
