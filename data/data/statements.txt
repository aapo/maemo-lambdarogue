It's better to live in NeoTerr than to be slaves of the Drekh'Nar.
I think the five gods don't exist. There's only one god for us all.
Do you really believe in your god?
I can't imagine how life was on the surface of the planet.
Our life could be easier, but at least we are alive at all.
The deeper you go, the more dangerous the areas become.
Don't enter areas far away without appropriate protection!
I wish I could change this world. It's so terrible.
I'm feeling lonely.
Are you happy?
My children are scared of the monsters. Same for me.
The way of Dionysos is decadent.
Caveworms are ugly. They should become all dead.
Parents should leave the decision for a god totally to the children.
Yesterday I ate caveworm larva.
The river is dangerous when the lava is flooding it.
Which season do you like most?
If you pray a lot, you might receive a gift from your god.
Sacrificing money to your god should be based on your possibilities.
Don't fool your god. You could be cursed.
Runes are very rare, but rumours speak of special powers in them.
You can deconatiminate contaminated ground.
