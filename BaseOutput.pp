{
     Copyright (C) 2006-2008 by Mario Donick
     mario.donick@gmail.com

     This program is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation; either version 2 of the License, or
     (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program; if not, write to the
     Free Software Foundation, Inc.,
     59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
}

unit BaseOutput;

interface

uses
    Constants, SDL, SDL_Image, Crt, Video, VidUtil, SysUtils, RandomArea, Dungeon, LineOfSight, Player, Items,
    Quests, Chants, CollectData;

procedure LoadImage_Title(n: ansistring);
procedure LoadImage_Back(n: ansistring);

procedure BlitImage_Title;

procedure SmallCharXY (x: integer; y: integer; zeichen: Char; transparent: Boolean);

procedure TopFrame;
procedure BottomBar;
procedure DialogWin;
procedure StatusDeco;
procedure ShowStatus;
function RedNumbers (n: longint): string;
procedure QuickInv(blOnlyConsume: boolean);
function IsInvisible: Boolean;
procedure CharXY (x: Integer; y: Integer; zeichen: Char; mode: integer);
procedure AnyCharXY  (x: Integer; y: Integer; zeichen: Char; mode: integer);
procedure TextXY (x: Integer; y: Integer; message: String);
procedure SmallTextXY (x: Integer; y: Integer; message: String; transparent: boolean);
procedure TransTextXY (x: Integer; y: Integer; message: String);
procedure ClearScreenSDL;
procedure ShowMessage (message: String; pressanykey: Boolean);
procedure ShowTransMessage (strMessage: String; pressanykey: Boolean);
procedure ShowDungeon (sx: Integer; sy: Integer; w: Integer; h: Integer; mode: Integer);

implementation

// Load image into "title" surface
procedure LoadImage_Title(n: ansistring);
begin
    SDL_FREESURFACE(title);
    title := IMG_LOAD(PChar(n));
end;


// Load image into "background" surface
procedure LoadImage_Back(n: ansistring);
begin
    SDL_FREESURFACE(backgroundGraph);
    backgroundGraph := IMG_LOAD(PChar(n));
end;


procedure BlitImage_Title;
var
   BlitRect: SDL_RECT;
begin
     BlitRect.w:=800;
     BlitRect.h:=0;
     BlitRect.x:=0+HiResOffsetX;
     BlitRect.y:=0+HiResOffsetY;
     SDL_BLITSURFACE(title, nil, screen, @BlitRect);
end;


// output small characters (7x12?) only for SDL mode
procedure SmallCharXY (x: integer; y: integer; zeichen: Char; transparent: Boolean);
var
    CharRect: SDL_RECT;
    OrigRect: SDL_RECT;
begin
    if UseSDL=true then
    begin
        // position of char on screen
        CharRect.x := (x * 7);

        if (UseHiRes=true) and (Netbook=false) then
            CharRect.y := (y * 12)-4
        else if (UseHiRes=true) and (Netbook=true) then
            CharRect.y := (y * 12)-2
        else
            CharRect.y := (y * 12);

        CharRect.w := 7;
        CharRect.h := 12;

        OrigRect.x := 760 + ((ord(zeichen)-32) * 7);
        OrigRect.w := 7;
        OrigRect.h := 12;

        // position of char in bitmap
        if transparent=false then
            OrigRect.y := 40
        else
            OrigRect.y := 80;

        SDL_BLITSURFACE(extratiles, @OrigRect, screen, @CharRect);
    end;
end;


// topbar
procedure TopFrame;
var
    i: Integer;
begin
    // top bar
    for i:=0 to 2 do
        TextXY(0, i, chr(211)+'                                                                            '+chr(212));

    TextXY(0, 0,  chr(205)+'                                                                            '+chr(206));
    TextXY(0, 2, chr(207)+'                                                                            '+chr(208));

    for i:=1 to 76 do
    begin
        TextXY(i, 0, chr(209));
        TextXY(i, 2, chr(210));
    end;
end;


// bottom bar (mostly for input boxes)
procedure BottomBar;
var
    i: integer;
begin
    if UseSDL=true then
    begin
        for i:=PlaceOfBottomBar to 3+PlaceOfBottomBar do
            TextXY(0, i, chr(211)+'                                                                            '+chr(212));

        TextXY(0, PlaceOfBottomBar,  chr(205)+'                                                                            '+chr(206));

        for i:=1 to 76 do
            TextXY(i, PlaceOfBottomBar, chr(209));
    end
    else
        for i:=PlaceOfBottomBar-4 to PlaceOfBottomBar-1 do
            TextXY(0, i, '                                                                                ');
end;

// dialog frame decoration
procedure DialogWin;
var
    i: integer;
begin
    if UseSDL=true then
    begin
        for i:=4 to 21 do
            TextXY(4, i, chr(211)+'                                                                    '+chr(212));

        TextXY(4, 4,  chr(205)+'                                                                    '+chr(206));
        TextXY(4, 21, chr(207)+'                                                                    '+chr(208));

        for i:=5 to 72 do
        begin
            TextXY(i, 4, chr(209));
            TextXY(i, 21, chr(210));
        end;
    end
    else
    begin
        ClearScreenSDL;
        for i:=4 to 21 do
            TextXY(4, i,  '|                                                                    |');
        TextXY(4, 4,  '+--------------------------------------------------------------------+');
        TextXY(4, 21, '+--------------------------------------------------------------------+');
    end;
end;


// statusbar decoration
procedure StatusDeco;
begin
    if UseSDL=true then
    begin
        TopFrame;
        BottomBar;
    end;
end;

// returns true, if the player is invisible
function IsInvisible: boolean;
begin
    IsInvisible:=false;
    if CheckEffect(5)=true then
        IsInvisible:=true;
    if ThePlayer.intInvis>0 then
        IsInvisible:=true;
end;


// shows the minimap of the dungeon
procedure MiniMap;
var
    i, j, k: Integer;
begin

    // as the first level is static and always known, the minimap is useless (and ugly)
    //if (DungeonLevel=1) or (DungeonLevel=22) then
        //exit;

    // player marker
    PlayerPos.x:=300;
    PlayerPos.y:=232;
    PlayerPos.w:=5;
    PlayerPos.h:=5;

    // NPC marker
    NPCPos.x:=310;
    NPCPos.y:=232;
    NPCPos.w:=5;
    NPCPos.h:=5;

    // stairs marker
    StairPos.x:=320;
    StairPos.y:=232;
    StairPos.w:=5;
    StairPos.h:=5;

    // trader marker
    TraderPos.x:=330;
    TraderPos.y:=232;
    TraderPos.w:=5;
    TraderPos.h:=5;

    // background of map
    PaperPos.x:=370;
    PaperPos.y:=158;
    PaperPos.w:=118;
    PaperPos.h:=89;

    PlotRect.x:= 693+(HiResOffsetX*2);
    PlotRect.y:= 9;
    PlotRect.h:= 89;
    PlotRect.w := 118;

    SDL_BLITSURFACE(extratiles, @PaperPos, screen, @PlotRect);

    PlotRect.x:=715+(HiResOffsetX*2);   // 670
    PlotRect.y:=11;
    PlotRect.w:=1;
    PlotRect.h:=1;

    if (DungeonLevel<>1) and (DungeonLevel<>22) and (DungeonLevel<>25) then
        for i:=1 to DngMaxWidth-1 do
        begin
            inc(PlotRect.x);
            PlotRect.y:=25;    // 12
            for j:=19 to DngMaxHeight-1 do
            begin
                inc(PlotRect.y);
                if DngLvl[i,j].blKnown=true then
                begin
                    if DngLvl[i,j].intIntegrity>0 then
                        SDL_BLITSURFACE(extratiles, @PixelWhiteRect, screen, @PlotRect);
                    if (DngLvl[i,j].intFloorType=2) or (DngLvl[i,j].intFloorType=60) or (DngLvl[i,j].intFloorType=61) then
                        SDL_BLITSURFACE(extratiles, @PixelGreyRect, screen, @PlotRect);
                    if (DngLvl[i,j].intFloorType=10) or (DngLvl[i,j].intFloorType=13) or (DngLvl[i,j].intFloorType=17) or (DngLvl[i,j].intFloorType=18) then
                        SDL_BLITSURFACE(extratiles, @PixelGreenRect, screen, @PlotRect);
                    if (DngLvl[i,j].intFloorType=12) or (DngLvl[i,j].intFloorType=21) or (DngLvl[i,j].intFloorType=22) or (DngLvl[i,j].intFloorType=23) then
                        SDL_BLITSURFACE(extratiles, @PixelBrownRect, screen, @PlotRect);
                    if DngLvl[i,j].intFloorType=5 then
                        SDL_BLITSURFACE(extratiles, @PixelBlueRect, screen, @PlotRect);
                    if DngLvl[i,j].intFloorType=20 then
                        SDL_BLITSURFACE(extratiles, @PixelRedRect, screen, @PlotRect);
                    if DngLvl[i,j].intFloorType=31 then
                        SDL_BLITSURFACE(extratiles, @PixelOrangeRect, screen, @PlotRect);
                end;
            end;
        end;

    // NPCs, stairs etc.
    PlotRect.x:=715+(HiResOffsetX*2);
    PlotRect.y:=25;
    PlotRect.w:=1;
    PlotRect.h:=1;
    for i:=1 to DngMaxWidth-1 do
    begin
        inc(PlotRect.x);
        PlotRect.y:=24;
        for j:=19 to DngMaxHeight-1 do
        begin
            inc(PlotRect.y);

            // Discovered NPCs
            for k:=1 to 9 do
                if (i=NPC[k].intX) and (j=NPC[k].intY) then
                begin
                    PlayerRect.x:=PlotRect.x;
                    PlayerRect.y:=PlotRect.y;
                    dec(PlayerRect.x,2);
                    dec(PlayerRect.y,2);
                    SDL_BLITSURFACE(extratiles, @NPCPos, screen, @PlayerRect);
                end;

            // Trader
            if DngLvl[i,j].intBuilding>0 then
            begin
                PlayerRect.x:=PlotRect.x;
                PlayerRect.y:=PlotRect.y;
                dec(PlayerRect.x,2);
                dec(PlayerRect.y,2);
                SDL_BLITSURFACE(extratiles, @TraderPos, screen, @PlayerRect);
            end;

            // Staircases up
            if (DngLvl[i,j].intFloorType=8) or (DngLvl[i,j].intFloorType=62) or (DngLvl[i,j].intFloorType=63) then
            begin
                    PlayerRect.x:=PlotRect.x;
                    PlayerRect.y:=PlotRect.y;
                    dec(PlayerRect.x,2);
                    dec(PlayerRect.y,2);
                    SDL_BLITSURFACE(extratiles, @StairPos, screen, @PlayerRect);
            end;

            // Staircases down
            if (DngLvl[i,j].intFloorType=9) and ((ThePlayer.longLevelVisits[DungeonLevel+1]>0) or (DngLvl[i,j].blKnown=true)) then
            begin
                    PlayerRect.x:=PlotRect.x;
                    PlayerRect.y:=PlotRect.y;
                    dec(PlayerRect.x,2);
                    dec(PlayerRect.y,2);
                    SDL_BLITSURFACE(extratiles, @StairPos, screen, @PlayerRect);
            end;

        end;
    end;

    // Player
    PlotRect.x:=715+(HiResOffsetX*2);
    PlotRect.y:=25;
    PlotRect.w:=1;
    PlotRect.h:=1;
    for i:=1 to DngMaxWidth-1 do
    begin
        inc(PlotRect.x);
        PlotRect.y:=24;
        for j:=19 to DngMaxHeight-1 do
        begin
            inc(PlotRect.y);

            // Player
            if (i=ThePlayer.intX) and (j=ThePlayer.intY) then
            begin
                PlayerRect.x:=PlotRect.x;
                PlayerRect.y:=PlotRect.y;
                dec(PlayerRect.x,2);
                dec(PlayerRect.y,2);
                SDL_BLITSURFACE(extratiles, @PlayerPos, screen, @PlayerRect);
            end;

        end;
    end;
end;


// This function transforms an integer or longint to a string using the special
// red chars from tileset 1. If called in console mode, it just returns a
// standard string
function RedNumbers (n: longint): string;
var
    strTemp, strTemp2: string;
    c: char;
    i: integer;
begin
    strTemp:=IntToStr(n);
    strTemp2:='';
    if UseSDL=true then
    begin
        for i:=0 to length(strTemp) do
        begin
            c := strTemp[i];
            case c of
            '0': strTemp2:=strTemp2+chr(219);
            '1': strTemp2:=strTemp2+chr(220);
            '2': strTemp2:=strTemp2+chr(221);
            '3': strTemp2:=strTemp2+chr(222);
            '4': strTemp2:=strTemp2+chr(223);
            '5': strTemp2:=strTemp2+chr(224);
            '6': strTemp2:=strTemp2+chr(225);
            '7': strTemp2:=strTemp2+chr(226);
            '8': strTemp2:=strTemp2+chr(227);
            '9': strTemp2:=strTemp2+chr(228);
            end;
        end;
    end
    else
        strTemp2:=strTemp;

    //writeln(IntToStr(n)+ '/' + strTemp + '/' + strTemp2);
    RedNumbers:=strTemp2;
end;


procedure QuickInv(blOnlyConsume: boolean);
var
    i: integer;
begin
    if UseSDL=true then
    begin
        if blOnlyConsume=true then
        begin
            for i:=1 to 16 do
                if inventory[i].intType>0 then
                    if inventory[i].longNumber>0 then
                        if (thing[inventory[i].intType].blEat=true) or (thing[inventory[i].intType].blDrink=true) then
                            TransTextXY(1, 4+i, chr(218+i)+' '+thing[inventory[i].intType].chLetter+' '+thing[inventory[i].intType].strName);
        end
        else
            for i:=1 to 16 do
                TransTextXY(1, 4+i, chr(218+i)+' '+thing[inventory[i].intType].chLetter+' '+thing[inventory[i].intType].strName);
    end
    else
    begin

        for i:=22 to 25 do
            TextXY(0, i, '                                                                                ');

        for i:=1 to 16 do
            TextXY(0, 4+i, '                        |');
        TextXY(0, 4,   '------------------------+');

        for i:=1 to 16 do
            TextXY(1, 4+i, IntToStr(i));

        if blOnlyConsume=true then
        begin
            for i:=1 to 16 do
                if inventory[i].intType>0 then
                    if inventory[i].longNumber>0 then
                        if (thing[inventory[i].intType].blEat=true) or (thing[inventory[i].intType].blDrink=true) then
                            TextXY(4, 4+i, thing[inventory[i].intType].chLetter+' '+thing[inventory[i].intType].strName);
        end
        else
            for i:=1 to 16 do
                TextXY(4, 4+i, thing[inventory[i].intType].chLetter+' '+thing[inventory[i].intType].strName);
    end;

end;


// shows quick access info in SDL mode, for chants
procedure ShowChantBar;
var
    i, j, x, y, intIconID, invID, id: integer;
    IconRect, IconRectS, ItemRect: SDL_RECT;
    blIsSpell: boolean;
begin


    // Icons
    IconRect.y:=415;
    IconRect.w:=38;
    IconRect.h:=38;

    ItemRect.x:=0;
    IconRectS.y:=ChantBarY+2;
    IconRectS.w:=38;
    IconRectS.h:=38;


    // NEU; zeigt QuickKey defines an
    for i:=1 to 12 do
    begin
        blIsSpell:=false;
        id:=-1;

        if strQuickKey[i]<>'-' then
        begin
            // okay, wir haben einen definierten Quickkey. Wir muessen nun rauskriegen, welcher Effekttyp dahinter steckt, denn
            // der Effekttyp ist die Icon-Nummer in extratiles

            // ist es spell oder item?
            for j:=1 to ChantCount do
                if Spell[j].strName = strQuickKey[i] then
                begin
                     id:=j;
                     blIsSpell:=true;
                end;

            if blIsSpell=false then
               for j:=1 to ItemCount do
                   if Thing[j].strRealName = strQuickKey[i] then
                      id:=j;

            if id>-1 then
            begin
            // entsprechend des Werts von blIsSpell wird nun intIconID belegt
                if blIsSpell=true then
                    intIconID:=Spell[id].intEffect
                else
                    intIconID:=Thing[id].intEffect;

                // jetzt wissen wir, welches Icon gezeichnet werden soll, das koennen wir nun also tun
                IconRect.x:=38*(intIconID-1);  // Icon ist hier die Icon-Nummer (von links nach rechts) in extratiles; das -1 muessen wir mal pruefen

                // wir muessen nun die Variante des Icons (farbig oder grau/dunkel waehlen, je nachdem, ob item/spell vorhanden/refreshed ist oder nicht
                // dazu brauchen wir die Nummer im Inventory bzw. im Spellbook (invID)
                invID:=-1;
                if blIsSpell=true then
                begin
                     for j:=1 to 12 do
                         if SpellBook[j].intType = id then
                            invID:=j;
                end
                else
                begin
                     for j:=1 to 16 do
                         if Inventory[j].intType = id then
                            invID:=j;
                end;


                // Variante des Icons waehlen
                if blIsSpell=true then
                begin
                    if Spellbook[invID].intRefresh<Spell[id].intRefresh then
                        IconRect.y:=454     // grau und dunkel
                    else
                        IconRect.y:=415;    // farbig und bereit zum Einsatz
                end
                else
                begin
                    if invID=-1 then
                        IconRect.y:=454
                    else
                        IconRect.y:=415;
                end;

                // nun koennen wir das Icon endlich darstellen
                IconRectS.x:=ChantBarX+(42*i);
                SDL_BLITSURFACE(extratiles, @IconRect, screen, @IconRectS);

                // wenn es sich nicht um einen Spell handelt, muss nun ein Potion- oder Food-Icon dargestellt werden
                if blIsSpell=false then
                begin
                     if Thing[id].blDrink=true then
                     begin
                          ItemRect.y:=0;
                          ItemRect.w:=6;
                          ItemRect.h:=10;
                          inc(IconRectS.x,2);
                          SDL_BLITSURFACE(extratiles, @ItemRect, screen, @IconRectS);
                     end
                     else
                         if Thing[id].blEat=true then
                         begin
                              ItemRect.y:=60;
                              ItemRect.w:=9;
                              ItemRect.h:=10;
                              inc(IconRectS.x,2);
                              SDL_BLITSURFACE(extratiles, @ItemRect, screen, @IconRectS);
                         end;
                end;
            end;
        end;
    end;


    // okay. nun muessen wir noch die Rahmen um die Symbole zeichnen
    IconRect.y:=373;
    IconRect.w:=40;
    IconRect.h:=40;

    IconRectS.y:=ChantBarY;
    IconRectS.w:=40;
    IconRectS.h:=40;

    for i:=1 to 12 do
    begin
        IconRect.x:=40*(i-1);
        IconRectS.x:=ChantBarX+(42*i);
        SDL_BLITSURFACE(extratiles, @IconRect, screen, @IconRectS);
    end;

    if longTotalTurns < 4 then
        SmallTextXY(1, 12, 'Press ['+KeyHelp+'] for help. Press [ESC] or '+KeyQuit+' for game menu.', false);

    if longTotalTurns < 7 then
        SmallTextXY(1, 12, 'Press ['+KeyHelp+'] for help. Press [ESC] or '+KeyQuit+' for game menu.', true);

end;


// shows status bars in SDL mode
procedure ShowBars;
var
    i, percent: integer;
    DecoPos, DecoPosS, PaperPos, PaperPosS, BarRect, BarRectS: SDL_RECT;
begin

    // player portrait
    PaperPos.w:=89;
    PaperPos.h:=89;
    PaperPos.y:=158;

    // deco next to player portrait
    DecoPos.x:=764;
    DecoPos.y:=0;
    DecoPos.w:=161;
    DecoPos.h:=29;

    DecoPosS.x:=48;
    DecoPosS.y:=8;
    DecoPosS.w:=161;
    DecoPosS.h:=29;

    if ThePlayer.intSex = 1 then  // male
    begin
        case ThePlayer.intProf of
             2: PaperPos.x:=678;
             3: PaperPos.x:=1100;
             4: PaperPos.x:=779;
             5: PaperPos.x:=580;
        end;
    end
    else
    begin                        // female
        case ThePlayer.intProf of
             2: PaperPos.x:=1325;
             3: PaperPos.x:=883;
             4: PaperPos.x:=992;
             5: PaperPos.x:=1210;
        end;
    end;

    BarRect.h := 5;
    BarRect.w := 1;
    BarRectS.h := 5;
    BarRectS.w := 1;

    PaperPosS.x := 9;
    PaperPosS.y := 8;
    PaperPosS.w := 89;
    PaperPosS.h := 89;


    // health bar
    BarRectS.y := 10;
    BarRect.y := 250;
    BarRect.x := 301;
    for i:=0 to 100 do
    begin
        BarRectS.x := 80+20 + i;
        SDL_BLITSURFACE(extratiles, @BarRect, screen, @BarRectS);
    end;

    BarRect.x := 300;
    percent := (ThePlayer.intHP * 100) div ThePlayer.intMaxHP;
    for i:=0 to percent do
    begin
        BarRectS.x := 80+20 + i;
        SDL_BLITSURFACE(extratiles, @BarRect, screen, @BarRectS);
    end;

    // divine rage bar above healthbar
    if ThePlayer.intLimit>0 then
    begin
        BarRect.y := 249;
        BarRect.h := 1;
        BarRectS.w := 1;
        BarRectS.h := 1;
        for i:=0 to ThePlayer.intLimit do
        begin
            BarRectS.x := 80+20 + i;
            SDL_BLITSURFACE(extratiles, @BarRect, screen, @BarRectS);
        end;

        BarRectS.h := 5;
        BarRectS.w := 1;
        BarRect.h := 5;
    end;

    // psychic bar
    BarRectS.y := 20;
    BarRect.y := 257;
    BarRect.x := 301;
    for i:=0 to 100 do
    begin
        BarRectS.x := 80+20 + i;
        SDL_BLITSURFACE(extratiles, @BarRect, screen, @BarRectS);
    end;

    if ThePlayer.intMaxPP > 0 then
    begin
        BarRect.x := 300;
        percent := (ThePlayer.intPP * 100) div ThePlayer.intMaxPP;
        for i:=0 to percent do
        begin
            BarRectS.x := 80+20 + i;
            SDL_BLITSURFACE(extratiles, @BarRect, screen, @BarRectS);
        end;
    end;

    // str bar
    BarRectS.y := 30;
    BarRect.y := 264;
    BarRect.x := 301;
    for i:=0 to 100 do
    begin
        BarRectS.x := 80+20 + i;
        SDL_BLITSURFACE(extratiles, @BarRect, screen, @BarRectS);
    end;

    BarRect.x := 300;
    percent := ThePlayer.intStrength;
    if percent > 100 then percent := 100;
    for i:=0 to percent do
    begin
        BarRectS.x := 80+20 + i;
        SDL_BLITSURFACE(extratiles, @BarRect, screen, @BarRectS);
    end;

    if ThePlayer.intStrength > 100 then
    begin
        percent := ThePlayer.intStrength - 100;
        BarRect.x := 302;
        for i:=0 to percent do
        begin
            BarRectS.x := 80+20 + i;
            SDL_BLITSURFACE(extratiles, @BarRect, screen, @BarRectS);
        end;
    end;

    SDL_BLITSURFACE(extratiles, @PaperPos, screen, @PaperPosS);
    SDL_BLITSURFACE(extratiles, @DecoPos, screen, @DecoPosS);

    if UseHiRes=false then
        TransTextXY(0, 4,IntToStr(ThePlayer.intLvl))
    else
    begin
        if Netbook=false then
           TransTextXY(1+HiResTextOffsetX, 0, IntToStr(ThePlayer.intLvl))
        else
           TransTextXY(1+HiResTextOffsetX, 4, IntToStr(ThePlayer.intLvl));
    end;

    ShowChantBar;

end;


procedure ShortMessageLog;
var
    i, s, z, l : integer;
begin

    // find last message (which is first message to display)
    s:= ThePlayer.intNumberOfLogs-1;

    // find message two steps before last message (which is last message to display)
    if s>2 then
        z:=s-2
    else
        z:=1;

    if UseSDL=true then
    begin
        l:=PlaceOfBottomBar;
        for i:=z to s do
        begin
               TransTextXY(1+HiResTextOffsetX, l+HiResTextOffsetY, ThePlayer.strMessageLog[i]);
            inc(l);
        end;
    end
    else
    begin
        l:=22;
        for i:=z to s do
        begin
                TransTextXY(1, l, ThePlayer.strMessageLog[i]);
            inc(l);
        end;
    end;

end;


// This procedure shows the status of the player
procedure ShowStatus;
var
    i: integer;
    msg1, direction: string;
    ComRect, ComRectS, StatRect, StatRectS: SDL_RECT;
begin
    if UseSDL=false then
    begin
        BottomBar;
        TextXY(0, 1, '                                                                                ');
        TextXY(0, 2, '--------------------------------------------------------------------------------');
        TextXY(0,21, '--------------------------------------------------------------------------------');
        TextXY(0,22, '                                                                                ');
        TextXY(0,23, '                                                                                ');
        TextXY(0,24, '                                                                                ');
        TextXY(0,25, '                                                                                ');
        TextXY(1,1, 'HP ' + IntToStr(ThePlayer.intHP) + '/' + IntToStr(ThePlayer.intMaxHP) + '  ');
        TextXY(13,1, 'PP ' + IntToStr(ThePlayer.intPP) + '/' + IntToStr(ThePlayer.intMaxPP) + '  ');
        TextXY(26,1, 'STR ' + IntToStr(ThePlayer.intStrength)+'%');
        TextXY(39,1, 'DRG ' + IntToStr(ThePlayer.intLimit)+'%');

        if (ThePlayer.longFood > 60) and (ThePlayer.longFood < 200) then
            TextXY(1, 3, 'Hungry');
        if (ThePlayer.longFood > 20) and (ThePlayer.longFood < 61) then
            TextXY(1, 3, 'Fainting');
        if (ThePlayer.longFood > 0) and (ThePlayer.longFood < 21) then
            TextXY(1, 3, 'Starving');


        // Effects
        i:=3;
        if ThePlayer.intPoison > 0 then
        begin
            GlobalConColor:=red;
            TextXY(1, i, 'Poisoned');
            inc(i);
        end;

        if ThePlayer.intConfusion > 0 then
        begin
            GlobalConColor:=red;
            TextXY(1, i, 'Confused');
            inc(i);
        end;

        if ThePlayer.intPara > 0 then
        begin
            GlobalConColor:=red;
            TextXY(1, i, 'Paralized');
            inc(i);
        end;

        if (ThePlayer.intCalm > 0) or (DngLvl[ThePlayer.intX,ThePlayer.intY].intFloorType = 11) then
        begin
            GlobalConColor:=red;
            TextXY(1, i, 'Calm');
            inc(i);
        end;

        if ThePlayer.blCursed = true then
        begin
            GlobalConColor:=red;
            TextXY(1, i, 'Cursed');
            inc(i);
        end;

        if (ThePlayer.intTotalVitari>0) and (ThePlayer.intNeedVitari >= ThePlayer.intNextVitari) then
        begin
            GlobalConColor:=red;
            TextXY(1, i, 'Craving');
            inc(i);
        end;

        if ThePlayer.blBlessed = true then
        begin
            GlobalConColor:=green;
            TextXY(1, i, 'Blessed');
            inc(i);
        end;

        if ThePlayer.intBlind > 0 then
        begin
            GlobalConColor:=red;
            TextXY(1, i, 'Blind');
            inc(i);
        end;

        if ThePlayer.intWall > 0 then
        begin
            GlobalConColor:=green;
            TextXY(1, i, 'Barrier');
            inc(i);
        end;

        if CheckEffect(37)=true then   // maximize magic
        begin
            GlobalConColor:=green;
            TextXY(1, i, 'Max. Magic');
            inc(i);
        end;

        if CheckEffect(49)=true then   // divine rage bonus
        begin
            GlobalConColor:=green;
            TextXY(1, i, 'Max. Divine Rage');
            inc(i);
        end;

        if CheckEffect(7)=true then   // extra gold
        begin
            GlobalConColor:=green;
            TextXY(1, i, 'Max. Credits');
            inc(i);
        end;


        // now the resistances

        i:=3;

        if CheckEffect(10)=true then   // resist poison
        begin
            GlobalConColor:=lightcyan;
            TextXY(68, i, 'Res. Poison');
            inc(i);
        end;

        if CheckEffect(13)=true then   // resist fire
        begin
            GlobalConColor:=lightcyan;
            TextXY(68, i, '  Res. Fire');
            inc(i);
        end;

        if CheckEffect(14)=true then   // resist ice
        begin
            GlobalConColor:=lightcyan;
            TextXY(68, i, '   Res. Ice');
            inc(i);
        end;

        if CheckEffect(17)=true then   // resist blindness
        begin
            GlobalConColor:=lightcyan;
            TextXY(68, i, ' Res. Blind');
            inc(i);
        end;

        if CheckEffect(26)=true then   // resist paralization
        begin
            GlobalConColor:=lightcyan;
            TextXY(68, i, 'Res. Paral.');
            inc(i);
        end;

        if CheckEffect(28)=true then   // resist calm
        begin
            GlobalConColor:=lightcyan;
            TextXY(68, i, '  Res. Calm');
            inc(i);
        end;

        if CheckEffect(31)=true then   // resist curses
        begin
            GlobalConColor:=lightcyan;
            TextXY(68, i, ' Res. Curse');
            inc(i);
        end;

        if CheckEffect(35)=true then   // resist water
        begin
            GlobalConColor:=lightcyan;
            TextXY(68, i, ' Res. Water');
            inc(i);
        end;

        if CheckEffect(51)=true then   // resist DrainPP
        begin
            GlobalConColor:=lightcyan;
            TextXY(66, i, 'Res. PP Drain');
            inc(i);
        end;

        if CheckEffect(52)=true then   // resist DrainSTR
        begin
            GlobalConColor:=lightcyan;
            TextXY(65, i, 'Res. STR Drain');
            inc(i);
        end;

        GlobalConColor:=-1;
    end;

    // status icons in SDL mode
    if UseSDL=true then
    begin
        StatRect.y := 250;
        StatRect.w := 20;
        StatRect.h := 20;

        StatRectS.x := 105;
        StatRectS.y := 42;
        StatRectS.w := 20;
        StatRectS.h := 20;

        if (ThePlayer.longFood > 60) and (ThePlayer.longFood < 200) then
        begin
            StatRect.x := 412;
            SDL_BLITSURFACE(extratiles, @StatRect, screen, @StatRectS);
            inc(StatRectS.x, 22);
        end;

        if (ThePlayer.longFood > 20) and (ThePlayer.longFood < 61) then
        begin
            StatRect.x := 433;
            SDL_BLITSURFACE(extratiles, @StatRect, screen, @StatRectS);
            inc(StatRectS.x, 22);
        end;

        if (ThePlayer.longFood > 0) and (ThePlayer.longFood < 21) then
        begin
            StatRect.x := 454;
            SDL_BLITSURFACE(extratiles, @StatRect, screen, @StatRectS);
            inc(StatRectS.x, 22);
        end;

        if ThePlayer.intPoison > 0 then
        begin
            StatRect.x := 370;
            SDL_BLITSURFACE(extratiles, @StatRect, screen, @StatRectS);
            inc(StatRectS.x, 22);
        end;

        if ThePlayer.intConfusion > 0 then
        begin
            StatRect.x := 580;
            SDL_BLITSURFACE(extratiles, @StatRect, screen, @StatRectS);
            inc(StatRectS.x, 22);
        end;

        if ThePlayer.intPara > 0 then
        begin
            StatRect.x := 601;
            SDL_BLITSURFACE(extratiles, @StatRect, screen, @StatRectS);
            inc(StatRectS.x, 22);
        end;

        if (ThePlayer.intCalm > 0) or (DngLvl[ThePlayer.intX,ThePlayer.intY].intFloorType = 11) then
        begin
            StatRect.x := 475;
            SDL_BLITSURFACE(extratiles, @StatRect, screen, @StatRectS);
            inc(StatRectS.x, 22);
        end;

        if ThePlayer.blCursed = true then
        begin
            StatRect.x := 538;
            SDL_BLITSURFACE(extratiles, @StatRect, screen, @StatRectS);
            inc(StatRectS.x, 22);
        end;

        if (ThePlayer.intTotalVitari>0) and (ThePlayer.intNeedVitari >= ThePlayer.intNextVitari) then
        begin
            StatRect.x := 517;
            SDL_BLITSURFACE(extratiles, @StatRect, screen, @StatRectS);
            inc(StatRectS.x, 22);
        end;

        if ThePlayer.blBlessed = true then
        begin
            StatRect.x := 559;
            SDL_BLITSURFACE(extratiles, @StatRect, screen, @StatRectS);
            inc(StatRectS.x, 22);
        end;

        if ThePlayer.intBlind > 0 then
        begin
            StatRect.x := 496;
            SDL_BLITSURFACE(extratiles, @StatRect, screen, @StatRectS);
            inc(StatRectS.x, 22);
        end;

        if ThePlayer.intWall > 0 then
        begin
            StatRect.x := 391;
            SDL_BLITSURFACE(extratiles, @StatRect, screen, @StatRectS);
            inc(StatRectS.x, 22);
        end;

        // und nun auch hier die ganzen resistances

        if CheckEffect(7)=true then   // extra gold
        begin
            StatRect.x := 811;
            SDL_BLITSURFACE(extratiles, @StatRect, screen, @StatRectS);
            inc(StatRectS.x, 22);
        end;

        if CheckEffect(10)=true then   // resist poison
        begin
            StatRect.x := 622;
            SDL_BLITSURFACE(extratiles, @StatRect, screen, @StatRectS);
            inc(StatRectS.x, 22);
        end;

        if CheckEffect(11)=true then   // resist confusion
        begin
            StatRect.x := 896;
            SDL_BLITSURFACE(extratiles, @StatRect, screen, @StatRectS);
            inc(StatRectS.x, 22);
        end;

        if CheckEffect(13)=true then   // resist fire
        begin
            StatRect.x := 643;
            SDL_BLITSURFACE(extratiles, @StatRect, screen, @StatRectS);
            inc(StatRectS.x, 22);
        end;

        if CheckEffect(14)=true then   // resist ice
        begin
            StatRect.x := 664;
            SDL_BLITSURFACE(extratiles, @StatRect, screen, @StatRectS);
            inc(StatRectS.x, 22);
        end;

        if CheckEffect(17)=true then   // resist blindness
        begin
            StatRect.x := 685;
            SDL_BLITSURFACE(extratiles, @StatRect, screen, @StatRectS);
            inc(StatRectS.x, 22);
        end;

        if CheckEffect(26)=true then   // resist paralization
        begin
            StatRect.x := 832;
            SDL_BLITSURFACE(extratiles, @StatRect, screen, @StatRectS);
            inc(StatRectS.x, 22);
        end;

        if CheckEffect(28)=true then   // resist calm
        begin
            StatRect.x := 769;
            SDL_BLITSURFACE(extratiles, @StatRect, screen, @StatRectS);
            inc(StatRectS.x, 22);
        end;

        if CheckEffect(31)=true then   // resist curses
        begin
            StatRect.x := 727;
            SDL_BLITSURFACE(extratiles, @StatRect, screen, @StatRectS);
            inc(StatRectS.x, 22);
        end;

        if CheckEffect(35)=true then   // resist water
        begin
            StatRect.x := 706;
            SDL_BLITSURFACE(extratiles, @StatRect, screen, @StatRectS);
            inc(StatRectS.x, 22);
        end;

        if CheckEffect(37)=true then   // maximize magic
        begin
            StatRect.x := 790;
            SDL_BLITSURFACE(extratiles, @StatRect, screen, @StatRectS);
            inc(StatRectS.x, 22);
        end;

        if CheckEffect(49)=true then   // divine rage bonus
        begin
            StatRect.x := 748;
            SDL_BLITSURFACE(extratiles, @StatRect, screen, @StatRectS);
            inc(StatRectS.x, 22);
        end;

        if CheckEffect(51)=true then   // resist PP drain
        begin
            StatRect.x := 854;
            SDL_BLITSURFACE(extratiles, @StatRect, screen, @StatRectS);
            inc(StatRectS.x, 22);
        end;

        if CheckEffect(52)=true then   // divine STR drain
        begin
            StatRect.x := 875;
            SDL_BLITSURFACE(extratiles, @StatRect, screen, @StatRectS);
            inc(StatRectS.x, 22);
        end;

    end;



    case DngLvl[ThePlayer.intX, ThePlayer.intY].intFloorType of
        1:
            msg1:='on a wall';
        2:
            msg1:='on plain floor';
        3:
            msg1:='in a closed door';
        4:
            msg1:='standing in an open door';
        5:
            msg1:='swimming in water';
        6:
            msg1:='on top of a mountain';
        7:
            msg1:='in front of a closed treasure chest. Loot with [ENTER] or ['+KeyEnter+']';
        8:
            msg1:='in front of a staircase. Go up with [ENTER] or ['+KeyEnter+']';
        9:
            msg1:='in front of a staircase. Go down with [ENTER] or ['+KeyEnter+']';
        10:
            msg1:='in front of a small tree';
        11:
            msg1:='stumping through worm excrements';
        12:
            msg1:='on sand';
        13:
            msg1:='in front of a big tree';
        14:
            msg1:='in front of an open treasure chest';
        15:
            msg1:='in front of an altar. Sacrifice money with [ENTER] or ['+KeyEnter+']';
        16:
            msg1:='on plain floor';
        17:
            msg1:='going through high grass';
        18:
            msg1:='on top of a hill';
        19:
            msg1:='on ash';
        20:
            msg1:='on lava. Hurts you and other creatures';
        21:
            msg1:='in front of a small dry tree';
        22:
            msg1:='in front of a big dry tree';
        23:
            msg1:='on dry grass';
        26:
            msg1:='looking into a crypt. Examine with [ENTER] or ['+KeyEnter+']';
        33:
            msg1:='in front of an already opened crypt';
        28:
            msg1:='in front of an enchanted well. Drink with [ENTER] or ['+KeyEnter+']';
        29:
            msg1:='in front of an empty well';
        30:
            msg1:='on plain floor';
        31:
        begin
            msg1:='on contaminated ground';
            if (ThePlayer.intProf=1) or (ThePlayer.intProf=4) then
                msg1:=msg1+'. Clear it with ['+KeyTunnel+']';
        end;
        34:
            msg1:='in front of a monster'+chr(39)+'s hive';
        35:
            msg1:='in front of one of your traps';
        36:
            msg1:='on sand';
        37:
            msg1:='on sand';
        38:
            msg1:='on shore';
        39:
            msg1:='on shore';
        40:
            msg1:='walking through shallow water';
        41:
            msg1:='on sand';
        42:
            msg1:='on sand';
        50:
            msg1:='sitting on a stool';
        51:
            msg1:='in front of a table';
        53:
            msg1:='in front of a big mushroom. Harvest with ['+KeyTake+']';
        54:
            msg1:='in front of a small mushroom. Harvest with ['+KeyTake+']';
        55:
            msg1:='on plain floor. You see some wooden garbage. Take it with ['+KeyTake+']';
        57:
            msg1:='slipping on machine oil';
        58:
            msg1:='on plain floor. You see some metal parts. Take it with ['+KeyTake+']';
        59:
            msg1:='a magical portal. Travel with [ENTER] or ['+KeyEnter+']';
        60:
            msg1:='on snow';
        61:
            msg1:='on ice';
        62:
            msg1:='in front of an old staircase. Go up with [ENTER] or ['+KeyEnter+']';
        63:
            msg1:='in front of an old staircase. Go down with [ENTER] or ['+KeyEnter+']';
    end;
    msg1:='You'+chr(39)+'re ' + msg1 + '.';

    if DngLvl[ThePlayer.intX,ThePlayer.intY].intAirType=5 then
        msg1:='You'+chr(39)+'re wandering through darkness.';

    if DngLvl[ThePlayer.intX,ThePlayer.intY].intAirType=6 then
        msg1:='You'+chr(39)+'re trapped in a spider'+chr(39)+'s web.';

    if DngLvl[ThePlayer.intX,ThePlayer.intY].intBuilding>0 then
        msg1:='There'+chr(39)+'s a trader here. Trade with ['+KeyTrade+']. Drop an item here to sell it.';

    for i:=1 to 9 do
        if (NPC[i].intX=ThePlayer.intX) and (NPC[i].intY=ThePlayer.intY) then
            msg1:='You'+chr(39)+'re in front of '+Quest[DungeonLevel,i].strNPCname+'. Talk with ['+KeyTrade+'].';

    if DngLvl[ThePlayer.intX,ThePlayer.intY].intItem>0 then
    begin
        if DngLvl[ThePlayer.intX,ThePlayer.intY].intAirType=5 then
            msg1:='There' + chr(39) + 's something on the ground.'
        else
            msg1:='You see an item: '+Thing[DngLvl[ThePlayer.intX,ThePlayer.intY].intItem].strName + '.';

        if (UseSDL=true) and (UseMouseToMove=true) then
            msg1:=msg1+' Take it with right-click or ['+KeyTake+'].'
        else
            msg1:=msg1+' Take it with ['+KeyTake+'].';
    end;

    LastMessage:='-';
    ShortMessageLog;

    GlobalConColor:=darkgray;
    TransTextXY(1+HiResTextOffsetX, 3+PlaceOfBottomBar+HiResTextOffsetY, msg1);
    GlobalConColor:=-1;

    // icons for mouse mode
    if (UseSDL=true) and (UseMouseToMove=true) then
    begin
        ComRect.w:=48;
        ComRect.h:=48;
        ComRect.y:=160;
        ComRectS.w:=48;
        ComRectS.h:=48;
        ComRectS.y:=445;

        // status
        if ThePlayer.intSex=1 then
            ComRect.x:=0
        else
            ComRect.x:=50;

        ComRectS.x:=13;
        SDL_BLITSURFACE(extratiles, @ComRect, screen, @ComRectS);

        // inventory
        ComRect.x:=150;
        ComRectS.x:=63;
        SDL_BLITSURFACE(extratiles, @ComRect, screen, @ComRectS);

        // spellbook
        ComRect.x:=100;
        ComRectS.x:=113;
        SDL_BLITSURFACE(extratiles, @ComRect, screen, @ComRectS);

        // talk icon
        if CheckForNPC(ThePlayer.intX,ThePlayer.intY)=true then
        begin
            ComRect.x:=250;
            ComRectS.x:=163;
            SDL_BLITSURFACE(extratiles, @ComRect, screen, @ComRectS);
        end;

        // fire with longrange weapon icon
        if ThePlayer.intWeapon>0 then
            if Thing[ThePlayer.intWeapon].chLetter=chr(158) then
            begin
                ComRect.x:=200;
                ComRectS.x:=213;
                SDL_BLITSURFACE(extratiles, @ComRect, screen, @ComRectS);
            end;

        // last song
        if LastSong>0 then
        begin
            ComRect.x:=300;
            ComRectS.x:=263;
            SDL_BLITSURFACE(extratiles, @ComRect, screen, @ComRectS);
        end;

    end;

    Minimap;

    // compass
    ComRect.x:=0;
    ComRect.y:=0;
    ComRect.w:=72;
    ComRect.h:=72;

    ComRectS.x:=714;
    ComRectS.y:=415;
    ComRectS.w:=72;
    ComRectS.h:=72;

    if (DownX=ThePlayer.intX) and (DownY<ThePlayer.intY) then
    begin
        direction:='N';
        ComRect.x := 0;
    end;

    if (DownX=ThePlayer.intX) and (DownY>ThePlayer.intY) then
    begin
        direction:='S';
        ComRect.x := 73;
    end;

    if (DownX>ThePlayer.intX) and (DownY=ThePlayer.intY) then
    begin
        direction:='E';
        ComRect.x := 146;
    end;

    if (DownX<ThePlayer.intX) and (DownY=ThePlayer.intY) then
    begin
        direction:='W';
        ComRect.x := 219;
    end;

    if (DownX>ThePlayer.intX) and (DownY<ThePlayer.intY) then
    begin
        direction:='NE';
        ComRect.x := 292;
    end;

    if (DownX<ThePlayer.intX) and (DownY<ThePlayer.intY) then
    begin
        direction:='NW';
        ComRect.x := 365;
    end;

    if (DownX>ThePlayer.intX) and (DownY>ThePlayer.intY) then
    begin
        direction:='SE';
        ComRect.x := 438;
    end;

    if (DownX<ThePlayer.intX) and (DownY>ThePlayer.intY) then
    begin
        direction:='SW';
        ComRect.x := 511;
    end;


    if (DownX=ThePlayer.intX) and (DownY=ThePlayer.intY) then
    begin
        direction:='X';
        ComRect.x := 584;
    end;

    if DungeonLevel>8 then
    begin
        direction:='';
        ComRect.x := 657;
    end;

    if PlayerHasItem('Ring of Guidance')>-1 then
        if UseSDL=true then
            SDL_BLITSURFACE(extratiles, @ComRect, screen, @ComRectS)
        else
            TextXY(75, 3, direction);

    if UseSDL=true then
        ShowBars;

    if UseSDL=true then
        SDL_UPDATERECT(screen,0,0,0,0)
    else
        UpdateScreen(true);
end;


// make screen black
procedure ClearScreenSDL;
var
    i, j: Integer;
begin

    if UseSDL=true then
        SDL_BLITSURFACE(backgroundGraph, nil, screen, nil)
    else
        for i:=0 to 80 do
            for j:=0 to 25 do
                TextXY(i,j,' ');
end;


// output BIG single character (SDL only; 20x40)
procedure BigCharXY (x: Integer; y: Integer; zeichen: Char; mode: integer);
var
    CharRect: SDL_RECT;
    OrigRect: SDL_RECT;
    TransRect: SDL_RECT;
begin

    if UseSmallTiles=true then
    begin
        // position of char on screen
        CharRect.x := x * 20;           // 1.x: 20
        CharRect.y := y * 40;           // 1.x: 40
        CharRect.w := 20;               // 1.x: 20
        CharRect.h := 40;               // 1.x: 40

        // position of char in bitmap
        OrigRect.x := (ord(zeichen)-32) * 20;
        OrigRect.y := 0;
        OrigRect.w := 20;
        OrigRect.h := 40;

        // position of semi-transparent char in bitmap
        TransRect.x := 3440;
        TransRect.y := 0;
        TransRect.w := 20;
        TransRect.h := 40;
    end
    else
    begin
        // position of char on screen
        CharRect.x := x * 40;           // 1.x: 20
        CharRect.y := y * 80;           // 1.x: 40
        CharRect.w := 40;               // 1.x: 20
        CharRect.h := 80;               // 1.x: 40

        // position of char in bitmap
        OrigRect.x := (ord(zeichen)-32) * 40;
        OrigRect.y := 0;
        OrigRect.w := 40;
        OrigRect.h := 80;

        // position of semi-transparent char in bitmap
        TransRect.x := 6880;
        TransRect.y := 0;
        TransRect.w := 40;
        TransRect.h := 80;
    end;


    SDL_BLITSURFACE(TilesetASCII, @OrigRect, screen, @CharRect);

    // faded
    if mode=1 then
        SDL_BLITSURFACE(TilesetASCII, @TransRect, screen, @CharRect);
end;




// output single character (10x20)
procedure CharXY (x: Integer; y: Integer; zeichen: Char; mode: integer);
var
    CharRect: SDL_RECT;
    OrigRect: SDL_RECT;
    TransRect: SDL_RECT;
    Color: integer;
begin

    // prevent landscape from drawing if would be hidden behind status panels
    if UseSDL=false then
        if (ord(zeichen)>128) and ((y<3) or (y>25)) then
            if (ord(zeichen)<205) or (ord(zeichen)>212) then
                if (ord(zeichen)<>156) and (ord(zeichen)<>157) and (ord(zeichen)<>162) and (ord(zeichen)<>159) and (ord(zeichen)<>164) and (ord(zeichen)<>163) and (ord(zeichen)<>158) and (ord(zeichen)<>172) and (ord(zeichen)<>166) and (ord(zeichen)<>165) then
                    exit;

    // SDL output
    if UseSDL=true then
    begin

        // position of char on screen
        CharRect.x := (x * 10)+HiResOffsetX;
        CharRect.y := (y * 20)+HiResOffsetY;

        CharRect.w := 10;
        CharRect.h := 20;

        // position of char in bitmap
        OrigRect.x := (ord(zeichen)-32) * 10;
        OrigRect.y := 0;
        OrigRect.w := 10;
        OrigRect.h := 20;

        // position of semi-transparent char in bitmap
        TransRect.x := 1720;
        TransRect.y := 0;
        TransRect.w := 10;
        TransRect.h := 20;

        // if char is space, then only display it, if mode is not 2
        if ((ord(zeichen)=32) and (mode<>2)) or (ord(zeichen)>32) then
            SDL_BLITSURFACE(TilesetGraph, @OrigRect, screen, @CharRect);

        // faded
        if mode=1 then
            SDL_BLITSURFACE(TilesetGraph, @TransRect, screen, @CharRect);
    end
    else
    begin

        if x<0 then
           inc(x,HiResTextOffsetX);
        if y>29 then
           dec(y,HiResTextOffsetY);

        // console output: SDL has 29 or 33 lines, console only 25, so adjust y-values
        if (y=29) then
            y:=25;
        if (y=28) then
            y:=24;
        if (y=27) then
            y:=23;
        if (y=26) then
            y:=22;
        if y=0 then
            y:=1;

        // basic color
        Color:=white;

        // retransform SDL specific chars to normal ASCII chars
        if (ord(zeichen)>127) and (y<22) then
        begin
            case ord(zeichen) of
                187, 146, 179:
                    zeichen:='@';        // player char

                128:
                begin
                    zeichen:='.';                // blood
                    Color:=red;
                end;

                129:
                begin
                    zeichen:='*';                // spitting
                    Color:=brown;
                end;

                130:
                begin
                    zeichen:='*';                // fire 1
                    Color:=yellow;
                end;

                131:
                begin
                    zeichen:='*';                // fire 2
                    Color:=red;
                end;


                132:
                begin
                    zeichen:='*';                // ice 1
                    Color:=cyan;
                end;

                133:
                begin
                    zeichen:='*';                // ice 2
                    Color:=lightcyan;
                end;

                134:
                begin
                    zeichen:='+';                // small tree
                    Color:=green;
                end;

                135:
                begin
                    zeichen:='=';                // caveworm excrements
                    Color:=darkgray;
                end;

                136:
                begin
                    zeichen:='.';                // sand, mud
                    Color:=brown;
                end;


                137:
                begin
                    zeichen:='T';                // big tree
                    Color:=green;
                end;

                139:
                begin
                    zeichen:='2';
                    Color:=brown;
                end;

                140:
                begin
                    zeichen:='3';
                    Color:=lightmagenta;
                end;

                141:
                begin
                    zeichen:='4';
                    Color:=magenta;
                end;

                143:
                begin
                    zeichen:='@';
                    Color:=lightgray;
                end;

                144:
                begin
                    zeichen:='1';
                    color:=green;
                end;
                145:
                begin
                    zeichen:='*';
                    color:=yellow;
                end;
                147:
                begin
                    zeichen:='*';
                    color:=lightcyan;
                end;

                148:
                begin
                    zeichen:=',';                // grass
                    color:=lightgreen;
                end;

                149:
                begin
                    zeichen:='^';                // hill
                    color:=green;
                end;

                150:
                begin
                    zeichen:='.';                // floor
                    color:=lightgray;
                end;
                151:
                begin
                    zeichen:='+';                // door
                    color:=brown;
                end;
                152:
                begin
                    zeichen:='\';                // open door
                    color:=brown;
                end;

                153:
                begin
                    zeichen:=',';                // closed chest
                    color:=yellow;
                end;

                154:
                begin
                    zeichen:=';';                // open chest
                    color:=yellow;
                end;

                155:
                begin
                    zeichen:='!';                // potion
                    color:=lightblue;
                end;

                156:
                    zeichen:='/';                // weapon
                157:
                    zeichen:='(';
                158:
                    zeichen:='{';
                159:
                    zeichen:='\';
                160:
                    zeichen:='"';                // ammu
                161:
                begin
                    zeichen:='?';                // scroll
                    color:=lightred;
                end;
                162:
                    zeichen:='/';
                163:
                    zeichen:=')';
                164:
                    zeichen:='~';                // lance
                165:
                    zeichen:='[';
                166:
                begin
                    zeichen:='=';                // ring
                    color:=yellow;
                end;

                167:
                begin
                    zeichen:='$';                // money
                    color:=yellow;
                end;

                168:
                begin
                    zeichen:='%';                // food
                    color:=brown;
                end;

                169:
                begin
                    zeichen:='*';                // rune
                    color:=green;
                end;

                170:
                    zeichen:='|';                // altar
                171:
                    zeichen:=':';                // rock
                172:
                begin
                    zeichen:='}';
                    color:=yellow;
                end;
                173:
                begin
                    zeichen:='.';
                    color:=lightblue;
                end;
                174:
                begin
                    zeichen:=chr(39);
                    color:=darkgray;
                end;

                175:
                begin
                    zeichen:='<';                // staircase up
                    color:=lightblue;

                end;
                176:
                begin
                    zeichen:='>';                // staircase down
                    color:=lightblue;
                end;

                177:
                begin
                    zeichen:='=';                // water
                    color:=blue;
                end;

                178:
                begin
                    zeichen:='^';                // mountain
                    color:=lightgray;
                end;

                180:
                begin
                    zeichen:='#';                // wall
                    color:=white;
                end;

                181:
                begin
                    zeichen:='+';                // small tree brown
                    color:=brown;
                end;

                182:
                begin
                    zeichen:='T';                // big tree brown
                    color:=brown;
                end;

                183:
                begin
                    zeichen:='=';                // lava
                    color:=red;
                end;

                184:
                begin
                    zeichen:=',';                // grass dark
                    color:=green;
                end;

                185:
                begin
                    zeichen:='+';                // sec. door
                    color:=cyan;
                end;

                186:
                begin
                    zeichen:='#';                // sec. wall
                    color:=lightcyan;
                end;

                188:
                begin
                    zeichen:='U';                // crypt
                    color:=lightgray;
                end;

                189:
                begin
                    zeichen:='U';                // well
                    color:=lightblue;
                end;

                190:
                begin
                    zeichen:='#';                // alternate std. wall
                    color:=cyan;
                end;

                191:
                    zeichen:='o';                // dungeon key

                192:
                begin
                    zeichen:='#';                // alternate std. wall
                    color:=green;
                end;

                193:
                begin
                    zeichen:='?';             // page
                    color:=brown;
                end;

                194:
                begin
                    zeichen:='*';                // blue star floor
                    color:=blue;
                end;

                195:
                begin
                    zeichen:='*';
                    color:=blue;
                end;
                196:
                begin
                    zeichen:='U';        // empty well
                    color:=darkgray;
                end;
                197:
                begin
                    zeichen:='0';
                    color:=red;
                end;
                198:
                begin
                    zeichen:='|';
                    color:=red;
                end;
                199:
                begin
                    zeichen:='#';
                    color:=lightblue;
                end;
                200:
                begin
                    zeichen:='5';
                    color:=yellow;
                end;
                201:
                begin
                    zeichen:='6';
                    color:=lightgreen;
                end;
                202:
                begin
                    zeichen:='7';
                    color:=darkgray;
                end;
                203:
                begin
                    zeichen:=chr(39);
                    color:=lightred;
                end;
                205:
                begin
                    zeichen:='_';
                    color:=lightred;
                end;
                206:
                begin
                    zeichen:='^';
                    color:=brown;
                end;
                207:
                begin
                    zeichen:='*';
                    color:=red;
                end;
                208:
                begin
                    zeichen:='*';
                    color:=white;
                end;
                209:
                begin
                    zeichen:=chr(39);                // grass border
                    color:=brown;
                end;
                210:
                begin
                    zeichen:=chr(39);                // grass border
                    color:=brown;
                end;
                211:
                begin
                    zeichen:='=';                // water
                    color:=lightblue;
                end;
                212:
                begin
                    zeichen:='=';                // water
                    color:=lightblue;
                end;
                213:
                begin
                    zeichen:='=';                // water
                    color:=lightblue;
                end;
                214:
                begin
                    zeichen:='+';
                    color:=lightgreen;
                end;
                215:
                begin
                    zeichen:='-';
                    color:=lightred;
                end;
                216:
                begin
                    zeichen:='=';
                    color:=lightgray;
                end;
                217:
                begin
                    zeichen:='?';
                    color:=cyan;
                end;
                218:
                begin
                    zeichen:='L';
                    color:=lightgray;
                end;
                220:
                begin                       // player in water
                    zeichen:='@';
                    color:=blue;
                end;
                221:
                begin                       // spider web
                    zeichen:='%';
                    color:=white;
                end;
                222:
                begin
                    zeichen:='0';           // barrel
                    color:=brown;
                end;
                223:
                begin
                    zeichen:='-';           // table
                    color:=brown;
                end;
                224:
                begin
                    zeichen:=',';           // stool
                    color:=brown;
                end;
                225:
                begin
                    zeichen:='#';           // bookshelf
                    color:=brown;
                end;
                226:
                begin
                    zeichen:=':';           // big mushroom
                    color:=cyan;
                end;
                227:
                begin
                    zeichen:='.';           // small mushroom
                    color:=cyan;
                end;
                228:
                begin
                    zeichen:='-';           // wood
                    color:=brown;
                end;
                229:
                begin
                    zeichen:='0';           // gas cylinder
                    color:=lightgray;
                end;
                230:
                begin
                    zeichen:='.';           // machine oil
                    color:=yellow;
                end;
                231:
                begin
                    zeichen:='/';           // old machine parts
                    color:=yellow;
                end;
                232:
                begin
                    zeichen:='>';           // portal through time
                    color:=lightgreen;
                end;
                233:
                begin
                    zeichen:=',';           // snow
                    color:=white;
                end;
                234:
                begin
                    zeichen:=chr(39);           // ice
                    color:=cyan;
                end;
                235:
                begin
                    zeichen:='^';           // Noldalur mountain
                    color:=white;
                end;
                236:
                begin
                    zeichen:='.';           // Noldalur floor
                    color:=white;
                end;
                237:
                begin
                    zeichen:='#';           // Noldalur wall
                    color:=darkgray;
                end;
                238:
                begin
                    zeichen:='+';                // small tree white
                    color:=white;
                end;
                239:
                begin
                    zeichen:='T';                // big tree white
                    color:=white;
                end;
                240:
                begin
                    zeichen:=',';                // big tree white
                    color:=white;
                end;
                241:
                begin
                    zeichen:='U';                // ice crypt
                    color:=white;
                end;
                242:
                begin
                    zeichen:='<';                // old stairs up
                    color:=brown;
                end;
                243:
                begin
                    zeichen:='>';                // old stairs down
                    color:=brown;
                end;
                244:
                begin
                    zeichen:='#';                // red wall
                    color:=red;
                end;
                245:
                begin
                    zeichen:='.';                // red floor
                    color:=red;
                end;
                246:
                begin
                    zeichen:='O';                // shield
                    color:=cyan;
                end;
                247:
                begin
                    zeichen:='=';                // shoes
                    color:=brown;
                end;
            end;
        end;

        // faded
        if mode=1 then
            color:=darkgray;

        // if a global color is defined, use it instead the color defined above
        if GlobalConColor>-1 then
            color:=GlobalConColor;

        // print character (special chars only in dungeon area)
        if ((ord(zeichen)>127) and (y<22)) or (ord(zeichen)<128) then
            TextOut(x,y, zeichen, color);
    end;
end;


// chooses between BIG char and normal Char depending on SDL or console mode
procedure AnyCharXY  (x: Integer; y: Integer; zeichen: Char; mode: integer);
begin
    if UseSDL=true then
        BigCharXY(x, y, zeichen, mode)
    else
        CharXY(x, y, zeichen, mode);
end;



// output a string (calls CharXY several times, until string is shown)
procedure TextXY (x: Integer; y: Integer; message: String);
var
    i: integer;
    cstr: char;
begin
    for i:=1 to length(message) do
    begin
        cstr := message[i];
        CharXY (x+i, y, cstr, 0);
    end;
end;


// like TextXY, but with transparent space
procedure TransTextXY (x: Integer; y: Integer; message: String);
var
    i: integer;
    cstr: char;
begin
    for i:=1 to length(message) do
    begin
        cstr := message[i];
        CharXY (x+i, y, cstr, 2);
    end;
end;


// like TextXY, but uses the smaller 7x12 chars and is always transparent
procedure SmallTextXY (x: Integer; y: Integer; message: String; transparent: boolean);
var
    i: integer;
    cstr: char;
begin
    for i:=1 to length(message) do
    begin
        cstr := message[i];
        SmallCharXY (x+i, y, cstr, transparent);
    end;
end;


// show a message (calls TextXY)
procedure ShowMessage (message: String; pressanykey: Boolean);
var
   intMY: integer;
begin
    intMY:=1+PlaceOfBottomBar;


    TextXY(1,intMY,'                                                                            ');

    TextXY(1,intMY, message);
    if pressanykey=true then
    begin
        TextXY(1,intMY+1,'                                                                            ');
        if (UseSDL=true) and (UseMouseToMove=true) then
            TextXY(34,intMY+1,' [Right-click, SPACE or ENTER to continue]')
        else
            TextXY(48,intMY+1,' [SPACE or ENTER to continue]');
    end;
    LastMessage:=message;

    if UseSDL=true then
        SDL_UPDATERECT(screen,0,0,0,0)
    else
        UpdateScreen(true);
end;

procedure StoreMessageInLog (strMessage: String);
var
    i: integer;
    blStored: boolean;
    LengthOfLog: integer;
begin
    //If array is full, double size of it
    LengthOfLog := Length(ThePlayer.strMessageLog);
    if ThePlayer.intNumberOfLogs = LengthOfLog then
    begin
        SetLength(ThePlayer.strMessageLog, 2*LengthOfLog);
        //Writeln('New log array size '+IntToStr(2*LengthOfLog));
    end;


    ThePlayer.strMessageLog[ThePlayer.intNumberOfLogs]:=strMessage;    
    ThePlayer.intNumberOfLogs := ThePlayer.intNumberOfLogs+1;
end;


// show a transparent message (calls TransTextXY)
procedure ShowTransMessage (strMessage: String; pressanykey: Boolean);
var
   intMY: integer;
begin
    intMY:=1+PlaceOfBottomBar;

    //TextXY(1,27,'                                                                            ');

    // save message in message history
    if length(strMessage)>1 then
        StoreMessageInLog(strMessage);

    //TransTextXY(1,27, strMessage);
    if pressanykey=true then
    begin
        //TextXY(1,28,'                                                                            ');
        if (UseSDL=true) and (UseMouseToMove=true) then
            TransTextXY(34,intMY+1,' [Right-click, SPACE or ENTER to continue]')
        else
            TransTextXY(48,intMY+1,' [SPACE or ENTER to continue]');

       if UseSDL=true then
            SDL_UPDATERECT(screen,0,0,0,0)
        else
            UpdateScreen(true);
      end;
    LastMessage:=strMessage;

end;


// This function returns the appropriate character of a given floor type
function ReturnDngChar (n: integer): char;
begin
    ReturnDngChar:=' ';
    case n of
        0:
            ReturnDngChar := ' ';
        1:
        begin // wall
            ReturnDngChar := chr(180);
            if (UseSDL=true) and (DungeonLevel=2) then
                ReturnDngChar := chr(214);
            if DungeonLevel=21 then
                ReturnDngChar := chr(237);
            if (DungeonLevel=23) or (DungeonLevel=25) then
                ReturnDngChar := chr(244);
        end;
        2:
        begin // floor
            ReturnDngChar := chr(150);
            if (UseSDL=true) and ((DungeonLevel=6) or (DungeonLevel=7))then
                ReturnDngChar := chr(138);
            if (UseSDL=true) and (DungeonLevel>=12) then
                ReturnDngChar := chr(142);
            if DungeonLevel=21 then
                ReturnDngChar := chr(236);
        end;
        3:
            ReturnDngChar := chr(151);
        4:
            ReturnDngChar := chr(152);
        5:
            ReturnDngChar := chr(177);
        6:
        begin // mountain, rock
            if (DungeonLevel<>10) and (DungeonLevel<>11) then
                ReturnDngChar := chr(178)
            else
                ReturnDngChar := chr(206);
            if (DungeonLevel=21) then
                ReturnDngChar := chr(235);
        end;
        7:
            ReturnDngChar := chr(153);
        8:
            ReturnDngChar := chr(175);
        9:
            ReturnDngChar := chr(176);
        10:
            ReturnDngChar := chr(134);
        11:
            ReturnDngChar := chr(135);
        12:
            if DungeonLevel=21 then
                ReturnDngChar := chr(240)
            else
                ReturnDngChar := chr(136);
        13:
            ReturnDngChar := chr(137);
        14:
            ReturnDngChar := chr(154);
        15:
            ReturnDngChar := chr(170);
        16:
            if DungeonLevel<23 then
                ReturnDngChar := chr(173)
            else
                ReturnDngChar := chr(245);
        17:
            ReturnDngChar := chr(148);
        18:
            ReturnDngChar := chr(149);
        19:
            ReturnDngChar := chr(174);
        20:
            ReturnDngChar := chr(183);
        21:
            if DungeonLevel=21 then
                ReturnDngChar := chr(238)
            else
                ReturnDngChar := chr(181);
        22:
            if DungeonLevel=21 then
                ReturnDngChar := chr(239)
            else
                ReturnDngChar := chr(182);
        23:
            ReturnDngChar := chr(184);
        24:
            ReturnDngChar := chr(185);
        25:
        begin  // special walls
            if DungeonLevel<17 then
                ReturnDngChar := chr(186)
            else
                ReturnDngChar := chr(199);
            if DungeonLevel>22 then
                ReturnDngChar := chr(244);
        end;
        26:
            if DungeonLevel=21 then
                ReturnDngChar := chr(241)
            else
                ReturnDngChar := chr(188);
        27:
            ReturnDngChar := chr(190);
        28:
            ReturnDngChar := chr(189);
        29:
            ReturnDngChar := chr(196);
        30:
            ReturnDngChar := chr(194);
        31:
            ReturnDngChar := chr(203);
        32:
            ReturnDngChar := chr(192);
        33:
            if DungeonLevel=21 then
                ReturnDngChar := chr(241)
            else
                ReturnDngChar := chr(188);
        34:
            ReturnDngChar := chr(198);
        35:
            ReturnDngChar := chr(205);
        36:
            ReturnDngChar := chr(209);
        37:
            ReturnDngChar := chr(210);
        38:
            ReturnDngChar := chr(211);
        39:
            ReturnDngChar := chr(212);
        40:
            ReturnDngChar := chr(213);
        48:
        begin // hidden door
            if (DungeonLevel=12) or (DungeonLevel=13) then
                ReturnDngChar := chr(178)
            else
                ReturnDngChar := chr(180);
            if (UseSDL=true) and (DungeonLevel=2) then
                ReturnDngChar := chr(214);
        end;
        49:
            ReturnDngChar := chr(225); // bookshelf
        50:
            ReturnDngChar := chr(224); // stool
        51:
            ReturnDngChar := chr(223); // table
        52:
            ReturnDngChar := chr(222); // barrel
        53:
            ReturnDngChar := chr(226); // big mushroom
        54:
            ReturnDngChar := chr(227); // small mushroom
        55:
            ReturnDngChar := chr(228); // wood rests
        56:
            ReturnDngChar := chr(229); // gas cylinder
        57:
            ReturnDngChar := chr(230); // machine oil
        58:
            ReturnDngChar := chr(231); // old machine
        59:
            ReturnDngChar := chr(232); // portal through time
        60:
            ReturnDngChar := chr(233); // snow
        61:
            ReturnDngChar := chr(234); // ice
        62:
            ReturnDngChar := chr(242); // old stairs up
        63:
            ReturnDngChar := chr(243); // old stairs down
    end;
end;


procedure ShowDungeon (sx: Integer; sy: Integer; w: Integer; h: Integer; mode: integer);
var
    i, j, k, l, m, intMonPer, intBorder: Integer;
    chDngChar, chItemChar, chMonHealthChar, chMonsterChar: Char;
    dummy: String;
begin

    if UseSDL=true then
        SDL_BLITSURFACE(backgroundGraph, nil, screen, nil)
    else
    begin
        LockScreenUpdate;
        ClearScreenSDL;
    end;

    if UseSDL=true then
    begin
        intBorder:=0;
        if UseSmallTiles=true then
        begin
            if UseHiRes=false then
            begin
                w:=40;
                h:=12;
            end
            else
            begin
                w:=51;
                h:=19;
            end;
        end
        else
        begin
            if UseHiRes=false then
            begin
                w:=20;
                h:=6;
            end
            else
            begin
                w:=25;
                h:=9;
            end;
        end;
    end
    else
        intBorder:=1;

    if ThePlayer.intBlind = 0 then
    begin
        // initialize LOS
        SetVisible;

        // upper left
        k := 0; l := 0;

        for i := (w div 2) downto intBorder do
        begin
            for j := (h div 2) downto intBorder do
            begin

//                 Writeln('D: '+IntToStr(sx-k)+'/'+IntToStr(sy-l));
                if (sx-k < 1) or (sy-l < 1) then
                    break;

                chDngChar:=ReturnDngChar(DngLvl[sx-k,sy-l].intFloorType);
                chItemChar:='^';
                chMonHealthChar := '^';
                chMonsterChar := '^';
                GlobalConColor := -1;

                if DngLvl[sx-k,sy-l].blLOS=true then
                begin
                    if (sx-k>0) and (sx-k<DngMaxWidth+1) and (sy-l>1) and (sy-l<DngMaxHeight) then
                    begin

                        // select auras to draw
                        case DngLvl[sx-k,sy-l].intAirType of
                            1:
                                chItemChar := chr(131);
                            2:
                                chItemChar := chr(133);
                            3:
                                chItemChar := chr(147);
                            4:
                                chItemChar := chr(208);
                            5:
                                if CheckEffect(48)=false then
                                    DngLvl[sx-k,sy-l].blKnown:=false;
                            6:
                                chItemChar := chr(221);
                        end;

                        // if tile is unknown, select nothing to draw
                        if DngLvl[sx-k,sy-l].blKnown=false then
                            chDngChar := ' ';

                        // if graphics is selected, draw it on screen
                        if chDngChar<>' ' then
                            AnyCharXY (i, j, chDngChar, 0);

                        // if tile is bloody, select blood to draw
                        if (UseSDL=true) and (ShowBlood=true) and (DngLvl[sx-k,sy-l].blBlood) then
                            chItemChar := chr(128);

                        // select NPCs to draw
                        for m:= 1 to 9 do
                            if (NPC[m].intX=sx-k) and (NPC[m].intY=sy-l) then
                            begin
                                dummy:=IntToStr(m);
                                chItemChar := dummy[1];
                            end;

                        // select shops to draw
                        if DngLvl[sx-k,sy-l].intBuilding > 0 then
                            chItemChar := chBuilding[DngLvl[sx-k,sy-l].intBuilding];

                        // if tile has an item, select item to draw
                        if (DngLvl[sx-k,sy-l].intItem > 0) and (DngLvl[sx-k,sy-l].blKnown=true) then
                            chItemChar := Thing[DngLvl[sx-k,sy-l].intItem].chLetter;

                        // select monsters to draw
                        for m:=1 to 510 do
                            if (Monster[m].intX=sx-k) and (Monster[m].intY=sy-l) and (DngLvl[sx-k,sy-l].blKnown=true) and (Monster[m].intInvis=0) then
                            begin
                                chMonsterChar := Monster[m].chLetter;

                                // display monster health for attacked monsters
                                if Monster[m].blAttacked = true then
                                begin

                                    intMonPer := (100 * Monster[m].intHP) div Monster[m].intMaxHP;
                                    if intMonPer > 0  then
                                    begin
                                        chMonHealthChar := chr(219);  // rest
                                        GlobalConColor := red;
                                    end;

                                    if intMonPer > 12 then
                                    begin
                                        chMonHealthChar := chr(218);  // 25%
                                        GlobalConColor := brown;
                                    end;

                                    if intMonPer > 25 then
                                    begin
                                        chMonHealthChar := chr(217);  // 50%
                                        GlobalConColor := yellow;
                                    end;

                                    if intMonPer > 50 then
                                    begin
                                        chMonHealthChar := chr(216);  // 75%
                                        GlobalConColor := lightgreen;
                                    end;

                                    if intMonPer > 75 then
                                    begin
                                        chMonHealthChar := chr(215);  // 100%
                                        GlobalConColor := green;
                                    end;
                                end
                                else
                                    chMonHealthChar := '^';
                            end;

                        if DngLvl[sx-k,sy-l].blKnown=false then
                        begin
                            chItemChar := '^';
                            chMonsterChar := '^';
                        end;

                        if chItemChar<>'^' then
                            AnyCharXY (i, j, chItemChar, mode);

                        if chMonsterChar<>'^' then
                        begin
                            AnyCharXY (i, j, chMonsterChar, mode);
                            if (UseSDL=true) and (chMonHealthChar<>'^') then
                                AnyCharXY (i, j, chMonHealthChar, mode);
                        end;
                    end;
                end
                else
                begin
                    if DngLvl[sx-k,sy-l].blKnown=false then
                    begin
                        chDngChar := ' ';
                        chMonsterChar := '^';
                    end;

                    if chDngChar<>' ' then
                        AnyCharXY (i, j, chDngChar, 1);
                end;

                inc(l);

            end;

            l := 0;
            inc(k);
        end;

        // upper right
        k := 0; l := 0;

        for i := (w div 2) to w-intBorder do
        begin

            for j := (h div 2) downto intBorder do
            begin

//                 Writeln('D: '+IntToStr(sx+k)+'/'+IntToStr(sy-l));
                if (sx+k < 1) or (sy-l < 1) then
                    break;

                chDngChar:=ReturnDngChar(DngLvl[sx+k,sy-l].intFloorType);
                chItemChar:='^';
                chMonHealthChar := '^';
                chMonsterChar := '^';
                GlobalConColor := -1;

                if DngLvl[sx+k,sy-l].blLOS=true then
                begin
                    if (sx+k>0) and (sx+k<DngMaxWidth+1) and (sy-l>1) and (sy-l<DngMaxHeight) then
                    begin

                        case DngLvl[sx+k,sy-l].intAirType of
                            1:
                                chItemChar := chr(131);
                            2:
                                chItemChar := chr(133);
                            3:
                                chItemChar := chr(147);
                            4:
                                chItemChar := chr(208);
                            5:
                                if CheckEffect(48)=false then
                                    DngLvl[sx+k,sy-l].blKnown:=false;
                            6:
                                chItemChar := chr(221);
                        end;

                        if DngLvl[sx+k,sy-l].blKnown=false then
                            chDngChar := ' ';

                        if chDngChar<>' ' then
                            AnyCharXY (i, j, chDngChar, 0);

                        if (UseSDL=true) and (ShowBlood=true) and (DngLvl[sx+k,sy-l].blBlood) then
                            chItemChar := chr(128);

                        for m:= 1 to 9 do
                            if (NPC[m].intX=sx+k) and (NPC[m].intY=sy-l) then
                            begin
                                dummy:=IntToStr(m);
                                chItemChar := dummy[1];
                            end;

                        if DngLvl[sx+k,sy-l].intBuilding > 0 then
                            chItemChar := chBuilding[DngLvl[sx+k,sy-l].intBuilding];

                        if (DngLvl[sx+k,sy-l].intItem > 0) and (DngLvl[sx+k,sy-l].blKnown=true) then
                            chItemChar := Thing[DngLvl[sx+k,sy-l].intItem].chLetter;

                        for m:=1 to 510 do
                            if (Monster[m].intX=sx+k) and (Monster[m].intY=sy-l)  and (DngLvl[sx+k,sy-l].blKnown=true) and (Monster[m].intInvis=0) then
                            begin
                                chMonsterChar := Monster[m].chLetter;

                                // display monster health for attacked monsters
                                if Monster[m].blAttacked = true then
                                begin
                                    intMonPer := (100 * Monster[m].intHP) div Monster[m].intMaxHP;
                                    if intMonPer > 0  then
                                    begin
                                        chMonHealthChar := chr(219);  // rest
                                        GlobalConColor := red;
                                    end;

                                    if intMonPer > 12 then
                                    begin
                                        chMonHealthChar := chr(218);  // 25%
                                        GlobalConColor := brown;
                                    end;

                                    if intMonPer > 25 then
                                    begin
                                        chMonHealthChar := chr(217);  // 50%
                                        GlobalConColor := yellow;
                                    end;

                                    if intMonPer > 50 then
                                    begin
                                        chMonHealthChar := chr(216);  // 75%
                                        GlobalConColor := lightgreen;
                                    end;

                                    if intMonPer > 75 then
                                    begin
                                        chMonHealthChar := chr(215);  // 100%
                                        GlobalConColor := green;
                                    end;
                                end
                                else
                                    chMonHealthChar := '^';
                            end;

                        if DngLvl[sx+k,sy-l].blKnown=false then
                        begin
                            chItemChar := '^';
                            chMonsterChar := '^';
                        end;

                        if chItemChar<>'^' then
                            AnyCharXY (i, j, chItemChar, mode);
                        if chMonsterChar<>'^' then
                        begin
                            AnyCharXY (i, j, chMonsterChar, mode);
                            if (UseSDL=true) and (chMonHealthChar<>'^') then
                                AnyCharXY (i, j, chMonHealthChar, mode);
                        end;
                    end;
                end
                else
                begin
                    if DngLvl[sx+k,sy-l].blKnown=false then
                    begin
                        chDngChar := ' ';
                        chMonsterChar := '^';
                    end;
                    if chDngChar<>' ' then
                        AnyCharXY (i, j, chDngChar, 1);
                end;

                inc(l);

            end;

            l := 0;
            inc(k);
        end;


        // lower left
        k := 0; l := 0;

        for i := (w div 2) downto intBorder do
        begin

            for j := (h div 2) to h-intBorder do
            begin
//                 Writeln('D: '+IntToStr(sx-k)+'/'+IntToStr(sy+l));
                if (sx-k < 1) or (sy+l < 1) then
                    break;

                chDngChar:=ReturnDngChar(DngLvl[sx-k,sy+l].intFloorType);
                chItemChar:='^';
                chMonHealthChar := '^';
                chMonsterChar := '^';
                GlobalConColor := -1;

                if DngLvl[sx-k,sy+l].blLOS=true then
                begin
                    if (sx-k>0) and (sx-k<DngMaxWidth+1) and (sy+l>1) and (sy+l<DngMaxHeight) then
                    begin

                        case DngLvl[sx-k,sy+l].intAirType of
                            1:
                                chItemChar := chr(131);
                            2:
                                chItemChar := chr(133);
                            3:
                                chItemChar := chr(147);
                            4:
                                chItemChar := chr(208);
                            5:
                                if CheckEffect(48)=false then
                                    DngLvl[sx-k,sy+l].blKnown:=false;
                            6:
                                chItemChar := chr(221);
                        end;

                        if DngLvl[sx-k,sy+l].blKnown=false then
                            chDngChar := ' ';

                        if chDngChar<>' ' then
                            AnyCharXY (i, j, chDngChar, 0);

                        if (UseSDL=true) and (ShowBlood=true) and (DngLvl[sx-k,sy+l].blBlood) then
                            chItemChar := chr(128);

                        for m:= 1 to 9 do
                            if (NPC[m].intX=sx-k) and (NPC[m].intY=sy+l) then
                            begin
                                dummy:=IntToStr(m);
                                chItemChar := dummy[1];
                            end;

                        if DngLvl[sx-k,sy+l].intBuilding > 0 then
                            chItemChar := chBuilding[DngLvl[sx-k,sy+l].intBuilding];

                        if (DngLvl[sx-k,sy+l].intItem > 0) and (DngLvl[sx-k,sy+l].blKnown=true) then
                            chItemChar := Thing[DngLvl[sx-k,sy+l].intItem].chLetter;

                        for m:=1 to 510 do
                            if (Monster[m].intX=sx-k) and (Monster[m].intY=sy+l)  and (DngLvl[sx-k,sy+l].blKnown=true) and (Monster[m].intInvis=0) then
                            begin
                                chMonsterChar := Monster[m].chLetter;

                                // display monster health for attacked monsters
                                if Monster[m].blAttacked = true then
                                begin
                                    intMonPer := (100 * Monster[m].intHP) div Monster[m].intMaxHP;
                                    if intMonPer > 0  then
                                    begin
                                        chMonHealthChar := chr(219);  // rest
                                        GlobalConColor := red;
                                    end;

                                    if intMonPer > 12 then
                                    begin
                                        chMonHealthChar := chr(218);  // 25%
                                        GlobalConColor := brown;
                                    end;

                                    if intMonPer > 25 then
                                    begin
                                        chMonHealthChar := chr(217);  // 50%
                                        GlobalConColor := yellow;
                                    end;

                                    if intMonPer > 50 then
                                    begin
                                        chMonHealthChar := chr(216);  // 75%
                                        GlobalConColor := lightgreen;
                                    end;

                                    if intMonPer > 75 then
                                    begin
                                        chMonHealthChar := chr(215);  // 100%
                                        GlobalConColor := green;
                                    end;
                                end
                                else
                                    chMonHealthChar := '^';

                            end;

                        if DngLvl[sx-k,sy+l].blKnown=false then
                        begin
                            chItemChar := '^';
                            chMonsterChar := '^';
                        end;

                        if chItemChar<>'^' then
                            AnyCharXY (i, j, chItemChar, mode);
                        if chMonsterChar<>'^' then
                        begin
                            AnyCharXY (i, j, chMonsterChar, mode);
                            if (UseSDL=true) and (chMonHealthChar<>'^') then
                                AnyCharXY (i, j, chMonHealthChar, mode);
                        end;
                    end;
                end
                else
                begin
                    if DngLvl[sx-k,sy+l].blKnown=false then
                    begin
                        chDngChar := ' ';
                        chMonsterChar := '^';
                    end;
                    if chDngChar<>' ' then
                        AnyCharXY (i, j, chDngChar, 1);
                end;

                inc(l);

            end;

            l := 0;
            inc(k);
        end;


        // lower right

        k := 0; l := 0;

        for i := (w div 2) to w-intBorder do
        begin

            for j := (h div 2) to h-intBorder do
            begin

//                 Writeln('D: '+IntToStr(sx+k)+'/'+IntToStr(sy+l));
                if (sx+k < 1) or (sy+l < 1) then
                    break;

                chDngChar:=ReturnDngChar(DngLvl[sx+k,sy+l].intFloorType);
                chItemChar:='^';
                chMonHealthChar := '^';
                chMonsterChar := '^';
                GlobalConColor := -1;

                if DngLvl[sx+k,sy+l].blLOS=true then
                begin
                    if (sx+k>0) and (sx+k<DngMaxWidth+1) and (sy+l>1) and (sy+l<DngMaxHeight) then
                    begin

                        case DngLvl[sx+k,sy+l].intAirType of
                            1:
                                chItemChar := chr(131);
                            2:
                                chItemChar := chr(133);
                            3:
                                chItemChar := chr(147);
                            4:
                                chItemChar := chr(208);
                            5:
                                if CheckEffect(48)=false then
                                    DngLvl[sx+k,sy+l].blKnown:=false;
                            6:
                                chItemChar := chr(221);
                        end;

                        if DngLvl[sx+k,sy+l].blKnown=false then
                            chDngChar := ' ';

                        if chDngChar<>' ' then
                            AnyCharXY (i, j, chDngChar, 0);

                        if (UseSDL=true) and (ShowBlood=true) and (DngLvl[sx+k,sy+l].blBlood) then
                            chItemChar := chr(128);

                        for m:= 1 to 9 do
                            if (NPC[m].intX=sx+k) and (NPC[m].intY=sy+l) then
                            begin
                                dummy:=IntToStr(m);
                                chItemChar := dummy[1];
                            end;

                        if DngLvl[sx+k,sy+l].intBuilding > 0 then
                            chItemChar := chBuilding[DngLvl[sx+k,sy+l].intBuilding];

                        if (DngLvl[sx+k,sy+l].intItem > 0) and (DngLvl[sx+k,sy+l].blKnown=true) then
                            chItemChar := Thing[DngLvl[sx+k,sy+l].intItem].chLetter;

                        for m:=1 to 510 do
                            if (Monster[m].intX=sx+k) and (Monster[m].intY=sy+l) and (DngLvl[sx+k,sy+l].blKnown=true) and (Monster[m].intInvis=0) then
                            begin
                                chMonsterChar := Monster[m].chLetter;

                                // display monster health for attacked monsters
                                if Monster[m].blAttacked = true then
                                begin
                                    intMonPer := (100 * Monster[m].intHP) div Monster[m].intMaxHP;
                                    if intMonPer > 0  then
                                    begin
                                        chMonHealthChar := chr(219);  // rest
                                        GlobalConColor := red;
                                    end;

                                    if intMonPer > 12 then
                                    begin
                                        chMonHealthChar := chr(218);  // 25%
                                        GlobalConColor := brown;
                                    end;

                                    if intMonPer > 25 then
                                    begin
                                        chMonHealthChar := chr(217);  // 50%
                                        GlobalConColor := yellow;
                                    end;

                                    if intMonPer > 50 then
                                    begin
                                        chMonHealthChar := chr(216);  // 75%
                                        GlobalConColor := lightgreen;
                                    end;

                                    if intMonPer > 75 then
                                    begin
                                        chMonHealthChar := chr(215);  // 100%
                                        GlobalConColor := green;
                                    end;
                                end
                                else
                                    chMonHealthChar := '^';
                            end;

                        if DngLvl[sx+k,sy+l].blKnown=false then
                        begin
                            chItemChar := '^';
                            chMonsterChar := '^';
                        end;

                        if chItemChar<>'^' then
                            AnyCharXY (i, j, chItemChar, mode);
                        if chMonsterChar<>'^' then
                        begin
                            AnyCharXY (i, j, chMonsterChar, mode);
                            if (UseSDL=true) and (chMonHealthChar<>'^') then
                                AnyCharXY (i, j, chMonHealthChar, mode);
                        end;
                    end;
                end
                else
                begin
                    if DngLvl[sx+k,sy+l].blKnown=false then
                    begin
                        chDngChar := ' ';
                        chMonsterChar := '^';
                    end;
                    if chDngChar<>' ' then
                        AnyCharXY (i, j, chDngChar, 1);
                end;

                inc (l);
            end;

            l := 0;
            inc (k);
        end;
    end;


    // draw player
    if IsInvisible=false then
    begin
        chDngChar:=chr(179);
        if ThePlayer.intPoison > 0 then
            chDngChar:=chr(187);
        if ThePlayer.intWall > 0 then
            chDngChar:=chr(146);
        if DngLvl[ThePlayer.intX, ThePlayer.intY].intFloorType=5 then
            chDngChar:=chr(220);
        if DngLvl[ThePlayer.intX, ThePlayer.intY].intAirType=5 then
            chDngChar:=chr(143);

        if UseSDL=true then
            AnyCharXY(ThePlayer.intBX, ThePlayer.intBY+1, chDngChar, 0)
        else
            AnyCharXY(ThePlayer.intBX, ThePlayer.intBY, chDngChar, 0);
    end;

    ShowStatus;

    if UseSDL=false then
        UnLockScreenUpdate;

end;

end.
