{
     Copyright (C) 2006-2008 by Mario Donick
     mario.donick@gmail.com    
                                                                            
     This program is free software; you can redistribute it and/or modify   
     it under the terms of the GNU General Public License as published by   
     the Free Software Foundation; either version 2 of the License, or      
     (at your option) any later version.                                    
                                                                            
     This program is distributed in the hope that it will be useful,        
     but WITHOUT ANY WARRANTY; without even the implied warranty of         
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
     GNU General Public License for more details.                           
                                                                            
     You should have received a copy of the GNU General Public License      
     along with this program; if not, write to the                          
     Free Software Foundation, Inc.,                                        
     59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              
}

unit Plot;


interface

uses
    SDL, SDL_Image, Crt, SysUtils, StrUtils, RandomArea, Player, BaseOutput, Output, Input;

procedure OutputPlot(strPlotfile: string);
procedure ShowPlot(part: integer);
procedure ShowText (strTextfile: string);

implementation



procedure OutputPlot(strPlotfile: string);
var
    PlotFile: Textfile;
    z: integer;
    zeile: string;
    TempAnsi: ansistring;
begin
    if UseSDL=true then
    begin
        //TempAnsi := PathData+'graphics/txtbg.jpg';
        //title:=SDL_DISPLAYFORMAT(IMG_LOAD(PChar(TempAnsi)));
        LoadImage_Title(PathData+'graphics/txtbg.jpg');
        BlitImage_Title;
    end
    else
        ClearScreenSDL;

    if fileexists(PathData+'story/'+strPlotfile+'.txt')=true then
    begin
        Assign (PlotFile, PathData+'story/'+strPlotfile+'.txt');
        Reset (PlotFile);

        z:=0;
        zeile:='';
        while eof(PlotFile)=false do
        begin
            ReadLn(PlotFile, zeile);
            TransTextXY(1, z, zeile);
            inc(z);
        end;
        Close (PlotFile);
    end
    else
    begin
        TransTextXY(1, 1, '[Warning: File '+PathData+'"story/'+strPlotfile+'.txt" not found!]');
    end;

    if UseSDL=false then
        GlobalConColor := darkgray;
    GetKeyInput('Press any key to continue.', false);
    LastMessage:='-';
    if UseSDL=false then
        GlobalConColor := white;
end;


procedure ShowPlot (part: integer);
begin
    if ThePlayer.blStory[part]=false then
    begin
        OutputPlot(IntToStr(part));
        ThePlayer.blStory[part]:=true;
        ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
    end;
end;


procedure ShowText (strTextfile: string);
var
    t: AnsiString;
    k: Integer;
    Knoepfe, KX, KY : Longint;
begin
    if strTextfile='-' then
        GetKeyInput('There is nothing to read on it.',true)
    else
    begin
        ClearScreenSDL;

        // show texts beginning with "chapter" or "interlude" as graphic
        if ((UseSDL=true) and ((NPos('chapter', strTextfile, 1)=1) or (NPos('interlude', strTextfile, 1)=1))) then
        begin
            t:=PathData+'graphics/chapters/'+strTextFile+'.jpg';
            LoadImage_Title(t);
            BlendTitleSurface(true);
            k:=0;
            repeat
                if SDL_POLLEVENT( @MyEvent ) > 0 then
                    case MyEvent.type_ of
                        SDL_KEYDOWN:
                            k:=cookKey(MyEvent.key.keysym.unicode, MyEvent.key.keysym.sym);
                        SDL_MOUSEBUTTONDOWN:
                            if UseMouseToMove=true then
                            begin
                                KX:=MyEvent.motion.x;
                                KY:=MyEvent.motion.y;
                                Knoepfe := SDL_GetMouseState(KX, KY);
                                if (Knoepfe and SDL_BUTTON(SDL_BUTTON_RIGHT))<>0 then
                                    k:=13;
                            end;
                    end
            until (k=13) or (k=32);
            if ThePlayer.intSex=1 then
                t := PathData+'graphics/'+IntToStr(ThePlayer.intProf)+'-m.png'
            else
                t := PathData+'graphics/'+IntToStr(ThePlayer.intProf)+'-f.png';
            LoadImage_Title(t);
        end
        else
        begin
            OutputPlot('books/'+strTextfile);
            if UseSDL=false then
                ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
        end;
    end;
end;

end.
